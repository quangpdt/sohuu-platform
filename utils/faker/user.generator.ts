const faker = require('faker/locale/vi');
const { format } = require('date-fns');
const { join } = require('path');
const { cwd } = require('process');
const { writeFileSync } = require('fs');

const users = [];

for (let i = 0; i < 100; i++) {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();
    const email = faker.internet.email(firstName, lastName);
    const phone = faker.phone.phoneNumber('+84#########');
    users.push({
        name: `${firstName} ${lastName}`,
        email,
        phone,
        password: '$2b$10$CGDT9NR.h9FE9cb.8HxBsuRjQDh6J0OyfrPAn03IV.g4XStJeWf2e',
        isActivated: true,
        isPhoneVerified: true,
    });
}
writeFileSync(join(cwd(), 'utils/faker/users.json'), JSON.stringify(users), { encoding: 'utf8' });
