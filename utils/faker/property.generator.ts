import { PropertyStatusModel } from '@sohuu-platform/models';

const faker = require('faker/locale/vi');
const { format } = require('date-fns');
const { join } = require('path');
const { cwd } = require('process');
const { writeFileSync, readFileSync } = require('fs');
const slugify = require('@sindresorhus/slugify');
const cities = JSON.parse(readFileSync(join(cwd(), 'apps/api/src/assets/cities.json'), { encoding: 'utf8' }));
const districts = JSON.parse(readFileSync(join(cwd(), 'apps/api/src/assets/districts.json'), { encoding: 'utf8' }));
const wards = JSON.parse(readFileSync(join(cwd(), 'apps/api/src/assets/wards.json'), { encoding: 'utf8' }));
const propertyTypeIds = [1, 2];

const sql = [];
const properties = [];

const titles = [
    'Bán căn hộ Vinhomes Golden River Ba Son',
    'Khu căn hộ cao cấp Saigon Pearl',
    'Chính chủ nhượng lại căn hộ Carillon 7',
    'Bán đất nền dự án tại Saigon Garden Riverside Village',
    'Cần bán thửa đất cực đẹp 2 mặt tiền',
    '168 căn Biệt thự vuờn Q9',
    'Bán căn nhà tại 778 đường Nguyễn Duy Trinh',
    'Bán nhanh căn hộ The Pegasuite 1',
    'Cần bán Căn hộ 1PN 56m2 Quận 7 - Dvela Building',
    'Bán căn hộ Homyland 3, đường Nguyễn Duy Trinh',
    'Nhà Chu Văn An hẻm 261 - đối diện học viện',
    'Bán dãy phố 4 tấm 3.5x10m, P16 Q8 cách Metro Bình Phú 2km',
    'Bán dãy phố trệt 1 lầu 3.5x14m, hẻm xe hơi gần chợ Hung Long',
    'Cần bán gấp căn nhà mặt tiền kinh doanh đường 12m',
    'Trực tiếp chính chủ cần bán lỗ 9 căn hộ Q7 Sài Gòn Riverside',
    'Chính chủ bán nhà HXH 196/22 Tô hiến Thành, f15, q10',
];

const userIds = [
    34,
    40,
    141,
    142,
    143,
    144,
    145,
    146,
    147,
    148,
    149,
    150,
    151,
    152,
    153,
    154,
    155,
    156,
    157,
    158,
    159,
    160,
    161,
    162,
    163,
    164,
    165,
    166,
    167,
    168,
    169,
    170,
    171,
    172,
    173,
    174,
    175,
    176,
    177,
    178,
    179,
    180,
    181,
    182,
    183,
    184,
    185,
    186,
    187,
    188,
    189,
    190,
    191,
    192,
    193,
    194,
    195,
    196,
    197,
    198,
    199,
    200,
    201,
    202,
    203,
    204,
    205,
    206,
    207,
    208,
    209,
    210,
    211,
    212,
    213,
    214,
    215,
    216,
    217,
    218,
    219,
    220,
    221,
    222,
    223,
    224,
    225,
    226,
    227,
    228,
    229,
    230,
    231,
    232,
    233,
    234,
    235,
    236,
    237,
    238,
    239,
    240,
];

const sampleFiles = [
    'b229b7e5-8fa1-4146-bec2-be17102aab76.jpg',
    'fdcc3e5e-0b1b-4db2-96c7-8683f15ab5c4.jpg',
    'f6b0d00a-375e-44a1-9190-3496da30a88b.jpg',
    '8178f270-a87b-419c-a61f-f8e38c929c21.jpg',
    'dfe05a09-4866-48bc-aec1-d74ee2436109.jpg',
    'af11336a-7ffb-46d7-80da-58889ff05b46.jpg',
    '9aa4cab1-898a-48c6-92e4-89c04bc5741c.jpg',
    '6ee51354-cf14-4e82-b286-2ad6df210f4d.jpg',
    'e3ca8add-02fb-4dde-b780-115996ec7a8d.jpg',
    'df292bc1-b63e-42bc-9400-3e35416858d7.jpg',
    'c2efd56d-7652-49f2-8e7a-ae993a32815e.jpg',
    'd0d8192a-06a6-413c-9309-64a14335178d.jpg',
    '1a7545e5-1064-4faf-9586-4bc6d65ff8c3.jpg',
    'c4c67ec0-f975-4eb1-9cdb-5773d968d053.jpg',
    'a87d53de-fcea-4323-91c4-7daff5d77c6f.jpg',
    '776805df-46d4-4d09-8b18-98549ce3a82b.jpg',
    '5fd86a8c-3e46-41a3-a52a-d59c55bdaf6a.jpg',
    '8d2af01d-f0a8-4aa7-8268-8c590e3aa4ea.jpg',
    '3a3c3222-5003-41e8-abec-c8cbd6dc8107.jpg',
    '498d9601-5133-4312-9d5f-920dc6c6049b.jpg',
    '7d03f0c1-8afe-48bc-a539-b4891c3804f9.jpg',
    '3ce5639a-b082-4a85-a43e-9a69325d3c51.jpg',
    'd5bc226c-bc19-4fdf-9b29-2b19728128f8.jpg',
    '6b0235a8-f02e-4a02-8c8a-44dd8c1b80d1.jpg',
    'c3aa3819-2fc7-490a-9e3c-aa5175262f04.jpg',
    '15347e01-7ea1-4115-9f1b-5f013298bd07.jpg',
    'c5e3781e-eee5-4026-a003-539558ebb7d1.jpg',
    'c139513b-2507-498a-a23c-9140a12b9af1.jpg',
    '42e55780-a07b-4fc6-9968-468518e6852a.jpg',
    '9b768110-4247-4bdf-b825-35d17091ac0d.jpg',
    '1977ceed-a0ee-4189-894b-ca4f03b1055d.jpg',
    'c79a505b-803e-4cb6-85bc-eaa6bd1e8394.jpg',
    '9720e8bb-5e29-437d-83e0-a60a27a4f48f.jpg',
    'f090493c-43f0-4392-be3e-19d72e6cdaaa.jpg',
    'eef4c13e-4f3b-4cad-8fa0-ae235fdea6a6.jpg',
    'cdbd686a-b9b0-4cf0-af19-a4663a8f4185.jpg',
    '91d158b3-af01-4f55-b9bd-c3c31586988a.jpg',
    '6e3f43e4-2865-4822-a11b-f5e62d42d07c.jpg',
    'ac19aa72-2e44-4329-9a59-40eea7476c7b.jpg',
    '369821cf-0a97-4871-9661-e0c43d93386d.jpg',
];

function randomIntFromInterval(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

function mapMetadata(data): any[] {
    const {
        mainDoorDirection,
        balconyDirection,
        kitchenDirection,
        legal,
        bedrooms,
        bathrooms,
        floors,
        area,
        features,
        additionalFeatures,
    } = data;

    return [
        {
            name: 'fengshui',
            value: JSON.stringify({
                mainDoorDirection,
                balconyDirection,
                kitchenDirection,
            }),
        },
        {
            name: 'structure',
            value: JSON.stringify({
                bedrooms,
                bathrooms,
                floors,
                area,
            }),
        },
        {
            name: 'legal',
            value: legal.toString(10),
        },
        {
            name: 'features',
            value: JSON.stringify({
                features,
                additionalFeatures,
            }),
        },
    ];
}

function mapStatuses(data: any): any[] {
    const { salePrice, rentPrice, saleNegotiable, rentNegotiable, propertyStatus } = data;

    const statuses: PropertyStatusModel[] = [];

    propertyStatus.forEach((status) => {
        if (status === 1) {
            statuses.push({
                status: 1,
                price: salePrice,
                negotiable: saleNegotiable,
            } as PropertyStatusModel);
        } else if (status === 2) {
            statuses.push({
                status: 2,
                price: rentPrice,
                negotiable: rentNegotiable,
            } as PropertyStatusModel);
        }
    });

    return statuses;
}

for (let i = 0; i < 100; i++) {
    const city = cities[0];
    const district = shuffle(districts.filter((p) => p.cityId === city.id))[0];
    const ward = shuffle(wards.filter((p) => p.districtId === district.id))[0];
    const address = faker.address.streetAddress();
    const title = shuffle(titles)[0];
    const slug = slugify(title);
    const description = faker.lorem.paragraphs(5, '<br/>');
    const ownerId = shuffle(userIds)[0];
    const propertyType = shuffle(propertyTypeIds)[0];
    const publishedAt = format(new Date(), 'yyyy-MM-dd HH:mm:ss');
    const files = shuffle(sampleFiles)
        .slice(0, 5)
        .map((p) => ({
            fileName: p,
        }));

    const numberOfStatus = randomIntFromInterval(1, 2);
    const statuses = [];
    for (let j = 1; j <= numberOfStatus; j++) {
        statuses.push({
            status: j,
            price: j === 1 ? randomIntFromInterval(1000000000, 3000000000) : randomIntFromInterval(5000000, 30000000),
            negotiable: shuffle([true, false])[0],
        });
    }

    properties.push({
        title,
        slug,
        description: `<p>${description}</p>`,
        ownerId,
        propertyType,
        cityId: city.id,
        districtId: district.id,
        wardId: ward.id,
        location: address,
        submitStatus: 3,
        publishedAt,
        fullLocation: `${address}, ${ward.name}, ${district.name}, ${city.name}`,
        files,
        metadata: mapMetadata({
            mainDoorDirection: randomIntFromInterval(1, 4),
            balconyDirection: randomIntFromInterval(1, 4),
            kitchenDirection: randomIntFromInterval(1, 4),
            legal: randomIntFromInterval(1, 4),
            bedrooms: randomIntFromInterval(1, 4),
            bathrooms: randomIntFromInterval(1, 4),
            floors: randomIntFromInterval(1, 30),
            area: randomIntFromInterval(50, 250),
            features: shuffle([1, 2, 3, 4, 5, 6, 7]).slice(0, 3),
        }),
        statuses,
    });
}

writeFileSync(join(cwd(), 'utils/faker/properties.json'), JSON.stringify(properties), { encoding: 'utf8' });
