const axios = require('axios');
const fs = require('fs');
const path = require('path');
const slugify = require('@sindresorhus/slugify');

let cities = [];
const districts = [];
const wards = [];

let cityId = 0;
let districtId = 0;
let wardId = 0;

async function asyncForEach(array: any[], callback: any) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

async function getCities() {
    const result = await axios.get('https://tiki.vn/api/v2/directory/regions?country_id=vn');
    cities = result.data.data;

    cities.map((item) => {
        cityId++;
        item.originalId = item.id;
        item.id = cityId;
        item.slug = slugify(item.name);
    });

    await asyncForEach(cities, async (city, index) => {
        await getDistricts(city.originalId, city.id);
    });

    cities.map((item) => {
        delete item.originalId;
    });

    districts.map((item) => {
        delete item.originalId;
    });

    fs.writeFileSync(path.join(process.cwd(), 'apps/app/src/assets/data/cities.json'), JSON.stringify(cities), { encoding: 'utf8' });
    fs.writeFileSync(path.join(process.cwd(), 'apps/app/src/assets/data/districts.json'), JSON.stringify(districts), { encoding: 'utf8' });
    fs.writeFileSync(path.join(process.cwd(), 'apps/app/src/assets/data/wards.json'), JSON.stringify(wards), { encoding: 'utf8' });
}

async function getDistricts(originalId: number, cityId: number) {
    console.log('Querying', 'https://tiki.vn/api/v2/directory/districts?region_id=' + originalId);
    const result = await axios.get('https://tiki.vn/api/v2/directory/districts?region_id=' + originalId);
    const data = result.data.data;

    data.map((item) => {
        item.cityId = cityId;
        districtId++;
        item.originalId = item.id;
        item.id = districtId;
        item.slug = slugify(item.name);
    });

    districts.push(...data);

    await asyncForEach(data, async (district) => {
        await getWards(district.originalId, district.id);
    });
}

async function getWards(originalId: number, districtId: number) {
    console.log('Querying', 'https://tiki.vn/api/v2/directory/wards?district_id=' + originalId);
    const result = await axios.get('https://tiki.vn/api/v2/directory/wards?district_id=' + originalId);
    const data = result.data.data;
    data.map((item) => {
        item.districtId = districtId;
        wardId++;
        item.id = wardId;
        item.slug = slugify(item.name);
    });

    wards.push(...data);
}

getCities();
