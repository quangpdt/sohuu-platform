#!/usr/bin/env bash

GREEN='\033[0;32m'
RED='\033[0;31m'
CYAN='\033[0;36m'
NC='\033[0m'
branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

# Cleaning up workspaces
rm -f dist.zip
rm -rf ./tmp
rm -rf dist/*

# Check mandatory commands for deployment process
if ! [ -x "$(command -v git)" ]; then
  echo "${RED}[ERROR]${NC} Please install ${GREEN}git${NC}, build is now aborting..."
  exit -1
elif ! [ -x "$(command -v zip)" ]; then
  echo "${RED}[ERROR]${NC} Please install ${GREEN}zip${NC}, build is now aborting..."
  exit -1
elif ! [ -x "$(command -v scp)" ]; then
  echo "${RED}[ERROR]${NC} Please install ${GREEN}scp${NC}, build is now aborting..."
  exit -1
fi

# Check the current branch
if [ $branch == 'stage-release' ] || [ $branch == 'master' ]; then
    echo "${GREEN}[INFO]${NC} Start releasing on ${GREEN}stage-release${NC} branch"
elif [ $branch == 'release' ]; then
    echo "${GREEN}[INFO]${NC} Start releasing on ${GREEN}release${NC} branch"
else
    echo "${RED}[ERROR]${NC} Cannot release on this branch, please switch to ${GREEN}release${NC} or ${GREEN}stage-release${NC} branch"
    exit -1
fi

# If there're files are not committed, abort the build
if [ -n "$(git status --porcelain)" ]; then
  echo "${RED}[ERROR]${NC} Please commit all the changed files first, build is now aborting..."
  exit -1
fi

# Preconditions are matched, start the real deployment process
echo "${GREEN}[INFO]${NC} Building ${CYAN}API${NC}..."
npm run api:build > /dev/null 2>&1

echo "${GREEN}[INFO]${NC} Building ${CYAN}Single Page Application${NC}..."
if [ $branch == 'stage-release' ] || [ $branch == 'master' ]; then
    npm run app:build:stage > /dev/null 2>&1
else
    npm run app:build:production > /dev/null 2>&1
fi

echo "${GREEN}[INFO]${NC} Zipping built applications from ${GREEN}dist${NC} folder..."
zip -r dist.zip dist/ > /dev/null 2>&1

echo "${GREEN}[INFO]${NC} Distributing ${CYAN}dist.zip${NC} file to server and Running ${CYAN}release.sh${NC} script..."
if [ $branch == 'stage-release' ] || [ $branch == 'master' ]; then
    scp -i ~/.ssh/sohuu_rsa -P 30132 dist.zip root@172.93.189.247:/home/stage.sohuu.vn/sohuu-platform/ > /dev/null 2>&1
    ssh -i ~/.ssh/sohuu_rsa root@172.93.189.247 -p 30132 "cd /home/stage.sohuu.vn/sohuu-platform && sh release.sh" > /dev/null 2>&1
else
    scp -i ~/.ssh/sohuu_rsa -P 30132 dist.zip root@172.93.189.247:/home/sohuu.vn/sohuu-platform/ > /dev/null 2>&1
    ssh -i ~/.ssh/sohuu_rsa root@172.93.189.247 -p 30132 "cd /home/sohuu.vn/sohuu-platform && sh release.sh" > /dev/null 2>&1
fi
