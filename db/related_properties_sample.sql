SELECT NOW() - INTERVAL 30 DAY;

SET
    @PROPERTY_ID = 13;
SET
    @CITY_ID = 1;
SET
    @DISTRICT_ID = 2;
SET
    @DIRECTION_ID = 3;
-- WEST
SET
    @RELATED_DIRECTION_1 = 7;
-- WEST NORTH
SET
    @RELATED_DIRECTION_2 = 9;
-- WEST SOUTH
SET
    @BEDROOMS = 2;

SELECT property_id,
       (
           (location_point + fengshui_point + bedroom_point) / 3
           ) as property_point
FROM (
         SELECT property_id,
                IF(ditrict_id = @DISTRICT_ID, 10, 8) AS location_point,
                (
                    CASE
                        WHEN main_door_direction = @DIRECTION_ID THEN 10
                        WHEN main_door_direction = @RELATED_DIRECTION_1 THEN 8
                        WHEN main_door_direction = @RELATED_DIRECTION_2 THEN 8
                        ELSE 0
                        END
                    )                                AS fengshui_point,
                (
                    CASE
                        WHEN bedroom_count = @BEDROOMS THEN 10
                        WHEN ABS(bedroom_count - @BEDROOMS) = 1 THEN 8
                        ELSE 0
                        END
                    )                                AS bedroom_point
         FROM (
                  SELECT `properties`.`id`                                        as property_id,
                         `properties`.`district_id`                               AS ditrict_id,
                         JSON_EXTRACT(
                                 `fengshui_metadata`.`value`,
                                 "$.mainDoorDirection"
                             )                                                    AS main_door_direction,
                         JSON_EXTRACT(`structure_metadata`.`value`, "$.bedrooms") AS bedroom_count
                  FROM `properties`
                           INNER JOIN `property_metadata` AS `fengshui_metadata`
                                      ON `properties`.`id` = `fengshui_metadata`.`property_id`
                                          AND `fengshui_metadata`.`name` = "fengshui"
                           INNER JOIN `property_metadata` AS `structure_metadata`
                                      ON `properties`.`id` = `structure_metadata`.`property_id`
                                          AND `structure_metadata`.`name` = "structure"
                  WHERE `properties`.`id` <> @PROPERTY_ID
                    AND `properties`.`published_at` > (NOW() - INTERVAL 30 DAY)
                    AND `city_id` = @CITY_ID
              ) AS raw_data
     ) AS points
ORDER BY property_point DESC
LIMIT
    5;
