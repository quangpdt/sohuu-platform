import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Icon } from '@visurel/iconify-angular';
import faCaretUp from '@iconify/icons-fa-solid/caret-up';
import faCaretDown from '@iconify/icons-fa-solid/caret-down';
import icHelp from '@iconify/icons-ic/help-outline';
import icShare from '@iconify/icons-ic/twotone-share';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ShareBottomSheetComponent } from '../../share-bottom-sheet/share-bottom-sheet.component';
import { scaleInOutAnimation } from '../../../animations/scale-in-out.animation';

@Component({
    selector: 'vex-widget-quick-value-center',
    templateUrl: './widget-quick-value-center.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [scaleInOutAnimation],
})
export class WidgetQuickValueCenterComponent implements OnInit {
    @Input() public icon: Icon;
    @Input() public value: string;
    @Input() public label: string;
    @Input() public change: number;
    @Input() public helpText: string;
    @Input() public iconClass: string;

    public faCaretUp = faCaretUp;
    public faCaretDown = faCaretDown;
    public icHelp = icHelp;
    public icShare = icShare;

    public showButton: boolean;

    constructor(private _bottomSheet: MatBottomSheet) {}

    public ngOnInit() {}

    public openSheet() {
        this._bottomSheet.open(ShareBottomSheetComponent);
    }
}
