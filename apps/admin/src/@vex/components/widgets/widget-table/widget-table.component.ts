import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import icMoreHoriz from '@iconify/icons-ic/twotone-more-horiz';
import icCloudDownload from '@iconify/icons-ic/twotone-cloud-download';
import { TableColumn } from '../../../interfaces/table-column.interface';

@Component({
    selector: 'vex-widget-table',
    templateUrl: './widget-table.component.html',
})
export class WidgetTableComponent<T> implements OnInit, OnChanges, AfterViewInit {
    @Input() public data: T[];
    @Input() public columns: TableColumn<T>[];
    @Input() public pageSize = 6;

    public visibleColumns: Array<keyof T | string>;
    public dataSource = new MatTableDataSource<T>();

    @ViewChild(MatPaginator, { static: true }) public paginator: MatPaginator;
    @ViewChild(MatSort, { static: true }) public sort: MatSort;

    public icMoreHoriz = icMoreHoriz;
    public icCloudDownload = icCloudDownload;

    constructor() {}

    public ngOnInit() {}

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.columns) {
            this.visibleColumns = this.columns.map((column) => column.property);
        }

        if (changes.data) {
            this.dataSource.data = this.data;
        }
    }

    public ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }
}
