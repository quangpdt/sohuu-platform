import { Component, Input, OnInit } from '@angular/core';
import { Icon } from '@visurel/iconify-angular';
import icHelp from '@iconify/icons-ic/help-outline';
import icShare from '@iconify/icons-ic/twotone-share';
import icTrendingUp from '@iconify/icons-ic/twotone-trending-up';
import icTrendingDown from '@iconify/icons-ic/twotone-trending-down';
import { scaleInOutAnimation } from '../../../animations/scale-in-out.animation';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ShareBottomSheetComponent } from '../../share-bottom-sheet/share-bottom-sheet.component';

@Component({
    selector: 'vex-widget-quick-value-start',
    templateUrl: './widget-quick-value-start.component.html',
    animations: [scaleInOutAnimation],
})
export class WidgetQuickValueStartComponent implements OnInit {
    @Input() public icon: Icon;
    @Input() public value: string;
    @Input() public label: string;
    @Input() public change: number;
    @Input() public changeSuffix: string;
    @Input() public helpText: string;

    public icTrendingUp = icTrendingUp;
    public icTrendingDown = icTrendingDown;
    public icHelp = icHelp;
    public icShare = icShare;

    public showButton: boolean;

    constructor(private _bottomSheet: MatBottomSheet) {}

    public ngOnInit() {}

    public openSheet() {
        this._bottomSheet.open(ShareBottomSheetComponent);
    }
}
