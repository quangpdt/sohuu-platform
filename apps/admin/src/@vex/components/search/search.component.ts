import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import icClose from '@iconify/icons-ic/twotone-close';
import { LayoutService } from '../../services/layout.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { filter } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { SearchService } from '../../services/search.service';

@Component({
    selector: 'vex-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit, OnDestroy {
    public show$ = this.layoutService.searchOpen$;
    public searchCtrl = new FormControl();
    public icClose = icClose;

    @ViewChild('searchInput', { static: true }) public input: ElementRef;

    constructor(private layoutService: LayoutService, private searchService: SearchService) {}

    public ngOnInit() {
        this.searchService.isOpenSubject.next(true);
        this.searchCtrl.valueChanges
            .pipe(untilDestroyed(this))
            .subscribe((value) => this.searchService.valueChangesSubject.next(value));

        this.show$
            .pipe(
                filter((show) => show),
                untilDestroyed(this)
            )
            .subscribe(() => this.input.nativeElement.focus());
    }

    public close() {
        this.layoutService.closeSearch();
        this.searchCtrl.setValue(undefined);
        this.searchService.isOpenSubject.next(false);
    }

    public search() {
        this.searchService.submitSubject.next(this.searchCtrl.value);
        this.close();
    }

    public ngOnDestroy(): void {
        this.layoutService.closeSearch();
        this.searchCtrl.setValue(undefined);
        this.searchService.isOpenSubject.next(false);
    }
}
