import { Component, Inject, Input, OnDestroy } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Component({
    selector: 'vex-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss'],
    host: {
        class: 'vex-sidebar',
    },
})
export class SidebarComponent implements OnDestroy {
    @Input() public position: 'left' | 'right' = 'left';
    @Input() public invisibleBackdrop: boolean;

    constructor(@Inject(DOCUMENT) private document: Document) {}

    private _opened: boolean;

    get opened() {
        return this._opened;
    }

    @Input() set opened(opened: boolean) {
        this._opened = opened;
        opened ? this.enableScrollblock() : this.disableScrollblock();
    }

    get positionLeft() {
        return this.position === 'left';
    }

    get positionRight() {
        return this.position === 'right';
    }

    public enableScrollblock() {
        if (!this.document.body.classList.contains('vex-scrollblock')) {
            this.document.body.classList.add('vex-scrollblock');
        }
    }

    public disableScrollblock() {
        if (this.document.body.classList.contains('vex-scrollblock')) {
            this.document.body.classList.remove('vex-scrollblock');
        }
    }

    public open() {
        this.opened = true;
    }

    public close() {
        this.opened = false;
    }

    public ngOnDestroy(): void {}
}
