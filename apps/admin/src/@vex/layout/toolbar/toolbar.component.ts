import { Component, ElementRef, HostBinding, Input, OnInit } from '@angular/core';
import { LayoutService } from '../../services/layout.service';
import icBookmarks from '@iconify/icons-ic/twotone-bookmarks';
import emojioneUS from '@iconify/icons-emojione/flag-for-flag-united-states';
import emojioneDE from '@iconify/icons-emojione/flag-for-flag-germany';
import icMenu from '@iconify/icons-ic/twotone-menu';
import { ConfigService } from '../../services/config.service';
import { map } from 'rxjs/operators';
import icPersonAdd from '@iconify/icons-ic/twotone-person-add';
import icAssignmentTurnedIn from '@iconify/icons-ic/twotone-assignment-turned-in';
import icBallot from '@iconify/icons-ic/twotone-ballot';
import icDescription from '@iconify/icons-ic/twotone-description';
import icAssignment from '@iconify/icons-ic/twotone-assignment';
import icReceipt from '@iconify/icons-ic/twotone-receipt';
import icDoneAll from '@iconify/icons-ic/twotone-done-all';
import { NavigationService } from '../../services/navigation.service';
import icArrowDropDown from '@iconify/icons-ic/twotone-arrow-drop-down';
import { PopoverService } from '../../components/popover/popover.service';
import { MegaMenuComponent } from '../../components/mega-menu/mega-menu.component';
import icSearch from '@iconify/icons-ic/twotone-search';

@Component({
    selector: 'vex-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {
    @Input() public mobileQuery: boolean;

    @Input()
    @HostBinding('class.shadow-b')
    public hasShadow: boolean;

    public navigationItems = this.navigationService.items;

    public isHorizontalLayout$ = this.configService.config$.pipe(map((config) => config.layout === 'horizontal'));
    public isVerticalLayout$ = this.configService.config$.pipe(map((config) => config.layout === 'vertical'));
    public isNavbarInToolbar$ = this.configService.config$.pipe(
        map((config) => config.navbar.position === 'in-toolbar')
    );
    public isNavbarBelowToolbar$ = this.configService.config$.pipe(
        map((config) => config.navbar.position === 'below-toolbar')
    );

    public icSearch = icSearch;
    public icBookmarks = icBookmarks;
    public emojioneUS = emojioneUS;
    public emojioneDE = emojioneDE;
    public icMenu = icMenu;
    public icPersonAdd = icPersonAdd;
    public icAssignmentTurnedIn = icAssignmentTurnedIn;
    public icBallot = icBallot;
    public icDescription = icDescription;
    public icAssignment = icAssignment;
    public icReceipt = icReceipt;
    public icDoneAll = icDoneAll;
    public icArrowDropDown = icArrowDropDown;

    constructor(
        private layoutService: LayoutService,
        private configService: ConfigService,
        private navigationService: NavigationService,
        private popoverService: PopoverService
    ) {}

    public ngOnInit() {}

    public openQuickpanel() {
        this.layoutService.openQuickpanel();
    }

    public openSidenav() {
        this.layoutService.openSidenav();
    }

    public openMegaMenu(origin: ElementRef | HTMLElement) {
        this.popoverService.open({
            content: MegaMenuComponent,
            origin,
            position: [
                {
                    originX: 'start',
                    originY: 'bottom',
                    overlayX: 'start',
                    overlayY: 'top',
                },
                {
                    originX: 'end',
                    originY: 'bottom',
                    overlayX: 'end',
                    overlayY: 'top',
                },
            ],
        });
    }

    public openSearch() {
        this.layoutService.openSearch();
    }
}
