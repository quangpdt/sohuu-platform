import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import icSearch from '@iconify/icons-ic/twotone-search';

@Component({
    selector: 'vex-toolbar-search',
    templateUrl: './toolbar-search.component.html',
    styleUrls: ['./toolbar-search.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarSearchComponent implements OnInit {
    public isOpen: boolean;
    public icSearch = icSearch;

    @ViewChild('input', { read: ElementRef, static: true }) public input: ElementRef;

    constructor(private cd: ChangeDetectorRef) {}

    public ngOnInit() {}

    public open() {
        this.isOpen = true;
        this.cd.markForCheck();

        setTimeout(() => {
            this.input.nativeElement.focus();
        }, 100);
    }

    public close() {
        this.isOpen = false;
        this.cd.markForCheck();
    }
}
