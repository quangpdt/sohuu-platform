import { Component, Input, OnInit } from '@angular/core';
import { trackByRoute } from '../../utils/track-by';
import { NavigationService } from '../../services/navigation.service';
import icRadioButtonChecked from '@iconify/icons-ic/twotone-radio-button-checked';
import icRadioButtonUnchecked from '@iconify/icons-ic/twotone-radio-button-unchecked';
import { LayoutService } from '../../services/layout.service';
import { ConfigService } from '../../services/config.service';
import { map } from 'rxjs/operators';

@Component({
    selector: 'vex-sidenav',
    templateUrl: './sidenav.component.html',
    styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {
    @Input() public collapsed: boolean;
    public collapsedOpen$ = this.layoutService.sidenavCollapsedOpen$;
    public title$ = this.configService.config$.pipe(map((config) => config.sidenav.title));
    public imageUrl$ = this.configService.config$.pipe(map((config) => config.sidenav.imageUrl));
    public showCollapsePin$ = this.configService.config$.pipe(map((config) => config.sidenav.showCollapsePin));

    public items = this.navigationService.items;
    public trackByRoute = trackByRoute;
    public icRadioButtonChecked = icRadioButtonChecked;
    public icRadioButtonUnchecked = icRadioButtonUnchecked;

    constructor(
        private navigationService: NavigationService,
        private layoutService: LayoutService,
        private configService: ConfigService
    ) {}

    public ngOnInit() {}

    public onMouseEnter() {
        this.layoutService.collapseOpenSidenav();
    }

    public onMouseLeave() {
        this.layoutService.collapseCloseSidenav();
    }

    public toggleCollapse() {
        this.collapsed ? this.layoutService.expandSidenav() : this.layoutService.collapseSidenav();
    }
}
