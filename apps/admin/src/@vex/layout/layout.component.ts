import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    Inject,
    Input,
    OnDestroy,
    OnInit,
    TemplateRef,
    ViewChild,
} from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { LayoutService } from '../services/layout.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { MatSidenav, MatSidenavContainer } from '@angular/material/sidenav';
import { Event, NavigationEnd, Router, Scroll } from '@angular/router';
import { filter, map, startWith, withLatestFrom } from 'rxjs/operators';
import { checkRouterChildsData } from '../utils/check-router-childs-data';
import { DOCUMENT } from '@angular/common';
import { ConfigService } from '../services/config.service';

@Component({
    selector: 'vex-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit, AfterViewInit, OnDestroy {
    @Input() public sidenavRef: TemplateRef<any>;
    @Input() public toolbarRef: TemplateRef<any>;
    @Input() public footerRef: TemplateRef<any>;
    @Input() public quickpanelRef: TemplateRef<any>;

    public isLayoutVertical$ = this.configService.config$.pipe(map((config) => config.layout === 'vertical'));
    public isBoxed$ = this.configService.config$.pipe(map((config) => config.boxed));
    public isToolbarFixed$ = this.configService.config$.pipe(map((config) => config.toolbar.fixed));
    public isFooterFixed$ = this.configService.config$.pipe(map((config) => config.footer.fixed));
    public isFooterVisible$ = this.configService.config$.pipe(map((config) => config.footer.visible));
    public sidenavCollapsed$ = this.layoutService.sidenavCollapsed$;
    public isDesktop$ = this.layoutService.isDesktop$;

    public scrollDisabled$ = this.router.events.pipe(
        filter((event) => event instanceof NavigationEnd),
        startWith(null),
        map(() => checkRouterChildsData(this.router.routerState.root.snapshot, (data) => data.scrollDisabled))
    );

    public containerEnabled$ = this.router.events.pipe(
        filter((event) => event instanceof NavigationEnd),
        startWith(null),
        map(() => checkRouterChildsData(this.router.routerState.root.snapshot, (data) => data.containerEnabled))
    );

    public searchOpen$ = this.layoutService.searchOpen$;

    @ViewChild('quickpanel', { static: true }) public quickpanel: MatSidenav;
    @ViewChild('sidenav', { static: true }) public sidenav: MatSidenav;
    @ViewChild(MatSidenavContainer, { static: true }) public sidenavContainer: MatSidenavContainer;

    constructor(
        private cd: ChangeDetectorRef,
        private breakpointObserver: BreakpointObserver,
        private layoutService: LayoutService,
        private configService: ConfigService,
        private router: Router,
        @Inject(DOCUMENT) private document: Document
    ) {}

    public ngOnInit() {
        this.isDesktop$
            .pipe(
                filter((matches) => !matches),
                untilDestroyed(this)
            )
            .subscribe(() => this.layoutService.expandSidenav());

        this.layoutService.quickpanelOpen$
            .pipe(untilDestroyed(this))
            .subscribe((open) => (open ? this.quickpanel.open() : this.quickpanel.close()));

        this.layoutService.sidenavOpen$
            .pipe(untilDestroyed(this))
            .subscribe((open) => (open ? this.sidenav.open() : this.sidenav.close()));

        this.router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                withLatestFrom(this.isDesktop$),
                filter(([event, matches]) => !matches),
                untilDestroyed(this)
            )
            .subscribe(() => this.sidenav.close());
    }

    public ngAfterViewInit(): void {
        this.router.events
            .pipe(
                filter<Event, Scroll>((e: Event): e is Scroll => e instanceof Scroll),
                untilDestroyed(this)
            )
            .subscribe((e) => {
                if (e.position) {
                    // backward navigation
                    this.sidenavContainer.scrollable.scrollTo({
                        start: e.position[0],
                        top: e.position[1],
                    });
                } else if (e.anchor) {
                    // anchor navigation

                    const scroll = (anchor: HTMLElement) =>
                        this.sidenavContainer.scrollable.scrollTo({
                            behavior: 'smooth',
                            top: anchor.offsetTop,
                            left: anchor.offsetLeft,
                        });

                    let anchorElem = this.document.getElementById(e.anchor);

                    if (anchorElem) {
                        scroll(anchorElem);
                    } else {
                        setTimeout(() => {
                            anchorElem = this.document.getElementById(e.anchor);
                            scroll(anchorElem);
                        }, 100);
                    }
                } else {
                    // forward navigation
                    this.sidenavContainer.scrollable.scrollTo({
                        top: 0,
                        start: 0,
                    });
                }
            });
    }

    public ngOnDestroy(): void {}
}
