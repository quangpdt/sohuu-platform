import { Injectable } from '@angular/core';
import {
    NavigationDropdown,
    NavigationItem,
    NavigationLink,
    NavigationSubheading,
} from '../interfaces/navigation-item.interface';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class NavigationService {
    public items: NavigationItem[] = [];

    private _openChangeSubject = new Subject<NavigationDropdown>();
    public openChange$ = this._openChangeSubject.asObservable();

    constructor() {}

    public triggerOpenChange(item: NavigationDropdown) {
        this._openChangeSubject.next(item);
    }

    public isLink(item: NavigationItem): item is NavigationLink {
        return item.type === 'link';
    }

    public isDropdown(item: NavigationItem): item is NavigationDropdown {
        return item.type === 'dropdown';
    }

    public isSubheading(item: NavigationItem): item is NavigationSubheading {
        return item.type === 'subheading';
    }
}
