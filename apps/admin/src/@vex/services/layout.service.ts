import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { BreakpointObserver } from '@angular/cdk/layout';

@Injectable({
    providedIn: 'root',
})
export class LayoutService {
    private _quickpanelOpenSubject = new BehaviorSubject<boolean>(false);
    public quickpanelOpen$ = this._quickpanelOpenSubject.asObservable();

    private _sidenavOpenSubject = new BehaviorSubject<boolean>(false);
    public sidenavOpen$ = this._sidenavOpenSubject.asObservable();

    private _sidenavCollapsedSubject = new BehaviorSubject<boolean>(false);
    public sidenavCollapsed$ = this._sidenavCollapsedSubject.asObservable();

    private _sidenavCollapsedOpenSubject = new BehaviorSubject<boolean>(false);
    public sidenavCollapsedOpen$ = this._sidenavCollapsedOpenSubject.asObservable();

    private _configpanelOpenSubject = new BehaviorSubject<boolean>(false);
    public configpanelOpen$ = this._configpanelOpenSubject.asObservable();

    private _searchOpen = new BehaviorSubject<boolean>(false);
    public searchOpen$ = this._searchOpen.asObservable();

    public isDesktop$ = this.breakpointObserver.observe(`(min-width: 1200px)`).pipe(map((state) => state.matches));

    constructor(private router: Router, private breakpointObserver: BreakpointObserver) {}

    public openQuickpanel() {
        this._quickpanelOpenSubject.next(true);
    }

    public closeQuickpanel() {
        this._quickpanelOpenSubject.next(false);
    }

    public openSidenav() {
        this._sidenavOpenSubject.next(true);
    }

    public closeSidenav() {
        this._sidenavOpenSubject.next(false);
    }

    public collapseSidenav() {
        this._sidenavCollapsedSubject.next(true);
    }

    public expandSidenav() {
        this._sidenavCollapsedSubject.next(false);
    }

    public collapseOpenSidenav() {
        this._sidenavCollapsedOpenSubject.next(true);
    }

    public collapseCloseSidenav() {
        this._sidenavCollapsedOpenSubject.next(false);
    }

    public openConfigpanel() {
        this._configpanelOpenSubject.next(true);
    }

    public closeConfigpanel() {
        this._configpanelOpenSubject.next(false);
    }

    public openSearch() {
        this._searchOpen.next(true);
    }

    public closeSearch() {
        this._searchOpen.next(false);
    }

    public enableRTL() {
        this.router.navigate([], {
            queryParams: {
                rtl: 'true',
            },
        });
    }

    public disableRTL() {
        this.router.navigate([], {
            queryParams: {
                rtl: 'false',
            },
        });
    }
}
