import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class SearchService {
    public valueChangesSubject = new BehaviorSubject<string>(undefined);
    public valueChanges$ = this.valueChangesSubject.asObservable();

    public submitSubject = new Subject<string>();
    public submit$ = this.submitSubject.asObservable();

    public isOpenSubject = new BehaviorSubject<boolean>(false);
    public isOpen$ = this.isOpenSubject.asObservable();

    constructor() {}
}
