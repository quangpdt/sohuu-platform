/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { NestFactory } from '@nestjs/core';
import * as cookieParser from 'cookie-parser';

import { ServerModule } from './app/server.module';
import * as express from 'express';
import { join } from 'path';

async function bootstrap() {
    const app = await NestFactory.create(ServerModule, {
        // logger: false,
    });
    // app.useLogger(app.get(LoggerService));
    app.use(cookieParser());
    app.use('/static', express.static(join(process.cwd(), 'static')));

    // app.use(csurf());

    const globalPrefix = 'api';
    app.setGlobalPrefix(globalPrefix);

    const port = process.env.port || 3333;
    await app.listen(port, () => {
        console.log('Listening at http://localhost:' + port + '/' + globalPrefix);
    });
}

// bootstrap();

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = (mainModule && mainModule.filename) || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
    bootstrap().catch((err) => console.error(err));
}
