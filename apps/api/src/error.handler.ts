import {
    CheckViolationError,
    DataError,
    DBError,
    ForeignKeyViolationError,
    NotFoundError,
    NotNullViolationError,
    UniqueViolationError,
    ValidationError,
} from 'objection';
import { ResponseCode } from '@sohuu-platform/enums';

export function modelErrorHandler(err) {
    if (
        err instanceof ValidationError ||
        err instanceof UniqueViolationError ||
        err instanceof NotNullViolationError ||
        err instanceof ForeignKeyViolationError ||
        err instanceof CheckViolationError ||
        err instanceof DataError
    ) {
        return ResponseCode.DataMismatched;
    } else if (err instanceof NotFoundError) {
        return ResponseCode.DataNotFound;
    } else if (err instanceof DBError) {
        return ResponseCode.GeneralError;
    } else {
        return ResponseCode.UnknownError;
    }
}
