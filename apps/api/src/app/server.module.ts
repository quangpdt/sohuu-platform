import { BadRequestException, Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { DatabaseModule } from './modules/database/database.module';
import { EmailModule } from './modules/email/email.module';
import { UserModule } from './modules/user/user.module';
import { AuthenticationModule } from './modules/authentication/authentication.module';
import { ProjectModule } from './modules/project/project.module';
import { ResourceModule } from './modules/resource/resource.module';
import { PropertyModule } from './modules/property/property.module';
import { ResponseCode } from '@sohuu-platform/enums';
import includes from 'lodash/includes';
import { ReCaptchaModule } from './modules/google-recaptcha/recaptcha.module';
import { environment } from '../environment';
import { findLast } from '@angular/compiler/src/directive_resolver';
import { LoggerService } from './modules/logger/logger.service';
import { LoggerModule } from './modules/logger/logger.module';
import { NotificationModule } from './modules/notification/notification.module';
import { EmailScalar, JSONObjectScalar, JSONScalar } from '@sohuu-platform/scalars';

const errorCodes = Object.values(ResponseCode);

@Module({
    imports: [
        DatabaseModule,
        EmailModule,
        AuthenticationModule,
        UserModule,
        ProjectModule,
        PropertyModule,
        ResourceModule,
        NotificationModule,
        GraphQLModule.forRootAsync({
            useFactory: async (loggerService: LoggerService) => {
                return {
                    debug: true,
                    playground: true,
                    installSubscriptionHandlers: true,
                    autoSchemaFile: 'schema.graphql',
                    introspection: true,
                    context: ({ req, res }) => ({ req, res }),
                    formatError: (err) => {
                        const appError = findLast(errorCodes, (code) => includes(err.message, code));
                        if (appError) {
                            return new Error(appError);
                        }
                        loggerService.error(err);
                        return new Error(ResponseCode.GeneralError);
                    },
                };
            },
            imports: [LoggerModule],
            inject: [LoggerService],
        }),
        ReCaptchaModule.forRoot({
            secretKey: environment.reCaptchaSecretKey,
            response: (req) => req.headers.authorization,
            skipIf: (req) => process.env.NODE_ENV !== 'production',
            onError: (e) => {
                throw new BadRequestException('Invalid recaptcha.');
            },
        }),
        LoggerModule,
    ],
    providers: [EmailScalar, JSONObjectScalar, JSONScalar],
})
export class ServerModule {}
