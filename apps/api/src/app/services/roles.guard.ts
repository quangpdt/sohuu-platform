import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import union from 'lodash/union';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private readonly reflector: Reflector) {}

    public canActivate(context: ExecutionContext): boolean {
        const classRoles = this.reflector.get<string[]>('roles', context.getClass()) || [];
        const handlerRoles = this.reflector.get<string[]>('roles', context.getHandler()) || [];

        if (classRoles.length === 0 && handlerRoles.length === 0) {
            return true;
        }

        const roles = union(classRoles, handlerRoles);
        const request = context.switchToHttp().getRequest();
        const user = request.user;
        const hasRole = () => roles.includes(user.role);
        return user && user.role && hasRole();
    }
}
