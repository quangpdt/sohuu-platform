import { ResponseCode } from '@sohuu-platform/enums';
import { ResponseMessage } from '@sohuu-platform/models';

export function generateResponse(code?: ResponseCode, data?: any): ResponseMessage {
    return {
        data,
        message: code,
    };
}
