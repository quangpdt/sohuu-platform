import * as crypto from 'crypto';

export function generateRandomToken() {
    return Math.floor(100000 + Math.random() * 900000).toString(10);
}

export async function generateToken(): Promise<string> {
    return new Promise((resolve, reject) => {
        crypto.randomBytes(48, (error, buffer) => {
            if (error) {
                return resolve(null);
            }
            return resolve(buffer.toString('hex'));
        });
    });
}
