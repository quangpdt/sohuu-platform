export const saltRounds = 10;
export const expiresIn = '2d'; // 0.25h
export const refreshTokenExpiresIn = 1209600; // 14d
