import { Injectable, Logger } from '@nestjs/common';
import winston from 'winston';
import 'winston-daily-rotate-file';
import { environment } from '../../../environment';
import { cwd } from 'process';
import { join } from 'path';

@Injectable()
export class LoggerService extends Logger {
    private logger: winston.Logger;
    constructor() {
        super();
        this.logger = winston.createLogger({
            level: environment.isProduction ? 'warn' : 'debug',
            format: winston.format.json(),
            transports: [
                new winston.transports.DailyRotateFile({
                    filename: 'error-%DATE%.log',
                    level: 'error',
                    zippedArchive: true,
                    datePattern: 'YYYY-MM-DD',
                    maxFiles: '14d',
                    dirname: join(cwd(), 'logs'),
                }),
                new winston.transports.DailyRotateFile({
                    filename: 'other-%DATE%.log',
                    zippedArchive: true,
                    datePattern: 'YYYY-MM-DD',
                    maxFiles: '14d',
                    dirname: join(cwd(), 'logs'),
                }),
                ...(environment.isProduction
                    ? []
                    : [
                          new winston.transports.Console({
                              format: winston.format.simple(),
                              level: 'info',
                          }),
                      ]),
            ],
        });
    }

    public error(message: any, trace?: string, context?: string) {
        super.error(message, trace, context);
        this.logger.error(message);
    }

    public log(message: any, context?: string) {
        super.log(message, context);
        this.logger.log('info', message);
    }

    public warn(message: any, context?: string) {
        super.warn(message, context);
        this.logger.warn(message);
    }

    public debug(message: any, context?: string) {
        super.debug(message, context);
        this.logger.debug(message);
    }

    public verbose(message: any, context?: string) {
        super.verbose(message, context);
        this.logger.verbose(message);
    }
}
