import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { UserService } from '../database/services/user.service';
import { ResponseMessage, UserModel } from '@sohuu-platform/models';
import { UseGuards } from '@nestjs/common';
import { JwtStrategy } from '../authentication/services/jwt.strategy';
import { CurrentUser } from '../../decorators/current-user.decorator';
import { UserMetadataService } from '../database/services/user-metadata.service';
import { LoggerService } from '../logger/logger.service';
import { UpdateBasicInfoInput, UpdateUserAddressInput } from '@sohuu-platform/input-types';

@Resolver(() => UserModel)
export class UserResolver {
    constructor(
        private readonly userService: UserService,
        private userMetadataService: UserMetadataService,
        private loggerService: LoggerService
    ) {}

    @Query(() => UserModel)
    @UseGuards(JwtStrategy)
    public async whoAmI(@CurrentUser() user: UserModel) {
        return this.userService.findUserById(user.id);
    }

    @Mutation(() => UserModel)
    @UseGuards(JwtStrategy)
    public async updateBasicInfo(
        @CurrentUser() user: UserModel,
        @Args('updateBasicInfoInput') input: UpdateBasicInfoInput
    ) {
        return this.userService.updateUserBasicInfo(user.id, input);
    }

    @Mutation(() => ResponseMessage)
    @UseGuards(JwtStrategy)
    public async updateUserAddress(
        @CurrentUser() user: UserModel,
        @Args('updateUserAddressInput') input: UpdateUserAddressInput
    ) {
        await this.userMetadataService.updateUserAddress(user.id, input);
        return {};
    }
}
