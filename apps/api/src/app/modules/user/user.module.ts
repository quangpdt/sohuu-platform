import { Module } from '@nestjs/common';
import { UserResolver } from './user.resolver';
import { AuthenticationModule } from '../authentication/authentication.module';
import { LoggerModule } from '../logger/logger.module';

@Module({
    imports: [AuthenticationModule, LoggerModule],
    providers: [UserResolver],
})
export class UserModule {}
