import { Controller, Get, Req, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { environment } from '../../../environment';
import { FacebookStrategy } from './services/facebook.strategy';
import { AuthenticationService } from './services/authentication.service';

@Controller('oauth')
export class OauthController {
    constructor(private facebookStrategy: FacebookStrategy, private authenticationService: AuthenticationService) {}

    @Get('google')
    @UseGuards(AuthGuard('google'))
    public googleLogin() {
        // initiates the Google OAuth2 login flow
    }

    @Get('facebook')
    @UseGuards(AuthGuard('facebook'))
    public facebookLogin() {
        // initiates the Google OAuth2 login flow
    }

    @Get('google/callback')
    @UseGuards(AuthGuard('google'))
    public googleLoginCallback(@Req() req, @Res() res) {
        const payload: any = req.user;
        if (payload) {
            this.authenticationService.setCookieToResponse(res, payload.tokens);
            if (payload.isFirstTime) {
                res.redirect(`${environment.baseUrl.client}/oauth-success/first-time`);
            } else {
                res.redirect(`${environment.baseUrl.client}/oauth-success`);
            }
        } else {
            res.redirect('http://localhost:4200/login/failure');
        }
    }

    @Get('facebook/callback')
    @UseGuards(AuthGuard('facebook'))
    public facebook(@Req() req, @Res() res) {
        const payload: any = req.user;
        if (payload) {
            this.authenticationService.setCookieToResponse(res, payload.tokens);
            if (payload.isFirstTime) {
                res.redirect(`${environment.baseUrl.client}/oauth-success/first-time`);
            } else {
                res.redirect(`${environment.baseUrl.client}/oauth-success`);
            }
        } else {
            res.redirect('http://localhost:4200/login/failure');
        }
    }
}
