import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-facebook';
import { AuthenticationService } from './authentication.service';
import { environment } from '../../../../environment';
import { OAuthProvider } from '@sohuu-platform/enums';

@Injectable()
export class FacebookStrategy extends PassportStrategy(Strategy, 'facebook') {
    public readonly options = {
        clientID: environment.oauth.fbAppId,
        clientSecret: environment.oauth.fbClientSecret,
        callbackURL: `${environment.baseUrl.server}/api/oauth/facebook/callback`,
        scope: ['email'],
        graphAPIVersion: 'v6.0',
    };

    constructor(private readonly authService: AuthenticationService) {
        super({
            clientID: environment.oauth.fbAppId,
            clientSecret: environment.oauth.fbClientSecret,
            callbackURL: `${environment.baseUrl.client}/api/oauth/facebook/callback`,
            scope: ['email'],
            passReqToCallback: true,
            graphAPIVersion: 'v6.0',
            profileFields: ['email', 'displayName', 'profileUrl', 'picture'],
        });
    }

    public async validate(request: any, accessToken: string, refreshToken: string, profile, done: Function) {
        try {
            const payload = await this.authService.validateOAuthLogin(
                {
                    id: profile.id,
                    email: profile.emails[0].value,
                    name: profile.displayName,
                },
                OAuthProvider.Facebook
            );

            done(null, payload);
        } catch (err) {
            done(err, false);
        }
    }
}
