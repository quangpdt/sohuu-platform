import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class PublicStrategy implements CanActivate {
    constructor(private readonly authService: AuthenticationService) {}

    public getRequest(context: ExecutionContext) {
        const ctx = GqlExecutionContext.create(context);
        return ctx.getContext().req;
    }

    public async canActivate(context: ExecutionContext): Promise<boolean> {
        const req = this.getRequest(context);

        const cookie = req.cookies;
        if (!(cookie && cookie.accessToken && cookie.refreshToken)) {
            return true;
        }

        const user = this.authService.verifyAccessToken(cookie.accessToken);

        if (!user) {
            return true;
        }

        req.user = user;
        return true;
    }
}
