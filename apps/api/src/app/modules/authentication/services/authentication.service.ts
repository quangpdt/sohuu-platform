import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../../database/services/user.service';
import { expiresIn, refreshTokenExpiresIn } from '../../../utils/authentication.constant';
import * as jwt from 'jsonwebtoken';
import { OAuthProvider, ResponseCode } from '@sohuu-platform/enums';
import { environment } from '../../../../environment';
import { UserModel } from '@sohuu-platform/models';

@Injectable()
export class AuthenticationService {
    constructor(private readonly userService: UserService, private readonly jwtService: JwtService) {}

    public createJWTToken({ id, email, role, isPhoneVerified }: Partial<UserModel>, refreshToken?: string) {
        const user = { id, email, role, isPhoneVerified };
        const accessToken = this.jwtService.sign(user);
        if (!refreshToken) {
            refreshToken = jwt.sign(user, environment.jwt.refreshTokenSecretKey, {
                expiresIn: refreshTokenExpiresIn,
            });
        }

        return {
            expiresIn,
            accessToken,
            refreshToken,
        };
    }

    public verifyAccessToken(token: string, ignoreExpiration: boolean = false): Partial<UserModel> | undefined {
        try {
            const { id, email, role, isPhoneVerified } = jwt.verify(token, environment.jwt.secretKey, {
                ignoreExpiration,
            }) as any;
            return {
                id,
                email,
                role,
                isPhoneVerified,
            };
        } catch (e) {
            return undefined;
        }
    }

    public async refreshToken({ email }: UserModel, refreshToken: string) {
        jwt.verify(refreshToken, environment.jwt.refreshTokenSecretKey);
        const user: UserModel = await this.userService.findByEmail(email);
        if (!user) {
            throw ResponseCode.DataNotFound;
        }
        return this.createJWTToken(user, refreshToken);
    }

    public async authenticate(email: string, password: string): Promise<Partial<UserModel>> {
        const user: UserModel = await this.userService.findByEmail(email);
        if (user && (await bcrypt.compare(password, user.password))) {
            return {
                id: user.id,
                email: user.email,
                role: user.role,
                isPhoneVerified: user.isPhoneVerified,
            };
        }
        return null;
    }

    public async validateOAuthLogin(profile: any, provider: OAuthProvider): Promise<any> {
        let isFirstTime = false;
        let user = await this.userService.findByThirdPartyId(profile.id, provider);
        if (!user) {
            isFirstTime = true;
            user = await this.userService.createUserFromThirdParty(profile.name, profile.email, provider, profile.id);
        }

        return {
            tokens: this.createJWTToken(user),
            isFirstTime,
        };
    }

    public setCookieToResponse(response: any, { accessToken, refreshToken }: any) {
        response.cookie('accessToken', accessToken, {
            sameSite: true,
        });
        response.cookie('refreshToken', refreshToken, {
            sameSite: true,
        });
    }
}
