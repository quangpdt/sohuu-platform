import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-google-oauth20';
import { AuthenticationService } from './authentication.service';
import { environment } from '../../../../environment';
import findLast from 'lodash/findLast';
import { OAuthProvider } from '@sohuu-platform/enums';

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
    constructor(private readonly authService: AuthenticationService) {
        super({
            clientID: environment.oauth.googleAppId,
            clientSecret: environment.oauth.googleAppClientSecret,
            callbackURL: `${environment.baseUrl.client}/api/oauth/google/callback`,
            passReqToCallback: true,
            scope: ['profile', 'email'],
        });
    }

    public async validate(request: any, accessToken: string, refreshToken: string, profile, done: Function) {
        try {
            const mainEmail = findLast(profile.emails, (item) => item.verified);

            if (!mainEmail || !mainEmail.value) {
                done(null, false);
            }

            const payload = await this.authService.validateOAuthLogin(
                {
                    name: profile.displayName,
                    email: mainEmail.value,
                    id: profile.id,
                },
                OAuthProvider.Google
            );

            done(null, payload);
        } catch (err) {
            done(err, false);
        }
    }
}
