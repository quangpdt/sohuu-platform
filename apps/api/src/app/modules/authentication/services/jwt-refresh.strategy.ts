import { BadRequestException, CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { AuthenticationService } from './authentication.service';

@Injectable()
export class JwtRefreshStrategy implements CanActivate {
    constructor(private readonly authService: AuthenticationService) {}

    public getRequest(context: ExecutionContext) {
        const ctx = GqlExecutionContext.create(context);
        return ctx.getContext().req;
    }

    public async canActivate(context: ExecutionContext): Promise<boolean> {
        const req = this.getRequest(context);
        const cookie = req.cookies;
        if (!(cookie && cookie.accessToken && cookie.refreshToken)) {
            throw new BadRequestException();
        }

        const user = this.authService.verifyAccessToken(cookie.accessToken, true);

        if (!user) {
            throw new UnauthorizedException();
        }

        req.user = user;
        return true;
    }
}
