import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthenticationService } from './services/authentication.service';
import { JwtStrategy } from './services/jwt.strategy';
import { JwtRefreshStrategy } from './services/jwt-refresh.strategy';
import { expiresIn } from '../../utils/authentication.constant';
import { AuthenticationResolver } from './authentication.resolver';
import { GoogleStrategy } from './services/google.strategy';
import { OauthController } from './oauth.controller';
import { FacebookStrategy } from './services/facebook.strategy';
import { environment } from '../../../environment';
import { PublicStrategy } from './services/public.strategy';
import { LoggerModule } from '../logger/logger.module';

@Module({
    imports: [
        PassportModule.register({ defaultStrategy: 'jwt' }),
        JwtModule.register({ secret: environment.jwt.secretKey, signOptions: { expiresIn } }),
        LoggerModule,
    ],
    providers: [
        AuthenticationService,
        JwtStrategy,
        JwtRefreshStrategy,
        GoogleStrategy,
        FacebookStrategy,
        PublicStrategy,
        AuthenticationResolver,
    ],
    controllers: [OauthController],
    exports: [
        AuthenticationService,
        JwtStrategy,
        JwtRefreshStrategy,
        GoogleStrategy,
        FacebookStrategy,
        AuthenticationResolver,
    ],
})
export class AuthenticationModule {}
