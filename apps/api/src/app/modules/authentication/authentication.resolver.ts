import { Args, Context, GraphQLExecutionContext, Mutation, Resolver } from '@nestjs/graphql';
import { UserService } from '../database/services/user.service';
import { UseGuards } from '@nestjs/common';
import { AuthenticationService } from './services/authentication.service';
import { JwtRefreshStrategy } from './services/jwt-refresh.strategy';
import { CurrentUser } from '../../decorators/current-user.decorator';
import { EmailService } from '../email/services/email.service';
import { environment } from '../../../environment';
import { AuthenticationError, UserInputError } from 'apollo-server';
import { ReCaptcha } from '../google-recaptcha/recaptcha.decorator';
import { google } from 'googleapis';
import { JwtStrategy } from './services/jwt.strategy';
import { UserPhoneSessionService } from '../database/services/user-phone-session.service';
import { AuthenticationModel, ResponseMessage, UserModel } from '@sohuu-platform/models';
import {
    ActivateUserInput,
    ForgotPasswordInput,
    LoginInput,
    PhoneVerificationInput,
    RefreshTokenInput,
    RegisterInput,
    ResetPasswordInput,
} from '@sohuu-platform/input-types';
import { EmailType, ResponseCode } from '@sohuu-platform/enums';

@Resolver('authentication')
export class AuthenticationResolver {
    constructor(
        private userService: UserService,
        private userPhoneSessionService: UserPhoneSessionService,
        private authenticationService: AuthenticationService,
        private emailService: EmailService
    ) {}

    @Mutation(() => AuthenticationModel)
    @ReCaptcha()
    public async login(
        @Args('loginInput') input: LoginInput,
        @Context() context: GraphQLExecutionContext
    ): Promise<AuthenticationModel> {
        const user = await this.authenticationService.authenticate(input.email, input.password);
        if (!user) {
            throw new AuthenticationError(ResponseCode.UserUnauthorized);
        }

        const payload = this.authenticationService.createJWTToken(user);
        (context as any).res.cookie('accessToken', payload.accessToken, {
            sameSite: true,
        });
        (context as any).res.cookie('refreshToken', payload.refreshToken, {
            sameSite: true,
        });
        return payload;
    }

    @Mutation(() => AuthenticationModel)
    @UseGuards(JwtRefreshStrategy)
    public async refreshToken(
        @Args('refreshTokenInput') input: RefreshTokenInput,
        @CurrentUser() user: UserModel
    ): Promise<AuthenticationModel> {
        try {
            return this.authenticationService.refreshToken(user, input.refreshToken);
        } catch (e) {
            throw new AuthenticationError(ResponseCode.UserUnauthorized);
        }
    }

    @Mutation(() => ResponseMessage)
    public async register(@Args('registerInput') input: RegisterInput): Promise<ResponseMessage> {
        const isUserExists = await this.userService.isUserExists(input.email);

        if (isUserExists) {
            throw new UserInputError(ResponseCode.DataAlreadyExists);
        }

        const user = await this.userService.createUser(input.name, input.email, input.password);
        this.emailService.sendMail(user.email, EmailType.UserActivation, {
            name: user.name,
            activateLink: `${environment.baseUrl.client}/kich-hoat-tai-khoan/${user.email}/${user.activateToken}`,
        });

        return {};
    }

    @Mutation(() => ResponseMessage)
    public async forgotPassword(@Args('forgotPasswordInput') input: ForgotPasswordInput) {
        const user = await this.userService.findByEmail(input.email);
        if (!user) {
            // Silently ignore further action if user not found
            return {};
        }
        const forgotPasswordToken = await this.userService.updateUserForgotPasswordToken(user);
        this.emailService.sendMail(input.email, EmailType.UserForgotPassword, {
            name: user.name,
            activateLink: `${environment.baseUrl.client}/khoi-phuc-mat-khau/${input.email}/${forgotPasswordToken}`,
        });
        return {};
    }

    @Mutation(() => ResponseMessage)
    public async reActivateUser(@Args('email') email: string) {
        const user = await this.userService.findByEmail(email);
        if (!user) {
            // Silently ignore further action if user not found
            return {};
        }

        const activateToken = await this.userService.updateUserActivationToken(user);
        this.emailService.sendMail(email, EmailType.UserActivation, {
            name: user.name,
            activateLink: `${environment.baseUrl.client}/kich-hoat-tai-khoan/${user.email}/${activateToken}`,
        });
        return {};
    }

    @Mutation(() => ResponseMessage)
    public async resetPassword(
        @Args('resetPasswordInput')
        { email, token, password }: ResetPasswordInput
    ) {
        const result = await this.userService.resetPassword(email, token, password);
        if (!result) {
            throw new UserInputError(ResponseCode.DataInvalid);
        }

        if (result !== 1) {
            throw new UserInputError(ResponseCode.DataInvalid);
        }

        return {};
    }

    @Mutation(() => ResponseMessage)
    public async activateUser(
        @Args('activateUserInput') input: ActivateUserInput,
        @Context() context: GraphQLExecutionContext
    ) {
        try {
            const user = await this.userService.activateUser(input.email, input.token);
            const payload = this.authenticationService.createJWTToken(user);

            this.authenticationService.setCookieToResponse((context as any).res, payload);

            return payload;
        } catch (e) {
            throw new UserInputError(e);
        }
    }

    @Mutation(() => ResponseMessage)
    @UseGuards(JwtStrategy)
    public async requestPhoneVerification(
        @Args('phoneVerificationInput') { phoneNumber, recaptchaToken }: PhoneVerificationInput,
        @CurrentUser() user: UserModel
    ) {
        const currentVerifiedPhone = await this.userService.getVerifiedPhoneNumber(user.id);
        if (currentVerifiedPhone && currentVerifiedPhone.phone === phoneNumber) {
            throw ResponseCode.PhoneWasVerifiedBefore;
        }

        const identityToolkit = google.identitytoolkit({
            auth: environment.googleIdentityApiKey,
            version: 'v3',
        });

        const response = await identityToolkit.relyingparty.sendVerificationCode({
            requestBody: {
                phoneNumber,
                recaptchaToken,
            },
        });

        const sessionInfo = response.data.sessionInfo;
        this.userPhoneSessionService.savePhoneSession(user.id, phoneNumber, sessionInfo);
        return {};
    }

    @Mutation(() => ResponseMessage)
    @UseGuards(JwtStrategy)
    public async validatePhoneNumber(@Args('code') code: string, @CurrentUser() user: UserModel) {
        const identityToolkit = google.identitytoolkit({
            auth: environment.googleIdentityApiKey,
            version: 'v3',
        });

        const { phone, phoneSession } = await this.userPhoneSessionService.getPhoneSession(user.id);

        try {
            await identityToolkit.relyingparty.verifyPhoneNumber({
                requestBody: {
                    code,
                    sessionInfo: phoneSession,
                },
            });
        } catch (e) {
            throw ResponseCode.PhoneValidationFailed;
        }

        await this.userService.updateVerifiedPhone(user.id, phone);
        this.userPhoneSessionService.removePhoneSession(user.id);

        return {};
    }
}
