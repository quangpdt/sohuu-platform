import { Controller, Get } from '@nestjs/common';
import { PropertyService } from '../database/services/property.service';

@Controller('dummy-properties')
export class PropertyController {
    constructor(private propertyService: PropertyService) {}
    @Get('generate')
    public generateData() {
        return this.propertyService.batchInsertProperties();
    }

    @Get('related')
    public relatedProperties() {
        return this.propertyService.getRelatedProperties(13, 'ban-nhanh-can-ho-the-pegasuite-1');
    }
}
