import { Module } from '@nestjs/common';
import { PropertyResolver } from './property.resolver';
import { AuthenticationModule } from '../authentication/authentication.module';
import { LoggerModule } from '../logger/logger.module';
import { PropertyController } from './property.controller';

@Module({
    imports: [AuthenticationModule, LoggerModule],
    providers: [PropertyResolver],
    controllers: [PropertyController],
})
export class PropertyModule {}
