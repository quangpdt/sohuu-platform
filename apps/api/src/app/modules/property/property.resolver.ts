import { Args, ID, Mutation, Query, Resolver } from '@nestjs/graphql';
import { PropertyModel, ResponseMessage, UserModel } from '@sohuu-platform/models';
import { CurrentUser } from '../../decorators/current-user.decorator';
import { UseGuards } from '@nestjs/common';
import { PropertyService } from '../database/services/property.service';
import { JwtStrategy } from '../authentication/services/jwt.strategy';
import { ResponseCode } from '@sohuu-platform/enums';
import { PublicStrategy } from '../authentication/services/public.strategy';
import {
    FindByIdAndSlugInput,
    PropertySearchInput,
    PublicPropertyInput,
    SubmitPropertyInput
} from '@sohuu-platform/input-types';

@Resolver('property')
export class PropertyResolver {
    constructor(private readonly propertyService: PropertyService) {}

    @Mutation(() => PropertyModel)
    @UseGuards(JwtStrategy)
    public submitProperty(@Args('submitPropertyInput') input: SubmitPropertyInput, @CurrentUser() user: UserModel) {
        return this.propertyService.createProperty(user.id, input);
    }

    @Mutation(() => PropertyModel)
    @UseGuards(JwtStrategy)
    public updateProperty(@Args('submitPropertyInput') input: SubmitPropertyInput, @CurrentUser() user: UserModel) {
        return this.propertyService.updateProperty(user.id, input);
    }

    @Query(() => PropertyModel)
    @UseGuards(JwtStrategy)
    public async getProperty(@CurrentUser() user: UserModel, @Args({ name: 'id', type: () => ID }) id: number) {
        return this.propertyService.getPropertyById(user.id, id);
    }

    @Query(() => [PropertyModel])
    @UseGuards(JwtStrategy)
    public getOwnedProperties(@CurrentUser() user: UserModel) {
        return this.propertyService.getOwnedProperties(user.id);
    }

    @Mutation(() => ResponseMessage)
    @UseGuards(JwtStrategy)
    public async reSubmitProperty(@CurrentUser() user: UserModel, @Args({ name: 'id', type: () => ID }) id: number) {
        await this.propertyService.reSubmitPropertyStatus(user.id, id);
        return {};
    }

    @Query(() => PropertyModel)
    @UseGuards(PublicStrategy)
    public async getPublicProperty(
        @CurrentUser() user: UserModel,
        @Args('findByIdAndSlugInput') { id, slug }: FindByIdAndSlugInput
    ) {
        const result = await this.propertyService.getPublicProperty(id, slug, user && user.isPhoneVerified, user?.id);
        if (!result) {
            throw ResponseCode.DataNotFound;
        }
        return result;
    }

    @Query(() => ResponseMessage)
    public async getPublicProperties(@Args('propertySearchInput') searchCriteria: PropertySearchInput) {
        const result = await this.propertyService.getPublicProperties(searchCriteria);
        return {
            data: result,
        };
    }

    @Query(() => [PropertyModel])
    public async getLatestProperties() {
        return this.propertyService.getLatestProperties(6);
    }

    @Query(() => [PropertyModel])
    public async getRelatedProperties(@Args('publicPropertyInput') { id, slug }: PublicPropertyInput) {
        return this.propertyService.getRelatedProperties(id, slug);
    }
}
