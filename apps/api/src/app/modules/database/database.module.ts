import { Global, Module } from '@nestjs/common';
import * as Knex from 'knex';
import { UserService } from './services/user.service';
import { environment } from '../../../environment';
import { Model } from 'objection';
import {
    CategoryModel,
    FileModel,
    NewsCategoryModel,
    NewsModel,
    ProjectMetadataModel,
    ProjectModel,
    PropertyFileModel,
    PropertyMetadataModel,
    PropertyModel,
    PropertyStatusModel,
    UserFavoriteModel,
    UserMetadataModel,
    UserModel,
    UserNotificationModel,
    UserPhoneSessionModel,
} from '@sohuu-platform/models';
import { ProjectService } from './services/project.service';
import { PropertyService } from './services/property.service';
import { UserPhoneSessionService } from './services/user-phone-session.service';
import { StaticResourceService } from './services/static-resource.service';
import { PropertyStatusService } from './services/property-status.service';
import { PropertyMetadataService } from './services/property-metadata.service';
import { UserMetadataService } from './services/user-metadata.service';
import { LoggerModule } from '../logger/logger.module';
import { UserNotificationService } from './services/user-notification.service';
import { UserFavoriteService } from './services/user-favorite.service';

const models = [
    PropertyModel,
    UserModel,
    CategoryModel,
    FileModel,
    NewsModel,
    NewsCategoryModel,
    ProjectModel,
    ProjectMetadataModel,
    PropertyMetadataModel,
    PropertyFileModel,
    PropertyStatusModel,
    UserPhoneSessionModel,
    UserMetadataModel,
    UserNotificationModel,
    UserFavoriteModel,
];
const modelProviders = models.map((model) => ({
    provide: model.name,
    useValue: model,
}));

const providers = [
    ...modelProviders,
    {
        provide: 'KnexConnection',
        useFactory: async () => {
            const knex = Knex(environment.knexConfig);
            if (!environment.isProduction) {
                knex.on('query', (data) => {
                    console.log('SQL', data.sql);
                    console.log('BINDINGS', data.bindings);
                });
            }
            Model.knex(knex);
            return knex;
        },
    },
];

const services = [
    UserService,
    ProjectService,
    PropertyStatusService,
    PropertyMetadataService,
    PropertyService,
    UserPhoneSessionService,
    StaticResourceService,
    UserMetadataService,
    UserNotificationService,
    UserFavoriteService,
];

@Global()
@Module({
    imports: [LoggerModule],
    providers: [...providers, ...services],
    exports: [...providers, ...services],
})
export class DatabaseModule {}
