import { Inject, Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { addDays, differenceInMilliseconds } from 'date-fns';
import { ModelClass } from 'objection';
import { BaseModelService } from './base-model.service';
import { saltRounds } from '../../../utils/authentication.constant';
import { generateToken } from '../../../utils/generator.util';
import { UserModel } from '@sohuu-platform/models';
import { OAuthProvider, ResponseCode } from '@sohuu-platform/enums';

@Injectable()
export class UserService extends BaseModelService<UserModel> {
    protected readonly defaultColumns = ['id', 'name', 'email', 'role', 'isActivated', 'phone', 'is_phone_verified'];
    protected readonly tableName = 'users';

    constructor(@Inject('UserModel') public readonly modelClass: ModelClass<UserModel>) {
        super();
    }

    public async findAll(): Promise<UserModel[]> {
        return this.modelClass.query().select(this.columns());
    }

    public findUserById(id: number) {
        return this.modelClass
            .query()
            .select(...this.columns())
            .findById(id)
            .withGraphFetched('[metadata(all)]')
            .where(this.notDeleted);
    }

    public async findByThirdPartyId(thirdPartyId: string, provider: OAuthProvider): Promise<UserModel> {
        const option = {};
        switch (provider) {
            case OAuthProvider.Facebook:
                option['facebookId'] = thirdPartyId;
                break;
            case OAuthProvider.Google:
                option['googleId'] = thirdPartyId;
                break;
        }
        return this.modelClass
            .query()
            .select(...this.columns())
            .findOne(option)
            .where(this.notDeleted);
    }

    public async findByEmail(email: string): Promise<UserModel> {
        return this.modelClass
            .query()
            .select(...this.columns(), 'password')
            .findOne({ email, isActivated: true })
            .where(this.notDeleted);
    }

    public async isUserExists(email: string): Promise<boolean> {
        return !!(await this.modelClass.query().select('id').findOne({ email }));
    }

    public async createUser(name: string, email: string, password: string): Promise<Partial<UserModel>> {
        const activateToken = await generateToken();
        const user: Partial<UserModel> = {
            name,
            email,
            password: bcrypt.hashSync(password, saltRounds),
            activateToken,
            activateExpirationTime: addDays(new Date(), 1),
            isActivated: false,
        };
        await this.modelClass.query().insert(user);

        return user as UserModel;
    }

    public async createUserFromThirdParty(
        name: string,
        email: string,
        provider: OAuthProvider,
        providerId: string
    ): Promise<UserModel> {
        let user: Partial<UserModel> = {
            name,
            email,
            isActivated: true,
        };
        switch (provider) {
            case OAuthProvider.Google:
                user.googleId = providerId;
                break;
            case OAuthProvider.Facebook:
                user.facebookId = providerId;
                break;
        }

        user = await this.modelClass.query().insert(user);

        return user as UserModel;
    }

    public async updateUserForgotPasswordToken(user: UserModel): Promise<string> {
        const forgotPasswordToken: string = await generateToken();
        if (!forgotPasswordToken) {
            throw ResponseCode.GeneralError;
        }

        await this.modelClass
            .query()
            .findById(user.id)
            .patch({
                forgotPasswordToken,
                forgotPasswordExpirationTime: addDays(new Date(), 1),
            })
            .where(this.notDeleted);

        return forgotPasswordToken;
    }

    public async updateUserActivationToken(user: UserModel): Promise<string> {
        const activateToken: string = await generateToken();
        if (!activateToken) {
            throw ResponseCode.GeneralError;
        }

        await this.modelClass
            .query()
            .findById(user.id)
            .patch({
                activateToken,
                activateExpirationTime: addDays(new Date(), 1),
            })
            .where(this.notDeleted);

        return activateToken;
    }

    public async updateUserBasicInfo(userId: number, user: Partial<UserModel>) {
        await this.modelClass
            .query()
            .findById(userId)
            .patch({
                name: user.name,
            })
            .where(this.notDeleted);

        return this.findById(userId);
    }

    public async resetPassword(email: string, forgotPasswordToken: string, password: string) {
        const user = await this.modelClass
            .query()
            .findOne({ email, forgotPasswordToken })
            .where('forgotPasswordExpirationTime', '>', new Date())
            .where(this.notDeleted);

        if (!user) {
            return null;
        }

        return this.modelClass
            .query()
            .findById(user.id)
            .patch({
                forgotPasswordToken: null,
                forgotPasswordExpirationTime: null,
                password: bcrypt.hashSync(password, saltRounds),
            });
    }

    public async activateUser(email: string, activateToken: string): Promise<UserModel> {
        const user = await this.modelClass.query().findOne({ email, activateToken }).where(this.notDeleted);

        if (!user) {
            throw ResponseCode.DataNotFound;
        }

        if (differenceInMilliseconds(user.activateExpirationTime, new Date()) <= 0) {
            throw ResponseCode.ActivationCodeExpired;
        }

        await this.modelClass
            .query()
            .findById(user.id)
            .patch({ isActivated: true, activateToken: null, activateExpirationTime: null })
            .where(this.notDeleted);

        return user;
    }

    public async getVerifiedPhoneNumber(userId: number) {
        return this.modelClass.query().select('phone').findById(userId);
    }

    public async updateVerifiedPhone(userId: number, phone: string) {
        const isPhoneExists = await this.modelClass.query().select('id').findOne({
            phone,
        });

        if (isPhoneExists) {
            throw ResponseCode.PhoneNumberAlreadyUsed;
        }

        await this.modelClass
            .query()
            .findById(userId)
            .patch({
                phone,
                isPhoneVerified: true,
            })
            .where(this.notDeleted);
    }
}
