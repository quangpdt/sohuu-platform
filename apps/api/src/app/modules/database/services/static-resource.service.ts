import { Injectable } from '@nestjs/common';
import { readFileSync } from 'fs';
import { join } from 'path';

@Injectable()
export class StaticResourceService {
    public readonly cities;
    public readonly districts;
    public readonly wards;

    constructor() {
        const generalPath = join(process.cwd(), 'apps', 'api', 'src', 'assets');
        this.cities = JSON.parse(readFileSync(join(generalPath, 'cities.json'), { encoding: 'utf8' }));
        this.wards = JSON.parse(readFileSync(join(generalPath, 'wards.json'), { encoding: 'utf8' }));
        this.districts = JSON.parse(readFileSync(join(generalPath, 'districts.json'), { encoding: 'utf8' }));
    }
}
