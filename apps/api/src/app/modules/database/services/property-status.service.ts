import { Inject, Injectable } from '@nestjs/common';
import { BaseModelService } from './base-model.service';
import { ModelClass, Transaction } from 'objection';
import findLast from 'lodash/findLast';
import differenceBy from 'lodash/differenceBy';
import size from 'lodash/size';
import { asyncForEach } from '@sohuu-platform/utils';
import { PropertyStatusModel } from '@sohuu-platform/models';

@Injectable()
export class PropertyStatusService extends BaseModelService<PropertyStatusModel> {
    protected readonly defaultColumns = ['id', 'status', 'price', 'negotiable'];
    protected readonly tableName = 'property_statuses';

    constructor(@Inject('PropertyStatusModel') public readonly modelClass: ModelClass<PropertyStatusModel>) {
        super();
    }

    public async updatePropertyStatuses(
        transaction: Transaction,
        propertyId: number,
        propertyStatuses: PropertyStatusModel[]
    ) {
        const currentStatuses = await this.modelClass.query(transaction).select('id', 'status').where({
            propertyId,
        });

        const statusesToDelete = differenceBy(currentStatuses, propertyStatuses, (value) => value.status);

        if (size(statusesToDelete)) {
            await this.modelClass
                .query(transaction)
                .delete()
                .whereIn(
                    'id',
                    statusesToDelete.map((p) => p.id)
                )
                .andWhere({ propertyId });
        }

        propertyStatuses.forEach((status) => {
            status.propertyId = propertyId;
            const current = findLast(currentStatuses, (p) => p.status === status.status);
            if (current) {
                status.id = current.id;
            }
        });

        await asyncForEach(propertyStatuses, async (item) => {
            if (item.id) {
                await this.modelClass
                    .query(transaction)
                    .patch({
                        price: item.price,
                        negotiable: item.negotiable,
                    })
                    .where({
                        id: item.id,
                    });
            } else {
                await this.modelClass.query(transaction).insert(item);
            }
        });
    }
}
