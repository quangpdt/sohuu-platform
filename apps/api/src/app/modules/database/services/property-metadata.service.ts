import { Inject, Injectable } from '@nestjs/common';
import { BaseModelService } from './base-model.service';
import { PropertyMetadataModel } from '@sohuu-platform/models';
import { ModelClass, Transaction } from 'objection';
import { asyncForEach } from '@sohuu-platform/utils';

@Injectable()
export class PropertyMetadataService extends BaseModelService<PropertyMetadataModel> {
    protected readonly defaultColumns = ['id', 'name', 'value'];
    protected readonly tableName = 'property_metadata';

    constructor(@Inject('PropertyMetadataModel') public readonly modelClass: ModelClass<PropertyMetadataModel>) {
        super();
    }

    public async updatePropertyMetadata(
        transaction: Transaction,
        propertyId: number,
        metadata: PropertyMetadataModel[]
    ) {
        await asyncForEach(metadata, async (item) => {
            await this.modelClass.query(transaction).patch({ value: item.value }).where({
                propertyId,
                name: item.name,
            });
        });
    }
}
