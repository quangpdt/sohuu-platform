import { Model, ModelClass } from 'objection';

export class BaseModelService<T extends Model> {
    protected modelClass: ModelClass<T>;
    protected tableName: string;
    protected readonly defaultColumns = [];

    public findById(id: number, columns?: string[]) {
        return this.modelClass
            .query()
            .select(...(columns || this.columns()))
            .findById(id)
            .where(this.notDeleted);
    }

    public hardDeleteById(id: number) {
        return this.modelClass.query().findById(id).delete();
    }

    public softDeleteById(id: number) {
        return this.modelClass
            .query()
            .findById(id)
            .patch({
                deleted: true,
                deletedAt: new Date(),
            } as any);
    }

    public columns = () => this.defaultColumns.map((p) => `${this.tableName}.${p}`);
    public notDeleted = (q: any, tableName?: string) =>
        q
            .where(`${tableName || this.tableName}.deleted`, false)
            .orWhere(`${tableName || this.tableName}.deleted`, null);
    public deleted = (q: any) => q.where(`${this.tableName}.deleted`, true);
}
