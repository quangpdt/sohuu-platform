import { Inject, Injectable } from '@nestjs/common';
import { BaseModelService } from './base-model.service';
import { AnyQueryBuilder, ModelClass, raw } from 'objection';
import { PropertyStatusService } from './property-status.service';
import { PropertyMetadataService } from './property-metadata.service';
import * as slugify from '@sindresorhus/slugify';
import { readFileSync, renameSync } from 'fs';
import { join } from 'path';
import { cwd } from 'process';
import * as findLast from 'lodash/findLast';
import { FileModel, PropertyMetadataModel, PropertyModel, PropertyStatusModel } from '@sohuu-platform/models';
import { PropertySearchInput, SubmitPropertyInput } from '@sohuu-platform/input-types';
import {
    PropertyDirection,
    PropertyMetadataName,
    PropertyStatus,
    ResponseCode,
    SubmitStatus,
} from '@sohuu-platform/enums';
import { asyncForEach } from '@sohuu-platform/utils';

@Injectable()
export class PropertyService extends BaseModelService<PropertyModel> {
    protected readonly defaultColumns = [
        'id',
        'slug',
        'title',
        'description',
        'propertyType',
        'cityId',
        'districtId',
        'wardId',
        'location',
        'submitStatus',
        'views',
        'publishedAt',
        'expiredAt',
        'fullLocation',
        'lat',
        'lng',
    ];
    protected readonly tableName = 'properties';

    constructor(
        @Inject('PropertyModel') public readonly modelClass: ModelClass<PropertyModel>,
        private readonly propertyStatusService: PropertyStatusService,
        private readonly propertyMetadataService: PropertyMetadataService
    ) {
        super();
    }

    public async createProperty(ownerId: number, submitPropertyInput: SubmitPropertyInput): Promise<PropertyModel> {
        const transaction = await this.modelClass.startTransaction();

        try {
            const model: Partial<PropertyModel> = this.mapInputToModel(ownerId, submitPropertyInput);
            const statuses: PropertyStatusModel[] = this.mapInputToStatuses(submitPropertyInput);
            const metadata: PropertyMetadataModel[] = this.mapInputToMetadata(submitPropertyInput);

            const newFiles = model.files.filter((p) => !p.id);
            if (newFiles && newFiles.length > 0) {
                newFiles.forEach((item) => this.moveFileFromTemp(item.fileName));
            }

            model.statuses = statuses;
            model.metadata = metadata;

            const result = await this.modelClass.query().insertGraph(model);
            await transaction.commit();
            return result;
        } catch (err) {
            await transaction.rollback();
            throw err;
        }
    }

    public async updateProperty(ownerId: number, submitPropertyInput: SubmitPropertyInput) {
        const transaction = await this.modelClass.startTransaction();

        try {
            const model: Partial<PropertyModel> = this.mapInputToModel(ownerId, submitPropertyInput);
            const statuses: PropertyStatusModel[] = this.mapInputToStatuses(submitPropertyInput);
            const metadata: PropertyMetadataModel[] = this.mapInputToMetadata(submitPropertyInput);

            const newFiles = model.files.filter((p) => !p.id);
            if (newFiles && newFiles.length > 0) {
                newFiles.forEach((item) => this.moveFileFromTemp(item.fileName));
            }

            const result = await this.modelClass.query(transaction).upsertGraph(model, {
                insertMissing: true,
            });
            await this.propertyMetadataService.updatePropertyMetadata(transaction, submitPropertyInput.id, metadata);
            await this.propertyStatusService.updatePropertyStatuses(transaction, submitPropertyInput.id, statuses);
            await transaction.commit();
            return result;
        } catch (err) {
            await transaction.rollback();
            throw err;
        }
    }

    public async reSubmitPropertyStatus(ownerId: number, propertyId: number) {
        const transaction = await this.modelClass.startTransaction();

        try {
            await this.modelClass
                .query(transaction)
                .patch({
                    submitStatus: SubmitStatus.Pending,
                })
                .where({ ownerId, id: propertyId });
            await transaction.commit();
        } catch (err) {
            await transaction.rollback();
            throw err;
        }
    }

    public async getPropertyById(ownerId: number, id: number) {
        return this.modelClass
            .query()
            .findOne({
                id,
                ownerId,
            })
            .withGraphFetched('[statuses, metadata, files]');
    }

    public async getPublicProperty(
        id: number,
        slug: string,
        requestedByVerifiedUser: boolean,
        requestedUserId?: number
    ) {
        let graphFetcher;
        if (requestedByVerifiedUser) {
            graphFetcher = '[statuses, metadata, files, owner(propertyOwnerWithContacts)]';
        } else {
            graphFetcher = '[statuses, metadata, files, owner(propertyOwner)]';
        }
        const property = await this.modelClass
            .query()
            .select(...this.columns(), 'ownerId')
            .findOne({
                id,
                slug,
            })
            .withGraphFetched(graphFetcher);

        if (!property) {
            return null;
        }

        if (property.ownerId === requestedUserId) {
            return property;
        } else if (property.submitStatus !== 3) {
            throw ResponseCode.DataNotFound;
        }
        return property;
    }

    public async getLatestProperties(pageSize: number) {
        return this.modelClass
            .query()
            .select(...this.columns())
            .distinct()
            .where({
                submitStatus: 3,
            })
            .withGraphFetched('[statuses, metadata, files, owner(propertyOwner)]')
            .groupBy('id')
            .limit(pageSize)
            .orderBy('publishedAt', 'DESC');
    }

    public async getPublicProperties(searchCriteria: PropertySearchInput) {
        const queryConditions: any = {
            submitStatus: 3,
        };
        const knex = this.modelClass.knex();

        const query: AnyQueryBuilder = this.modelClass
            .query()
            .select(...this.columns())
            .distinct()
            .joinRelated('[statuses]')
            .where(queryConditions)
            .withGraphFetched('[statuses, metadata, files, owner(propertyOwner)]')
            .groupBy('id')
            .page(searchCriteria.page, searchCriteria.pageSize);

        if (searchCriteria) {
            // Basic conditions belongs to `properties` table
            if (searchCriteria.city) {
                queryConditions.cityId = searchCriteria.city.id;
            }
            if (searchCriteria.district) {
                queryConditions.districtId = searchCriteria.district.id;
            }
            if (searchCriteria.ward) {
                queryConditions.wardId = searchCriteria.ward.id;
            }
            if (searchCriteria.propertyType) {
                queryConditions.propertyType = searchCriteria.propertyType;
            }

            if (searchCriteria.propertyStatus) {
                query.andWhere('statuses.status', searchCriteria.propertyStatus);
            }

            if (searchCriteria.priceFrom) {
                query.andWhere('statuses.price', '>=', searchCriteria.priceFrom);
            }

            if (searchCriteria.priceTo) {
                query.andWhere('statuses.price', '<=', searchCriteria.priceTo);
            }

            if (
                searchCriteria.bedrooms ||
                searchCriteria.bathrooms ||
                searchCriteria.areaFrom ||
                searchCriteria.areaTo
            ) {
                query.join('property_metadata as structure_metadata', (joinClause) => {
                    joinClause
                        .on('properties.id', 'structureMetadata.property_id')
                        .on('structure_metadata.name', knex.raw('?', PropertyMetadataName.Structure));

                    if (searchCriteria.bedrooms) {
                        joinClause.on(
                            knex.raw(
                                'JSON_EXTRACT(structure_metadata.value, "$.bedrooms") = ?',
                                searchCriteria.bedrooms
                            )
                        );
                    }

                    if (searchCriteria.bathrooms) {
                        joinClause.on(
                            knex.raw(
                                'JSON_EXTRACT(structure_metadata.value, "$.bathrooms") = ?',
                                searchCriteria.bathrooms
                            )
                        );
                    }

                    if (searchCriteria.areaFrom || searchCriteria.areaTo) {
                        if (searchCriteria.areaFrom) {
                            joinClause.on(
                                knex.raw(
                                    'JSON_EXTRACT(structure_metadata.value, "$.area") >= ?',
                                    searchCriteria.areaFrom
                                )
                            );
                        }
                        if (searchCriteria.areaTo) {
                            joinClause.on(
                                knex.raw('JSON_EXTRACT(structure_metadata.value, "$.area") <= ?', searchCriteria.areaTo)
                            );
                        }
                    }
                });
            }

            if (searchCriteria.features && searchCriteria.features.length > 0) {
                query.join('property_metadata as features_metadata', (joinClause) => {
                    joinClause
                        .on('properties.id', 'features_metadata.property_id')
                        .on('features_metadata.name', knex.raw('?', PropertyMetadataName.Features));

                    searchCriteria.features.forEach((featureCode) => {
                        joinClause.on(
                            knex.raw(
                                'JSON_CONTAINS(features_metadata.value, ?, "$.features")',
                                featureCode.toString(10)
                            )
                        );
                    });
                });
            }
        }

        return query;
    }

    public async getRelatedProperties(id: number, slug: string) {
        const knex = this.modelClass.knex();

        const property: Partial<PropertyModel> = await this.getPublicProperty(id, slug, false);

        if (!property) {
            return [];
        }

        const fengshuiMetadata = JSON.parse(
            findLast(property.metadata, (metadata) => metadata.name === PropertyMetadataName.Fengshui).value
        );
        const relatedDirectionIds = this.getRelatedDirection(fengshuiMetadata.mainDoorDirection);

        const structureMetadata = JSON.parse(
            findLast(property.metadata, (metadata) => metadata.name === PropertyMetadataName.Structure).value
        );

        let forRent = false;
        let forSale = false;
        let salePrice = null;
        let rentPrice = null;

        property.statuses.map((p) => {
            if (p.status === PropertyStatus.ForSale) {
                forSale = true;
                salePrice = p.price;
            } else if (p.status === PropertyStatus.ForRent) {
                forRent = true;
                rentPrice = p.price;
            }
        });

        const relatedProperties: any[] = await this.modelClass
            .query()
            .select('property_id')
            .from((builder) =>
                builder
                    .as('points')
                    .select(
                        'property_id',
                        knex.raw(`IF(district_id = ??, ??, ??) as location_point`, [property.districtId, 10, 8]),
                        knex.raw(
                            `
                                (
                                    CASE
                                        WHEN main_door_direction = ?? THEN 10
                                        WHEN main_door_direction = ?? THEN 8
                                        WHEN main_door_direction = ?? THEN 8
                                        ELSE 0
                                    END
                                ) as fengshui_point
                            `,
                            [fengshuiMetadata.mainDoorDirection, ...relatedDirectionIds]
                        ),
                        knex.raw(
                            `
                                (
                                    CASE
                                        WHEN bedroom_count = ?? THEN 10
                                        WHEN ABS(bedroom_count - ??) = 1 THEN 8
                                        ELSE 0
                                    END
                                ) as bedroom_point
                            `,
                            [structureMetadata.bedrooms, structureMetadata.bedrooms]
                        ),
                        ...(forSale
                            ? [
                                  knex.raw(
                                      `
                                    (
                                        CASE
                                            WHEN ABS(sale_price - ?? <= 50000000) THEN 10
                                            WHEN ABS(sale_price - ?? <= 100000000) THEN 9
                                            WHEN ABS(sale_price - ?? <= 200000000) THEN 8
                                            WHEN ABS(sale_price - ?? <= 300000000) THEN 7
                                            WHEN ABS(sale_price - ?? <= 400000000) THEN 6
                                            WHEN ABS(sale_price - ?? <= 500000000) THEN 5
                                            ELSE 0
                                        END
                                    ) as sale_point
                                    `,
                                      [salePrice, salePrice, salePrice, salePrice, salePrice, salePrice]
                                  ),
                              ]
                            : []),
                        ...(forRent
                            ? [
                                  knex.raw(
                                      `
                                        (
                                            CASE
                                            WHEN ABS(rent_price - ?? <= 500000) THEN 10
                                            WHEN ABS(rent_price - ?? <= 1000000) THEN 8
                                            WHEN ABS(rent_price - ?? <= 2000000) THEN 6
                                            ELSE 0
                                            END
                                        ) as rent_point
                                    `,
                                      [rentPrice, rentPrice, rentPrice]
                                  ),
                              ]
                            : [])
                    )
                    .from((childBuilder) => {
                        childBuilder
                            .as('raw_data')
                            .select(
                                knex.ref('properties.id').as('property_id'),
                                'properties.districtId',
                                knex.raw(
                                    'JSON_EXTRACT(fengshui_metadata.value, "$.mainDoorDirection") as main_door_direction'
                                ),
                                knex.raw('JSON_EXTRACT(structure_metadata.value, "$.bedrooms") as bedroom_count'),
                                ...(forSale ? [knex.ref('sale_status.price').as('sale_price')] : []),
                                ...(forRent ? [knex.ref('rent_status.price').as('rent_price')] : [])
                            )
                            .from('properties')
                            .join('property_metadata as structure_metadata', (joinClause) =>
                                joinClause
                                    .on('properties.id', 'structure_metadata.property_id')
                                    .on('structure_metadata.name', knex.raw('?', PropertyMetadataName.Structure))
                            )
                            .join('property_metadata as fengshui_metadata', (joinClause) =>
                                joinClause
                                    .on('properties.id', 'fengshuiMetadata.property_id')
                                    .on('fengshui_metadata.name', knex.raw('?', PropertyMetadataName.Fengshui))
                            )
                            .where('properties.id', '<>', property.id)
                            .where('properties.submitStatus', 3)
                            .where('publishedAt', '>', raw('(NOW() - INTERVAL 30 DAY)'))
                            .where({
                                cityId: property.cityId,
                            });

                        if (forSale) {
                            childBuilder.leftJoin('property_statuses as sale_status', (joinClause) =>
                                joinClause
                                    .on('properties.id', 'sale_status.property_id')
                                    .on('sale_status.status', knex.raw('?', '1'))
                            );
                        }

                        if (forRent) {
                            childBuilder.leftJoin('property_statuses as rent_status', (joinClause) =>
                                joinClause
                                    .on('properties.id', 'rent_status.property_id')
                                    .on('rent_status.status', knex.raw('?', '2'))
                            );
                        }

                        return childBuilder;
                    })
            )
            .orderBy(
                knex.raw(
                    `((location_point + fengshui_point + bedroom_point + ${
                        forSale ? salePrice : forRent ? rentPrice : 0
                    }) / ${forSale && forRent ? 5 : 4})`
                ),
                'DESC'
            )
            .limit(5);

        if (!relatedProperties || relatedProperties.length === 0) {
            return [];
        }

        const relatedPropertyIds = relatedProperties.map((p) => p.propertyId);

        return this.modelClass
            .query()
            .select(...this.columns())
            .withGraphFetched('[statuses, metadata, files, owner(propertyOwner)]')
            .whereIn('id', relatedPropertyIds);
    }

    public async getOwnedProperties(ownerId) {
        return this.modelClass
            .query()
            .select(...this.columns())
            .withGraphFetched('[files]')
            .where({
                ownerId,
            });
    }

    public async batchInsertProperties() {
        const rawData = JSON.parse(readFileSync(join(cwd(), 'utils/faker/properties.json'), { encoding: 'utf-8' }));
        await asyncForEach(rawData, async (item) => {
            await this.modelClass.query().insertGraph(item);
        });
        return {};
    }

    public async getPropertiesForNotifications(propertyIds: number[]) {
        return this.modelClass.query().select('id', 'title', 'slug').findByIds(propertyIds);
    }

    private mapInputToModel(ownerId: number, submitPropertyInput: SubmitPropertyInput): Partial<PropertyModel> {
        const { id } = submitPropertyInput;

        const { title, description, propertyType, files } = submitPropertyInput.basic;

        const { location, city, district, ward, lat, lng } = submitPropertyInput.address;
        const fullLocation = [location, ward.name, district.name, city.name].join(', ');

        let slug;
        if (!id) {
            slug = slugify(title);
        }

        return {
            id,
            slug,
            title,
            description,
            ownerId,
            propertyType,
            cityId: city.id,
            districtId: district.id,
            wardId: ward.id,
            fullLocation,
            location,
            lat,
            lng,
            submitStatus: SubmitStatus.Pending,
            files: files as FileModel[],
        };
    }

    private mapInputToStatuses(submitPropertyInput: SubmitPropertyInput): PropertyStatusModel[] {
        const { salePrice, rentPrice, saleNegotiable, rentNegotiable, propertyStatus } = submitPropertyInput.basic;

        const statuses: PropertyStatusModel[] = [];

        propertyStatus.forEach((status) => {
            let price, negotiable;
            if (status === PropertyStatus.ForSale) {
                price = salePrice;
                negotiable = saleNegotiable;
            } else {
                price = rentPrice;
                negotiable = rentNegotiable;
            }
            statuses.push({
                status,
                price,
                negotiable,
            } as PropertyStatusModel);
        });

        return statuses;
    }

    private mapInputToMetadata(submitPropertyInput: SubmitPropertyInput): PropertyMetadataModel[] {
        const {
            mainDoorDirection,
            balconyDirection,
            kitchenDirection,
            legal,
            bedrooms,
            bathrooms,
            floors,
            area,
            features,
            additionalFeatures,
        } = submitPropertyInput.additional;

        return [
            {
                name: PropertyMetadataName.Fengshui,
                value: JSON.stringify({
                    mainDoorDirection,
                    balconyDirection,
                    kitchenDirection,
                }),
            },
            {
                name: PropertyMetadataName.Structure,
                value: JSON.stringify({
                    bedrooms,
                    bathrooms,
                    floors,
                    area,
                }),
            },
            {
                name: PropertyMetadataName.Legal,
                value: legal.toString(10),
            },
            {
                name: PropertyMetadataName.Features,
                value: JSON.stringify({
                    features,
                    additionalFeatures,
                }),
            },
        ] as PropertyMetadataModel[];
    }

    private getRelatedDirection(direction: PropertyDirection): PropertyDirection[] {
        switch (direction) {
            case PropertyDirection.Unknown:
                return [PropertyDirection.EastNorth, PropertyDirection.EastSouth];
            case PropertyDirection.East:
                return [PropertyDirection.EastNorth, PropertyDirection.EastSouth];
            case PropertyDirection.West:
                return [PropertyDirection.WestNorth, PropertyDirection.WestSouth];
            case PropertyDirection.South:
                return [PropertyDirection.EastSouth, PropertyDirection.WestSouth];
            case PropertyDirection.North:
                return [PropertyDirection.EastNorth, PropertyDirection.WestNorth];
            case PropertyDirection.EastNorth:
                return [PropertyDirection.East, PropertyDirection.North];
            case PropertyDirection.WestNorth:
                return [PropertyDirection.West, PropertyDirection.North];
            case PropertyDirection.EastSouth:
                return [PropertyDirection.East, PropertyDirection.South];
            case PropertyDirection.WestSouth:
                return [PropertyDirection.West, PropertyDirection.South];
        }
    }

    private async moveFileFromTemp(fileName: string) {
        const staticDir = join(cwd(), 'static');
        renameSync(join(staticDir, 'temp', fileName), join(staticDir, 'images', fileName));
    }
}
