import { Inject, Injectable } from '@nestjs/common';
import { BaseModelService } from './base-model.service';
import { UserMetadataModel } from '@sohuu-platform/models';
import { ModelClass } from 'objection';
import { UpdateUserAddressInput } from '@sohuu-platform/input-types';

@Injectable()
export class UserMetadataService extends BaseModelService<UserMetadataModel> {
    protected readonly defaultColumns = ['id', 'name', 'value'];
    protected readonly tableName = 'user_metadata';

    constructor(@Inject('UserMetadataModel') public readonly modelClass: ModelClass<UserMetadataModel>) {
        super();
    }

    public async updateUserAddress(userId: number, address: UpdateUserAddressInput) {
        const currentAddress = await this.modelClass.query().select('id').findOne({
            userId,
            name: 'address',
        });

        const addressObj = {
            location: address.location,
            cityId: address.city.id,
            districtId: address.district.id,
            wardId: address.ward.id,
        };

        if (!currentAddress) {
            await this.modelClass.query().insert({
                userId,
                name: 'address',
                value: JSON.stringify(addressObj),
            });
            return;
        }

        await this.modelClass
            .query()
            .patch({
                value: JSON.stringify(addressObj),
            })
            .where({
                userId,
                name: 'address',
            });
    }
}
