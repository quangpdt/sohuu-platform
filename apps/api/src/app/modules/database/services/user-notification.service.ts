import { BaseModelService } from './base-model.service';
import { PropertyModel, UserNotificationModel } from '@sohuu-platform/models';
import { Inject } from '@nestjs/common';
import { ModelClass } from 'objection';
import { NotificationStatus, NotificationType, ResponseCode } from '@sohuu-platform/enums';
import { classToPlain } from 'class-transformer';
import { PropertyService } from './property.service';
import { UserInputError } from 'apollo-server';
import { RequestCallbackInput } from '@sohuu-platform/input-types';

export class UserNotificationService extends BaseModelService<UserNotificationModel> {
    protected readonly defaultColumns = ['id', 'status', 'type', 'message'];

    protected readonly tableName = 'user_notifications';

    constructor(
        @Inject('UserNotificationModel')
        public readonly modelClass: ModelClass<UserNotificationModel>,
        private readonly propertyService: PropertyService
    ) {
        super();
    }

    public async createPropertyCallbackNotification(input: RequestCallbackInput, userId: number) {
        const message = JSON.stringify(classToPlain(input));

        const property: Partial<PropertyModel> = await this.propertyService.findById(input.propertyId, ['ownerId']);

        if (!property || !property.ownerId) {
            throw new UserInputError(ResponseCode.DataNotFound);
        }

        return this.modelClass.query().insert({
            requestedBy: userId,
            userId: property.ownerId,
            status: NotificationStatus.Unread,
            type: NotificationType.RequestCallback,
            message,
        });
    }

    public async getSummaryNotifications(userId: number) {
        return this.modelClass
            .query()
            .select(...this.columns())
            .withGraphFetched('[user(propertyOwner)]')
            .where({
                userId,
            })
            .limit(5)
            .offset(0)
            .orderBy('createdAt', 'desc');
    }

    public async getNotifications(userId: number, page: number, pageSize: number) {
        return this.modelClass
            .query()
            .select(...this.columns(), 'createdAt')
            .withGraphFetched('[user(propertyOwner)]')
            .where({
                userId,
            })
            .orderBy('createdAt', 'desc')
            .page(page, pageSize);
    }

    public async markAsRead(userId: number, id: number) {
        return this.modelClass
            .query()
            .patch({
                status: NotificationStatus.Read,
            })
            .where({
                userId,
                id,
            });
    }

    public async markAllAsRead(userId: number) {
        return this.modelClass
            .query()
            .patch({
                status: NotificationStatus.Read,
            })
            .where({
                userId,
            });
    }

    public async delete(userId: number, id: number) {
        return this.modelClass
            .query()
            .where({
                userId,
                id,
            })
            .delete();
    }

    public async deleteAll(userId) {
        return this.modelClass
            .query()
            .where({
                userId,
            })
            .delete();
    }
}
