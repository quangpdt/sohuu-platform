import { Inject, Injectable } from '@nestjs/common';
import { ModelClass } from 'objection';
import { BaseModelService } from './base-model.service';
import { UserPhoneSessionModel } from '@sohuu-platform/models';

@Injectable()
export class UserPhoneSessionService extends BaseModelService<UserPhoneSessionModel> {
    protected readonly defaultColumns = ['id', 'userId', 'phoneSession'];
    protected readonly tableName = 'user_phone_session';

    constructor(@Inject('UserPhoneSessionModel') public readonly modelClass: ModelClass<UserPhoneSessionModel>) {
        super();
    }

    public async savePhoneSession(userId: number, phone: string, phoneSession: string) {
        await this.removePhoneSession(userId);
        return this.modelClass.query().insert({
            userId,
            phoneSession,
            phone,
        });
    }

    public async getPhoneSession(userId: number) {
        return this.modelClass.query().select(['phone', 'phoneSession']).findOne({ userId });
    }

    public async removePhoneSession(userId: number) {
        return this.modelClass.query().findOne({ userId }).delete();
    }
}
