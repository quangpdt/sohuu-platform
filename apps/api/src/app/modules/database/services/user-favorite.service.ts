import { Inject, Injectable } from '@nestjs/common';
import { BaseModelService } from 'apps/api/src/app/modules/database/services/base-model.service';
import { UserFavoriteModel } from '@sohuu-platform/models';
import { ModelClass } from 'objection';

@Injectable()
export class UserFavoriteService extends BaseModelService<UserFavoriteModel> {
    protected readonly defaultColumns = ['id', 'userId', 'propertyId'];
    protected readonly tableName = 'user_favorites';

    constructor(@Inject('UserFavoriteModel') public readonly modelClass: ModelClass<UserFavoriteModel>) {
        super();
    }
}
