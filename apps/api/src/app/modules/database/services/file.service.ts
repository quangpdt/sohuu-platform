import { Inject, Injectable } from '@nestjs/common';
import { BaseModelService } from './base-model.service';
import { FileModel } from '@sohuu-platform/models';
import { ModelClass } from 'objection';

@Injectable()
export class FileService extends BaseModelService<FileModel> {
    protected readonly defaultColumns = ['id', 'file_name', 'file_size', 'file_type'];
    protected readonly tableName = 'files';

    constructor(@Inject('fileModel') public readonly modelClass: ModelClass<FileModel>) {
        super();
    }
}
