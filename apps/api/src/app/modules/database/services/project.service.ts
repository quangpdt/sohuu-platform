import { BaseModelService } from './base-model.service';
import { ProjectModel } from '@sohuu-platform/models';
import { Inject } from '@nestjs/common';
import { ModelClass } from 'objection';
import { ProjectSortOption, PropertyMetadataName } from '@sohuu-platform/enums';
import { ProjectMetadataName } from 'libs/data-types/src/enum/project-metadata-name.enum';

export class ProjectService extends BaseModelService<ProjectModel> {
    protected readonly defaultColumns = ['id', 'title', 'location', 'slug'];

    protected readonly tableName = 'projects';

    constructor(
        @Inject('ProjectModel')
        public readonly modelClass: ModelClass<ProjectModel>
    ) {
        super();
    }

    public async findAll(): Promise<ProjectModel[]> {
        return this.modelClass.query().select(this.columns());
    }

    public async findByIdAndSlug(id: number, slug: string): Promise<ProjectModel> {
        return this.modelClass
            .query()
            .select(...this.columns(), 'description')
            .findOne({ id, slug })
            .withGraphFetched('[files, metadata(selectNameAndValue)]');
    }

    public async getLatestProjects(limit: number, sortOption: ProjectSortOption) {
        const knex = this.modelClass.knex();
        const query = this.modelClass
            .query()
            .select(...this.columns())
            .withGraphFetched('[files]')
            .limit(limit);
        console.log(sortOption);
        switch (sortOption) {
            case ProjectSortOption.RecentlyAdded:
                query.orderBy('updatedAt', 'DESC');
                break;
            case ProjectSortOption.HandOverTime:
                query
                    .select(...this.columns())
                    .join('project_metadata as metadata', (joinClause) => {
                        joinClause
                            .on('projects.id', 'metadata.projectId')
                            .on('metadata.name', knex.raw('?', ProjectMetadataName.HandOverTime));
                    })
                    .orderBy('metadata.value', 'DESC');
                break;
            case ProjectSortOption.ConstructionTime:
                query
                    .select(...this.columns())
                    .join('project_metadata as metadata', (joinClause) => {
                        joinClause
                            .on('projects.id', 'metadata.projectId')
                            .on('metadata.name', knex.raw('?', ProjectMetadataName.ConstructionTime));
                    })
                    .orderBy('metadata.value', 'DESC');
                break;
            case ProjectSortOption.PriceAscending:
                query
                    .select(...this.columns())
                    .join('project_metadata as metadata', (joinClause) => {
                        joinClause
                            .on('projects.id', 'metadata.projectId')
                            .on('metadata.name', knex.raw('?', ProjectMetadataName.PriceFrom));
                    })
                    .orderBy('metadata.value', 'ASC');
                break;
            case ProjectSortOption.PriceDescending:
                query
                    .select(...this.columns())
                    .join('project_metadata as metadata', (joinClause) => {
                        joinClause
                            .on('projects.id', 'metadata.projectId')
                            .on('metadata.name', knex.raw('?', ProjectMetadataName.PriceTo));
                    })
                    .orderBy('metadata.value', 'DESC');
                break;
            default:
                query.orderBy('updatedAt', 'DESC');
                break;
        }
        return query;
    }
}
