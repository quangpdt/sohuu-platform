import { Args, ID, Int, Query, Resolver } from '@nestjs/graphql';
import { ProjectService } from '../database/services/project.service';
import { ProjectModel, UserModel } from '@sohuu-platform/models';
import { FindByIdAndSlugInput } from '@sohuu-platform/input-types';

@Resolver('project')
export class ProjectResolver {
    constructor(private projectService: ProjectService) {}

    @Query(() => ProjectModel)
    public async getPublicProject(@Args('findByIdAndSlugInput') { id, slug }: FindByIdAndSlugInput) {
        return this.projectService.findByIdAndSlug(id, slug);
    }

    @Query(() => [ProjectModel])
    public async getHomePageProjects(@Args({ name: 'sortOption', type: () => Int }) sortOption: number) {
        return this.projectService.getLatestProjects(6, sortOption);
    }
}
