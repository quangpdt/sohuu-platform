import { Module } from '@nestjs/common';
import { ProjectResolver } from './project.resolver';
import { LoggerModule } from '../logger/logger.module';

@Module({
    imports: [LoggerModule],
    providers: [ProjectResolver],
})
export class ProjectModule {}
