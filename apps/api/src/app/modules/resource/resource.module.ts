import { BadRequestException, Module } from '@nestjs/common';
import { ResourceController } from './resource.controller';
import { MulterModule } from '@nestjs/platform-express';
import * as path from 'path';
import { extname } from 'path';
import * as multer from 'multer';
import { generateUUID } from '@sohuu-platform/utils';
import { DatabaseModule } from '../database/database.module';
import { StaticResourceResolver } from './static-resource.resolver';
import { LoggerModule } from '../logger/logger.module';

@Module({
    imports: [
        LoggerModule,
        MulterModule.register({
            storage: multer.diskStorage({
                destination: path.join(process.cwd(), 'static/temp'),
                filename: (request, file, callback) => {
                    callback(null, `${generateUUID()}${extname(file.originalname)}`);
                },
            }),
            fileFilter: (request, file, callback) => {
                if (file.mimetype.match(/\/(jpg|jpeg|png)$/)) {
                    callback(null, true);
                } else {
                    callback(new BadRequestException(), false);
                }
            },
            limits: {
                files: 8,
            },
        }),
        DatabaseModule,
    ],
    providers: [StaticResourceResolver],
    controllers: [ResourceController],
})
export class ResourceModule {}
