import { Controller, Post, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { UploadedFile } from '@sohuu-platform/interfaces';

@Controller('resource')
export class ResourceController {
    @Post('upload')
    @UseInterceptors(FilesInterceptor('files'))
    public uploadFile(@UploadedFiles() files) {
        return files.reduce((acc, curr) => {
            acc.push({
                originalName: curr.originalname,
                name: curr.filename,
                size: curr.size,
            });
            return acc;
        }, []) as UploadedFile[];
    }
}
