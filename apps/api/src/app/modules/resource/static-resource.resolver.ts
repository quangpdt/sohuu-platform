import { Query, Resolver } from '@nestjs/graphql';
import { ResponseMessage } from '@sohuu-platform/models';
import { StaticResourceService } from '../database/services/static-resource.service';

@Resolver('static')
export class StaticResourceResolver {
    constructor(private staticResourceService: StaticResourceService) {}

    @Query(() => ResponseMessage)
    public getCities() {
        return {
            result: this.staticResourceService.cities,
        };
    }

    @Query(() => ResponseMessage)
    public getDistricts() {
        return {
            result: this.staticResourceService.districts,
        };
    }

    @Query(() => ResponseMessage)
    public getWards() {
        return {
            result: this.staticResourceService.wards,
        };
    }
}
