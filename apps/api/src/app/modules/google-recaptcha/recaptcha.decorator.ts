import { UseGuards } from '@nestjs/common';
import { ReCaptchaGuard } from './recaptcha.guard';

export function ReCaptcha(): MethodDecorator {
    return UseGuards(ReCaptchaGuard);
}
