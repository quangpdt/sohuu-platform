import { DynamicModule, Global, HttpModule, HttpService, Module, Provider } from '@nestjs/common';
import { ReCaptchaValidator } from './service/recaptcha.validator';
import { ReCaptchaModuleOptions } from './recaptcha.interfaces';
import { ReCaptchaGuard } from './recaptcha.guard';

@Global()
@Module({})
export class ReCaptchaModule {
    public static forRoot(options: ReCaptchaModuleOptions): DynamicModule {
        const providers: Provider[] = [
            {
                provide: ReCaptchaValidator,
                useFactory: (http: HttpService) => new ReCaptchaValidator(http, options),
                inject: [HttpService],
            },
            {
                provide: ReCaptchaGuard,
                useFactory: (validator: ReCaptchaValidator) => new ReCaptchaGuard(validator),
                inject: [ReCaptchaValidator],
            },
        ];

        return {
            module: ReCaptchaModule,
            imports: [HttpModule],
            providers,
            exports: providers,
        };
    }
}
