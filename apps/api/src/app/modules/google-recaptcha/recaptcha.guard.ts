import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { ReCaptchaValidator } from './service/recaptcha.validator';
import { GqlExecutionContext } from '@nestjs/graphql';

@Injectable()
export class ReCaptchaGuard implements CanActivate {
    constructor(private readonly validator: ReCaptchaValidator) {}

    public getRequest(context: ExecutionContext) {
        const ctx = GqlExecutionContext.create(context);
        return ctx.getContext().req;
    }

    public async canActivate(context: ExecutionContext): Promise<boolean> {
        const ctx = GqlExecutionContext.create(context);
        console.log();
        const input = ctx.getArgs()[this.getInput(ctx.getInfo().fieldName)];
        const token = input.token;

        return await this.validator.validate(token);
    }

    private getInput(fieldName: string) {
        switch (fieldName) {
            case 'login':
                return 'loginInput';
        }
    }
}
