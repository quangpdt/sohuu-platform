export interface ReCaptchaGuardOptions {
    response: (req) => string | Promise<string>;
    skipIf?: (req) => boolean | Promise<boolean>;
}

export interface ReCaptchaValidatorOptions {
    secretKey: string;
    onError?: (errorCodes: string) => never;
}

export interface ReCaptchaModuleOptions extends ReCaptchaValidatorOptions, ReCaptchaGuardOptions {}
