import { HttpService, Injectable } from '@nestjs/common';
import { ReCaptchaValidatorOptions } from '../recaptcha.interfaces';

@Injectable()
export class ReCaptchaValidator {
    private readonly apiUrl = 'https://www.google.com/recaptcha/api/siteverify';
    private readonly headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

    constructor(private readonly http: HttpService, private readonly options: ReCaptchaValidatorOptions) {}

    public validate(token: string): Promise<boolean> {
        const params = new URLSearchParams();
        params.append('secret', this.options.secretKey);
        params.append('response', token);

        return this.http
            .post(this.apiUrl, params, { headers: this.headers })
            .toPromise()
            .then((res) => {
                return res.data.success;
            })
            .catch((e) => {
                console.log(e);
                return false;
            });
    }
}
