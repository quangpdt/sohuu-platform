import { Injectable } from '@nestjs/common';
import * as process from 'process';
import * as path from 'path';
import * as nodemailer from 'nodemailer';
import * as EmailTemplates from 'email-templates';
import { environment } from '../../../../environment';
import { EmailType } from '@sohuu-platform/enums';

@Injectable()
export class EmailService {
    public email: EmailTemplates;

    constructor() {
        const emailConfig = environment.email;
        const transporter = nodemailer.createTransport(emailConfig as any);
        this.email = new EmailTemplates({
            message: {
                from: emailConfig.address,
            },
            juice: true,
            juiceResources: {
                preserveImportant: true,
                webResources: {
                    relativeTo: path.join(process.cwd(), 'assets/email-assets'),
                    images: true,
                },
            },
            transport: transporter,
            views: {
                root: path.join(process.cwd(), 'assets/email-templates'),
            },
            preview: false,
            send: true,
        });
        transporter.verify((error) => {
            if (error) {
                console.log(error);
            } else {
                console.log('Server is ready to take our messages');
            }
        });
    }

    public async sendMail(toAddress: string, emailType: EmailType, payload: any): Promise<void> {
        await this.email.send({
            message: {
                to: toAddress,
            },
            template: emailType,
            locals: payload,
        });
    }
}
