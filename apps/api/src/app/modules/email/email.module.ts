import { Global, Module } from '@nestjs/common';
import { EmailService } from './services/email.service';
import { LoggerModule } from '../logger/logger.module';

@Global()
@Module({
    imports: [LoggerModule],
    providers: [EmailService],
    exports: [EmailService],
})
export class EmailModule {}
