import { Module } from '@nestjs/common';
import { NotificationResolver } from './notification.resolver';
import { AuthenticationModule } from '../authentication/authentication.module';
import { LoggerModule } from '../logger/logger.module';

@Module({
    imports: [AuthenticationModule, LoggerModule],
    providers: [NotificationResolver],
})
export class NotificationModule {}
