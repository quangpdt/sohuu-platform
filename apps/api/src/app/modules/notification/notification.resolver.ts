import { Args, ID, Mutation, Query, Resolver } from '@nestjs/graphql';
import { ResponseMessage, UserModel, UserNotificationModel } from '@sohuu-platform/models';
import { CurrentUser } from '../../decorators/current-user.decorator';
import { UserNotificationService } from '../database/services/user-notification.service';
import { UseGuards } from '@nestjs/common';
import { PublicStrategy } from '../authentication/services/public.strategy';
import { JwtStrategy } from '../authentication/services/jwt.strategy';
import { NotificationType } from '@sohuu-platform/enums';
import * as uniq from 'lodash/uniq';
import * as findLast from 'lodash/findLast';
import { PropertyService } from '../database/services/property.service';
import { GetNotificationsInput, RequestCallbackInput } from '@sohuu-platform/input-types';

@Resolver()
export class NotificationResolver {
    constructor(
        private readonly userNotificationService: UserNotificationService,
        private propertyService: PropertyService
    ) {}

    @Mutation(() => ResponseMessage)
    @UseGuards(PublicStrategy)
    public async requestPropertyCallback(
        @Args('requestCallbackInput') input: RequestCallbackInput,
        @CurrentUser() user: UserModel
    ) {
        await this.userNotificationService.createPropertyCallbackNotification(input, user?.id);

        return {};
    }

    @Query(() => [UserNotificationModel])
    @UseGuards(JwtStrategy)
    public async getSummaryNotifications(@CurrentUser() user: UserModel) {
        const result = await this.userNotificationService.getSummaryNotifications(user.id);
        result.map((item) => (item.parsedMessage = JSON.parse(item.message)));

        return result;
    }

    @Query(() => ResponseMessage)
    @UseGuards(JwtStrategy)
    public async getNotifications(
        @CurrentUser() user: UserModel,
        @Args('getNotificationsInput') input: GetNotificationsInput
    ) {
        const result = await this.userNotificationService.getNotifications(user.id, input.page, input.pageSize);
        let propertyIds = [];

        result.results.map((item: UserNotificationModel) => {
            item.parsedMessage = JSON.parse(item.message);

            switch (item.type) {
                case NotificationType.RequestCallback:
                    propertyIds.push(item.parsedMessage.propertyId);
                    break;
            }
        });

        propertyIds = uniq(propertyIds);

        const properties = await this.propertyService.getPropertiesForNotifications(propertyIds);

        result.results.map((item: UserNotificationModel) => {
            switch (item.type) {
                case NotificationType.RequestCallback:
                    const property = findLast(properties, (p) => p.id === parseInt(item.parsedMessage.propertyId, 10));
                    item.parsedMessage.property = property;
                    break;
            }
        });

        return {
            data: result,
        };
    }

    @Mutation(() => ResponseMessage)
    @UseGuards(JwtStrategy)
    public async markNotificationAsRead(
        @CurrentUser() user: UserModel,
        @Args({ name: 'id', type: () => ID }) id: number
    ) {
        await this.userNotificationService.markAsRead(user.id, id);
        return {};
    }

    @Mutation(() => ResponseMessage)
    @UseGuards(JwtStrategy)
    public async markAllNotificationsAsRead(@CurrentUser() user: UserModel) {
        await this.userNotificationService.markAllAsRead(user.id);
        return {};
    }

    @Mutation(() => ResponseMessage)
    @UseGuards(JwtStrategy)
    public async deleteNotification(@CurrentUser() user: UserModel, @Args({ name: 'id', type: () => ID }) id: number) {
        await this.userNotificationService.delete(user.id, id);
        return {};
    }

    @Mutation(() => ResponseMessage)
    @UseGuards(JwtStrategy)
    public async deleteAllNotifications(@CurrentUser() user: UserModel) {
        await this.userNotificationService.deleteAll(user.id);
        return {};
    }
}
