import { join } from 'path';
import process, { cwd } from 'process';
import { knexSnakeCaseMappers } from 'objection';
import * as Knex from 'knex';

require('dotenv').config({ path: join(cwd(), 'apps', 'api', '.env') });

export const environment = {
    isProduction: process.env.ENVIRONMENT === 'PROD',
    email: {
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASS,
        },
        address: process.env.EMAIL_ADDRESS,
    },
    jwt: {
        secretKey: process.env.JWT_SECRET_KEY,
        refreshTokenSecretKey: process.env.JWT_REFRESH_TOKEN_SECRET_KEY,
    },
    oauth: {
        fbAppId: process.env.FB_APP_ID,
        fbClientSecret: process.env.FB_APP_SECRET,
        googleAppId: process.env.GOOGLE_APP_ID,
        googleAppClientSecret: process.env.GOOGLE_APP_SECRET,
    },
    knexConfig: {
        client: process.env.DB_CLIENT,
        dialect: process.env.DB_DIALECT,
        connection: {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USER,
            password: process.env.DB_PASS,
            database: process.env.DB_DATABASE,
        },
        ...knexSnakeCaseMappers(),
    } as Knex.Config,
    baseUrl: {
        client: process.env.BASE_URL_CLIENT,
        server: process.env.BASE_URL_SERVER,
    },
    reCaptchaSecretKey: process.env.RECAPTCHA_SECRET_KEY,
    googleIdentityApiKey: process.env.GOOGLE_API_KEY,
};
