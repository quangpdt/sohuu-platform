export const environment = {
    production: true,
    baseUrl: {
        base: 'https://stage.sohuu.vn',
        graphql: 'https://stage.sohuu.vn/graphql',
        api: 'https://stage.sohuu.vn/api',
    },
    cookieDomain: 'stage.sohuu.vn',
    cookieSecure: false,
    clientPort: null,
    reCaptchaKey: '6LctyOkUAAAAAPnDvDXTXWLh50dieLYpEsV3Jrm5',
    googleMapsKey: 'AIzaSyCXW9dH6nNlj2rdev3iveCHZoGQc8eWRKI',
    firebase: {
        apiKey: 'AIzaSyDZjPUBakUPr3Egr6fNk2dX50GfrDSMpt8',
        authDomain: 'so-huu-platforms.firebaseapp.com',
        databaseURL: 'https://so-huu-platforms.firebaseio.com',
        projectId: 'so-huu-platforms',
        storageBucket: 'so-huu-platforms.appspot.com',
        messagingSenderId: '953024265132',
        appId: '1:953024265132:web:942bd3966e77c4490ad809',
        measurementId: 'G-PCTK0939BF',
    },
    stateKey: {
        translate: 'translate',
        graphql: 'graphql',
    },
};
