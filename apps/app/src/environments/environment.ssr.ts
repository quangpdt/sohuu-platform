export const environment = {
    production: false,
    baseUrl: {
        base: 'http://localhost:4200',
        graphql: 'http://localhost:4200/graphql',
        api: 'http://localhost:4200/api',
    },
    cookieDomain: 'localhost',
    cookieSecure: false,
    clientPort: 4200,
    reCaptchaKey: '6LctyOkUAAAAAPnDvDXTXWLh50dieLYpEsV3Jrm5',
    googleMapsKey: 'AIzaSyCXW9dH6nNlj2rdev3iveCHZoGQc8eWRKI',
    firebase: {
        apiKey: 'AIzaSyDZjPUBakUPr3Egr6fNk2dX50GfrDSMpt8',
        authDomain: 'so-huu-platforms.firebaseapp.com',
        databaseURL: 'https://so-huu-platforms.firebaseio.com',
        projectId: 'so-huu-platforms',
        storageBucket: 'so-huu-platforms.appspot.com',
        messagingSenderId: '953024265132',
        appId: '1:953024265132:web:942bd3966e77c4490ad809',
        measurementId: 'G-PCTK0939BF',
    },
    stateKey: {
        translate: 'translate',
        graphql: 'graphql',
    },
};
