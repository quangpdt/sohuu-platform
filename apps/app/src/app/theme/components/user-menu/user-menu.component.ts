import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AppState } from '../../../core/states/app/app.state';
import { BaseComponent } from '../../../pages/base.component';

@Component({
    selector: 'app-user-menu',
    templateUrl: './user-menu.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserMenuComponent extends BaseComponent {
    @Select(AppState.getUser)
    public user$: Observable<any>;

    constructor() {
        super();
    }
}
