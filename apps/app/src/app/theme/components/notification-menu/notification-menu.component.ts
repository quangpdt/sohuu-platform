import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseComponent } from '../../../pages/base.component';
import { Select } from '@ngxs/store';
import { AppState } from '../../../core/states/app/app.state';
import { Observable, of } from 'rxjs';
import { UserModel, UserNotificationModel } from '@sohuu-platform/models';
import { TranslateService } from '@ngx-translate/core';
import { AppDataPipe } from '../../../shared/pipes/app-data.pipe';
import { AppDataType, NotificationStatus, NotificationType } from '@sohuu-platform/enums';

@Component({
    selector: 'app-notification-menu',
    templateUrl: './notification-menu.component.html',
    styleUrls: ['./notification-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationMenuComponent extends BaseComponent {
    @Select(AppState.getNotifications)
    public notifications$: Observable<Partial<UserNotificationModel>[]>;

    @Select(AppState.getUser)
    public user$: Observable<Partial<UserModel>>;

    public notificationStatus = NotificationStatus;

    constructor(private translateService: TranslateService) {
        super();
    }

    public countUnreadNotification(notifications: Partial<UserNotificationModel>[]) {
        if (!notifications) {
            return 0;
        }

        return notifications.filter((p) => p.status === NotificationStatus.Unread).length;
    }

    public getMessageContent(notification: Partial<UserNotificationModel>): Observable<any> {
        const translateStr = new AppDataPipe().transform(notification.type, AppDataType.NotificationType);
        switch (notification.type) {
            case NotificationType.RequestCallback:
                return this.translateService.get(translateStr, { name: notification.parsedMessage.name });
        }
        return of('');
    }
}
