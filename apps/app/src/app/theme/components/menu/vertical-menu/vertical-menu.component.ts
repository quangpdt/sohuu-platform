import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { MenuService } from '../menu.service';
import { Menu } from '../menu.model';

@Component({
    selector: 'app-vertical-menu',
    templateUrl: './vertical-menu.component.html',
    styleUrls: ['./vertical-menu.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [MenuService],
})
export class VerticalMenuComponent implements OnInit {
    @Input('menuParentId') public menuParentId;
    public menuItems: Array<Menu>;
    constructor(public menuService: MenuService) {}

    public ngOnInit() {
        this.menuItems = this.menuService.getVerticalMenuItems();
        this.menuItems = this.menuItems.filter((item) => item.parentId === this.menuParentId);
    }

    public onClick(menuId) {
        this.menuService.toggleMenuItem(menuId);
        this.menuService.closeOtherSubMenus(this.menuService.getVerticalMenuItems(), menuId);
    }
}
