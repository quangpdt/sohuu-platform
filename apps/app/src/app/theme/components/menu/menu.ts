import { Menu } from './menu.model';
import { routePaths } from '../../../core/route.names';

export const horizontalMenuItems = [
    new Menu(10, 'home', '/'),

    new Menu(20, 'buy', routePaths.buyProperties, null, null, true),
    new Menu(21, 'buyHouses', routePaths.buyHouses, null, null, false, 20),
    new Menu(22, 'buyApartments', routePaths.buyApartments, null, null, false, 20),

    new Menu(30, 'rent', routePaths.rentProperties, null, null, true),
    new Menu(31, 'rentHouses', routePaths.rentHouses, null, null, false, 30),
    new Menu(32, 'rentApartments', routePaths.rentApartments, null, null, false, 30),

    new Menu(40, 'project', '/project'),
    new Menu(50, 'news', null, null, null, true, 0),
    // TODO: This category should be loaded from API
    new Menu(51, 'marketNews', '/tin-tuc/thi-truong', null, null, null, 50),
    new Menu(51, 'analysisNews', '/tin-tuc/phan-tich-nhan-dinh', null, null, null, 50),
    new Menu(60, 'info', null, null, null, true, 0),
    new Menu(61, 'contact', '/lien-he', null, null, false, 60),
    new Menu(62, 'aboutUs', '/ve-chung-toi', null, null, false, 60),
    new Menu(63, 'faq', '/faq', null, null, false, 60),

    // new Menu(40, 'sell', null, null, null, true, 0),
    // new Menu(41, 'Agents', '/agents', null, null, false, 40),
    // new Menu(42, 'Agent', '/agents/1', null, null, false, 40),
    // new Menu(43, 'Login', '/login', null, null, false, 40),
    // new Menu(44, 'Register', '/register', null, null, false, 40),
    // new Menu(45, 'FAQs', '/faq', null, null, false, 40),
    // new Menu(46, 'Pricing', '/pricing', null, null, false, 40),
    // new Menu(
    //     47,
    //     'Terms & Conditions',
    //     '/terms-conditions',
    //     null,
    //     null,
    //     false,
    //     40
    // ),
    // new Menu(48, 'Landing', '/landing', null, null, false, 40),
    // new Menu(50, '404 Page', '/404', null, null, false, 40),
    // new Menu(60, 'Contact', '/contact'),
    // new Menu(70, 'About Us', '/about'),
    // new Menu(140, 'Others', null, null, null, true, 40),
    // new Menu(
    //     141,
    //     'External Link',
    //     null,
    //     'http://themeseason.com',
    //     '_blank',
    //     false,
    //     140
    // ),
    // new Menu(
    //     142,
    //     'Menu item',
    //     null,
    //     'http://themeseason.com',
    //     '_blank',
    //     false,
    //     140
    // ),
    // new Menu(
    //     143,
    //     'Menu item',
    //     null,
    //     'http://themeseason.com',
    //     '_blank',
    //     false,
    //     140
    // ),
    // new Menu(
    //     144,
    //     'Menu item',
    //     null,
    //     'http://themeseason.com',
    //     '_blank',
    //     false,
    //     140
    // )
];

export const verticalMenuItems = [
    new Menu(1, 'Home', '/'),
    new Menu(2, 'Properties', '/properties'),
    new Menu(40, 'Pages', null, null, null, true, 0),
    new Menu(41, 'Agents', '/agents', null, null, false, 40),
    new Menu(42, 'Agent', '/agents/1', null, null, false, 40),
    new Menu(43, 'Login', '/login', null, null, false, 40),
    new Menu(44, 'Register', '/register', null, null, false, 40),
    new Menu(45, 'FAQs', '/faq', null, null, false, 40),
    new Menu(46, 'Pricing', '/pricing', null, null, false, 40),
    new Menu(47, 'Terms & Conditions', '/terms-conditions', null, null, false, 40),
    new Menu(48, 'Landing', '/landing', null, null, false, 40),
    new Menu(50, '404 Page', '/404', null, null, false, 40),
    new Menu(60, 'Contact', '/contact'),
    new Menu(70, 'About Us', '/about'),
    new Menu(140, 'Level 1', null, null, null, true, 0),
    new Menu(141, 'Level 2', null, null, null, true, 140),
    new Menu(142, 'Level 3', null, null, null, true, 141),
    new Menu(143, 'Level 4', null, null, null, true, 142),
    new Menu(144, 'Level 5', null, 'http://themeseason.com', null, false, 143),
    new Menu(200, 'External Link', null, 'http://themeseason.com', '_blank', false, 0),
];
