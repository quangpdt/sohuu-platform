import { Injectable } from '@angular/core';

import { Menu } from './menu.model';
import { horizontalMenuItems } from './menu';

@Injectable()
export class MenuService {
    constructor() {}

    public getVerticalMenuItems(): Array<Menu> {
        return horizontalMenuItems;
    }

    public getHorizontalMenuItems(): Array<Menu> {
        return horizontalMenuItems;
    }

    public toggleMenuItem(menuId) {
        const menuItem = document.getElementById('menu-item-' + menuId);
        const subMenu = document.getElementById('sub-menu-' + menuId);
        if (subMenu) {
            if (subMenu.classList.contains('show')) {
                subMenu.classList.remove('show');
                menuItem.classList.remove('expanded');
            } else {
                subMenu.classList.add('show');
                menuItem.classList.add('expanded');
            }
        }
    }

    public closeOtherSubMenus(menu: Array<Menu>, menuId) {
        const currentMenuItem = menu.filter((item) => item.id === menuId)[0];
        menu.forEach((item) => {
            if (
                (item.id !== menuId && item.parentId === currentMenuItem.parentId) ||
                (currentMenuItem.parentId === 0 && item.id !== menuId)
            ) {
                const subMenu = document.getElementById('sub-menu-' + item.id);
                const menuItem = document.getElementById('menu-item-' + item.id);
                if (subMenu) {
                    if (subMenu.classList.contains('show')) {
                        subMenu.classList.remove('show');
                        menuItem.classList.remove('expanded');
                    }
                }
            }
        });
    }
}
