import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { MenuService } from '../menu.service';
import { Menu } from '../menu.model';

@Component({
    selector: 'app-horizontal-menu',
    templateUrl: './horizontal-menu.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [MenuService],
})
export class HorizontalMenuComponent implements OnInit {
    @Input('menuParentId') public menuParentId;
    public menuItems: Array<Menu>;
    constructor(public menuService: MenuService) {}

    public ngOnInit() {
        this.menuItems = this.menuService.getHorizontalMenuItems();
        this.menuItems = this.menuItems.filter((item) => item.parentId === this.menuParentId);
    }
}
