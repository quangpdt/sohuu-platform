import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-contacts',
    templateUrl: './contacts.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactsComponent {
    @Input() public dividers = true;
    @Input() public iconSize = 'sm';
    @Input() public iconColor = '';
    constructor() {}
}
