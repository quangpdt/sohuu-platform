import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';
import { AppService } from '../../../app.service';
import { BaseComponent } from '../../../pages/base.component';

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolbarComponent extends BaseComponent {
    @Output() public onMenuIconClick: EventEmitter<any> = new EventEmitter<any>();
    constructor(public appService: AppService) {
        super();
    }

    public sidenavToggle() {
        this.onMenuIconClick.emit();
    }
}
