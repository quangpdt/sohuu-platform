import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Initialize } from './core/states/app/app.action';
import { Observable } from 'rxjs';
import { UIState } from './core/states/ui/ui.state';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
    @Select(UIState.shouldDisplayFixedMainToolbar)
    public displayFixedMainToolbar$: Observable<boolean>;

    constructor(private store: Store) {}

    public ngOnInit(): void {
        this.store.dispatch(new Initialize());
    }
}
