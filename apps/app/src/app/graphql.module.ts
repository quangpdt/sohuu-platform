import { Inject, NgModule, PLATFORM_ID } from '@angular/core';
import { Apollo, ApolloModule } from 'apollo-angular';
import { HttpLink, HttpLinkModule } from 'apollo-angular-link-http';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { environment } from '../environments/environment';
import { BrowserTransferStateModule, makeStateKey, TransferState } from '@angular/platform-browser';
import { ApolloClientOptions } from 'apollo-client';
import { isPlatformBrowser } from '@angular/common';

const uri = environment.baseUrl.graphql;
const graphqlStateKey = makeStateKey<any>(environment.stateKey.graphql);

export function createApollo(cache: InMemoryCache, httpLink: HttpLink, isBrowser: boolean): ApolloClientOptions<any> {
    return {
        link: httpLink.create({ uri, withCredentials: true, credentials: 'include' } as any),
        cache,
        ...(isBrowser
            ? {
                  // queries with `forceFetch` enabled will be delayed
                  ssrForceFetchDelay: 200,
              }
            : {
                  // avoid to run twice queries with `forceFetch` enabled
                  ssrMode: true,
              }),
        defaultOptions: {
            query: {
                fetchPolicy: isBrowser ? 'no-cache' : 'cache-first',
                errorPolicy: 'all',
            },
        },
    };
}

@NgModule({
    imports: [BrowserTransferStateModule],
    exports: [ApolloModule, HttpLinkModule],
})
export class GraphQLModule {
    private readonly cache: InMemoryCache;

    constructor(
        apollo: Apollo,
        httpLink: HttpLink,
        private readonly transferState: TransferState,
        @Inject(PLATFORM_ID) readonly platformId: Object
    ) {
        const isBrowser = isPlatformBrowser(platformId);
        this.cache = new InMemoryCache();

        apollo.create(createApollo(this.cache, httpLink, isBrowser));

        if (isBrowser) {
            this.onBrowser();
        } else {
            this.onServer();
        }
    }
    public onServer() {
        this.transferState.onSerialize(graphqlStateKey, () => this.cache.extract());
    }

    public onBrowser() {
        // reads the serialized cache
        const state = this.transferState.get<NormalizedCacheObject>(graphqlStateKey, null);
        // and puts it in the Apollo
        this.cache.restore(state);
    }
}
