const jsdom = require('jsdom');

export class BrowserMock {
    public win;

    constructor() {
        const doc = new jsdom.JSDOM('<!DOCTYPE html><html><body></body></html>');
        this.win = doc.window;
        this.win.document.execCommand = () => {};
        //
        // if (!win.localStorage) {
        //     (win).localStorage = options.localStorage || new MockStorage();
        // }
        //
        // if (!win.sessionStorage) {
        //     (win).sessionStorage = options.sessionStorage = new MockStorage();
        // }
    }

    public getDocument() {
        return this.win.document;
    }

    public getWindow() {
        return this.win;
    }

    public getNode() {
        return this.win.Node;
    }

    public getHistory() {
        return this.win.history;
    }

    public getLocation() {
        return this.win.location;
    }

    public getLocalStorage() {
        return this.win.localStorage;
    }

    public getSessionStorage() {
        return this.win.sessionStorage;
    }

    public getNavigator() {
        return this.win.navigator;
    }
}
