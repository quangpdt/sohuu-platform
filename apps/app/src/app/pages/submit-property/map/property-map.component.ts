import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output,
} from '@angular/core';
import * as L from 'leaflet';
import { LatLng, LayerGroup, Map } from 'leaflet';
import { Observable } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { Coordinates, LocationSearchResult } from '@sohuu-platform/interfaces';

@Component({
    selector: 'app-property-map',
    templateUrl: './property-map.component.html',
    styleUrls: ['./property-map.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropertyMapComponent implements OnInit, OnDestroy, AfterViewInit {
    private baseUrl = ' https://maps.vietmap.vn/api/';
    private apiKey = '200efe9747190d9480869f35074be6c3865aa8de39706c46';
    private map: Map;
    private layerGroup: LayerGroup;
    @Input('location') public location$: Observable<LocationSearchResult>;
    @Output() public locationChange: EventEmitter<Coordinates> = new EventEmitter<Coordinates>();

    public ngOnInit(): void {}

    public ngAfterViewInit(): void {
        this.map = L.map('map', {
            center: [10.7721148, 106.6960844],
            zoom: 15,
        });

        const tiles = L.tileLayer(`${this.baseUrl}tm/{z}/{x}/{y}.png?apikey=${this.apiKey}`, {
            maxZoom: 19,
        });

        tiles.addTo(this.map);

        this.layerGroup = L.layerGroup().addTo(this.map);

        this.location$.pipe(untilDestroyed(this)).subscribe((location: LocationSearchResult) => {
            this.map.setView(location.coordinates, 15);
            this.layerGroup.clearLayers();
            const marker = L.marker(location.coordinates, {
                draggable: true,
                icon: L.icon({
                    iconSize: [25, 41],
                    iconAnchor: [13, 41],
                    iconUrl: '/assets/marker-icon.png',
                    shadowUrl: '/assets/marker-shadow.png',
                }),
            }).addTo(this.layerGroup);
            this.createPopup(location.coordinates, location.fullLocation);
            marker.on('dragend', (e) => {
                this.createPopup(e.target._latlng, location.fullLocation);
                this.locationChange.emit(e.target._latlng);
            });
        });
    }

    public ngOnDestroy(): void {}

    public getMapCenter(): LatLng {
        return this.map.getCenter();
    }

    private createPopup(coordinates: Coordinates, fullLocation: string) {
        this.map.closePopup();

        L.popup({
            closeButton: false,
            offset: [0, -30],
            closeOnClick: false,
            closeOnEscapeKey: false,
            autoClose: false,
        })
            .setLatLng(coordinates)
            .setContent(fullLocation)
            .openOn(this.map);
    }
}
