import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    NgZone,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { MatStepper } from '@angular/material/stepper';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, switchMap, take } from 'rxjs/operators';
import { PropertyService } from '../../core/services/property.service';
import { Store } from '@ngxs/store';
import { LoadAllUnits, LoadCities } from '../../core/states/property/property.action';
import { UploadService } from '../../core/services/upload.service';
import * as cloneDeep from 'lodash/cloneDeep';
import * as isInteger from 'lodash/isInteger';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '../../core/services/notification.service';
import { Location } from '@angular/common';
import { RouteNames, routePaths } from '../../core/route.names';
import { BaseComponent } from '../base.component';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { MatQuillComponent } from './mat-quill/mat-quill.component';
import { Observable, of } from 'rxjs';
import { PropertyMapComponent } from './map/property-map.component';
import { LatLng } from 'leaflet';
import {
    propertyDirections,
    propertyFeatures,
    propertyLegals,
    PropertyStatus,
    propertyStatuses,
    propertyTypes,
    SubmitStatus,
} from '@sohuu-platform/enums';
import { City, District, LocationSearchResult, Ward } from '@sohuu-platform/interfaces';

@Component({
    selector: 'app-submit-property',
    templateUrl: './submit-property.component.html',
    styleUrls: ['./submit-property.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubmitPropertyComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('myPond') public myPond: any;
    @ViewChild('matEditor', {
        static: true,
    })
    public matEditor: MatQuillComponent;
    @ViewChild(PropertyMapComponent, { static: false }) public map: PropertyMapComponent;

    public filteredAddress: Observable<LocationSearchResult[]>;
    public location$: EventEmitter<LocationSearchResult> = new EventEmitter<LocationSearchResult>();
    public pondOptions = {
        class: 'my-filepond',
        multiple: true,
        labelIdle: 'Chọn hoặc kéo thả ảnh vào',
        acceptedFileTypes: 'image/jpeg, image/jpg, image/png',
        allowImagePreview: true,
        name: 'files',
        server: {
            url: '/api/resource',
            process: {
                url: '/upload',
                onload: (data) => {
                    return JSON.parse(data)[0].name;
                },
            },
            load: async (source, load, error) => {
                try {
                    load(await this.uploadService.fetchDownloadedImage(source));
                } catch (e) {
                    error('Error Occurred');
                }
            },
        },
    };
    public pondFiles = [];

    constructor(
        private formBuilder: FormBuilder,
        private ngZone: NgZone,
        private propertyService: PropertyService,
        private store: Store,
        private uploadService: UploadService,
        private router: Router,
        private route: ActivatedRoute,
        private notificationService: NotificationService,
        private location: Location
    ) {
        super();
        this.isEditing = this.router.url.includes(RouteNames.EditProperty);
    }

    @ViewChild('horizontalStepper') public horizontalStepper: MatStepper;
    public form: FormGroup;
    public propertyTypes = propertyTypes;
    public propertyStatuses = propertyStatuses;
    public propertyDirections = propertyDirections;
    public propertyLegals = propertyLegals;

    public isRentAvailable = false;
    public isSaleAvailable = false;
    public propertyId: number;
    public isEditing: boolean;

    public async ngOnInit() {
        this.createFormGroup();
        this.initPropertyPrice();

        if (this.isEditing) {
            this.route.paramMap.pipe(take(1)).subscribe((params) => {
                const propertyId = params.get('id');
                if (!propertyId) {
                    return;
                }

                if (!isInteger(parseInt(propertyId, 10))) {
                    this.location.back();
                    return;
                }

                this.propertyId = parseInt(propertyId, 10);
                this.getExistingPropertyData();
            });
        } else {
            this.initNewProperty();
        }
    }

    public ngAfterViewInit() {
        this.filteredAddress = this.addressForm.get('location').valueChanges.pipe(
            untilDestroyed(this),
            debounceTime(400),
            distinctUntilChanged(),
            switchMap((value) => {
                if (value && typeof value === 'object') {
                    this.location$.next(value);
                    this.addressForm.patchValue({
                        location: value.fullLocation,
                        lat: value.coordinates.lat,
                        lng: value.coordinates.lng,
                    });
                    return of(undefined);
                }

                if (!value || value.length <= 4) {
                    return of(undefined);
                }

                return this.searchAddressByKeyword(value);
            })
        );
    }

    public ngOnDestroy(): void {}

    private createFormGroup() {
        this.form = this.formBuilder.group({
            basic: this.formBuilder.group({
                title: [null, Validators.required],
                description: [null, Validators.required],
                rentPrice: null,
                salePrice: null,
                saleNegotiable: [true],
                rentNegotiable: [true],
                propertyType: [propertyTypes[0], Validators.required],
                propertyStatus: [null, Validators.required],
                files: null,
            }),
            address: this.formBuilder.group({
                location: ['', Validators.required],
                city: [null, Validators.required],
                district: [{ value: null, disabled: true }, Validators.required],
                ward: [{ value: null, disabled: true }, Validators.required],
                lat: null,
                lng: null,
            }),
            additional: this.formBuilder.group({
                mainDoorDirection: propertyDirections[0],
                balconyDirection: propertyDirections[0],
                kitchenDirection: propertyDirections[0],
                legal: propertyLegals[0],
                bedrooms: '',
                bathrooms: '',
                floors: '',
                area: '',
                features: this.buildFeatures(),
                additionalFeatures: this.formBuilder.array([this.createFeature(false)]),
            }),
        });
    }

    public onSelectionChange(event: StepperSelectionEvent) {
        if (event.selectedIndex === 4) {
            this.horizontalStepper._steps.forEach((step) => (step.editable = false));
        }
    }

    public reset() {
        this.horizontalStepper.reset();

        const additionalFeatures = this.additionalForm.get('additionalFeatures') as FormArray;

        while (additionalFeatures.length > 1) {
            additionalFeatures.removeAt(0);
        }

        this.form.reset({
            additional: {
                features: this.buildFeatures(),
            },
        });
    }

    public buildFeatures() {
        const arr = propertyFeatures.map((feature) =>
            this.formBuilder.group({
                id: feature,
                selected: false,
            })
        );
        return this.formBuilder.array(arr);
    }

    public createFeature(isRequired: boolean = true): FormGroup {
        return this.formBuilder.group({
            name: [null, isRequired ? Validators.required : undefined],
            value: [null, isRequired ? Validators.required : undefined],
        });
    }

    public addFeature(): void {
        const features = this.additionalForm.get('additionalFeatures') as FormArray;
        features.push(this.createFeature());
    }

    public deleteFeature(index) {
        const features = this.additionalForm.get('additionalFeatures') as FormArray;
        features.removeAt(index);
    }

    public submitProperty() {
        const files = [];

        if (this.myPond.pond.getFiles() && this.myPond.pond.getFiles().length > 0) {
            this.myPond.pond.getFiles().forEach((file) => {
                const metadata = file.getMetadata();
                if (metadata && metadata.id) {
                    files.push({
                        id: parseInt(metadata.id, 10),
                        fileName: metadata.fileName,
                    });
                } else {
                    files.push({
                        fileName: file.serverId,
                    });
                }
            });
        }

        if (files.length === 0) {
            this.notificationService.notifyError('pleaseAddAtLeastOneImageToProperty', false);
            return;
        }

        this.basicForm.get('files').setValue(files);
        if (this.propertyId) {
            this.propertyService.updateProperty(this.propertyId, cloneDeep(this.form.value)).subscribe(() => {
                this.router.navigate([routePaths.propertySubmitted]);
            });
        } else {
            this.propertyService.createProperty(cloneDeep(this.form.value)).subscribe(() => {
                this.router.navigate([routePaths.propertySubmitted]);
            });
        }
    }

    public getPropertyStatuses() {
        const result = [];
        const rawStatuses = this.basicForm.get('propertyStatus').value;

        if (!rawStatuses) {
            return result;
        }

        rawStatuses.forEach((status: PropertyStatus) => {
            if (status === PropertyStatus.ForSale) {
                result.push({
                    status,
                    info: {
                        price: this.basicForm.get('salePrice').value,
                        negotiable: this.basicForm.get('saleNegotiable').value ? 'negotiable' : 'nonNegotiable',
                    },
                });
            } else if (status === PropertyStatus.ForRent) {
                result.push({
                    status,
                    info: {
                        price: this.basicForm.get('rentPrice').value,
                        negotiable: this.basicForm.get('rentNegotiable').value ? 'negotiable' : 'nonNegotiable',
                    },
                });
            }
        });

        return result;
    }

    public getAdministrativeDivisionAddress(): string {
        const { location, city, district, ward } = this.addressForm.getRawValue();

        if (!(city && district && ward && location)) {
            return '';
        }

        const address = [location, (ward as Ward).name, (district as District).name, (city as City).name];
        return address.join(', ');
    }

    public getFeatures() {
        return (this.additionalControls['features'] as FormGroup)
            .getRawValue()
            .filter((item) => item.selected)
            .map((p) => p.id);
    }

    public getAdditionalFeatures() {
        const selectedFeatures = [];

        const additionalFeaturesForm = this.additionalForm.get('additionalFeatures') as FormArray;
        if (additionalFeaturesForm && additionalFeaturesForm.length > 0) {
            selectedFeatures.push(...additionalFeaturesForm.getRawValue());
        }

        return selectedFeatures;
    }

    public updateCoordinates(coordinates: LatLng) {
        this.addressForm.patchValue({
            lat: coordinates.lat,
            lng: coordinates.lng,
        });
    }

    public get basicForm(): FormGroup {
        return this.form.get('basic') as FormGroup;
    }

    public get basicControls() {
        return this.basicForm.controls;
    }

    public get addressForm(): FormGroup {
        return this.form.get('address') as FormGroup;
    }

    public get addressControls() {
        return this.addressForm.controls;
    }

    public get additionalForm(): FormGroup {
        return this.form.get('additional') as FormGroup;
    }

    public get additionalControls() {
        return this.additionalForm.controls;
    }

    private searchAddressByKeyword(keyword: string): Observable<any> {
        const centerCoords = this.map.getMapCenter();
        return this.propertyService.searchAddress(keyword, centerCoords.lat, centerCoords.lng);
    }

    public getFullLocation(option: LocationSearchResult) {
        if (!option) {
            return null;
        }
        if (typeof option === 'string') {
            return option;
        }

        return option.fullLocation;
    }

    private initNewProperty() {
        this.store.dispatch(new LoadCities());
    }

    private getExistingPropertyData() {
        this.propertyService
            .getProperty(this.propertyId)
            .pipe(take(1))
            .subscribe(
                async (property) => {
                    if (property.submitStatus === SubmitStatus.Processing) {
                        await this.notificationService.notifyError('cannotEditProcessingProperty');
                        return this.router.navigate([routePaths.myProperties]);
                    }

                    const formData = this.propertyService.mapPropertyToForm(property);

                    this.pondFiles = property.files.map((p) => ({
                        source: `/static/images/${p.fileName}`,
                        options: {
                            type: 'local',
                            metadata: p,
                        },
                    }));

                    const { cityId, districtId, wardId } = formData.address;
                    this.form.patchValue(formData);

                    if (property.lat && property.lng) {
                        this.location$.next({
                            coordinates: {
                                lat: property.lat,
                                lng: property.lng,
                            },
                            fullLocation: property.fullLocation,
                        });
                    }

                    this.store.dispatch(new LoadAllUnits(cityId, districtId, wardId));
                },
                async (e) => {
                    await this.notificationService.notifyError('errorGeneral');
                    return this.router.navigate([routePaths.myProperties]);
                }
            );
    }

    private initPropertyPrice() {
        this.basicForm
            .get('propertyStatus')
            .valueChanges.pipe(untilDestroyed(this))
            .subscribe((statuses) => {
                this.isSaleAvailable = statuses.filter((item) => item === PropertyStatus.ForSale).length > 0;
                this.isRentAvailable = statuses.filter((item) => item === PropertyStatus.ForRent).length > 0;

                if (!this.isSaleAvailable) {
                    this.basicForm.get('salePrice').reset(null);
                    this.basicForm.get('saleNegotiable').reset(true);
                } else {
                    this.basicForm.get('salePrice').setValidators(Validators.required);
                }

                if (!this.isRentAvailable) {
                    this.basicForm.get('rentPrice').reset(null);
                    this.basicForm.get('rentNegotiable').reset(true);
                } else {
                    this.basicForm.get('rentPrice').setValidators(Validators.required);
                }
            });
    }
}
