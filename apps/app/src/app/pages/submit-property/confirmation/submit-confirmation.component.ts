import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-submit-confirmation',
    templateUrl: './submit-confirmation.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubmitConfirmationComponent {}
