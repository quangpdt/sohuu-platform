import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InputFileModule } from 'ngx-input-file';
import { SubmitPropertyComponent } from './submit-property.component';
import { SubmitConfirmationComponent } from './confirmation/submit-confirmation.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgxMaskModule } from 'ngx-mask';
import { PropertyMapComponent } from './map/property-map.component';
import { QuillModule } from 'ngx-quill';
import { submitPropertyRoutes } from '../submit-property.routes';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FilePondComponent } from './filepond/filepond.component';
import FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import FilePondPluginImageValidateSize from 'filepond-plugin-image-validate-size';
import { registerPlugin } from 'filepond';
import { MatQuillComponent } from './mat-quill/mat-quill.component';

@NgModule({
    declarations: [
        SubmitPropertyComponent,
        SubmitConfirmationComponent,
        PropertyMapComponent,
        FilePondComponent,
        MatQuillComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(submitPropertyRoutes),
        SharedModule,
        InputFileModule,
        TranslateModule.forChild(),
        NgxMaskModule,
        QuillModule,
        MatCardModule,
        MatStepperModule,
        MatSelectModule,
        MatInputModule,
        MatCheckboxModule,
        MatIconModule,
        MatButtonModule,
        MatAutocompleteModule,
        MatTooltipModule,
    ],
})
export class SubmitPropertyModule {
    constructor() {
        registerPlugin(FilePondPluginImagePreview, FilePondPluginImageValidateSize);
    }
}
