import { ChangeDetectionStrategy, Component, DoCheck, HostBinding, Input } from '@angular/core';
import { MatFormFieldControl } from '@angular/material/form-field';
import { MatQuillBase } from './mat-quill-base';

let nextUniqueId = 0;
const selector = 'mat-quill';

@Component({
    selector: selector,
    exportAs: 'matQuill',
    template: `
        <ng-content select="[quill-editor-toolbar]"></ng-content>
        <ng-content></ng-content>
    `,
    providers: [{ provide: MatFormFieldControl, useExisting: MatQuillComponent }],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MatQuillComponent extends MatQuillBase implements DoCheck {
    public static ngAcceptInputType_disabled: boolean | string | null | undefined;
    public static ngAcceptInputType_required: boolean | string | null | undefined;
    @Input() public disabled;
    public controlType = selector;
    @HostBinding() public id = `${selector}-${nextUniqueId++}`;

    public ngDoCheck() {
        if (this.ngControl) {
            this.updateErrorState();
        }
    }
}
