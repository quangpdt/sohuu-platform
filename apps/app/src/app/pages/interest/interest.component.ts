import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, ViewChild } from '@angular/core';
import { InterestCalculatorService } from '../../core/services/interest-calculator.service';
import { ActivatedRoute, Router } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { BehaviorSubject, Observable } from 'rxjs';
import { BaseComponent } from '../base.component';
import { Select, Store } from '@ngxs/store';
import { PropertyState } from '@sohuu/app/core/states/property/property.state';
import { PropertyModel } from '@sohuu-platform/models';
import { LoadPropertyDetail, ResetPropertyDetail } from '@sohuu/app/core/states/property/property.action';
import { InterestInputsComponent } from '@sohuu/app/shared/components/interest-inputs/interest-inputs.component';
import { InterestInput } from '@sohuu-platform/interfaces';

@Component({
    selector: 'app-interest',
    templateUrl: './interest.component.html',
    styleUrls: ['./interest.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InterestComponent extends BaseComponent implements AfterViewInit, OnDestroy {
    public results;
    public displayedColumns: string[] = ['year', 'period', 'interest', 'installment', 'total', 'amount', 'annualTotal'];
    public initialInterestInput: BehaviorSubject<Partial<InterestInput>> = new BehaviorSubject<Partial<InterestInput>>(
        {}
    );
    @Select(PropertyState.getSelectedProperty)
    public property$: Observable<Partial<PropertyModel>>;

    @ViewChild('interestInputsComponent', { static: false }) public interestInput: InterestInputsComponent;

    constructor(
        private interestCalculatorService: InterestCalculatorService,
        private route: ActivatedRoute,
        private router: Router,
        private store: Store
    ) {
        super();
    }

    public ngAfterViewInit() {
        this.applyInputFromRoutePath();
    }

    public ngOnDestroy() {}

    public searchClicked(input: InterestInput) {
        const { routeCommands, queryParams } = this.interestCalculatorService.mapInterestInputToRoutePath(input);
        this.router.navigate(routeCommands, { queryParams });
    }

    public isSummaryRow(index: number) {
        return index % 13 === 0;
    }

    public applyInputFromRoutePath() {
        this.route.queryParams.pipe(untilDestroyed(this)).subscribe((queryParams) => {
            const input = this.interestCalculatorService.mapRoutePathToInterestInput(queryParams);
            this.initialInterestInput.next(input);
            this.calculate(input);

            if (input.propertyId && input.propertySlug) {
                this.store.dispatch(new LoadPropertyDetail(input.propertyId, input.propertySlug));
            }
        });
    }

    private calculate(input: InterestInput) {
        const { years, amount, maximumDebt, interestRate } = input;

        this.results = this.interestCalculatorService.calculateInterest(
            years,
            interestRate,
            (amount * maximumDebt) / 100
        );
    }

    public removePropertyFromCalculator() {
        this.store.dispatch(new ResetPropertyDetail());
        const input: InterestInput = this.interestInput.form.getRawValue();
        input.propertySlug = undefined;
        input.propertyId = undefined;

        this.searchClicked(input);
    }
}
