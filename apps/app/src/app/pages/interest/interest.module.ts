import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InterestComponent } from './interest.component';
import { NgxMaskModule } from 'ngx-mask';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule } from '@angular/material/button';

export const routes = [{ path: '', component: InterestComponent, pathMatch: 'full' }];

@NgModule({
    declarations: [InterestComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        NgxMaskModule,
        MatCardModule,
        MatTableModule,
        TranslateModule,
        MatButtonModule,
    ],
})
export class InterestModule {}
