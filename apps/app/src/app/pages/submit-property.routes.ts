import { Routes } from '@angular/router';
import { RouteNames } from '../core/route.names';
import { SubmitPropertyComponent } from './submit-property/submit-property.component';
import { AccountVerificationGuard } from '../core/guards/account-verification.guard';
import { SubmitConfirmationComponent } from './submit-property/confirmation/submit-confirmation.component';

export const submitPropertyRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: RouteNames.NewProperty,
    },
    {
        path: RouteNames.NewProperty,
        component: SubmitPropertyComponent,
        canActivate: [AccountVerificationGuard],
    },
    {
        path: `${RouteNames.EditProperty}/:id`,
        component: SubmitPropertyComponent,
        canActivate: [AccountVerificationGuard],
    },
    { path: RouteNames.PropertySubmitted, component: SubmitConfirmationComponent },
];
