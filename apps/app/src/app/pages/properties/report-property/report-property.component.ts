import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { PropertyState } from '../../../core/states/property/property.state';
import { Observable } from 'rxjs';
import { PropertyModel, UserModel } from '@sohuu-platform/models';
import { AppState } from '../../../core/states/app/app.state';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { NotificationService } from '../../../core/services/notification.service';
import { MatDialog } from '@angular/material/dialog';
import { propertyReportTypes } from '@sohuu-platform/enums';

@Component({
    selector: 'app-report-property',
    templateUrl: './report-property.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportPropertyComponent extends BaseComponent implements OnInit {
    @Select(PropertyState.getSelectedProperty)
    public selectedProperty$: Observable<Partial<PropertyModel>>;

    @Select(AppState.getUser)
    public user$: Observable<Partial<UserModel>>;

    public reportForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private notificationService: NotificationService,
        private matDialog: MatDialog
    ) {
        super();
    }

    public ngOnInit() {
        this.buildForm();
    }

    public buildForm() {
        const arr = propertyReportTypes.map((type) =>
            this.formBuilder.group({
                id: type,
                selected: false,
            })
        );
        this.reportForm = this.formBuilder.group({
            types: this.formBuilder.array(arr),
            remarks: null,
            name: [null, Validators.required],
            phone: [null, Validators.required],
            email: [null],
        });
    }

    public get controls() {
        return this.reportForm.controls;
    }

    public submitForm() {
        this.matDialog.closeAll();
        this.notificationService.notifySuccess('thanksForReporting');
    }
}
