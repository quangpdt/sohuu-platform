import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes, UrlSegment } from '@angular/router';
import { MatVideoModule } from 'mat-video';
import { SharedModule } from '../../shared/shared.module';
import { PropertiesComponent } from './properties.component';
import { PropertyComponent } from './property/property.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgxMaskModule } from 'ngx-mask';
import { QuillModule } from 'ngx-quill';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { RouteNames } from '../../core/route.names';
import { ReportPropertyComponent } from './report-property/report-property.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCheckboxModule } from '@angular/material/checkbox';

const idRegex = /^\d+$/;
const slugRegex = /^[a-z0-9]+(?:-[a-z0-9]+)*$/;

export const routes: Routes = [
    { path: '', component: PropertiesComponent, pathMatch: 'full' },
    {
        path: ':id-:slug',
        component: PropertyComponent,
        matcher: (segments) => {
            if (segments.length === 1) {
                const url = segments[0].path.split('-');
                const id = url.shift();
                const slug = url.join('-');
                if (id.match(idRegex) && slug.match(slugRegex)) {
                    return {
                        consumed: segments,
                        posParams: {
                            id: new UrlSegment(id, {}),
                            slug: new UrlSegment(slug, {}),
                        },
                    };
                }
            }
            return null;
        },
    },
    {
        matcher: (segments: UrlSegment[]) => {
            if (!segments || segments.length === 0) {
                return null;
            }
            const baseRoutePath = segments[0].path;
            if (
                ![
                    RouteNames.BuyProperties,
                    RouteNames.RentProperties,
                    RouteNames.RentHouses,
                    RouteNames.BuyHouses,
                    RouteNames.RentApartments,
                    RouteNames.BuyApartments,
                    RouteNames.BuyRent,
                    RouteNames.BuyRentApartments,
                    RouteNames.BuyRentHouses,
                ].includes(baseRoutePath as any)
            ) {
                return null;
            }

            const posParams = {};

            // City
            if (segments[1]) {
                posParams['citySlug'] = segments[1].path;
            }
            // District
            if (segments[2]) {
                posParams['districtSlug'] = segments[2].path;
            }
            // District
            if (segments[3]) {
                posParams['wardSlug'] = segments[3].path;
            }
            return {
                consumed: segments,
                posParams,
            };
        },
        component: PropertiesComponent,
    },
];

@NgModule({
    declarations: [PropertiesComponent, PropertyComponent, ReportPropertyComponent],
    exports: [PropertiesComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatVideoModule,
        SharedModule,
        TranslateModule,
        NgxMaskModule,
        QuillModule,
        MatSidenavModule,
        MatIconModule,
        MatCardModule,
        MatTooltipModule,
        MatDividerModule,
        MatInputModule,
        MatButtonModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        MatPaginatorModule,
        MatDialogModule,
        MatCheckboxModule,
    ],
})
export class PropertiesModule {}
