import { ChangeDetectionStrategy, Component, Inject, OnDestroy, PLATFORM_ID, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MediaObserver } from '@angular/flex-layout';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';
import { untilDestroyed } from 'ngx-take-until-destroy';
import {
    ChangeSortOption,
    LoadAllUnitsBySlug,
    LoadAllUnitsBySlugDone,
    LoadProperties,
    ResetProperties,
} from '../../core/states/property/property.action';
import { Actions, ofActionDispatched, Select, Store } from '@ngxs/store';
import { PropertyModel } from '@sohuu-platform/models';
import { PropertyState } from '../../core/states/property/property.state';
import { ActivatedRoute, Router } from '@angular/router';
import { PropertyPublicService } from '../../core/services/property.public-service';
import { SidenavService } from '../../core/services/sidenav.service';
import { PropertySearchCriteria } from '@sohuu-platform/interfaces';
import { SortOption } from '@sohuu-platform/enums';

@Component({
    selector: 'app-properties',
    templateUrl: './properties.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropertiesComponent implements OnDestroy {
    @ViewChild(MatPaginator) public paginator: MatPaginator;

    public routeSearchCriteria$: BehaviorSubject<PropertySearchCriteria> = new BehaviorSubject<PropertySearchCriteria>(
        null
    );
    public sortOption: SortOption;
    public searchCriteria: PropertySearchCriteria;
    public removedSearchField: string;

    @Select(PropertyState.getProperties)
    public properties$: Observable<Partial<PropertyModel>[]>;

    @Select(PropertyState.getTotalProperties)
    public totalProperties$: Observable<number>;

    @Select(PropertyState.getPageSize)
    public pageSize$: Observable<number>;

    @Select(PropertyState.getCurrentPage)
    public currentPage$: Observable<number>;

    constructor(
        public mediaObserver: MediaObserver,
        @Inject(PLATFORM_ID) private platformId: Object,
        private store: Store,
        private route: ActivatedRoute,
        private propertyPublicService: PropertyPublicService,
        private actions$: Actions,
        private router: Router,
        public sidenavService: SidenavService
    ) {
        this.applySearchCriteriaFromRoute();
    }

    public ngOnDestroy() {}

    public getProperties(searchCriteria?: PropertySearchCriteria) {
        this.store.dispatch(
            new LoadProperties(
                searchCriteria ? this.propertyPublicService.mapSearchCriteriaToSearchInput(searchCriteria) : null
            )
        );
    }

    public searchClicked(searchCriteria: PropertySearchCriteria) {
        if (isPlatformBrowser(this.platformId)) {
            window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        }
        this.searchCriteria = searchCriteria;
        this.navigateToNewSearchCriteria();
    }

    public removeSearchFields(fields) {
        fields.forEach((field) => {
            if (field.indexOf('.') > -1) {
                const arr = field.split('.');
                this.searchCriteria[arr[0]][arr[1]] = null;
            } else if (field.indexOf(',') > -1) {
                const arr = field.split(',');
                this.searchCriteria[arr[0]][arr[1]]['selected'] = false;
            } else {
                this.searchCriteria[field] = null;
            }
        });
        this.searchClicked(this.searchCriteria);
    }

    public async changePageSize(pageSize) {
        this.searchCriteria.pageSize = pageSize;
        this.navigateToNewSearchCriteria();
    }

    public async changeSortOption(sortOption: SortOption) {
        this.sortOption = sortOption;
        await this.store.dispatch([new ResetProperties(), new ChangeSortOption(sortOption)]).toPromise();
        this.getProperties();
    }

    public changePage(event: PageEvent) {
        this.searchCriteria.page = event.pageIndex;
        this.searchClicked(this.searchCriteria);
    }

    private applySearchCriteriaFromRoute() {
        combineLatest([this.route.url, this.route.queryParams])
            .pipe(untilDestroyed(this))
            .subscribe(([segments, queryParams]) => {
                const searchCriteria = this.propertyPublicService.mapRoutePathToSearchCriteria(segments, queryParams);

                let citySlug, districtSlug, wardSlug;

                if (segments[1]) {
                    citySlug = segments[1].path;
                }
                if (segments[2]) {
                    districtSlug = segments[2].path;
                }
                if (segments[3]) {
                    wardSlug = segments[3].path;
                }

                if (citySlug || districtSlug || wardSlug) {
                    this.store.dispatch(new LoadAllUnitsBySlug(citySlug, districtSlug, wardSlug));

                    this.actions$.pipe(ofActionDispatched(LoadAllUnitsBySlugDone), take(1)).subscribe(() => {
                        this.updateSearchForm({
                            ...searchCriteria,
                            ...this.store.selectSnapshot(PropertyState.getFilteredAdministrativeUnits),
                        });
                    });
                    return;
                }
                this.updateSearchForm(searchCriteria);
            });
    }

    private navigateToNewSearchCriteria() {
        const { routeCommands, queryParams } = this.propertyPublicService.mapSearchCriteriaToRoutePath(
            this.searchCriteria
        );
        this.router.navigate(routeCommands, { queryParams, relativeTo: this.route });
    }

    private updateSearchForm(searchCriteria: PropertySearchCriteria) {
        this.searchCriteria = searchCriteria;
        this.routeSearchCriteria$.next(this.searchCriteria);
        this.getProperties(this.searchCriteria);
    }
}
