import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    HostListener,
    Inject,
    OnDestroy,
    OnInit,
    QueryList,
    ViewChild,
    ViewChildren,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { SwiperConfigInterface, SwiperDirective } from '@sohuu-platform/swiper';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { Meta } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { Property } from '../../../app.models';
import { AppService } from '../../../app.service';
import { emailValidator } from '../../../theme/utils/app-validators';
import { ClientSideService } from '../../../core/services/client-side.service';
import { BaseComponent } from '../../base.component';
import { PropertyModel, PropertyStatusModel, UserModel } from '@sohuu-platform/models';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { PropertyPublicService } from '../../../core/services/property.public-service';
import { NotificationService } from '../../../core/services/notification.service';
import { routePaths } from '../../../core/route.names';
import { getPropertyMetadata } from '../../../shared/util.function';
import { untilDestroyed } from 'ngx-take-until-destroy';
import * as findLast from 'lodash/findLast';
import { Actions, ofActionErrored, ofActionSuccessful, Select, Store } from '@ngxs/store';
import { LoadPropertyDetail } from '../../../core/states/property/property.action';
import { PropertyState } from '../../../core/states/property/property.state';
import { InterestCalculatorService } from '../../../core/services/interest-calculator.service';
import { AppState } from '../../../core/states/app/app.state';
import { MatDialog } from '@angular/material/dialog';
import { ReportPropertyComponent } from '../report-property/report-property.component';
import { environment } from '../../../../environments/environment';
import { HttpParams } from '@angular/common/http';
import { UserNotificationService } from '../../../core/services/user-notification.service';
import { SidenavService } from '../../../core/services/sidenav.service';
import {
    PropertyFeature,
    propertyFeatures,
    PropertyMetadataName,
    PropertyStatus,
    ResponseCode,
    SubmitStatus,
} from '@sohuu-platform/enums';
import { InterestInput } from '@sohuu-platform/interfaces';

@Component({
    selector: 'app-property',
    templateUrl: './property.component.html',
    styleUrls: ['./property.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropertyComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('sidenav', { static: false }) public sidenav: any;
    @ViewChildren(SwiperDirective) public swipers: QueryList<SwiperDirective>;
    public sidenavOpen = true;
    public config: SwiperConfigInterface;
    public config2: SwiperConfigInterface;
    public relatedProperties$: Observable<Partial<PropertyModel>[]>;
    public featuredProperties: Property[];
    public agent: any;
    public contactForm: FormGroup;
    public propertyMetadataName = PropertyMetadataName;
    public initialInterestInput: BehaviorSubject<Partial<InterestInput>> = new BehaviorSubject<Partial<InterestInput>>(
        {}
    );
    public submitStatus = SubmitStatus;

    @Select(PropertyState.getSelectedProperty)
    public property$: Observable<Partial<PropertyModel>>;

    @Select(AppState.getUser)
    public user$: Observable<Partial<UserModel>>;

    constructor(
        private appService: AppService,
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private meta: Meta,
        private clientSideService: ClientSideService,
        private propertyService: PropertyPublicService,
        private notificationService: NotificationService,
        private router: Router,
        private store: Store,
        private action$: Actions,
        private interestCalculatorService: InterestCalculatorService,
        private dialog: MatDialog,
        private userNotificationService: UserNotificationService,
        @Inject(DOCUMENT) private document,
        public sidenavService: SidenavService
    ) {
        super();
    }

    public ngOnInit() {
        this.contactForm = this.formBuilder.group({
            name: [null, Validators.required],
            email: [null, emailValidator],
            phone: [null, Validators.required],
            message: [null],
        });
        this.config = {
            observer: true,
            slidesPerView: 1,
            spaceBetween: 0,
            keyboard: true,
            navigation: true,
            pagination: false,
            grabCursor: true,
            loop: false,
            lazy: {
                loadPrevNext: true,
            },
        };

        this.config2 = {
            observer: true,
            slidesPerView: 4,
            spaceBetween: 16,
            keyboard: true,
            navigation: false,
            pagination: false,
            grabCursor: true,
            loop: false,
            breakpoints: {
                480: {
                    slidesPerView: 2,
                },
                600: {
                    slidesPerView: 3,
                },
            },
            lazy: {
                loadPrevNext: true,
            },
        };
        this.activatedRoute.paramMap.pipe(untilDestroyed(this)).subscribe((params: ParamMap) => {
            this.getProperty(parseInt(params.get('id'), 10), params.get('slug'));
        });
    }

    public ngOnDestroy() {}

    @HostListener('window:resize')
    public onWindowResize(): void {
        window.innerWidth < 960 ? (this.sidenavOpen = false) : (this.sidenavOpen = true);
    }

    public getProperty(id: number, slug: string) {
        this.store.dispatch(new LoadPropertyDetail(id, slug));
        this.action$.pipe(ofActionSuccessful(LoadPropertyDetail), untilDestroyed(this)).subscribe(() => {
            const property = this.store.selectSnapshot(PropertyState.getSelectedProperty);
            if (property.submitStatus === SubmitStatus.Active) {
                this.getRelatedProperties(id, slug);
            }

            this.getFeaturedProperties();
            this.getAgent(1);
            this.getInterestInput(this.store.selectSnapshot(PropertyState.getSelectedProperty));

            setTimeout(() => {
                this.swipers.forEach((swiper) => {
                    if (swiper) {
                        swiper.setIndex(0);
                        if (swiper.swiper()) {
                            swiper.swiper().lazy.load();
                        }
                    }
                });
            });
        });

        this.action$
            .pipe(ofActionErrored(LoadPropertyDetail), untilDestroyed(this))
            .subscribe((errorCode: ResponseCode) => {
                if (errorCode === ResponseCode.DataNotFound) {
                    this.router.navigate([routePaths.notFound]);
                } else {
                    // TODO: Implement Internal error page
                    this.router.navigate([routePaths.notFound]);
                }
                return of(undefined);
            });

        this.user$.pipe(untilDestroyed(this)).subscribe((user: Partial<UserModel>) => {
            if (user) {
                const { name, email, phone } = user;
                this.contactForm.patchValue({
                    name,
                    email,
                    phone: phone?.replace('+84', ''),
                });
            }
        });
    }

    public ngAfterViewInit() {
        this.clientSideService.doOnClient(() => {
            if (window.innerWidth < 960) {
                this.sidenavOpen = false;
            }
        });
    }

    public onOpenedChange() {
        this.swipers.forEach((swiper) => {
            if (swiper) {
                swiper.update();
            }
        });
    }

    public selectImage(index: number) {
        this.swipers.forEach((swiper) => {
            if (swiper['elementRef'].nativeElement.id === 'main-carousel') {
                swiper.setIndex(index);
            }
        });
    }

    public onIndexChange() {
        let realIndex = 0;
        this.swipers.forEach((swiper, index) => {
            if (swiper['elementRef'].nativeElement.id === 'main-carousel') {
                realIndex = swiper['instance'].realIndex;
                return;
            }
            const elem = swiper['elementRef'].nativeElement;
            if (elem.id === 'small-carousel') {
                swiper.setIndex(realIndex);
                for (let i = 0; i < elem.children[0].children.length; i++) {
                    const element = elem.children[0].children[i];
                    if (element.classList.contains('thumb-' + realIndex)) {
                        element.classList.add('active-thumb');
                    } else {
                        element.classList.remove('active-thumb');
                    }
                }
            }
        });
    }

    public addToCompare() {
        // this.appService.addToCompare(this.property, CompareOverviewComponent);
    }

    public onCompare() {
        // return this.appService.Data.compareList.filter((item) => item.id === this.property.id)[0];
    }

    public addToFavorites() {
        // this.appService.addToFavorites(this.property);
    }

    public onFavorites() {
        // return this.appService.Data.favorites.filter((item) => item.id === this.property.id)[0];
    }

    public getRelatedProperties(id: number, slug: string) {
        this.relatedProperties$ = this.propertyService.getRelatedProperties(id, slug);
    }

    public getFeaturedProperties() {
        this.appService.getFeaturedProperties().subscribe((properties) => {
            this.featuredProperties = properties.slice(0, 3);
        });
    }

    public getAgent(agentId: number = 1) {
        const ids = [1, 2, 3, 4, 5]; //agent ids
        agentId = ids[Math.floor(Math.random() * ids.length)]; //random agent id
        this.agent = this.appService.getAgents().filter((agent) => agent.id === agentId)[0];
    }

    public submitContactForm() {
        if (!this.contactForm.valid) {
            return;
        }

        const propertyId = this.store.selectSnapshot(PropertyState.getSelectedProperty)?.id;

        this.userNotificationService
            .requestPropertyCallback(propertyId, this.contactForm.getRawValue())
            .subscribe(() => {
                this.notificationService.notifySuccess('ownerContactBackRequested');
                this.contactForm.patchValue({
                    message: null,
                });
            });
    }

    public getPropertyMetadata(property: Partial<PropertyModel>, metadataName: string) {
        return getPropertyMetadata(property, metadataName);
    }

    public getFeatureMap(featureIds: number[]) {
        return propertyFeatures.map((p: PropertyFeature) => ({
            value: p,
            checked: findLast(featureIds, (id: number) => id === p) >= 0,
        }));
    }

    public getSaleStatus(property: Partial<PropertyModel>): PropertyStatusModel {
        return findLast(property.statuses, (status: PropertyStatusModel) => status.status === PropertyStatus.ForSale);
    }

    public calcInterest(input: InterestInput, property: Partial<PropertyModel>) {
        input.propertyId = property.id;
        input.propertySlug = property.slug;
        const { routeCommands, queryParams } = this.interestCalculatorService.mapInterestInputToRoutePath(input);
        this.router.navigate(routeCommands, { queryParams });
    }

    public reportProperty() {
        this.dialog.open(ReportPropertyComponent);
    }

    public getPropertyStaticMap(property: Partial<PropertyModel>) {
        const params: HttpParams = new HttpParams()
            .append('key', environment.googleMapsKey)
            .append('language', 'vi')
            .append('region', 'vn')
            .append('scale', '2')
            .append('center', `${property.lat},${property.lng}`)
            .append('zoom', '15')
            .append('markers', `${property.lat},${property.lng}`)
            .append('size', `800x400`);

        return `https://maps.googleapis.com/maps/api/staticmap?${params.toString()}`;
    }

    private getInterestInput(property: Partial<PropertyModel>) {
        const saleStatus: PropertyStatusModel = this.getSaleStatus(property);
        if (!saleStatus) {
            return;
        }

        this.initialInterestInput.next({
            amount: saleStatus.price,
        });
    }
}
