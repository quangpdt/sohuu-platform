import { RouteNames, routePaths } from '../core/route.names';
import { AppDataType, PropertyMetadataName } from '@sohuu-platform/enums';

export class BaseComponent {
    public routeNames = RouteNames;
    public routePaths = routePaths;
    public appDataType = AppDataType;
    public metadataName = PropertyMetadataName;
}
