import { ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit, PLATFORM_ID, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { isPlatformBrowser } from '@angular/common';
import { Pagination, Property } from '../../../app.models';
import { AppService } from '../../../app.service';
import { emailValidator } from '../../../theme/utils/app-validators';
import { SidenavService } from '../../../core/services/sidenav.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { MediaChange, MediaObserver } from '@angular/flex-layout';

@Component({
    selector: 'app-agent',
    templateUrl: './agent.component.html',
    styleUrls: ['./agent.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AgentComponent implements OnInit, OnDestroy {
    public agent: any;
    public agentId: any;
    @ViewChild(MatPaginator) public paginator: MatPaginator;
    public properties: Property[];
    public viewType = 'grid';
    public viewCol = 33.3;
    public count = 12;
    public sort: string;
    public searchFields: any;
    public removedSearchField: string;
    public pagination: Pagination = new Pagination(1, this.count, null, 2, 0, 0);
    public message: string;
    public contactForm: FormGroup;

    constructor(
        public appService: AppService,
        private activatedRoute: ActivatedRoute,
        public fb: FormBuilder,
        @Inject(PLATFORM_ID) private platformId: Object,
        public sidenavService: SidenavService,
        private mediaObserver: MediaObserver
    ) {
        mediaObserver.media$.subscribe((change: MediaChange) => {
            if (change.mqAlias === 'xs') {
                this.viewCol = 100;
            } else if (change.mqAlias === 'sm') {
                this.viewCol = 50;
            } else if (change.mqAlias === 'md') {
                this.viewCol = 50;
            } else {
                this.viewCol = 33.3;
            }
        });
    }

    public ngOnInit() {
        this.activatedRoute.params.pipe(untilDestroyed(this)).subscribe((params) => {
            this.agentId = params['id'];
            this.getAgentById(params['id']);
            this.getProperties();
        });

        this.contactForm = this.fb.group({
            name: ['', Validators.required],
            email: ['', Validators.compose([Validators.required, emailValidator])],
            phone: ['', Validators.required],
            message: ['', Validators.required],
        });
    }

    public ngOnDestroy() {}

    public getAgentById(id) {
        this.agent = this.appService.getAgents().filter((agent) => agent.id === parseInt(id, 10))[0];
    }

    public getProperties() {
        this.appService.getPropertiesByAgentId(this.agentId).subscribe((data) => {
            const result = this.filterData(data);
            if (result.data.length === 0) {
                this.properties.length = 0;
                this.pagination = new Pagination(1, this.count, null, 2, 0, 0);
                this.message = 'No Results Found';
                return false;
            }
            this.properties = result.data;
            this.pagination = result.pagination;
            this.message = null;
        });
    }

    public resetPagination() {
        if (this.paginator) {
            this.paginator.pageIndex = 0;
        }
        this.pagination = new Pagination(1, this.count, null, null, this.pagination.total, this.pagination.totalPages);
    }

    public filterData(data) {
        return this.appService.filterData(
            data,
            this.searchFields,
            this.sort,
            this.pagination.page,
            this.pagination.perPage
        );
    }

    public searchClicked() {
        this.properties.length = 0;
        this.getProperties();
        if (isPlatformBrowser(this.platformId)) {
            window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        }
    }
    public searchChanged(event) {
        event.valueChanges.subscribe(() => {
            this.resetPagination();
            this.searchFields = event.value;
            setTimeout(() => {
                this.removedSearchField = null;
            });
        });
    }
    public removeSearchField(field) {
        this.message = null;
        this.removedSearchField = field;
    }

    public changeCount(count) {
        this.count = count;
        this.properties.length = 0;
        this.resetPagination();
        this.getProperties();
    }
    public changeSorting(sort) {
        this.sort = sort;
        this.properties.length = 0;
        this.getProperties();
    }
    public changeViewType(obj) {
        this.viewType = obj.viewType;
        this.viewCol = obj.viewCol;
    }

    public onPageChange(e) {
        this.pagination.page = e.pageIndex + 1;
        this.getProperties();
        if (isPlatformBrowser(this.platformId)) {
            window.scroll({ top: 0, left: 0, behavior: 'smooth' });
        }
    }

    public onContactFormSubmit(values: Object) {
        if (this.contactForm.valid) {
            console.log(values);
        }
    }
}
