import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AgentsComponent } from './agents.component';
import { AgentComponent } from './agent/agent.component';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDividerModule } from '@angular/material/divider';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatBadgeModule } from '@angular/material/badge';
import { MatInputModule } from '@angular/material/input';

export const routes = [
    { path: '', component: AgentsComponent, pathMatch: 'full' },
    { path: ':id', component: AgentComponent },
];

@NgModule({
    declarations: [AgentsComponent, AgentComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        MatIconModule,
        MatCardModule,
        MatButtonModule,
        MatSidenavModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        MatPaginatorModule,
        MatDividerModule,
        MatTooltipModule,
        MatBadgeModule,
        MatInputModule,
    ],
})
export class AgentsModule {}
