import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AppService } from '../../app.service';
import { BaseComponent } from '../base.component';

@Component({
    selector: 'app-agents',
    templateUrl: './agents.component.html',
    styleUrls: ['./agents.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AgentsComponent extends BaseComponent implements OnInit {
    public agents;
    constructor(public appService: AppService) {
        super();
    }

    public ngOnInit() {
        this.agents = this.appService.getAgents();
    }
}
