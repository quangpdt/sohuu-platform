import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { AppService } from '../../app.service';
import { Property } from '../../app.models';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ShowHeaderBgImage } from '../../core/states/ui/ui.action';
import { LoadHomePageProperties, ResetHomePageProperties } from '../../core/states/property/property.action';
import { PropertyState } from '../../core/states/property/property.state';
import { ProjectModel, PropertyModel } from '@sohuu-platform/models';
import { PropertyPublicService } from '../../core/services/property.public-service';
import { Router } from '@angular/router';
import { PropertySearchCriteria } from '@sohuu-platform/interfaces';
import { ProjectState } from '@sohuu/app/core/states/project/project.state';
import { LoadHomePageProjects } from '@sohuu/app/core/states/project/project.action';
import { ProjectSortOption } from '@sohuu-platform/enums';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent implements OnInit, OnDestroy {
    public searchCriteria: PropertySearchCriteria;
    public featuredProperties: Property[];

    @Select(PropertyState.getHomePageProperties)
    public homePageProperties$: Observable<Partial<PropertyModel>[]>;

    @Select(ProjectState.getHomePageProjects)
    public homePageProjects$: Observable<Partial<ProjectModel>[]>;

    constructor(
        public appService: AppService,
        private store: Store,
        private propertyPublicService: PropertyPublicService,
        private router: Router
    ) {}

    public ngOnInit() {
        this.store.dispatch(new ShowHeaderBgImage(true));
        this.getProperties();
        this.getProjects();
        this.getFeaturedProperties();
    }

    public ngOnDestroy() {
        this.store.dispatch(new ResetHomePageProperties());
    }

    public getProperties() {
        this.store.dispatch(new LoadHomePageProperties());
    }
    public getProjects(sortOption: ProjectSortOption = ProjectSortOption.RecentlyAdded) {
        this.store.dispatch(new LoadHomePageProjects(sortOption))
    }

    public searchClicked(event: PropertySearchCriteria) {
        const { routeCommands, queryParams } = this.propertyPublicService.mapSearchCriteriaToRoutePath(event);
        this.router.navigate(routeCommands, { queryParams });
    }

    public getFeaturedProperties() {
        this.appService.getFeaturedProperties().subscribe((properties) => {
            this.featuredProperties = properties;
        });
    }
}
