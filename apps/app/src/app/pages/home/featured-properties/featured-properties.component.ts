import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-featured-properties',
    templateUrl: './featured-properties.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeaturedPropertiesComponent {
    @Input('properties') public featuredProperties;
}
