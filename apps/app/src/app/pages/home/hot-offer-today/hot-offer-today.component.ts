import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { AppService } from '../../../app.service';

@Component({
    selector: 'app-hot-offer-today',
    templateUrl: './hot-offer-today.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HotOfferTodayComponent implements OnInit {
    @Input('propertyId') public propertyId;
    public property;
    constructor(public appService: AppService) {}

    public ngOnInit() {
        this.appService.getPropertyById(this.propertyId).subscribe((property) => {
            this.property = property;
        });
    }
}
