import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { Store } from '@ngxs/store';
import { Logout } from '../../../core/states/app/app.action';

@Component({
    selector: 'app-logout',
    template: '',
})
export class LogoutComponent implements OnInit {
    constructor(private authenticationService: AuthenticationService, private store: Store) {}

    public ngOnInit(): void {
        this.store.dispatch(new Logout());
    }
}
