import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ClientSideService } from '../../../core/services/client-side.service';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { LoginWithThirdParty } from '../../../core/states/app/app.action';

@Component({
    selector: 'app-oauth',
    templateUrl: './oauth.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OAuthComponent implements OnInit, AfterViewInit {
    private isFirstTimeLogin = false;

    constructor(private clientSideService: ClientSideService, private router: Router, private store: Store) {}

    public ngOnInit(): void {
        if (this.router.url.includes('first-time')) {
            this.isFirstTimeLogin = true;
        }
    }

    public ngAfterViewInit(): void {
        this.clientSideService.doOnClient(() => document.getElementById('preloader').classList.add('hide'));
        if (screen.width > 1024) {
            if (window.opener) {
                window.opener.postMessage({
                    isFirstTimeLogin: this.isFirstTimeLogin,
                });
                window.close();
            }
            return;
        }
        this.store.dispatch(new LoginWithThirdParty(this.isFirstTimeLogin));
    }
}
