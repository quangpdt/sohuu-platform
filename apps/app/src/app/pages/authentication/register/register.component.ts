import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { emailValidator, matchingPasswords } from '../../../theme/utils/app-validators';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { Subject } from 'rxjs';
import { ResponseCode } from '@sohuu-platform/enums';
import { NotificationService } from '../../../core/services/notification.service';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { take } from 'rxjs/operators';
import { BaseComponent } from '../../base.component';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RegisterComponent extends BaseComponent implements OnInit, OnDestroy {
    public registerForm: FormGroup;
    public isSuccess$ = new Subject<boolean>();
    public hide = true;

    constructor(
        public fb: FormBuilder,
        public router: Router,
        private authenticationService: AuthenticationService,
        private notificationService: NotificationService,
        private recaptchaV3Service: ReCaptchaV3Service
    ) {
        super();
    }

    public ngOnInit() {
        this.registerForm = this.fb.group(
            {
                name: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
                email: ['', Validators.compose([Validators.required, emailValidator])],
                password: ['', Validators.required],
                confirmPassword: ['', Validators.required],
            },
            { validator: matchingPasswords('password', 'confirmPassword') }
        );
        this.isSuccess$.pipe(untilDestroyed(this));
    }

    public ngOnDestroy(): void {}

    public onRegisterFormSubmit(): void {
        if (!this.registerForm.valid) {
            return;
        }

        this.registerForm.disable();
        this.recaptchaV3Service
            .execute('importantAction')
            .pipe(take(1))
            .subscribe(
                (token) => this.register(),
                (error) => {
                    this.notificationService.notifyError('loginUnsuccessfully');
                }
            );
    }

    private register() {
        this.authenticationService.register(this.registerForm.getRawValue()).subscribe(
            () => {
                this.isSuccess$.next(true);
            },
            (error) => {
                this.registerForm.enable();
                if (error.graphQLErrors) {
                    const message = error.graphQLErrors[0].message;
                    if (message === ResponseCode.DataAlreadyExists) {
                        this.notificationService.notifyError('errorUserAlreadyExists');
                    } else {
                        this.notificationService.notifyError('errorGeneral');
                    }
                }
            }
        );
    }
}
