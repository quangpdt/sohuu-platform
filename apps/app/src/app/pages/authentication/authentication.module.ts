import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { TranslateModule } from '@ngx-translate/core';
import { NonAuthenticationGuard } from '../../core/guards/non-authentication.guard';
import { LogoutComponent } from './logout/logout.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ActivateUserComponent } from './activate-user/activate-user.component';
import { AuthenticationGuard } from '../../core/guards/authentication.guard';
import { PhoneVerificationComponent } from './phone-verification/phone-verification.component';
import { NgxMaskModule } from 'ngx-mask';
import { RouteNames } from '../../core/route.names';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

export const routes: Routes = [
    {
        path: '',
        canActivate: [NonAuthenticationGuard],
        children: [
            {
                path: RouteNames.Login,
                component: LoginComponent,
            },
            { path: RouteNames.Register, component: RegisterComponent },
            { path: RouteNames.ForgotPassword, component: ForgotPasswordComponent },
            { path: RouteNames.RecoverPassword + '/:email/:token', component: ResetPasswordComponent },
            { path: RouteNames.ActivateAccount + '/:email/:token', component: ActivateUserComponent },
        ],
    },
    {
        path: '',
        canActivate: [AuthenticationGuard],
        children: [
            {
                path: RouteNames.VerifyAccount,
                component: PhoneVerificationComponent,
            },
        ],
    },
    { path: RouteNames.Logout, component: LogoutComponent },
];

@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
        LogoutComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        ActivateUserComponent,
        PhoneVerificationComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        TranslateModule,
        NgxMaskModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatInputModule,
        MatTooltipModule,
        MatProgressSpinnerModule,
    ],
})
export class AuthenticationModule {}
