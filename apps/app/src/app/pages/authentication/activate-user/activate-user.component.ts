import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { NotificationService } from '../../../core/services/notification.service';
import { Subject } from 'rxjs';
import { ResponseCode } from '@sohuu-platform/enums';
import { timeout } from '../../../shared/util.function';
import { Store } from '@ngxs/store';
import { Initialize } from '../../../core/states/app/app.action';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { BaseComponent } from '../../base.component';

@Component({
    selector: 'app-activate-user',
    templateUrl: './activate-user.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivateUserComponent extends BaseComponent implements OnInit, OnDestroy {
    public isUserActivated$: Subject<boolean> = new Subject<boolean>();
    public message: string;
    public buttonText: string;

    private email: string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private notificationService: NotificationService,
        private store: Store
    ) {
        super();
    }

    public ngOnInit(): void {
        this.route.paramMap.subscribe((params) => {
            this.email = params.get('email');
            const token = params.get('token');

            this.activateUser(this.email, token);
        });
        this.isUserActivated$.pipe(untilDestroyed(this));
    }

    public ngOnDestroy(): void {}

    private activateUser(email: string, token: string) {
        this.authenticationService.activateUser(email, token).subscribe(async () => {
            this.message = 'accountActivatedSuccessfully';
            this.isUserActivated$.next(true);
            await timeout();
            this.store.dispatch(new Initialize());
        }, this.handleError.bind(this));
    }

    public reActivateAccount() {
        this.authenticationService.reActivateUser(this.email).subscribe(() => {
            this.message = 'accountReactivationRequested';
            this.buttonText = 'backToHomePage';
            this.isUserActivated$.next(true);
        }, this.handleError.bind(this));
    }

    private handleError(error) {
        if (error.graphQLErrors) {
            const message = error.graphQLErrors[0].message;
            switch (message) {
                case ResponseCode.GeneralError:
                    this.message = 'errorGeneral';
                    this.buttonText = 'backToHomePage';
                    break;
                case ResponseCode.DataNotFound:
                    this.message = 'errorCannotActivateNotFoundUser';
                    this.buttonText = 'backToHomePage';
                    break;
                case ResponseCode.ActivationCodeExpired:
                    this.message = 'errorActivationCodeExpired';
                    this.buttonText = 'reActivateYourAccount';
                    break;
                default:
                    this.message = 'errorGeneral';
                    this.buttonText = 'backToHomePage';
                    break;
            }
        }

        this.isUserActivated$.next(false);
    }
}
