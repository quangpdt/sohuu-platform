import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { emailValidator } from '../../../theme/utils/app-validators';
import { Subject } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { take } from 'rxjs/operators';
import { NotificationService } from '../../../core/services/notification.service';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { BaseComponent } from '../../base.component';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ForgotPasswordComponent extends BaseComponent implements OnInit, OnDestroy {
    public forgotPasswordForm: FormGroup;
    public isSubmitted$ = new Subject<boolean>();

    constructor(
        public fb: FormBuilder,
        public router: Router,
        private authenticationService: AuthenticationService,
        private notificationService: NotificationService,
        private recaptchaV3Service: ReCaptchaV3Service
    ) {
        super();
    }

    public ngOnInit(): void {
        this.forgotPasswordForm = this.fb.group({
            email: [null, Validators.compose([Validators.required, emailValidator])],
        });
        this.isSubmitted$.pipe(untilDestroyed(this));
    }

    public ngOnDestroy(): void {}

    public submitForgotPassword() {
        if (!this.forgotPasswordForm.valid) {
            return;
        }

        this.recaptchaV3Service
            .execute('importantAction')
            .pipe(take(1))
            .subscribe(
                (token) => this.requestForgotPassword(),
                (error) => {
                    this.notificationService.notifyError('loginUnsuccessfully');
                }
            );
    }

    public get forgotControls() {
        return this.forgotPasswordForm.controls;
    }

    private requestForgotPassword() {
        this.authenticationService.forgotPassword(this.forgotPasswordForm.get('email').value).subscribe(() => {
            this.isSubmitted$.next(true);
        });
    }
}
