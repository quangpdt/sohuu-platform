import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { NotificationService } from '../../../core/services/notification.service';
import { BehaviorSubject, interval } from 'rxjs';
import { Store } from '@ngxs/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { finalize, take } from 'rxjs/operators';
import { ClientSideService } from '../../../core/services/client-side.service';
import { ResponseCode } from '@sohuu-platform/enums';
import { routePaths } from '../../../core/route.names';
import { BaseComponent } from '../../base.component';

enum SMSSendingStatus {
    Pending,
    Sending,
    Sent,
    Failed,
}
@Component({
    selector: 'app-phone-verification',
    templateUrl: './phone-verification.component.html',
    styleUrls: ['./phone-verification.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PhoneVerificationComponent extends BaseComponent implements OnInit, OnDestroy, AfterViewInit {
    public contactForm: FormGroup;

    public sendingStatus = SMSSendingStatus;
    public smsSendingStatus$: BehaviorSubject<SMSSendingStatus> = new BehaviorSubject<SMSSendingStatus>(
        SMSSendingStatus.Pending
    );
    public countDownTimer: BehaviorSubject<number> = new BehaviorSubject<number>(60);

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private authenticationService: AuthenticationService,
        private notificationService: NotificationService,
        private store: Store,
        private formBuilder: FormBuilder,
        private clientSideService: ClientSideService
    ) {
        super();
    }

    public ngOnInit(): void {
        this.contactForm = this.formBuilder.group({
            phone: [null, Validators.required],
            verificationCode: [null, Validators.required],
        });
    }

    public ngAfterViewInit(): void {
        this.clientSideService.doOnClient(() => {
            (window as any).recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-placeholder', {
                size: 'invisible',
                callback: (recapchaToken) => {
                    this.sendVerificationCode(recapchaToken);
                },
            });
        });
    }

    public ngOnDestroy(): void {}

    public async onInfoFormSubmit(): Promise<void> {
        this.contactForm.disable();
        const code = this.contactForm.get('verificationCode').value;
        this.authenticationService.validatePhoneNumber(code).subscribe(
            async () => {
                await this.notificationService.notifySuccess('verificationCodeSubmitted');
                this.router.navigate([routePaths.profile]);
            },
            (e) => {
                this.contactForm.enable();
                switch (e) {
                    case ResponseCode.PhoneValidationFailed:
                        this.notificationService.notifyError('incorrectVerificationCode');
                        break;
                    case ResponseCode.PhoneNumberAlreadyUsed:
                        this.notificationService.notifyError(ResponseCode.PhoneNumberAlreadyUsed);
                        break;
                }
            }
        );
    }

    public async verifyRecaptcha() {
        try {
            this.smsSendingStatus$.next(SMSSendingStatus.Sending);
            const appVerifier = (window as any).recaptchaVerifier;
            await appVerifier.verify();
        } catch (e) {
            this.smsSendingStatus$.next(SMSSendingStatus.Failed);
            (window as any).recaptchaVerifier.render().then((widgetId) => {
                grecaptcha.reset(widgetId);
            });
            this.setCountDownTimer();
        }
    }

    public async sendVerificationCode(recaptchaToken: string): Promise<void> {
        this.contactForm.get('phone').disable();
        const phoneNumber = '+84' + this.contactForm.get('phone').value;
        this.authenticationService.requestPhoneVerification(phoneNumber, recaptchaToken).subscribe(
            () => {
                this.smsSendingStatus$.next(SMSSendingStatus.Sent);
                this.setCountDownTimer();
            },
            (e) => {
                (window as any).recaptchaVerifier.render().then((widgetId) => {
                    grecaptcha.reset(widgetId);
                });

                if (e === ResponseCode.PhoneWasVerifiedBefore) {
                    this.resetCountDownTimer();
                    this.notificationService.notifyError(ResponseCode.PhoneWasVerifiedBefore, false);
                    return;
                } else {
                    this.smsSendingStatus$.next(SMSSendingStatus.Failed);
                    this.setCountDownTimer();
                }
            }
        );
    }

    private setCountDownTimer() {
        interval(1000)
            .pipe(
                take(60),
                finalize(() => this.resetCountDownTimer())
            )
            .subscribe((value) => this.countDownTimer.next(60 - value));
    }

    private resetCountDownTimer() {
        this.smsSendingStatus$.next(SMSSendingStatus.Pending);
        this.countDownTimer.next(60);
        this.contactForm.get('phone').enable();
    }
}
