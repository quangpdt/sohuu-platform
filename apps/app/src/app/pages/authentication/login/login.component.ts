import { ChangeDetectionStrategy, Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { emailValidator } from '../../../theme/utils/app-validators';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { NotificationService } from '../../../core/services/notification.service';
import { Store } from '@ngxs/store';
import { Login, LoginWithThirdParty } from '../../../core/states/app/app.action';
import { OAuthProvider } from '@sohuu-platform/enums';
import { ClientSideService } from '../../../core/services/client-side.service';
import { ReCaptchaV3Service } from 'ng-recaptcha';
import { take } from 'rxjs/operators';
import * as get from 'lodash/get';
import { BaseComponent } from '../../base.component';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent extends BaseComponent implements OnInit, OnDestroy {
    public loginForm: FormGroup;
    public hide = true;

    private windowObjectReference: Window;
    private previousUrl: string;
    public oAuthProvider = OAuthProvider;

    constructor(
        public fb: FormBuilder,
        public router: Router,
        private authenticationService: AuthenticationService,
        private notificationService: NotificationService,
        private store: Store,
        private zone: NgZone,
        private clientSideService: ClientSideService,
        private recaptchaV3Service: ReCaptchaV3Service
    ) {
        super();
    }

    public ngOnInit() {
        this.loginForm = this.fb.group({
            email: [null, Validators.compose([Validators.required, emailValidator])],
            password: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
        });
    }

    public ngOnDestroy(): void {
        this.clientSideService.doOnClient(() =>
            window.removeEventListener('message', (event) => this.receiveMessage(event), true)
        );
    }

    public onLoginFormSubmit(): void {
        if (!this.loginForm.valid) {
            return;
        }

        const { email, password } = this.loginForm.value;
        this.recaptchaV3Service
            .execute('importantAction')
            .pipe(take(1))
            .subscribe(
                (token) => this.store.dispatch(new Login(email, password, token)),
                (error) => {
                    this.notificationService.notifyError('loginUnsuccessfully');
                }
            );
    }

    public startOAuth(oAuthProvider: OAuthProvider): void {
        let url;
        if (oAuthProvider === OAuthProvider.Google) {
            url = `${environment.baseUrl.api}/oauth/google`;
        } else {
            url = `${environment.baseUrl.api}/oauth/facebook`;
        }

        if (screen.width > 1024) {
            this.startOAuthForDesktop(url);
            return;
        }

        this.startOAuthForMobile(url);
    }

    private startOAuthForDesktop(url: string) {
        window.removeEventListener('message', (event) => this.receiveMessage(event), true);

        const windowFeatures = 'toolbar=no, menubar=no, width=600, height=700, top=100, left=100';

        if (this.windowObjectReference === null || (this.windowObjectReference && this.windowObjectReference.closed)) {
            this.windowObjectReference = window.open(url, name, windowFeatures);
        } else if (this.previousUrl !== url) {
            this.windowObjectReference = window.open(url, name, windowFeatures);
            this.windowObjectReference.focus();
        } else {
            this.windowObjectReference.focus();
        }
        window.addEventListener('message', (event) => this.receiveMessage(event), {
            once: true,
        });

        this.previousUrl = url;
    }

    private startOAuthForMobile(url: string) {
        window.location.href = url;
    }

    public receiveMessage(message) {
        this.zone.run(() => {
            this.store.dispatch(new LoginWithThirdParty(!!get(message, 'data.isFirstTimeLogin')));
        });
    }
}
