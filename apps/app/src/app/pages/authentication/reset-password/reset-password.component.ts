import { ChangeDetectionStrategy, Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { NotificationService } from '../../../core/services/notification.service';
import { Store } from '@ngxs/store';
import { emailValidator, matchingPasswords } from '../../../theme/utils/app-validators';
import { routePaths } from '../../../core/route.names';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResetPasswordComponent implements OnInit {
    public resetPasswordForm: FormGroup;
    public hide = true;

    constructor(
        public fb: FormBuilder,
        public router: Router,
        private authenticationService: AuthenticationService,
        private notificationService: NotificationService,
        private store: Store,
        private zone: NgZone,
        private route: ActivatedRoute
    ) {}

    public ngOnInit(): void {
        this.resetPasswordForm = this.fb.group(
            {
                email: [{ value: null, disabled: true }, Validators.compose([Validators.required, emailValidator])],
                token: [{ value: null, disabled: true }, Validators.required],
                password: [null, Validators.required],
                confirmPassword: [null, Validators.required],
            },
            { validator: matchingPasswords('password', 'confirmPassword') }
        );

        this.route.paramMap.subscribe((params) => {
            this.resetPasswordForm.patchValue({
                email: params.get('email'),
                token: params.get('token'),
            });
        });
    }

    public submitResetPassword() {
        if (!this.resetPasswordForm.valid) {
            return;
        }

        this.authenticationService.resetPassword(this.resetPasswordForm.getRawValue()).subscribe(async (isSuccess) => {
            if (isSuccess) {
                await this.notificationService.notifySuccess('passwordResetSuccessfully');
                return this.router.navigate([routePaths.login]);
            }

            await this.notificationService.notifyError('passwordResetFailed');
            return this.router.navigate([routePaths.forgotPassword]);
        });
    }

    public get resetControls() {
        return this.resetPasswordForm.controls;
    }
}
