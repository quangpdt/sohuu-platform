import {
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    HostListener,
    Inject,
    OnDestroy,
    OnInit,
    PLATFORM_ID,
    ViewChild,
    ViewContainerRef,
} from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { UIState } from '../core/states/ui/ui.state';
import { Observable } from 'rxjs';
import { DisplayFixedMainToolbar } from '../core/states/ui/ui.action';
import { ClientSideService } from '../core/services/client-side.service';
import { MatSidenav } from '@angular/material/sidenav';
import { SidenavService } from '../core/services/sidenav.service';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
    selector: 'app-pages',
    templateUrl: './pages.component.html',
    styleUrls: ['./pages.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PagesComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild('sidenav') public sidenav: any;
    @ViewChild('rightSidenav', { static: true }) private rightSidenav: MatSidenav;
    @ViewChild('content', { static: true, read: ViewContainerRef }) private rightSidenavContent: ViewContainerRef;

    public showBackToTop = false;
    public scrolledCount = 0;

    @Select(UIState.hasBgImage)
    public hasBgImage$: Observable<boolean>;

    @Select(UIState.shouldDisplayFixedMainToolbar)
    public displayFixedMainToolbar$: Observable<boolean>;

    constructor(
        public router: Router,
        @Inject(PLATFORM_ID) private platformId: Object,
        private store: Store,
        private clientSideService: ClientSideService,
        private sidenavService: SidenavService
    ) {}

    public ngOnInit() {
        this.sidenavService.setPanel(this.rightSidenav);
        this.sidenavService.setContentVcf(this.rightSidenavContent);
    }

    public ngAfterViewInit() {
        this.clientSideService.doOnClient(() => {
            document.getElementById('preloader').classList.add('hide');
        });
        this.clientSideService.doOnClient(() => {
            this.router.events.pipe(untilDestroyed(this)).subscribe((event) => {
                if (event instanceof NavigationEnd) {
                    this.sidenav.close();
                    this.sidenavService.close();
                    this.store.dispatch(new DisplayFixedMainToolbar(false));

                    setTimeout(() => this.scrollToTop());
                }
            });
        });
    }

    public ngOnDestroy() {}

    @HostListener('window:scroll') public onWindowScroll() {
        const scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop);
        this.showBackToTop = scrollTop > 300;
    }

    public scrollToTop() {
        window.scroll({ top: 0, left: 0, behavior: 'smooth' });
    }
}
