import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { CompareComponent } from './compare.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

export const routes = [{ path: '', component: CompareComponent, pathMatch: 'full' }];

@NgModule({
    declarations: [CompareComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        MatChipsModule,
        MatCardModule,
        MatIconModule,
        MatButtonModule,
    ],
})
export class CompareModule {}
