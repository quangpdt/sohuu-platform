import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { AppService } from '../../app.service';
import { SwiperConfigInterface, SwiperDirective } from '@sohuu-platform/swiper';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { Property } from '../../app.models';
import { BaseComponent } from '../base.component';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
    selector: 'app-compare',
    templateUrl: './compare.component.html',
    styleUrls: ['./compare.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CompareComponent extends BaseComponent implements OnInit, OnDestroy {
    @ViewChild(SwiperDirective) public directiveRef: SwiperDirective;
    public config: SwiperConfigInterface = {};
    constructor(public appService: AppService, public mediaObserver: MediaObserver) {
        super();
    }

    public ngOnInit() {
        this.config = {
            observer: true,
            slidesPerView: 4,
            spaceBetween: 16,
            keyboard: false,
            navigation: false,
            pagination: false,
            simulateTouch: false,
            grabCursor: true,
            loop: false,
            preloadImages: true,
            lazy: false,
            breakpoints: {
                600: {
                    slidesPerView: 1,
                },
                960: {
                    slidesPerView: 2,
                },
                1280: {
                    slidesPerView: 3,
                },
            },
        };
        this.watchForChanges();
    }

    public ngOnDestroy() {}

    public disableSwiper() {
        setTimeout(() => {
            if (this.directiveRef) {
                this.config.keyboard = false;
                this.config.navigation = false;
                this.config.simulateTouch = false;
                this.directiveRef.update();
            }
        });
    }
    public enableSwiper() {
        setTimeout(() => {
            if (this.directiveRef) {
                this.config.keyboard = true;
                this.config.navigation = {
                    nextEl: '.carousel-next',
                    prevEl: '.carousel-prev',
                };
                this.config.simulateTouch = true;
                this.directiveRef.update();
            }
        });
    }

    public clear() {
        this.appService.Data.compareList.length = 0;
    }

    public remove(property: Property) {
        const index: number = this.appService.Data.compareList.indexOf(property);
        if (index !== -1) {
            this.appService.Data.compareList.splice(index, 1);
        }
        this.watchForChanges();
    }

    public watchForChanges() {
        this.mediaObserver.media$.pipe(untilDestroyed(this)).subscribe((change: MediaChange) => {
            if (change.mqAlias === 'xs' && this.appService.Data.compareList.length > 1) {
                this.enableSwiper();
            } else if (change.mqAlias === 'sm' && this.appService.Data.compareList.length > 2) {
                this.enableSwiper();
            } else if (change.mqAlias === 'md' && this.appService.Data.compareList.length > 3) {
                this.enableSwiper();
            } else if (change.mqAlias === 'lg' && this.appService.Data.compareList.length > 4) {
                this.enableSwiper();
            } else if (change.mqAlias === 'xl' && this.appService.Data.compareList.length > 4) {
                this.enableSwiper();
            } else {
                this.disableSwiper();
            }
        });
    }
}
