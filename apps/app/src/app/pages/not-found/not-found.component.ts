import { AfterViewInit, ChangeDetectionStrategy, Component } from '@angular/core';
import { Router } from '@angular/router';
import { ClientSideService } from '../../core/services/client-side.service';
import { routePaths } from '../../core/route.names';

@Component({
    selector: 'app-not-found',
    templateUrl: './not-found.component.html',
    styleUrls: ['./not-found.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotFoundComponent implements AfterViewInit {
    constructor(public router: Router, private clientSideService: ClientSideService) {}

    public goHome(): void {
        this.router.navigate([routePaths.home]);
    }

    public ngAfterViewInit() {
        this.clientSideService.doOnClient(() => document.getElementById('preloader').classList.add('hide'));
    }
}
