import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { FaqComponent } from './faq.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';

export const routes = [{ path: '', component: FaqComponent, pathMatch: 'full' }];

@NgModule({
    declarations: [FaqComponent],
    imports: [CommonModule, RouterModule.forChild(routes), SharedModule, MatExpansionModule, MatButtonModule],
})
export class FaqModule {}
