import { ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Property } from '../../../app.models';
import { AppService } from '../../../app.service';
import { BaseComponent } from '../../base.component';

@Component({
    selector: 'app-favorites',
    templateUrl: './favorites.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FavoritesComponent extends BaseComponent implements OnInit {
    public displayedColumns: string[] = ['id', 'image', 'title', 'actions'];
    public dataSource: MatTableDataSource<Property>;
    @ViewChild(MatPaginator) public paginator: MatPaginator;
    @ViewChild(MatSort) public sort: MatSort;

    constructor(public appService: AppService) {
        super();
    }

    public ngOnInit() {
        this.dataSource = new MatTableDataSource(this.appService.Data.favorites);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }

    public remove(property: Property) {
        const index: number = this.dataSource.data.indexOf(property);
        if (index !== -1) {
            this.dataSource.data.splice(index, 1);
            this.dataSource = new MatTableDataSource<Property>(this.dataSource.data);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
        }
    }

    public applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
}
