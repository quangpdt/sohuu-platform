import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Select } from '@ngxs/store';
import { AppState } from '../../core/states/app/app.state';
import { Observable } from 'rxjs';
import { UserModel } from '@sohuu-platform/models';
import { routePaths } from '../../core/route.names';
import { SidenavService } from '../../core/services/sidenav.service';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AccountComponent {
    @Select(AppState.getUser)
    public user$: Observable<UserModel>;

    public links = [
        { name: 'profile', href: routePaths.profile, icon: 'person' },
        { name: 'myProperties', href: routePaths.myProperties, icon: 'view_list' },
        { name: 'myNotifications', href: routePaths.myNotifications, icon: 'notifications_active' },
        { name: 'favorites', href: routePaths.myFavorites, icon: 'favorite' },
        {
            name: 'submitProperty',
            href: routePaths.submitProperty,
            icon: 'add_circle',
        },
        {
            name: 'logout',
            href: routePaths.logout,
            icon: 'power_settings_new',
        },
    ];
    constructor(public sidenavService: SidenavService) {}
}
