import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { PropertyService } from '../../../core/services/property.service';
import { PropertyModel } from '@sohuu-platform/models';
import { Property } from '../../../app.models';
import { Router } from '@angular/router';
import { NotificationService } from '../../../core/services/notification.service';
import { routePaths } from '../../../core/route.names';
import { BaseComponent } from '../../base.component';
import { SubmitStatus } from '@sohuu-platform/enums';

@Component({
    selector: 'app-my-properties',
    templateUrl: './my-properties.component.html',
    styleUrls: ['./my-properties.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyPropertiesComponent extends BaseComponent implements OnInit {
    public displayedColumns: string[] = ['image', 'title', 'published', 'status', 'views', 'actions'];
    public dataSource: MatTableDataSource<Partial<PropertyModel>>;
    public submitStatus = SubmitStatus;

    constructor(
        private propertyService: PropertyService,
        private router: Router,
        private notificationService: NotificationService
    ) {
        super();
    }

    public ngOnInit() {
        this.dataSource = new MatTableDataSource();
        this.propertyService.getOwnedProperties().subscribe((properties) => {
            this.dataSource.data = properties;
        });
    }

    public remove(property: Property) {}

    public isProcessingProperty(property: Partial<PropertyModel>) {
        return property.submitStatus === SubmitStatus.Processing;
    }

    public isExpiredProperty(property: Partial<PropertyModel>) {
        return property.submitStatus === SubmitStatus.Expired;
    }

    public getPropertySubmitStatus(property): string {
        switch (property.submitStatus) {
            case SubmitStatus.Pending:
                return 'pending';
            case SubmitStatus.Processing:
                return 'processing';
            case SubmitStatus.Active:
                return 'active';
            case SubmitStatus.Expired:
                return 'expired';
        }
    }

    public async editProperty(property: PropertyModel) {
        if (property.submitStatus === SubmitStatus.Processing) {
            return;
        }

        if (property.submitStatus !== SubmitStatus.Active) {
            return this.router.navigate([routePaths.editProperty, property.id]);
        }

        const shouldRedirect = await this.notificationService.notifyConfirmation(
            'notification',
            'activePropertyWillBeSentBackToPendingStatus'
        );

        if (!shouldRedirect) {
            return;
        }
        return this.router.navigate([routePaths.editProperty, property.id]);
    }

    public async reSubmitExpiredProperty(property: PropertyModel) {
        const shouldReSubmit = await this.notificationService.notifyConfirmation(
            'notification',
            'expiredPropertyWillBeSentBackToPendingStatus'
        );

        if (!shouldReSubmit) {
            return;
        }

        this.propertyService.reSubmitProperty(property.id).subscribe(() => {
            property.submitStatus = SubmitStatus.Pending;
        });
    }
}
