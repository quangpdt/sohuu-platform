import { ChangeDetectionStrategy, Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BaseComponent } from '../../../base.component';
import { UserNotificationModel } from '@sohuu-platform/models';

@Component({
    selector: 'app-my-notification-detail',
    templateUrl: './my-notification-detail.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyNotificationDetailComponent extends BaseComponent implements OnInit {
    public notification: Partial<UserNotificationModel>;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
        super();
    }

    public ngOnInit() {
        this.notification = this.data.notification;
    }

    public get parsedMessage() {
        return this.notification.parsedMessage;
    }
}
