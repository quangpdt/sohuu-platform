import { AfterViewInit, ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { UserNotificationService } from '../../../core/services/user-notification.service';
import { MatTableDataSource } from '@angular/material/table';
import { UserNotificationModel } from '@sohuu-platform/models';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { BaseComponent } from '../../base.component';
import { AppDataPipe } from '../../../shared/pipes/app-data.pipe';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngxs/store';
import { GetSummaryNotifications } from '../../../core/states/app/app.action';
import { NotificationService } from '../../../core/services/notification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MyNotificationDetailComponent } from './my-notification-detail/my-notification-detail.component';
import produce from 'immer';
import * as findLast from 'lodash/findLast';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { AppDataType, NotificationStatus, NotificationType } from '@sohuu-platform/enums';

@Component({
    selector: 'app-my-notifications',
    templateUrl: './my-notifications.component.html',
    styleUrls: ['./my-notifications.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyNotificationsComponent extends BaseComponent implements OnInit, AfterViewInit, OnDestroy {
    @ViewChild(MatPaginator) public paginator: MatPaginator;

    public displayedColumns: string[] = ['createdAt', 'type', 'actions'];
    public dataSource: MatTableDataSource<Partial<UserNotificationModel>>;
    public length$ = new BehaviorSubject(0);
    public pageSize = 5;
    public pageIndex = 0;
    public notificationStatus = NotificationStatus;

    constructor(
        private userNotificationService: UserNotificationService,
        private translateService: TranslateService,
        private store: Store,
        private notificationService: NotificationService,
        private router: Router,
        private matDialog: MatDialog,
        private route: ActivatedRoute
    ) {
        super();
    }

    public ngOnInit() {
        this.dataSource = new MatTableDataSource();
    }

    public ngAfterViewInit() {
        this.getNotifications(this.pageSize, 0);
        this.route.queryParamMap.pipe(untilDestroyed(this)).subscribe((paramMap) => {
            const notificationId = paramMap.get('id');
            console.log(notificationId);
            if (!notificationId || isNaN(parseInt(notificationId, 10))) {
                return;
            }

            this.markAsRead(parseInt(notificationId, 10));
        });
    }

    public ngOnDestroy() {}

    public pageChanged(pageEvent: PageEvent) {
        return this.getNotifications(pageEvent.pageSize, pageEvent.pageIndex);
    }

    private getNotifications(pageSize: number, pageIndex?: number) {
        if (pageIndex !== undefined) {
            this.pageIndex = pageIndex;
        }

        this.userNotificationService.getNotifications(this.pageIndex, pageSize).subscribe((data) => {
            this.dataSource.data = data.results;
            this.length$.next(data.total);
        });
    }

    public getMessageContent(notification: Partial<UserNotificationModel>): Observable<any> {
        const translateStr = new AppDataPipe().transform(notification.type, AppDataType.NotificationTypeInDetail);
        switch (notification.type) {
            case NotificationType.RequestCallback:
                const property = notification.parsedMessage.property;
                return this.translateService.get(translateStr, {
                    name: notification.parsedMessage.name,
                    propertyLink: this.router.createUrlTree([
                        this.routePaths.properties,
                        property.id + '-' + property.slug,
                    ]),
                    propertyTitle: property.title,
                });
        }
        return of('');
    }

    public markAsRead(id: number) {
        this.userNotificationService.markAsRead(id).subscribe(() => {
            this.dataSource.data = produce(this.dataSource.data, (draft) => {
                const item: Partial<UserNotificationModel> = findLast(draft, (p) => p.id === id);
                if (!item) {
                    return;
                }
                item.status = NotificationStatus.Read;
            });
            this.store.dispatch(new GetSummaryNotifications());
        });
    }

    public markAllAsRead() {
        this.userNotificationService.markAllAsRead().subscribe(() => {
            this.store.dispatch(new GetSummaryNotifications());
            this.getNotifications(this.pageSize, 0);
        });
    }

    public async deleteAll() {
        const confirmation = await this.notificationService.notifyConfirmation(
            'notification',
            'deleteAllNotificationsConfirmation'
        );

        if (!confirmation) {
            return;
        }
        this.userNotificationService.deleteAll().subscribe(() => {
            this.store.dispatch(new GetSummaryNotifications());
            this.getNotifications(this.pageSize, 0);
        });
    }

    public async delete(notification: Partial<UserNotificationModel>) {
        const confirmation = await this.notificationService.notifyConfirmation(
            'notification',
            'deleteNotificationConfirmation'
        );

        if (!confirmation) {
            return;
        }

        this.userNotificationService.delete(notification.id).subscribe(() => {
            this.pageIndex = 0;
            this.store.dispatch(new GetSummaryNotifications());
            this.getNotifications(this.pageIndex);
        });
    }

    public viewNotification(notification: Partial<UserNotificationModel>) {
        this.matDialog.open(MyNotificationDetailComponent, {
            width: '500px',
            data: {
                notification,
            },
        });
    }
}
