import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { InputFileModule } from 'ngx-input-file';
import { AccountComponent } from './account.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MyPropertiesComponent } from './my-properties/my-properties.component';
import { FavoritesComponent } from './favorites/favorites.component';
import { ProfileComponent } from './profile/profile.component';
import { TranslateModule } from '@ngx-translate/core';
import { NgxMaskModule } from 'ngx-mask';
import { RouteNames } from '../../core/route.names';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSortModule } from '@angular/material/sort';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { MyNotificationsComponent } from './my-notifications/my-notifications.component';
import { MatMenuModule } from '@angular/material/menu';
import { MyNotificationDetailComponent } from './my-notifications/my-notification-detail/my-notification-detail.component';
import { MatDialogModule } from '@angular/material/dialog';

export const routes = [
    {
        path: '',
        component: AccountComponent,
        children: [
            { path: '', redirectTo: RouteNames.Profile, pathMatch: 'full' },
            { path: RouteNames.MyProperties, component: MyPropertiesComponent },
            { path: RouteNames.MyFavorites, component: FavoritesComponent },
            { path: RouteNames.Profile, component: ProfileComponent },
            { path: RouteNames.MyNotifications, component: MyNotificationsComponent },
        ],
    },
];

@NgModule({
    declarations: [
        DashboardComponent,
        AccountComponent,
        MyPropertiesComponent,
        FavoritesComponent,
        ProfileComponent,
        MyNotificationsComponent,
        MyNotificationDetailComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        SharedModule,
        InputFileModule,
        TranslateModule,
        NgxMaskModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatButtonModule,
        MatIconModule,
        MatSortModule,
        MatTooltipModule,
        MatSidenavModule,
        MatCardModule,
        MatMenuModule,
        MatDialogModule,
    ],
})
export class AccountModule {}
