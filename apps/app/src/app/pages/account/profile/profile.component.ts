import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { take } from 'rxjs/operators';
import { SetUserInfo, UpdateUserAddress, UpdateUserInfo } from '../../../core/states/app/app.action';
import { BaseComponent } from '../../base.component';
import { LoadAllUnits, LoadCities } from '../../../core/states/property/property.action';
import * as findLast from 'lodash/findLast';
import { BehaviorSubject } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent extends BaseComponent implements OnInit, OnDestroy {
    public infoForm: FormGroup;
    public addressForm: FormGroup;
    public isEditingAddress$: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public administrativeUnitsReady$: BehaviorSubject<boolean> = new BehaviorSubject(false);

    constructor(
        public formBuilder: FormBuilder,
        private store: Store,
        private authenticationService: AuthenticationService
    ) {
        super();
    }

    public ngOnInit() {
        this.isEditingAddress$.pipe(untilDestroyed(this));
        this.administrativeUnitsReady$.pipe(untilDestroyed(this));

        this.infoForm = this.formBuilder.group({
            name: [null, Validators.compose([Validators.required, Validators.minLength(3)])],
            email: [{ value: null, disabled: true }],
            phone: [{ value: null, disabled: true }],
            isPhoneVerified: null,
        });

        this.addressForm = this.formBuilder.group({
            location: ['', Validators.required],
            city: [null, Validators.required],
            district: [{ value: null, disabled: true }, Validators.required],
            ward: [{ value: null, disabled: true }, Validators.required],
        });

        this.authenticationService
            .whoAmI()
            .pipe(take(1))
            .subscribe(({ name, email, phone, isPhoneVerified, metadata }) => {
                this.infoForm.patchValue({
                    name,
                    email,
                    phone,
                    isPhoneVerified,
                });

                this.store.dispatch(
                    new SetUserInfo({
                        name,
                        email,
                        phone,
                        isPhoneVerified,
                        metadata,
                    })
                );
                this.getUserAddress(findLast(metadata, (p) => p.name === 'address'));
            });
    }

    public ngOnDestroy(): void {}

    public get infoControls() {
        return this.infoForm.controls;
    }

    public get addressControls() {
        return this.addressForm.controls;
    }

    public updateBasicInfo() {
        if (!this.infoForm.valid) {
            return;
        }

        this.store.dispatch(new UpdateUserInfo({ name: this.infoForm.get('name').value }));
    }

    private getUserAddress(pureAddress: any) {
        if (!pureAddress) {
            this.isEditingAddress$.next(false);
            this.store.dispatch(new LoadCities());
            return;
        } else {
            this.isEditingAddress$.next(true);
            const address = JSON.parse(pureAddress.value);

            this.addressForm.patchValue({
                location: address.location,
            });

            const { cityId, districtId, wardId } = address;
            this.store.dispatch(new LoadAllUnits(cityId, districtId, wardId));
        }
        this.administrativeUnitsReady$.next(true);
    }

    public updateUserAddress() {
        if (!this.addressForm.valid) {
            return;
        }
        const { location, city, district, ward } = this.addressForm.getRawValue();
        this.store.dispatch(new UpdateUserAddress(location, city, district, ward));
    }
}
