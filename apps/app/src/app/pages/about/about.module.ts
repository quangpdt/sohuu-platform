import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AboutComponent } from './about.component';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';

export const routes = [{ path: '', component: AboutComponent, pathMatch: 'full' }];

@NgModule({
    declarations: [AboutComponent],
    imports: [CommonModule, RouterModule.forChild(routes), SharedModule, MatCardModule, MatIconModule],
})
export class AboutModule {}
