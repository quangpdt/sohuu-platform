import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { emailValidator } from '../../theme/utils/app-validators';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactComponent implements OnInit {
    public contactForm: FormGroup;
    public lat = 40.678178;
    public lng = -73.944158;
    public zoom = 12;
    constructor(public formBuilder: FormBuilder) {}

    public ngOnInit() {
        this.contactForm = this.formBuilder.group({
            name: ['', Validators.required],
            email: ['', Validators.compose([Validators.required, emailValidator])],
            phone: ['', Validators.required],
            message: ['', Validators.required],
        });
    }

    public onContactFormSubmit(values: Object): void {
        if (this.contactForm.valid) {
            console.log(values);
        }
    }
}
