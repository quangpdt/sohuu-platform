import { NgModule } from '@angular/core';
import { ServerModule, ServerTransferStateModule } from '@angular/platform-server';
import { FlexLayoutServerModule } from '@angular/flex-layout/server';
import { AppModule } from './app.module';
import { AppComponent } from './app.component';
import { TransferState } from '@angular/platform-browser';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateServerLoader } from './core/services/translate.server.loader';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, XhrFactory } from '@angular/common/http';
import { AppInterceptor } from './core/interceptors/app-interceptor';
import * as xhr2 from 'xhr2';
import { CookieServerService } from './core/services/cookie.server-serivce';

export function translateFactory(transferState: TransferState) {
    return new TranslateServerLoader('/assets/i18n', '.json', transferState);
}

export class ServerXhr implements XhrFactory {
    public build(): XMLHttpRequest {
        xhr2.prototype._restrictedHeaders.cookie = false;
        return new xhr2.XMLHttpRequest();
    }
}

@NgModule({
    imports: [
        AppModule,
        ServerModule,
        NoopAnimationsModule,
        FlexLayoutServerModule,
        ServerTransferStateModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: translateFactory,
                deps: [TransferState],
            },
        }),
    ],
    bootstrap: [AppComponent],
    providers: [
        CookieServerService,
        { provide: XhrFactory, useClass: ServerXhr },
        { provide: HTTP_INTERCEPTORS, useClass: AppInterceptor, multi: true },
    ],
})
export class AppServerModule {}
