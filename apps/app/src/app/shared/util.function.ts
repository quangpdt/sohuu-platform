import { ResponseCode } from '@sohuu-platform/enums';
import { PropertyModel } from '@sohuu-platform/models';
import * as findLast from 'lodash/findLast';

export const errorCodes = Object.values(ResponseCode);
export async function timeout(time: number = 2000) {
    return new Promise((resolve) => setTimeout(resolve, time));
}

export function handleGraphQLError(error: any): ResponseCode {
    if (error.graphQLErrors && error.graphQLErrors[0] && errorCodes.indexOf(error.graphQLErrors[0].message) >= 0) {
        return error.graphQLErrors[0].message;
    }

    return ResponseCode.GeneralError;
}

export function getPropertyMetadata(property: Partial<PropertyModel>, metadataName: string) {
    const metadata = findLast(property.metadata, (p) => p.name === metadataName);
    if (!metadata || !metadata.parsedValue) {
        return null;
    }
    return metadata.parsedValue;
}
