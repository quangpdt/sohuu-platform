import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { AppDataType } from '@sohuu-platform/enums';

@Pipe({
    name: 'appData',
})
@Injectable()
export class AppDataPipe implements PipeTransform {
    public transform(value: number, type: AppDataType): any {
        if (!value) {
            return;
        }

        return `${type}${value}`;
    }
}
