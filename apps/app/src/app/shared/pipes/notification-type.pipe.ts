import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { NotificationType } from '@sohuu-platform/enums';

@Pipe({
    name: 'notificationType',
})
@Injectable()
export class NotificationTypePipe implements PipeTransform {
    public transform(value: NotificationType): any {
        switch (value) {
            case NotificationType.RequestCallback:
                return 'phone';
        }
        return 'notifications_none';
    }
}
