import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { AppService } from '../../../app.service';
import { BaseComponent } from '../../../pages/base.component';
import { PropertyModel } from '@sohuu-platform/models';
import { getPropertyMetadata } from '../../util.function';
import { PropertyStatus } from '@sohuu-platform/enums';

@Component({
    selector: 'app-property-item',
    templateUrl: './property-item.component.html',
    styleUrls: ['./property-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropertyItemComponent extends BaseComponent {
    @Input() public property: Partial<PropertyModel>;

    public propertyStatus = PropertyStatus;

    constructor(public appService: AppService) {
        super();
    }

    public getStatusBgColor(status: PropertyStatus) {
        switch (status) {
            case PropertyStatus.ForSale:
                return '#558B2F';
            case PropertyStatus.ForRent:
                return '#1E88E5';
            case PropertyStatus.Sold:
                return '#000';
        }
    }

    public addToCompare() {
        // this.appService.addToCompare(this.property, CompareOverviewComponent);
    }

    public onCompare() {
        // return this.appService.Data.compareList.filter((item) => item.id === this.property.id)[0];
    }

    public addToFavorites() {
        // this.appService.addToFavorites(this.property);
    }

    public onFavorites() {
        // return this.appService.Data.favorites.filter((item) => item.id === this.property.id)[0];
    }

    public getPropertyMetadata(property: Partial<PropertyModel>, metadataName: string) {
        return getPropertyMetadata(property, metadataName);
    }
}
