import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { SwiperConfigInterface } from '@sohuu-platform/swiper';
import { PropertyModel } from '@sohuu-platform/models';

@Component({
    selector: 'app-properties-carousel',
    templateUrl: './properties-carousel.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropertiesCarouselComponent {
    @Input() public properties: Partial<PropertyModel>[];
    public config: SwiperConfigInterface;

    constructor() {
        this.config = {
            observer: true,
            slidesPerView: 1,
            spaceBetween: 16,
            keyboard: true,
            navigation: { nextEl: '.prop-next', prevEl: '.prop-prev' },
            pagination: true,
            grabCursor: true,
            loop: false,
            preloadImages: true,
            lazy: false,
            breakpoints: {
                768: {
                    slidesPerView: 2,
                },
                1024: {
                    slidesPerView: 3,
                },
                1280: {
                    slidesPerView: 4,
                },
            },
        };
    }
}
