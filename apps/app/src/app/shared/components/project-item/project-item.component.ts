import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BaseComponent } from '../../../pages/base.component';
import { ProjectModel } from '@sohuu-platform/models';

@Component({
    selector: 'app-project-item',
    templateUrl: './project-item.component.html',
    styleUrls: ['./project-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectItemComponent extends BaseComponent {
    @Input() public project: Partial<ProjectModel>;

    constructor() {
        super();
    }
}
