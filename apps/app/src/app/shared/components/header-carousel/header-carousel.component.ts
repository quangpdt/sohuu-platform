import { AfterViewInit, ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core';
import { SwiperConfigInterface } from '@sohuu-platform/swiper';

@Component({
    selector: 'app-header-carousel',
    templateUrl: './header-carousel.component.html',
    styleUrls: ['./header-carousel.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderCarouselComponent implements AfterViewInit, OnChanges {
    @Input() public slides: Array<any> = [];
    @Input() public contentOffsetToTop;
    public config: SwiperConfigInterface = {};
    public currentSlide;
    constructor() {}

    public ngAfterViewInit() {
        this.initCarousel();
    }

    public ngOnChanges() {
        if (this.slides.length > 0) {
            this.currentSlide = this.slides[0];
        }
    }

    public initCarousel() {
        this.config = {
            slidesPerView: 1,
            spaceBetween: 0,
            keyboard: true,
            navigation: true,
            pagination: false,
            grabCursor: true,
            loop: true,
            preloadImages: false,
            lazy: true,
            autoplay: {
                delay: 5000,
                disableOnInteraction: false,
            },
            speed: 500,
            effect: 'slide',
        };
    }

    public onIndexChange(index: number) {
        this.currentSlide = this.slides[index];
    }
}
