import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AppService } from '../../../app.service';
import { Store } from '@ngxs/store';
import { LoadCities } from '../../../core/states/property/property.action';
import { Observable } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { BaseComponent } from '../../../pages/base.component';
import { propertyFeatures, PropertyStatus, propertyStatuses, propertyTypes } from '@sohuu-platform/enums';
import { PropertySearchCriteria } from '@sohuu-platform/interfaces';

@Component({
    selector: 'app-properties-search',
    templateUrl: './properties-search.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropertiesSearchComponent extends BaseComponent implements OnInit, OnDestroy {
    @Input() public alignment: 'horizontal' | 'vertical' = 'horizontal';
    @Input() public initialSearchCriteria: Observable<PropertySearchCriteria>;

    @Input() public removedSearchField: string;
    @Output() public searchClick: EventEmitter<PropertySearchCriteria> = new EventEmitter<PropertySearchCriteria>();

    public showMore = true;
    public form: FormGroup;
    public propertyTypes = propertyTypes;
    public propertyStatuses = propertyStatuses;

    constructor(public appService: AppService, public formBuilder: FormBuilder, private store: Store) {
        super();
    }

    public ngOnInit() {
        this.store.dispatch(new LoadCities());

        if (this.alignment) {
            this.showMore = true;
        }

        this.form = this.formBuilder.group(this.resetFormState(true));

        if (this.initialSearchCriteria) {
            this.initialSearchCriteria.pipe(untilDestroyed(this)).subscribe((data) => {
                if (!data) {
                    return;
                }
                this.form.patchValue(data);

                if (data.city) {
                    this.form.patchValue({
                        city: data.city,
                    });

                    if (data.district) {
                        setTimeout(() => {
                            this.form.patchValue({
                                district: data.district,
                            });

                            if (data.ward) {
                                setTimeout(() => {
                                    this.form.patchValue({
                                        ward: data.ward,
                                    });
                                });
                            }
                        });
                    }
                }
            });
        }
    }

    public ngOnDestroy(): void {}

    public buildFeatures() {
        return this.formBuilder.array(this.resetFeatures());
    }

    private resetFeatures() {
        return propertyFeatures.map((feature) => {
            return this.formBuilder.group({
                id: feature,
                selected: false,
            });
        });
    }

    public reset() {
        this.form.reset(this.resetFormState(false));
    }

    public search() {
        if (!this.form.valid) {
            return;
        }
        this.form.patchValue({
            page: 0,
        });

        this.searchClick.emit(this.form.value);
    }

    public getPropertyStatus(propertyStatus: PropertyStatus): string {
        switch (propertyStatus) {
            case PropertyStatus.ForRent:
                return 'rent';
            case PropertyStatus.ForSale:
                return 'buy';
        }
    }

    private resetFormState(isCreating: boolean) {
        return {
            propertyType: null,
            propertyStatus: 1,
            priceFrom: [
                null,
                {
                    onlySelf: true,
                    emitEvent: false,
                },
            ],
            priceTo: null,
            city: null,
            district: [{ value: null, disabled: true }],
            ward: [{ value: null, disabled: true }],
            bedrooms: null,
            bathrooms: null,
            areaFrom: null,
            areaTo: null,
            features: isCreating ? this.buildFeatures() : this.resetFeatures(),
            page: 0,
            pageSize: 12,
        };
    }
}
