import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-our-services',
    templateUrl: './our-services.component.html',
    styleUrls: ['./our-services.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OurServicesComponent {}
