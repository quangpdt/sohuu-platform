import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { SwiperConfigInterface } from '@sohuu-platform/swiper';
import { AppService } from '../../../app.service';

@Component({
    selector: 'app-clients',
    templateUrl: './clients.component.html',
    styleUrls: ['./clients.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClientsComponent implements OnInit {
    public clients;
    public config: SwiperConfigInterface = {};
    constructor(public appService: AppService) {}

    public ngOnInit() {
        this.config = {
            observer: true,
            slidesPerView: 7,
            spaceBetween: 16,
            keyboard: true,
            navigation: false,
            pagination: false,
            grabCursor: true,
            loop: false,
            autoplay: {
                delay: 6000,
                disableOnInteraction: false,
            },
            speed: 500,
            effect: 'slide',
            breakpoints: {
                320: {
                    slidesPerView: 1,
                },
                480: {
                    slidesPerView: 2,
                },
                600: {
                    slidesPerView: 3,
                },
                960: {
                    slidesPerView: 4,
                },
                1280: {
                    slidesPerView: 5,
                },
                1500: {
                    slidesPerView: 6,
                },
            },
        };
        this.clients = this.appService.getClients();
    }
}
