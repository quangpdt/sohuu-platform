import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { PropertyState } from '../../../core/states/property/property.state';
import { Observable, Subject } from 'rxjs';
import { City, District, Ward } from '@sohuu-platform/interfaces';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import {
    FilterCities,
    FilterDistricts,
    FilterWards,
    LoadDistricts,
    LoadWards,
    ResetAllFilteredUnits,
} from '../../../core/states/property/property.action';

@Component({
    selector: 'app-administrative-units',
    templateUrl: './administrative-units.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdministrativeUnitsComponent implements OnInit, OnDestroy {
    @Input() public alignment: 'vertical' | 'horizontal';
    @Input() public parentForm: FormGroup;
    @Input() public isEditing: boolean;

    @Select(PropertyState.getCities)
    public cities$: Observable<City[]>;

    @Select(PropertyState.getFilteredCities)
    public filteredCities$: Observable<City[]>;

    @Select(PropertyState.getFilteredDistricts)
    public filteredDistricts$: Observable<District[]>;

    @Select(PropertyState.getFilteredWards)
    public filteredWards$: Observable<Ward[]>;

    constructor(private store: Store) {}

    public ngOnInit(): void {
        if (!this.isEditing) {
            this.initAdministrativeUnits();
            return;
        }

        const ngDataLoaded$ = new Subject();

        this.store
            .select(PropertyState.getFilteredAdministrativeUnits)
            .pipe(takeUntil(ngDataLoaded$), distinctUntilChanged())
            .subscribe(({ city, district, ward }) => {
                if (!(city && district && ward)) {
                    return;
                }

                this.initAdministrativeUnits();
                this.parentForm.patchValue({
                    city,
                });
                setTimeout(() => {
                    this.parentForm.patchValue({
                        district,
                    });
                });
                setTimeout(() => {
                    this.parentForm.patchValue({
                        ward,
                    });
                });

                this.parentForm.get('district').enable();
                this.parentForm.get('ward').enable();

                ngDataLoaded$.next();
                ngDataLoaded$.complete();
            });
    }

    public ngOnDestroy(): void {
        this.store.dispatch(new ResetAllFilteredUnits());
    }

    public getOptionText(option: any) {
        if (!option) {
            return null;
        }
        return option.name;
    }

    private initAdministrativeUnits() {
        this.parentForm
            .get('city')
            .valueChanges.pipe(untilDestroyed(this), distinctUntilChanged())
            .subscribe(async (value) => {
                if (value && typeof value === 'object') {
                    await this.store.dispatch(new LoadDistricts(value.id)).toPromise();
                    this.resetDistrictAndWardInputs(true);
                    return;
                }
                this.resetDistrictAndWardInputs(false);
                this.store.dispatch(new FilterCities(value));
                this.store.dispatch(new LoadDistricts(null));
            });

        this.parentForm
            .get('district')
            .valueChanges.pipe(untilDestroyed(this), distinctUntilChanged())
            .subscribe(async (value) => {
                if (value && typeof value === 'object') {
                    await this.store.dispatch(new LoadWards(value.id)).toPromise();
                    this.resetWardInput(true);
                    return;
                }
                this.resetWardInput(false);
                this.store.dispatch(new FilterDistricts(value));
                this.store.dispatch(new LoadWards(null));
            });

        this.parentForm
            .get('ward')
            .valueChanges.pipe(untilDestroyed(this), distinctUntilChanged())
            .subscribe((value) => {
                this.store.dispatch(new FilterWards(value));
            });
    }

    private resetDistrictAndWardInputs(isCitySelected: boolean) {
        this.parentForm.get('district').reset({ value: null, disabled: !isCitySelected });
        this.parentForm.get('ward').reset({ value: null, disabled: true });
    }

    private resetWardInput(isDistrictSelected: boolean) {
        this.parentForm.get('ward').reset({ value: null, disabled: !isDistrictSelected });
    }

    public get controls() {
        return this.parentForm.controls;
    }
}
