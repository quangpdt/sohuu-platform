import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SortOption } from '@sohuu-platform/enums';

@Component({
    selector: 'app-properties-toolbar',
    templateUrl: './properties-toolbar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropertiesToolbarComponent implements OnInit {
    @Input() public isHomePage = false;

    @Output() public sidenavToggle: EventEmitter<any> = new EventEmitter<any>();
    @Output() public pageSizeChange: EventEmitter<any> = new EventEmitter<any>();
    @Output() public sortOptionChange: EventEmitter<any> = new EventEmitter<any>();

    public counts = [8, 12, 16, 24, 36];
    public count: any;
    public sortOptions: SortOption[] = Object.values(SortOption);
    public sort: SortOption;

    constructor() {}

    public ngOnInit() {
        this.count = this.isHomePage ? this.counts[0] : this.counts[1];
        this.sort = this.sortOptions[0];
    }

    public changeCount(count) {
        this.count = count;
        this.pageSizeChange.emit(count);
    }

    public changeSortOption(sortOption: SortOption) {
        this.sort = sortOption;
        this.sortOptionChange.emit(sortOption);
    }

    public toggleSidenav() {
        this.sidenavToggle.emit();
    }
}
