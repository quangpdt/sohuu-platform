import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { SwiperConfigInterface } from '@sohuu-platform/swiper';
import { AppService } from '../../../app.service';

@Component({
    selector: 'app-testimonials',
    templateUrl: './testimonials.component.html',
    styleUrls: ['./testimonials.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TestimonialsComponent implements OnInit {
    public testimonials;
    public config: SwiperConfigInterface;

    constructor(public appService: AppService) {}

    public ngOnInit() {
        this.config = {
            observer: true,
            slidesPerView: 1,
            spaceBetween: 0,
            keyboard: true,
            navigation: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            grabCursor: true,
            loop: false,
        };
        this.testimonials = this.appService.getTestimonials();
    }
}
