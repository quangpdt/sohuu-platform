import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ProjectSortOption, SortOption } from '@sohuu-platform/enums';
import { BaseComponent } from '@sohuu/app/pages/base.component';

@Component({
    selector: 'app-projects-toolbar',
    templateUrl: './projects-toolbar.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectsToolbarComponent extends BaseComponent implements OnInit {
    @Input() public isHomePage = false;

    @Output() public sortOptionChange: EventEmitter<any> = new EventEmitter<any>();

    public sortOptions: ProjectSortOption[] = [
        ProjectSortOption.RecentlyAdded,
        ProjectSortOption.HandOverTime,
        ProjectSortOption.ConstructionTime,
        ProjectSortOption.PriceDescending,
        ProjectSortOption.PriceAscending,
    ];

    public sort: ProjectSortOption;

    constructor() {
        super();
    }

    public ngOnInit() {
        this.sort = this.sortOptions[0];
    }

    public changeSortOption(sortOption: ProjectSortOption) {
        this.sort = sortOption;
        this.sortOptionChange.emit(sortOption);
    }
}
