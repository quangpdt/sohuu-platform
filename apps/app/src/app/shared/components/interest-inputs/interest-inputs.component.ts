import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BaseComponent } from '../../../pages/base.component';
import { Observable } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { InterestInput } from '@sohuu-platform/interfaces';
import { debtPeriods } from '@sohuu-platform/enums';

@Component({
    selector: 'app-interest-inputs',
    templateUrl: './interest-inputs.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InterestInputsComponent extends BaseComponent implements OnInit, OnDestroy {
    @Input() public vertical = false;
    @Input() public initialInput: Observable<InterestInput>;
    @Output() public searchClick: EventEmitter<InterestInput> = new EventEmitter<InterestInput>();
    public form: FormGroup;
    public debtPeriods = debtPeriods;
    public results;

    constructor(public fb: FormBuilder) {
        super();
    }

    public ngOnInit() {
        this.form = this.fb.group({
            years: [null, Validators.required],
            amount: [null, Validators.required],
            maximumDebt: [null, Validators.required],
            interestRate: [null, Validators.required],
            propertyId: [null],
            propertySlug: [null],
        });

        if (this.initialInput) {
            this.initialInput
                .pipe(untilDestroyed(this))
                .subscribe((input: InterestInput) => this.form.patchValue(input));
        }
    }

    public ngOnDestroy() {}

    public reset() {
        this.form.reset({
            years: null,
            amount: null,
            maximumDebt: null,
            interestRate: null,
        });
    }

    public search() {
        if (!this.form.valid) {
            return;
        }
        this.searchClick.emit(this.form.value);
    }

    public save() {
        alert('Tính năng đang được phát triển');
    }
}
