import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-logo',
    templateUrl: './logo.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogoComponent {
    @Input() public color: 'light' | 'dark' = 'dark';
}
