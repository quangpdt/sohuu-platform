import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
    selector: 'app-mission',
    templateUrl: './mission.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MissionComponent {
    constructor() {}
}
