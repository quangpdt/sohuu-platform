import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { SwiperConfigInterface } from '@sohuu-platform/swiper';
import { AppService } from '../../../app.service';
import { BaseComponent } from '../../../pages/base.component';

@Component({
    selector: 'app-our-agents',
    templateUrl: './our-agents.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OurAgentsComponent extends BaseComponent implements OnInit {
    public agents;
    public config: SwiperConfigInterface = {};
    constructor(public appService: AppService) {
        super();
    }

    public ngOnInit() {
        this.config = {
            observer: true,
            slidesPerView: 4,
            spaceBetween: 16,
            keyboard: true,
            navigation: true,
            pagination: false,
            grabCursor: true,
            loop: false,
            breakpoints: {
                600: {
                    slidesPerView: 1,
                },
                960: {
                    slidesPerView: 2,
                },
                1280: {
                    slidesPerView: 3,
                },
            },
        };
        this.agents = this.appService.getAgents();
    }
}
