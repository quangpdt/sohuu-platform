import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { BaseComponent } from '../../../pages/base.component';

@Component({
    selector: 'app-header-image',
    templateUrl: './header-image.component.html',
    styleUrls: ['./header-image.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderImageComponent extends BaseComponent implements OnInit {
    @Input() public backgroundImage;
    @Input() public bgImageAnimate;
    @Input() public contentOffsetToTop;
    @Input() public contentMinHeight;
    @Input() public title;
    @Input() public desc;
    @Input() public isHomePage = false;

    public bgImage;
    constructor(private sanitizer: DomSanitizer) {
        super();
    }

    public ngOnInit() {
        if (this.backgroundImage)
            this.bgImage = this.sanitizer.bypassSecurityTrustStyle('url(' + this.backgroundImage + ')');
    }
}
