import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { Property } from '../../../app.models';
import { AppService } from '../../../app.service';
import { BaseComponent } from '../../../pages/base.component';

@Component({
    selector: 'app-compare-overview',
    templateUrl: './compare-overview.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CompareOverviewComponent extends BaseComponent implements OnInit {
    public properties: Property[];
    constructor(public appService: AppService, private bottomSheetRef: MatBottomSheetRef<CompareOverviewComponent>) {
        super();
    }

    public ngOnInit() {
        this.properties = this.appService.Data.compareList;
    }

    public hideSheet(isRedirect: boolean) {
        this.bottomSheetRef.dismiss(isRedirect);
    }

    public remove(property, event: any) {
        const index: number = this.appService.Data.compareList.indexOf(property);
        if (index !== -1) {
            this.appService.Data.compareList.splice(index, 1);
        }
        if (this.appService.Data.compareList.length === 0) {
            this.hideSheet(false);
        }
        event.preventDefault();
    }
}
