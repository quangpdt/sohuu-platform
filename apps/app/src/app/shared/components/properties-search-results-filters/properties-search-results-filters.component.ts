import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import * as filter from 'lodash/filter';
import { BaseComponent } from '../../../pages/base.component';
import { PropertySearchCriteria } from '@sohuu-platform/interfaces';
import { PropertyStatus } from '@sohuu-platform/enums';

@Component({
    selector: 'app-properties-search-results-filters',
    templateUrl: './properties-search-results-filters.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PropertiesSearchResultsFiltersComponent extends BaseComponent {
    @Input() public searchCriteria: PropertySearchCriteria;
    @Output() public searchFieldsRemove: EventEmitter<any> = new EventEmitter<any>();

    constructor() {
        super();
    }

    public removeSearchField(field) {
        this.searchFieldsRemove.emit([field]);
    }

    public removeSearchFields(fields) {
        this.searchFieldsRemove.emit(fields);
    }

    public getPropertyStatus(propertyStatus: PropertyStatus): string {
        switch (propertyStatus) {
            case PropertyStatus.ForRent:
                return 'rent';
            case PropertyStatus.ForSale:
                return 'buy';
        }
    }

    public getAddress() {
        const addresses = [
            this.searchCriteria?.ward?.name,
            this.searchCriteria?.district?.name,
            this.searchCriteria?.city?.name,
        ];

        return filter(addresses, (p) => !!p).join(', ');
    }
}
