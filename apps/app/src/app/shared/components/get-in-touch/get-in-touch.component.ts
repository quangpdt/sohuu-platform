import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseComponent } from '../../../pages/base.component';

@Component({
    selector: 'app-get-in-touch',
    templateUrl: './get-in-touch.component.html',
    styleUrls: ['./get-in-touch.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GetInTouchComponent extends BaseComponent {
    constructor() {
        super();
    }
}
