import { AfterViewInit, Directive, Host, Input, OnDestroy, Self } from '@angular/core';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { MatAutocomplete, MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { NgControl } from '@angular/forms';
import * as isObject from 'lodash/isObject';

@Directive({
    selector: '[matAutocompleteExt]',
})
export class MatAutocompleteExtDirective implements AfterViewInit, OnDestroy {
    @Input()
    public matAutocomplete: MatAutocomplete;

    constructor(
        @Host()
        @Self()
        private readonly autoCompleteTrigger: MatAutocompleteTrigger,
        private readonly ngControl: NgControl
    ) {}

    public ngAfterViewInit() {
        this.autoCompleteTrigger.panelClosingActions.pipe(untilDestroyed(this)).subscribe((e) => {
            if (!e || !e.source) {
                const selected = this.matAutocomplete.options
                    .map((option) => option.value)
                    .find((option) => option === this.ngControl.value);

                if (selected == null && !isObject(this.ngControl.value)) {
                    this.ngControl.reset(null);
                }
            }
        });
    }

    public ngOnDestroy() {}
}
