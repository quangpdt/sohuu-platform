import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { SWIPER_CONFIG, SwiperModule } from '@sohuu-platform/swiper';
import { FlexLayoutModule } from '@angular/flex-layout';
import {
    PERFECT_SCROLLBAR_CONFIG,
    PerfectScrollbarConfigInterface,
    PerfectScrollbarModule,
} from 'ngx-perfect-scrollbar';
import { PipesModule } from '../theme/pipes/pipes.module';
import { DirectivesModule } from '../theme/directives/directives.module';

import { HeaderImageComponent } from './components/header-image/header-image.component';
import { HeaderCarouselComponent } from './components/header-carousel/header-carousel.component';
import { PropertyItemComponent } from './components/property-item/property-item.component';
import { PropertiesToolbarComponent } from './components/properties-toolbar/properties-toolbar.component';
import { PropertiesSearchComponent } from './components/properties-search/properties-search.component';
import { CompareOverviewComponent } from './components/compare-overview/compare-overview.component';
import { RatingComponent } from './components/rating/rating.component';
import { PropertiesSearchResultsFiltersComponent } from './components/properties-search-results-filters/properties-search-results-filters.component';
import { PropertiesCarouselComponent } from './components/properties-carousel/properties-carousel.component';
import { ClientsComponent } from './components/clients/clients.component';
import { GetInTouchComponent } from './components/get-in-touch/get-in-touch.component';
import { CommentsComponent } from './components/comments/comments.component';
import { TestimonialsComponent } from './components/testimonials/testimonials.component';
import { OurAgentsComponent } from './components/our-agents/our-agents.component';
import { MissionComponent } from './components/mission/mission.component';
import { OurServicesComponent } from './components/our-services/our-services.component';
import { LogoComponent } from './components/logo/logo.component';
import { InterestInputsComponent } from './components/interest-inputs/interest-inputs.component';
import { NgxMaskModule } from 'ngx-mask';
import { LetDirective } from './directives/let.directive';

import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSelectModule } from '@angular/material/select';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatMenuModule } from '@angular/material/menu';
import { AdministrativeUnitsComponent } from './components/administrative-units/administrative-units.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatAutocompleteExtDirective } from './directives/mat-autocomplete-ext.directive';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AppDataPipe } from './pipes/app-data.pipe';
import { NotificationTypePipe } from './pipes/notification-type.pipe';
import { ProjectItemComponent } from '@sohuu/app/shared/components/project-item/project-item.component';
import { ProjectsToolbarComponent } from '@sohuu/app/shared/components/projects-toolbar/projects-toolbar.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
    wheelPropagation: false,
    suppressScrollX: true,
};

const materialModules = [
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTooltipModule,
    MatToolbarModule,
    MatSelectModule,
    MatChipsModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatMenuModule,
    MatCheckboxModule,
];

const sharedComponents = [
    LogoComponent,
    HeaderImageComponent,
    HeaderCarouselComponent,
    PropertyItemComponent,
    PropertiesToolbarComponent,
    PropertiesSearchComponent,
    CompareOverviewComponent,
    RatingComponent,
    PropertiesSearchResultsFiltersComponent,
    PropertiesCarouselComponent,
    ClientsComponent,
    GetInTouchComponent,
    CommentsComponent,
    TestimonialsComponent,
    OurAgentsComponent,
    MissionComponent,
    OurServicesComponent,
    InterestInputsComponent,
    AdministrativeUnitsComponent,
    ProjectItemComponent,
    ProjectsToolbarComponent,
];

const sharedPipes = [AppDataPipe, NotificationTypePipe];

const sharedDirectives = [LetDirective, MatAutocompleteExtDirective];

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        ReactiveFormsModule,
        SwiperModule,
        FlexLayoutModule,
        PerfectScrollbarModule,
        PipesModule,
        DirectivesModule,
        NgxMaskModule.forChild(),
        TranslateModule,
        ...materialModules,
    ],
    exports: [
        RouterModule,
        ReactiveFormsModule,
        SwiperModule,
        FlexLayoutModule,
        PerfectScrollbarModule,
        PipesModule,
        DirectivesModule,
        ...sharedComponents,
        ...sharedPipes,
        ...sharedDirectives,
    ],
    declarations: [...sharedComponents, ...sharedComponents, ...sharedPipes, ...sharedDirectives],
    entryComponents: [CompareOverviewComponent],
    providers: [
        {
            provide: PERFECT_SCROLLBAR_CONFIG,
            useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG,
        },
        {
            provide: SWIPER_CONFIG,
            useValue: {
                preloadImages: false,
                lazy: {
                    loadPrevNext: true,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            },
        },
    ],
})
export class SharedModule {}
