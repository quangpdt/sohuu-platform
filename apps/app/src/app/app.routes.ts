import { Routes } from '@angular/router';

import { PagesComponent } from './pages/pages.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { OAuthComponent } from './pages/authentication/oauth/oauth.component';
import { AuthenticationGuard } from './core/guards/authentication.guard';
import { AppDataGuard } from './core/guards/app-data.guard';
import { RouteNames, routePaths } from './core/route.names';

export const appRoutes: Routes = [
    {
        path: '',
        component: PagesComponent,
        canActivate: [AppDataGuard],
        children: [
            {
                path: '',
                loadChildren: () => import('./pages/home/home.module').then((m) => m.HomeModule),
            },
            {
                path: RouteNames.AboutUs,
                loadChildren: () => import('./pages/about/about.module').then((m) => m.AboutModule),
            },
            {
                path: RouteNames.Contact,
                loadChildren: () => import('./pages/contact/contact.module').then((m) => m.ContactModule),
            },
            {
                path: RouteNames.Properties,
                loadChildren: () => import('./pages/properties/properties.module').then((m) => m.PropertiesModule),
            },
            {
                path: RouteNames.Agencies,
                loadChildren: () => import('./pages/agents/agents.module').then((m) => m.AgentsModule),
            },
            {
                path: RouteNames.Compare,
                loadChildren: () => import('./pages/compare/compare.module').then((m) => m.CompareModule),
            },
            {
                path: 'pricing',
                loadChildren: () => import('./pages/pricing/pricing.module').then((m) => m.PricingModule),
            },
            {
                path: RouteNames.FAQ,
                loadChildren: () => import('./pages/faq/faq.module').then((m) => m.FaqModule),
            },
            {
                path: '',
                loadChildren: () =>
                    import('./pages/authentication/authentication.module').then((m) => m.AuthenticationModule),
            },
            {
                path: RouteNames.TermsConditions,
                loadChildren: () =>
                    import('./pages/terms-conditions/terms-conditions.module').then((m) => m.TermsConditionsModule),
            },
            {
                path: RouteNames.Account,
                canActivate: [AuthenticationGuard],
                loadChildren: () => import('./pages/account/account.module').then((m) => m.AccountModule),
            },
            {
                path: RouteNames.SubmitProperty,
                canActivate: [AuthenticationGuard],
                loadChildren: () =>
                    import('./pages/submit-property/submit-property.module').then((m) => m.SubmitPropertyModule),
            },
            {
                path: RouteNames.Interest,
                loadChildren: () => import('./pages/interest/interest.module').then((m) => m.InterestModule),
            },
            {
                path: 'oauth-success',
                children: [
                    {
                        path: '',
                        pathMatch: 'full',
                        component: OAuthComponent,
                    },
                    {
                        path: 'first-time',
                        component: OAuthComponent,
                    },
                ],
            },
        ],
    },
    { path: '**', redirectTo: routePaths.notFound },
    { path: RouteNames.NotFound, component: NotFoundComponent },
];
