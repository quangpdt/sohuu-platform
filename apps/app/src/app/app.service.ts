import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Location, Property } from './app.models';
import { isPlatformBrowser } from '@angular/common';
import { environment } from '../environments/environment';

export class Data {
    constructor(
        public properties: Property[],
        public compareList: Property[],
        public favorites: Property[],
        public locations: Location[]
    ) {}
}

@Injectable({
    providedIn: 'root',
})
export class AppService {
    public Data = new Data(
        [], // properties
        [], // compareList
        [], // favorites
        [] // locations
    );

    public url = `${environment.baseUrl.base}/assets/data/`;
    public apiKey = 'AIzaSyAir4tXhx3X-wcdZnhe8TLlo9J2m_AKx6w';

    constructor(
        public http: HttpClient,
        private bottomSheet: MatBottomSheet,
        private snackBar: MatSnackBar,
        @Inject(PLATFORM_ID) private platformId: Object
    ) {}

    public getProperties(): Observable<Property[]> {
        return this.http.get<Property[]>(this.url + 'properties.json');
    }

    public getPropertyById(id): Observable<Property> {
        return this.http.get<Property>(this.url + 'property-' + id + '.json');
    }

    public getFeaturedProperties(): Observable<Property[]> {
        return this.http.get<Property[]>(this.url + 'featured-properties.json');
    }

    public getRelatedProperties(): Observable<Property[]> {
        return this.http.get<Property[]>(this.url + 'related-properties.json');
    }

    public getPropertiesByAgentId(agentId): Observable<Property[]> {
        return this.http.get<Property[]>(this.url + 'properties-agentid-' + agentId + '.json');
    }

    public getLocations(): Observable<Location[]> {
        return this.http.get<Location[]>(this.url + 'locations.json');
    }

    public getAddress(lat = 40.714224, lng = -73.961452) {
        return this.http.get(
            'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&key=' + this.apiKey
        );
    }

    public getLatLng(address) {
        return this.http.get(
            'https://maps.googleapis.com/maps/api/geocode/json?key=' + this.apiKey + '&address=' + address
        );
    }

    public getFullAddress(lat = 40.714224, lng = -73.961452) {
        return this.http
            .get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + lat + ',' + lng + '&key=' + this.apiKey)
            .subscribe((data) => {
                return data['results'][0]['formatted_address'];
            });
    }

    public addToCompare(property: Property, component) {
        if (!this.Data.compareList.filter((item) => item.id === property.id)[0]) {
            this.Data.compareList.push(property);
            this.bottomSheet
                .open(component)
                .afterDismissed()
                .subscribe((isRedirect) => {
                    if (isRedirect) {
                        if (isPlatformBrowser(this.platformId)) {
                            window.scroll({ top: 0, left: 0, behavior: 'smooth' });
                        }
                    }
                });
        }
    }

    public getCities() {
        return [
            { id: 1, name: 'New York' },
            { id: 2, name: 'Chicago' },
            { id: 3, name: 'Los Angeles' },
            { id: 4, name: 'Seattle' },
        ];
    }

    public getHomeCarouselSlides() {
        return this.http.get<any[]>(this.url + 'slides.json');
    }

    public filterData(data, params: any, sort?, page?, perPage?) {
        if (params) {
            if (params.propertyType) {
                data = data.filter((property) => property.propertyType === params.propertyType.name);
            }

            if (params.propertyStatus && params.propertyStatus.length) {
                const statuses = [];
                params.propertyStatus.forEach((status) => {
                    statuses.push(status.name);
                });
                const properties = [];
                data.filter((property) =>
                    property.propertyStatus.forEach((status) => {
                        if (statuses.indexOf(status) > -1) {
                            if (!properties.includes(property)) {
                                properties.push(property);
                            }
                        }
                    })
                );
                data = properties;
            }

            if (params.price) {
                if (params.price.from) {
                    data = data.filter((property) => {
                        if (property.priceDollar.sale && property.priceDollar.sale >= params.price.from) {
                            return true;
                        }
                        return property.priceDollar.rent && property.priceDollar.rent >= params.price.from;
                    });
                }
                if (params.price.to) {
                    data = data.filter((property) => {
                        if (property.priceDollar.sale && property.priceDollar.sale <= params.price.to) {
                            return true;
                        }
                        return property.priceDollar.rent && property.priceDollar.rent <= params.price.to;
                    });
                }
            }

            if (params.city) {
                data = data.filter((property) => property.city === params.city.name);
            }

            if (params.zipCode) {
                data = data.filter((property) => property.zipCode === params.zipCode);
            }

            if (params.neighborhood && params.neighborhood.length) {
                const neighborhoods = [];
                params.neighborhood.forEach((item) => {
                    neighborhoods.push(item.name);
                });
                const properties = [];
                data.filter((property) =>
                    property.neighborhood.forEach((item) => {
                        if (neighborhoods.indexOf(item) > -1) {
                            if (!properties.includes(property)) {
                                properties.push(property);
                            }
                        }
                    })
                );
                data = properties;
            }

            if (params.street && params.street.length) {
                const streets = [];
                params.street.forEach((item) => {
                    streets.push(item.name);
                });
                const properties = [];
                data.filter((property) =>
                    property.street.forEach((item) => {
                        if (streets.indexOf(item) > -1) {
                            if (!properties.includes(property)) {
                                properties.push(property);
                            }
                        }
                    })
                );
                data = properties;
            }

            if (params.bedrooms) {
                if (params.bedrooms.from) {
                    data = data.filter((property) => property.bedrooms >= params.bedrooms.from);
                }
                if (params.bedrooms.to) {
                    data = data.filter((property) => property.bedrooms <= params.bedrooms.to);
                }
            }

            if (params.bathrooms) {
                if (params.bathrooms.from) {
                    data = data.filter((property) => property.bathrooms >= params.bathrooms.from);
                }
                if (params.bathrooms.to) {
                    data = data.filter((property) => property.bathrooms <= params.bathrooms.to);
                }
            }

            if (params.garages) {
                if (params.garages.from) {
                    data = data.filter((property) => property.garages >= params.garages.from);
                }
                if (params.garages.to) {
                    data = data.filter((property) => property.garages <= params.garages.to);
                }
            }

            if (params.area) {
                if (params.area.from) {
                    data = data.filter((property) => property.area.value >= params.area.from);
                }
                if (params.area.to) {
                    data = data.filter((property) => property.area.value <= params.area.to);
                }
            }

            if (params.yearBuilt) {
                if (params.yearBuilt.from) {
                    data = data.filter((property) => property.yearBuilt >= params.yearBuilt.from);
                }
                if (params.yearBuilt.to) {
                    data = data.filter((property) => property.yearBuilt <= params.yearBuilt.to);
                }
            }

            if (params.features) {
                const arr = [];
                params.features.forEach((feature) => {
                    if (feature.selected) arr.push(feature.name);
                });
                if (arr.length > 0) {
                    const properties = [];
                    data.filter((property) =>
                        property.features.forEach((feature) => {
                            if (arr.indexOf(feature) > -1) {
                                if (!properties.includes(property)) {
                                    properties.push(property);
                                }
                            }
                        })
                    );
                    data = properties;
                }
            }
        }

        // console.log(data)

        //for show more properties mock data
        for (let index = 0; index < 2; index++) {
            data = data.concat(data);
        }

        this.sortData(sort, data);
        return this.paginator(data, page, perPage);
    }

    public sortData(sort, data) {
        if (sort) {
            switch (sort) {
                case 'Newest':
                    data = data.sort((a, b) => {
                        return <any>new Date(b.published) - <any>new Date(a.published);
                    });
                    break;
                case 'Oldest':
                    data = data.sort((a, b) => {
                        return <any>new Date(a.published) - <any>new Date(b.published);
                    });
                    break;
                case 'Popular':
                    data = data.sort((a, b) => {
                        if (a.ratingsValue / a.ratingsCount < b.ratingsValue / b.ratingsCount) {
                            return 1;
                        }
                        if (a.ratingsValue / a.ratingsCount > b.ratingsValue / b.ratingsCount) {
                            return -1;
                        }
                        return 0;
                    });
                    break;
                case 'Price (Low to High)':
                    data = data.sort((a, b) => {
                        if ((a.priceDollar.sale || a.priceDollar.rent) > (b.priceDollar.sale || b.priceDollar.rent)) {
                            return 1;
                        }
                        if ((a.priceDollar.sale || a.priceDollar.rent) < (b.priceDollar.sale || b.priceDollar.rent)) {
                            return -1;
                        }
                        return 0;
                    });
                    break;
                case 'Price (High to Low)':
                    data = data.sort((a, b) => {
                        if ((a.priceDollar.sale || a.priceDollar.rent) < (b.priceDollar.sale || b.priceDollar.rent)) {
                            return 1;
                        }
                        if ((a.priceDollar.sale || a.priceDollar.rent) > (b.priceDollar.sale || b.priceDollar.rent)) {
                            return -1;
                        }
                        return 0;
                    });
                    break;
                default:
                    break;
            }
        }
        return data;
    }

    public paginator(items, page?, perPage?) {
        perPage = perPage || 4;
        page = page || 1;

        const offset = (page - 1) * perPage,
            paginatedItems = items.slice(offset).slice(0, perPage),
            totalPages = Math.ceil(items.length / perPage);
        return {
            data: paginatedItems,
            pagination: {
                page: page,
                perPage: perPage,
                prePage: page - 1 ? page - 1 : null,
                nextPage: totalPages > page ? page + 1 : null,
                total: items.length,
                totalPages: totalPages,
            },
        };
    }

    public getTestimonials() {
        return [
            {
                text:
                    'Donec molestie turpis ut mollis efficitur. Nam fringilla libero vel dictum vulputate. In malesuada, ligula non ornare consequat, augue nibh luctus nisl, et lobortis justo ipsum nec velit. Praesent lacinia quam ut nulla gravida, at viverra libero euismod. Sed tincidunt tempus augue vitae malesuada. Vestibulum eu lectus nisi. Aliquam erat volutpat.',
                author: 'Mr. Adam Sandler',
                position: 'General Director',
                image: 'assets/images/profile/adam.jpg',
            },
            {
                text:
                    'Donec molestie turpis ut mollis efficitur. Nam fringilla libero vel dictum vulputate. In malesuada, ligula non ornare consequat, augue nibh luctus nisl, et lobortis justo ipsum nec velit. Praesent lacinia quam ut nulla gravida, at viverra libero euismod. Sed tincidunt tempus augue vitae malesuada. Vestibulum eu lectus nisi. Aliquam erat volutpat.',
                author: 'Ashley Ahlberg',
                position: 'Housewife',
                image: 'assets/images/profile/ashley.jpg',
            },
            {
                text:
                    'Donec molestie turpis ut mollis efficitur. Nam fringilla libero vel dictum vulputate. In malesuada, ligula non ornare consequat, augue nibh luctus nisl, et lobortis justo ipsum nec velit. Praesent lacinia quam ut nulla gravida, at viverra libero euismod. Sed tincidunt tempus augue vitae malesuada. Vestibulum eu lectus nisi. Aliquam erat volutpat.',
                author: 'Bruno Vespa',
                position: 'Blogger',
                image: 'assets/images/profile/bruno.jpg',
            },
            {
                text:
                    'Donec molestie turpis ut mollis efficitur. Nam fringilla libero vel dictum vulputate. In malesuada, ligula non ornare consequat, augue nibh luctus nisl, et lobortis justo ipsum nec velit. Praesent lacinia quam ut nulla gravida, at viverra libero euismod. Sed tincidunt tempus augue vitae malesuada. Vestibulum eu lectus nisi. Aliquam erat volutpat.',
                author: 'Mrs. Julia Aniston',
                position: 'Marketing Manager',
                image: 'assets/images/profile/julia.jpg',
            },
        ];
    }

    public getAgents() {
        return [
            {
                id: 1,
                fullName: 'Lusia Manuel',
                desc:
                    'Phasellus sed metus leo. Donec laoreet, lacus ut suscipit convallis, erat enim eleifend nulla, at sagittis enim urna et lacus.',
                organization: 'SoHuu.VN',
                email: 'lusia.m@housekey.com',
                phone: '(224) 267-1346',
                social: {
                    facebook: 'lusia',
                    twitter: 'lusia',
                    linkedin: 'lusia',
                    instagram: 'lusia',
                    website: 'https://lusia.manuel.com',
                },
                ratingsCount: 6,
                ratingsValue: 480,
                image: 'assets/images/agents/a-1.jpg',
            },
            {
                id: 2,
                fullName: 'Andy Warhol',
                desc:
                    'Phasellus sed metus leo. Donec laoreet, lacus ut suscipit convallis, erat enim eleifend nulla, at sagittis enim urna et lacus.',
                organization: 'SoHuu.VN',
                email: 'andy.w@housekey.com',
                phone: '(212) 457-2308',
                social: {
                    facebook: '',
                    twitter: '',
                    linkedin: '',
                    instagram: '',
                    website: 'https://andy.warhol.com',
                },
                ratingsCount: 4,
                ratingsValue: 400,
                image: 'assets/images/agents/a-2.jpg',
            },
            {
                id: 3,
                fullName: 'Tereza Stiles',
                desc:
                    'Phasellus sed metus leo. Donec laoreet, lacus ut suscipit convallis, erat enim eleifend nulla, at sagittis enim urna et lacus.',
                organization: 'SoHuu.VN',
                email: 'tereza.s@housekey.com',
                phone: '(214) 617-2614',
                social: {
                    facebook: '',
                    twitter: '',
                    linkedin: '',
                    instagram: '',
                    website: 'https://tereza.stiles.com',
                },
                ratingsCount: 4,
                ratingsValue: 380,
                image: 'assets/images/agents/a-3.jpg',
            },
            {
                id: 4,
                fullName: 'Michael Blair',
                desc:
                    'Phasellus sed metus leo. Donec laoreet, lacus ut suscipit convallis, erat enim eleifend nulla, at sagittis enim urna et lacus.',
                organization: 'SoHuu.VN',
                email: 'michael.b@housekey.com',
                phone: '(267) 388-1637',
                social: {
                    facebook: '',
                    twitter: '',
                    linkedin: '',
                    instagram: '',
                    website: 'https://michael.blair.com',
                },
                ratingsCount: 6,
                ratingsValue: 480,
                image: 'assets/images/agents/a-4.jpg',
            },
            {
                id: 5,
                fullName: 'Michelle Ormond',
                desc:
                    'Phasellus sed metus leo. Donec laoreet, lacus ut suscipit convallis, erat enim eleifend nulla, at sagittis enim urna et lacus.',
                organization: 'SoHuu.VN',
                email: 'michelle.o@housekey.com',
                phone: '(267) 388-1637',
                social: {
                    facebook: '',
                    twitter: '',
                    linkedin: '',
                    instagram: '',
                    website: 'https://michelle.ormond.com',
                },
                ratingsCount: 6,
                ratingsValue: 480,
                image: 'assets/images/agents/a-5.jpg',
            },
        ];
    }

    public getClients() {
        return [
            { name: 'VinGroup', image: 'assets/investors/vingroup.jpg' },
            { name: 'NovaLand', image: 'assets/investors/nova-land.jpg' },
            { name: 'Sunshine', image: 'assets/investors/sunshine.jpg' },
            { name: 'Khang Điền', image: 'assets/investors/khang-dien.jpg' },
            { name: 'Đất Xanh', image: 'assets/investors/dat-xanh-group.jpg' },
            { name: 'Capital Land', image: 'assets/investors/capital-land.jpg' },
        ];
    }
}
