import { BrowserModule, BrowserTransferStateModule, TransferState } from '@angular/platform-browser';
import { Inject, NgModule, PLATFORM_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import { EmbedVideo } from 'ngx-embed-video';
import { InputFileConfig, InputFileModule } from 'ngx-input-file';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { OverlayContainer } from '@angular/cdk/overlay';
import { CustomOverlayContainer } from './theme/utils/custom-overlay-container';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';

import { PagesComponent } from './pages/pages.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ToolbarComponent } from './theme/components/toolbar/toolbar.component';
import { UserMenuComponent } from './theme/components/user-menu/user-menu.component';
import { ContactsComponent } from './theme/components/contacts/contacts.component';
import { HorizontalMenuComponent } from './theme/components/menu/horizontal-menu/horizontal-menu.component';
import { VerticalMenuComponent } from './theme/components/menu/vertical-menu/vertical-menu.component';
import { FooterComponent } from './theme/components/footer/footer.component';
import { OAuthComponent } from './pages/authentication/oauth/oauth.component';
import { GraphQLModule } from './graphql.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';
import { environment } from '../environments/environment';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { CookieService } from 'ngx-cookie-service';
import { AppState } from './core/states/app/app.state';
import { appRoutes } from './app.routes';
import { UIState } from './core/states/ui/ui.state';
import { RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module } from 'ng-recaptcha';
import * as firebase from 'firebase/app';
import { isPlatformBrowser } from '@angular/common';
import { TranslateBrowserLoader } from './core/services/translate.browser.loader';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { QuillConfig, QuillModule } from 'ngx-quill';
import { RouterModule } from '@angular/router';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatInputModule } from '@angular/material/input';
import { PropertyState } from './core/states/property/property.state';
import { NotificationMenuComponent } from './theme/components/notification-menu/notification-menu.component';
import { ProjectState } from '@sohuu/app/core/states/project/project.state';

const config: InputFileConfig = {
    fileAccept: '*',
};

export let masksOptions: Partial<IConfig> | (() => Partial<IConfig>);

export const translateConfig = {
    loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient, transferState: TransferState) =>
            new TranslateBrowserLoader('/assets/i18n/', '.json', transferState, http),
        deps: [HttpClient, TransferState],
    },
};

export const quillConfig: QuillConfig = {
    modules: {
        toolbar: {
            container: [
                [{ font: [] }],
                [{ size: [] }],
                ['bold', 'italic', 'underline'],
                [{ color: [] }, { background: [] }],
                [{ list: 'ordered' }, { list: 'bullet' }],
                ['link'],
            ],
        },
    },
};

const materialModules = [
    MatSidenavModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatToolbarModule,
    MatTooltipModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatSnackBarModule,
];

@NgModule({
    declarations: [
        AppComponent,
        PagesComponent,
        NotFoundComponent,
        UserMenuComponent,
        NotificationMenuComponent,
        ContactsComponent,
        ToolbarComponent,
        HorizontalMenuComponent,
        VerticalMenuComponent,
        FooterComponent,
        OAuthComponent,
    ],
    imports: [
        BrowserModule.withServerTransition({ appId: 'sohuu' }),
        BrowserAnimationsModule,
        TransferHttpCacheModule,
        BrowserTransferStateModule,
        FormsModule,
        HttpClientModule,
        EmbedVideo.forRoot(),
        InputFileModule.forRoot(config),
        RouterModule.forRoot(appRoutes, {
            initialNavigation: 'enabled',
        }),
        NgxsModule.forRoot([AppState, UIState, PropertyState, ProjectState], {
            developmentMode: !environment.production,
        }),
        NgxsLoggerPluginModule.forRoot({
            logger: console,
            collapsed: false,
            disabled: true,
        }),
        NgxsReduxDevtoolsPluginModule.forRoot({
            disabled: environment.production,
        }),
        SharedModule,
        NgxMaskModule.forRoot(masksOptions),
        GraphQLModule,
        TranslateModule.forRoot(translateConfig),
        RecaptchaV3Module,
        QuillModule.forRoot(quillConfig),
        ...materialModules,
    ],
    providers: [
        { provide: OverlayContainer, useClass: CustomOverlayContainer },
        { provide: RECAPTCHA_V3_SITE_KEY, useValue: environment.reCaptchaKey },
        CookieService,
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
    constructor(@Inject(PLATFORM_ID) private platformId: Object) {
        if (isPlatformBrowser(this.platformId)) {
            firebase.initializeApp(environment.firebase);
        }
    }
}
