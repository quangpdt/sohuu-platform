import { UserModel } from '@sohuu-platform/models';
import { City, District, Ward } from '@sohuu-platform/interfaces';

export enum AppAction {
    Initialize = '[Auth] Initialize Authentication action',
    Login = '[Auth] Login action',
    Register = '[Auth] Register action',
    LoginWithThirdParty = '[Auth] Login With Third Party action',
    ForgotPassword = '[Auth] Forgot Password action',
    ResetPassword = '[Auth] Reset Password action',
    Logout = '[Auth] Logout action',
    SetUserInfo = '[Auth] Set User Info From API action',
    UpdateUserInfo = '[User] Update User Info action',
    UpdateUserAddress = '[User] Update User Address action',
    GetSummaryNotifications = '[User] Get Quick User Notification action',
}

export class Initialize {
    public static readonly type = AppAction.Initialize;
}

export class Login {
    public static readonly type = AppAction.Login;
    constructor(public email: string, public password: string, public token: string) {}
}

export class LoginWithThirdParty {
    public static readonly type = AppAction.LoginWithThirdParty;
    constructor(public isFirstTimeLogin: boolean) {}
}

export class Logout {
    public static readonly type = AppAction.Logout;
    constructor() {}
}

export class SetUserInfo {
    public static readonly type = AppAction.SetUserInfo;
    constructor(public user: Partial<UserModel>) {}
}

export class UpdateUserInfo {
    public static readonly type = AppAction.UpdateUserInfo;
    constructor(public user: Partial<UserModel>) {}
}

export class UpdateUserAddress {
    public static readonly type = AppAction.UpdateUserAddress;
    constructor(public location: string, public city: City, public district: District, public ward: Ward) {}
}

export class GetSummaryNotifications {
    public static readonly type = AppAction.GetSummaryNotifications;
}
