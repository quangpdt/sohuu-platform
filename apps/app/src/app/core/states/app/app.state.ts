import { Action, Selector, State, StateContext } from '@ngxs/store';
import { AuthenticationService } from '../../services/authentication.service';
import { NotificationService } from '../../services/notification.service';
import { Router } from '@angular/router';
import { Injectable, NgZone } from '@angular/core';
import { AppStateModel } from './app-state.model';
import {
    GetSummaryNotifications,
    Initialize,
    Login,
    LoginWithThirdParty,
    Logout,
    SetUserInfo,
    UpdateUserAddress,
    UpdateUserInfo,
} from './app.action';
import { UserModel } from '@sohuu-platform/models';
import { routePaths } from '../../route.names';
import { UserService } from '../../services/user.service';
import { UserNotificationService } from '../../services/user-notification.service';
import { map } from 'rxjs/operators';

@State<AppStateModel>({
    name: 'AppState',
    defaults: {
        user: undefined,
        notifications: undefined,
    },
})
@Injectable()
export class AppState {
    constructor(
        private authenticationService: AuthenticationService,
        private notificationService: NotificationService,
        private userService: UserService,
        private userNotificationService: UserNotificationService,
        private router: Router,
        private zone: NgZone
    ) {}

    @Selector()
    public static getUser(state: AppStateModel): any {
        return state.user;
    }

    @Selector()
    public static getNotifications(state: AppStateModel): any {
        return state.notifications;
    }

    @Action(Initialize)
    public initialize(context: StateContext<AppStateModel>) {
        this.authenticationService.whoAmI().subscribe((user) => {
            context.patchState({
                user,
            });

            if (!user) {
                this.authenticationService.logout();
            } else {
                context.dispatch(new GetSummaryNotifications());
            }
        });
    }

    @Action(Login)
    public login(context: StateContext<AppStateModel>, { email, password, token }: Login) {
        this.authenticationService.login(email, password, token).subscribe(
            async (payload: UserModel) => {
                context.patchState({
                    user: payload,
                });

                await this.notificationService.notifySuccess('loginSuccessfully');
                return this.router.navigate([routePaths.home]);
            },
            (err) => {
                this.notificationService.notifyError('loginUnsuccessfully');
            }
        );
    }

    @Action(LoginWithThirdParty)
    public loginWithThirdParty(context: StateContext<AppStateModel>, { isFirstTimeLogin }: LoginWithThirdParty) {
        this.authenticationService.whoAmI().subscribe(
            async (user: UserModel) => {
                context.patchState({
                    user,
                });
                await this.notificationService.notifySuccess('loginSuccessfully');
                if (isFirstTimeLogin) {
                    this.router.navigate([routePaths.verifyAccount]);
                } else {
                    this.router.navigate([routePaths.home]);
                }
            },
            (err) => {
                this.notificationService.notifyError('loginUnsuccessfully');
            }
        );
    }

    @Action(Logout)
    public logout(context: StateContext<AppStateModel>) {
        context.patchState({
            user: undefined,
        });
        this.authenticationService.logout();
        this.zone.run(() => this.router.navigate([routePaths.home]));
    }

    @Action(SetUserInfo)
    public updateUserInfoAction(context: StateContext<AppStateModel>, { user }: SetUserInfo) {
        context.patchState({
            user: {
                ...context.getState().user,
                ...user,
            },
        });
    }

    @Action(UpdateUserInfo)
    public updateUserInfo(context: StateContext<AppStateModel>, { user }: UpdateUserInfo) {
        this.userService.updateUserBasicInfo(user.name).subscribe(
            (result) => {
                context.dispatch(new SetUserInfo(result));
                this.notificationService.notifySuccess('updateSuccessfully');
            },
            () => this.notificationService.notifyError('updateUnsuccessfully')
        );
    }

    @Action(UpdateUserAddress)
    public updateUserAddress(
        context: StateContext<AppStateModel>,
        { location, city, district, ward }: UpdateUserAddress
    ) {
        this.userService.updateUserAddress(location, city, district, ward).subscribe(
            (result) => {
                this.notificationService.notifySuccess('updateSuccessfully');
            },
            () => this.notificationService.notifyError('updateUnsuccessfully')
        );
    }

    @Action(GetSummaryNotifications)
    public getSummaryNotifications(context: StateContext<AppStateModel>) {
        return this.userNotificationService.getSummaryNotifications().pipe(
            map((data) => {
                context.patchState({
                    notifications: data,
                });
            })
        );
    }
}
