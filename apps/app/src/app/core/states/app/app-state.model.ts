import { UserModel, UserNotificationModel } from '@sohuu-platform/models';

export interface AppStateModel {
    user: Partial<UserModel>;
    notifications: Partial<UserNotificationModel>[];
}
