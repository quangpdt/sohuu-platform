export interface UIStateModel {
    hasBgImage: boolean;
    displayFixedMainToolbar: boolean;
}
