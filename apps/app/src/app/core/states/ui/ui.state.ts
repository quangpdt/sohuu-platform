import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { DisplayFixedMainToolbar, ShowHeaderBgImage } from './ui.action';
import { UIStateModel } from './ui.state-model';

@State<UIStateModel>({
    name: 'UIState',
    defaults: {
        hasBgImage: false,
        displayFixedMainToolbar: false,
    },
})
@Injectable({
    providedIn: 'root',
})
export class UIState {
    @Selector()
    public static hasBgImage(state: UIStateModel): boolean {
        return state.hasBgImage;
    }

    @Selector()
    public static shouldDisplayFixedMainToolbar(state: UIStateModel): boolean {
        return state.displayFixedMainToolbar;
    }

    @Action(ShowHeaderBgImage)
    public showHeaderBgImage(context: StateContext<UIStateModel>, { showBgImage }: ShowHeaderBgImage) {
        context.patchState({
            hasBgImage: showBgImage,
        });
    }

    @Action(DisplayFixedMainToolbar)
    public displayFixedMainToolbar(
        context: StateContext<UIStateModel>,
        { displayFixedMainToolbar }: DisplayFixedMainToolbar
    ) {
        context.patchState({
            displayFixedMainToolbar,
        });
    }
}
