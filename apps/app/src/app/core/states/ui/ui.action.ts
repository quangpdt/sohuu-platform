export enum UIAction {
    ShowHeaderBgImage = '[UI] Show Header Background Image action',
    DisplayFixedMainToolbar = '[UI] Display Fixed Main Toolbar action',
}

export class ShowHeaderBgImage {
    public static readonly type = UIAction.ShowHeaderBgImage;
    constructor(public showBgImage: boolean) {}
}

export class DisplayFixedMainToolbar {
    public static readonly type = UIAction.DisplayFixedMainToolbar;
    constructor(public displayFixedMainToolbar: boolean) {}
}
