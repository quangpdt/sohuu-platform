import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { PropertyService } from '../../services/property.service';
import {
    ChangePageSize,
    ChangeSortOption,
    FilterCities,
    FilterDistricts,
    FilterWards,
    LoadAllUnits,
    LoadAllUnitsBySlug,
    LoadAllUnitsBySlugDone,
    LoadCities,
    LoadDistricts,
    LoadHomePageProperties,
    LoadProperties,
    LoadPropertyDetail,
    LoadWards,
    ResetAllFilteredUnits,
    ResetHomePageProperties,
    ResetProperties,
    ResetPropertyDetail,
} from './property.action';
import { of } from 'rxjs';
import { map, mergeAll, tap, toArray } from 'rxjs/operators';
import * as findLast from 'lodash/findLast';
import * as isEmpty from 'lodash/isEmpty';
import * as size from 'lodash/size';
import { PropertyModel } from '@sohuu-platform/models';
import { PropertyPublicService } from '../../services/property.public-service';
import { City, District, Ward } from '@sohuu-platform/interfaces';
import { LoadState, SortOption } from '@sohuu-platform/enums';

export interface PropertyStateModel {
    cities: City[];
    districts: District[];
    wards: Ward[];
    filteredCities: City[];
    filteredDistricts: District[];
    filteredWards: Ward[];
    homePageProperties: Partial<PropertyModel>[];

    properties: Partial<PropertyModel>[];
    currentPage: number;
    totalPage: number;
    totalProperties: number;
    loadState: LoadState;
    pageSize: number;
    sortOption: SortOption;

    selectedProperty: Partial<PropertyModel>;
}

@State<PropertyStateModel>({
    name: 'PropertyState',
    defaults: {
        cities: [],
        districts: [],
        wards: [],
        filteredCities: [],
        filteredDistricts: [],
        filteredWards: [],
        homePageProperties: [],
        properties: [],
        currentPage: 0,
        totalPage: 0,
        totalProperties: 0,
        loadState: LoadState.Completed,
        pageSize: 8,
        sortOption: SortOption.Default,
        selectedProperty: undefined,
    },
})
@Injectable()
export class PropertyState {
    private static cities: City[];
    private static districts: District[];
    private static wards: Ward[];

    @Selector()
    public static getCities(state: PropertyStateModel): City[] {
        return state.cities;
    }

    @Selector()
    public static getFilteredCities(state: PropertyStateModel): City[] {
        return state.filteredCities;
    }

    @Selector()
    public static getFilteredDistricts(state: PropertyStateModel): District[] {
        return state.filteredDistricts;
    }

    @Selector()
    public static getFilteredWards(state: PropertyStateModel): Ward[] {
        return state.filteredWards;
    }

    @Selector()
    public static getSelectedProperty(state: PropertyStateModel): Partial<PropertyModel> {
        return state.selectedProperty;
    }

    @Selector()
    public static getFilteredAdministrativeUnits(state: PropertyStateModel): any {
        let city, district, ward;
        if (size(state.filteredCities) > 0) {
            city = state.filteredCities[0];
        }
        if (size(state.filteredDistricts) > 0) {
            district = state.filteredDistricts[0];
        }
        if (size(state.filteredWards) > 0) {
            ward = state.filteredWards[0];
        }

        return {
            city,
            district,
            ward,
        };
    }

    @Selector()
    public static getHomePageProperties(state: PropertyStateModel): any {
        return state.homePageProperties;
    }

    // TODO: Add Pagination here
    @Selector()
    public static getProperties(state: PropertyStateModel): any {
        return state.properties;
    }

    @Selector()
    public static canLoadMore(state: PropertyStateModel): boolean {
        return state.currentPage < state.totalPage;
    }

    @Selector()
    public static getLoadState(state: PropertyStateModel): LoadState {
        return state.loadState;
    }

    @Selector()
    public static getTotalProperties(state: PropertyStateModel): number {
        return state.totalProperties;
    }

    @Selector()
    public static getPageSize(state: PropertyStateModel): number {
        return state.pageSize;
    }

    @Selector()
    public static getCurrentPage(state: PropertyStateModel): number {
        return state.currentPage;
    }

    constructor(private propertyService: PropertyService, private propertyPublicService: PropertyPublicService) {}

    @Action(LoadCities)
    public async loadCities(context: StateContext<PropertyStateModel>, { defaultValue }: LoadCities) {
        let stream$;
        if (PropertyService.districts) {
            stream$ = of(PropertyService.districts);
        } else {
            stream$ = this.propertyService.getCities().pipe(
                tap((data) => {
                    PropertyState.cities = data;
                })
            );
        }

        stream$.subscribe((cities: City[]) => {
            context.patchState({
                cities,
            });
            context.dispatch(new FilterCities(defaultValue));
        });
    }

    @Action(LoadDistricts)
    public async loadDistricts(context: StateContext<PropertyStateModel>, { cityId, defaultValue }: LoadDistricts) {
        if (!cityId) {
            context.patchState({
                districts: [],
                filteredDistricts: [],
            });
        }

        let stream$;
        if (PropertyService.districts) {
            stream$ = of(PropertyService.districts);
        } else {
            stream$ = this.propertyService.getDistricts().pipe(
                tap((data) => {
                    PropertyState.districts = data;
                })
            );
        }

        stream$.subscribe((data: District[]) => {
            const districts = data.filter((district: District) => district.cityId === cityId);
            context.patchState({
                districts,
            });
            context.dispatch(new FilterDistricts(defaultValue));
        });
    }

    @Action(LoadWards)
    public async getWards(context: StateContext<PropertyStateModel>, { districtId, defaultValue }: LoadWards) {
        if (!districtId) {
            context.patchState({
                wards: [],
                filteredWards: [],
            });
        }

        let stream$;
        if (PropertyService.wards) {
            stream$ = of(PropertyService.wards);
        } else {
            stream$ = this.propertyService.getWards().pipe(
                tap((data) => {
                    PropertyState.wards = data;
                })
            );
        }

        stream$.subscribe((data: Ward[]) => {
            const wards = data.filter((ward: Ward) => ward.districtId === districtId);
            context.patchState({
                wards,
            });
            context.dispatch(new FilterWards(defaultValue));
        });
    }

    @Action(FilterCities)
    public async filterCities(context: StateContext<PropertyStateModel>, { searchValue }: FilterCities) {
        const cities = context.getState().cities;
        if (isEmpty(searchValue)) {
            context.patchState({
                filteredCities: cities,
            });
            return;
        }

        let filterValue, filteredCities;
        if (typeof searchValue === 'object') {
            filterValue = searchValue.id;
            filteredCities = cities.filter((city) => city.id === filterValue);
        } else {
            filterValue = searchValue.toLowerCase();
            filteredCities = cities.filter((city) => city.name.toLowerCase().includes(filterValue));
        }

        context.patchState({
            filteredCities,
        });
    }

    @Action(FilterDistricts)
    public filterDistricts(context: StateContext<PropertyStateModel>, { searchValue }: FilterDistricts) {
        const districts = context.getState().districts;
        if (districts.length === 0) {
            return;
        }

        if (isEmpty(searchValue)) {
            context.patchState({
                filteredDistricts: districts,
            });
            return;
        }

        let filterValue, filteredDistricts;

        if (typeof searchValue === 'object') {
            filterValue = searchValue.id;
            filteredDistricts = districts.filter((district) => district.id === filterValue);
        } else {
            filterValue = searchValue.toLowerCase();
            filteredDistricts = districts.filter((district) => district.name.toLowerCase().includes(filterValue));
        }

        context.patchState({
            filteredDistricts,
        });
    }

    @Action(FilterWards)
    public filterWards(context: StateContext<PropertyStateModel>, { searchValue }: FilterWards) {
        const wards = context.getState().wards;
        if (wards.length === 0) {
            return;
        }
        if (isEmpty(searchValue)) {
            context.patchState({
                filteredWards: wards,
            });
            return;
        }

        let filterValue, filteredWards;
        if (typeof searchValue === 'object') {
            filterValue = searchValue.id;
            filteredWards = wards.filter((ward) => ward.id === filterValue);
        } else {
            filterValue = searchValue.toLowerCase();
            filteredWards = wards.filter((ward) => ward.name.toLowerCase().includes(filterValue));
        }

        context.patchState({
            filteredWards,
        });
    }

    @Action(LoadAllUnits)
    public loadAllUnits(context: StateContext<PropertyStateModel>, { cityId, districtId, wardId }: LoadAllUnits) {
        return of(...this.loadInitialUnits()).pipe(
            mergeAll(1),
            toArray(),
            map(([cities, districts, wards]: any[]) => {
                const filteredCity = findLast(cities, (p: City) => p.id === cityId);
                const filteredDistrict = findLast(districts, (p: District) => p.id === districtId);
                const filteredWard = findLast(wards, (p: Ward) => p.id === wardId);

                context.dispatch(new LoadCities(filteredCity));
                context.dispatch(new LoadDistricts(filteredDistrict.cityId, filteredDistrict));
                context.dispatch(new LoadWards(filteredWard.districtId, filteredWard));
            }),
            tap(() => {
                setTimeout(() => {
                    context.dispatch(new LoadAllUnitsBySlugDone());
                }, 100);
            })
        );
    }

    @Action(LoadAllUnitsBySlug)
    public loadAllUnitsBySlug(
        context: StateContext<PropertyStateModel>,
        { citySlug, districtSlug, wardSlug }: LoadAllUnitsBySlug
    ) {
        return of(...this.loadInitialUnits()).pipe(
            mergeAll(1),
            toArray(),
            map(([cities, districts, wards]: any[]) => {
                if (!citySlug) {
                    context.patchState({
                        filteredCities: [],
                        filteredDistricts: [],
                        filteredWards: [],
                    });
                    return;
                }

                const filteredCity = findLast(cities, (p: City) => p.slug === citySlug);
                context.dispatch(new LoadCities(filteredCity));

                if (!districtSlug) {
                    context.patchState({
                        filteredDistricts: [],
                        filteredWards: [],
                    });
                    return;
                }

                const filteredDistrict = findLast(districts, (p: District) => p.slug === districtSlug);
                context.dispatch(new LoadDistricts(filteredDistrict.cityId, filteredDistrict));
                if (!wardSlug) {
                    context.patchState({
                        filteredWards: [],
                    });
                    return;
                }

                const filteredWard = findLast(wards, (p: Ward) => p.slug === wardSlug);
                context.dispatch(new LoadWards(filteredWard.districtId, filteredWard));
            }),
            tap(() => {
                setTimeout(() => {
                    context.dispatch(new LoadAllUnitsBySlugDone());
                }, 100);
            })
        );
    }

    @Action(LoadAllUnitsBySlugDone)
    public loadAllUnitsBySlugDone(_context: StateContext<PropertyStateModel>) {}

    @Action(ResetAllFilteredUnits)
    public resetAllFilteredUnits(context: StateContext<PropertyStateModel>): void {
        context.patchState({
            filteredCities: undefined,
            filteredDistricts: undefined,
            filteredWards: undefined,
        });
    }

    @Action(LoadHomePageProperties)
    public loadHomePageProperties(context: StateContext<PropertyStateModel>): void {
        this.propertyPublicService.getHomePageProperties().subscribe((data) => {
            context.patchState({
                homePageProperties: data,
            });
        });
    }

    @Action(ResetProperties)
    public resetProperties(context: StateContext<PropertyStateModel>): void {
        context.patchState({
            currentPage: 0,
            totalPage: 0,
            totalProperties: 0,
            loadState: LoadState.Completed,
        });
    }

    @Action(ResetHomePageProperties)
    public resetHomePageProperties(context: StateContext<PropertyStateModel>): void {
        context.patchState({
            homePageProperties: [],
        });
    }

    @Action(LoadProperties)
    public loadProperties(context: StateContext<PropertyStateModel>, { searchCriteria }: LoadProperties): void {
        context.patchState({
            loadState: LoadState.Loading,
        });

        this.propertyPublicService.getProperties(searchCriteria).subscribe((data) => {
            context.patchState({
                properties: data.data,
                totalPage: Math.ceil(data.total / searchCriteria.pageSize),
                totalProperties: data.total,
                currentPage: searchCriteria.page,
                pageSize: searchCriteria.pageSize,
                loadState: LoadState.Completed,
            });
        });
    }

    @Action(ChangeSortOption)
    public changeSortOption(context: StateContext<PropertyStateModel>, { sortOption }: ChangeSortOption): void {
        context.patchState({
            sortOption,
        });
    }

    @Action(ChangePageSize)
    public changePageSize(context: StateContext<PropertyStateModel>, { pageSize }: ChangePageSize): void {
        context.patchState({
            pageSize,
        });
    }

    @Action(LoadPropertyDetail)
    public loadPropertyDetail(context: StateContext<PropertyStateModel>, { id, slug }: LoadPropertyDetail) {
        const currentSelectedProperty = context.getState().selectedProperty;
        if (currentSelectedProperty && currentSelectedProperty.id === id && currentSelectedProperty.slug === slug) {
            return;
        }

        return this.propertyPublicService.getProperty(id, slug).pipe(
            tap((property: Partial<PropertyModel>) => {
                context.patchState({
                    selectedProperty: property,
                });
            })
        );
    }

    @Action(ResetPropertyDetail)
    public resetPropertyDetail(context: StateContext<PropertyStateModel>) {
        context.patchState({
            selectedProperty: undefined,
        });
    }

    private loadInitialUnits() {
        let cities$, districts$, wards$;

        if (PropertyService.cities) {
            cities$ = of(PropertyService.cities);
        } else {
            cities$ = this.propertyService.getCities().pipe(
                tap((data) => {
                    PropertyState.cities = data;
                })
            );
        }

        if (PropertyService.districts) {
            districts$ = of(PropertyService.districts);
        } else {
            districts$ = this.propertyService.getDistricts().pipe(
                tap((data) => {
                    PropertyState.districts = data;
                })
            );
        }

        if (PropertyService.wards) {
            wards$ = of(PropertyService.wards);
        } else {
            wards$ = this.propertyService.getWards().pipe(
                tap((data) => {
                    PropertyState.wards = data;
                })
            );
        }

        return [cities$, districts$, wards$];
    }
}
