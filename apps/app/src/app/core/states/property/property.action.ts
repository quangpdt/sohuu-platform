import { PropertySearchInput } from '@sohuu-platform/input-types';
import { ResponseCode, SortOption } from '@sohuu-platform/enums';
import { City, District, Ward } from '@sohuu-platform/interfaces';

export enum PropertyAction {
    LoadCities = '[PropertyAddress] Load Cities action',
    FilterCities = '[PropertyAddress] Filter Cities action',

    LoadDistricts = '[PropertyAddress] Load District action',
    FilterDistricts = '[PropertyAddress] Filter District action',

    LoadWards = '[PropertyAddress] Load Wards action',
    FilterWards = '[PropertyAddress] Filter Wards action',

    LoadAllUnits = '[PropertyAddress] Load All Units action',
    LoadAllUnitsBySlug = '[PropertyAddress] Load All Units By Slugs action',
    LoadAllUnitsBySlugDone = '[PropertyAddress] Load All Units By Slugs Done action',
    ResetAllFilteredUnits = '[PropertyAddress] Reset All Filtered Units action',

    ResetProperties = '[PropertiesFilter] Reset Property List action',
    ResetHomePageProperties = '[PropertiesFilter] Reset Home Page Properties action',
    LoadProperties = '[PropertiesFilter] Load Property List action',
    ChangeSortOption = '[PropertiesFilter] Change Sort Option action',
    ChangePageSize = '[PropertiesFilter] Change Page Size action',

    LoadHomePageProperties = '[HomePageProperties] Load Home Page Properties action',

    LoadPropertyDetail = '[Property] Load Property Detail action',
    ResetPropertyDetail = '[Property] Reset Property Detail action',
    LoadPropertyDetailDone = '[Property] Load Property Detail Done action',
    LoadPropertyDetailFailed = '[Property] Load Property Detail Failed action',
}

export class LoadCities {
    public static readonly type = PropertyAction.LoadCities;
    constructor(public defaultValue?: City) {}
}

export class LoadDistricts {
    public static readonly type = PropertyAction.LoadDistricts;
    constructor(public cityId: number, public defaultValue?: District) {}
}

export class LoadWards {
    public static readonly type = PropertyAction.LoadWards;
    constructor(public districtId: number, public defaultValue?: Ward) {}
}

export class LoadAllUnits {
    public static readonly type = PropertyAction.LoadAllUnits;
    constructor(public cityId: number, public districtId: number, public wardId: number) {}
}

export class LoadAllUnitsBySlug {
    public static readonly type = PropertyAction.LoadAllUnitsBySlug;
    constructor(public citySlug?: string, public districtSlug?: string, public wardSlug?: string) {}
}

export class LoadAllUnitsBySlugDone {
    public static readonly type = PropertyAction.LoadAllUnitsBySlugDone;
    constructor() {}
}

export class ResetAllFilteredUnits {
    public static readonly type = PropertyAction.ResetAllFilteredUnits;
    constructor() {}
}

export class FilterCities {
    public static readonly type = PropertyAction.FilterCities;
    constructor(public searchValue: any) {}
}

export class FilterDistricts {
    public static readonly type = PropertyAction.FilterDistricts;
    constructor(public searchValue: any) {}
}

export class FilterWards {
    public static readonly type = PropertyAction.FilterWards;
    constructor(public searchValue: any) {}
}

export class ResetProperties {
    public static readonly type = PropertyAction.ResetProperties;
    constructor() {}
}

export class ResetHomePageProperties {
    public static readonly type = PropertyAction.ResetHomePageProperties;
    constructor() {}
}

export class LoadProperties {
    public static readonly type = PropertyAction.LoadProperties;
    constructor(public searchCriteria?: PropertySearchInput) {}
}

export class ChangeSortOption {
    public static readonly type = PropertyAction.ChangeSortOption;
    constructor(public sortOption: SortOption) {}
}

export class ChangePageSize {
    public static readonly type = PropertyAction.ChangePageSize;
    constructor(public pageSize: number) {}
}

export class LoadHomePageProperties {
    public static readonly type = PropertyAction.LoadHomePageProperties;
    constructor() {}
}

export class LoadPropertyDetail {
    public static readonly type = PropertyAction.LoadPropertyDetail;
    constructor(public id: number, public slug: string) {}
}

export class ResetPropertyDetail {
    public static readonly type = PropertyAction.ResetPropertyDetail;
    constructor() {}
}

export class LoadPropertyDetailDone {
    public static readonly type = PropertyAction.LoadPropertyDetailDone;
}

export class LoadPropertyDetailFailed {
    public static readonly type = PropertyAction.LoadPropertyDetailFailed;
    constructor(public errorCode: ResponseCode) {}
}
