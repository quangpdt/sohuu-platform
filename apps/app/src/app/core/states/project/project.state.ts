import { ProjectModel } from '@sohuu-platform/models';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { LoadHomePageProjects } from '@sohuu/app/core/states/project/project.action';
import { ProjectPublicService } from '@sohuu/app/core/services/project.public-service';
import { map } from 'rxjs/operators';

export interface ProjectStateModel {
    homepageProjects: Partial<ProjectModel>[];
}

@State<ProjectStateModel>({
    name: 'ProjectState',
    defaults: {
        homepageProjects: undefined,
    },
})
@Injectable()
export class ProjectState {
    constructor(private projectPublicService: ProjectPublicService) {}

    @Selector()
    public static getHomePageProjects(state: ProjectStateModel): Partial<ProjectModel>[] {
        return state.homepageProjects;
    }

    @Action(LoadHomePageProjects)
    public loadHomePageProjects(context: StateContext<ProjectStateModel>, { sortOption }: LoadHomePageProjects) {
        return this.projectPublicService.getHomePageProjects(sortOption).pipe(
            map((projects: Partial<ProjectModel>[]) => {
                context.patchState({
                    homepageProjects: projects,
                });
            })
        );
    }
}
