import { ProjectSortOption } from '@sohuu-platform/enums';

export enum ProjectAction {
    LoadHomePageProject = '[HomePageProjects] Load Home Page Projects action',
}

export class LoadHomePageProjects {
    public static type = ProjectAction.LoadHomePageProject;
    constructor(public sortOption: ProjectSortOption) {}
}
