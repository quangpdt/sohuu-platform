import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Apollo } from 'apollo-angular';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { UserNotificationModel } from '@sohuu-platform/models';
import {
    deleteAllGql,
    deleteGql,
    getNotificationsGql,
    markAllAsReadGql,
    markAsReadGql,
    requestPropertyCallbackGql,
    summaryNotificationsGql,
} from '@sohuu/app/core/services/gql/notification.gql';

@Injectable({ providedIn: 'root' })
export class UserNotificationService {
    constructor(private http: HttpClient, private apollo: Apollo) {}

    public requestPropertyCallback(propertyId: number, formValue: any) {
        return this.apollo
            .mutate({
                mutation: requestPropertyCallbackGql,
                variables: {
                    requestCallbackInput: {
                        propertyId,
                        ...formValue,
                    },
                },
            })
            .pipe(
                take(1),
                map((result: any) => result)
            );
    }

    public getSummaryNotifications(): Observable<Partial<UserNotificationModel>[]> {
        return this.apollo
            .query({
                query: summaryNotificationsGql,
                fetchPolicy: 'network-only',
            })
            .pipe(map((result: any) => result.data.getSummaryNotifications));
    }

    public getNotifications(page: number, pageSize: number): Observable<any> {
        return this.apollo
            .query({
                query: getNotificationsGql,
                fetchPolicy: 'network-only',
                variables: {
                    getNotificationsInput: {
                        page,
                        pageSize,
                    },
                },
            })
            .pipe(
                take(1),
                map((result: any) => result.data.getNotifications.data)
            );
    }

    public markAsRead(id: number) {
        return this.apollo
            .mutate({
                mutation: markAsReadGql,
                variables: {
                    id,
                },
            })
            .pipe(take(1));
    }

    public markAllAsRead() {
        return this.apollo
            .mutate({
                mutation: markAllAsReadGql,
            })
            .pipe(take(1));
    }

    public delete(id: number) {
        return this.apollo
            .mutate({
                mutation: deleteGql,
                variables: {
                    id,
                },
            })
            .pipe(take(1));
    }

    public deleteAll() {
        return this.apollo
            .mutate({
                mutation: deleteAllGql,
            })
            .pipe(take(1));
    }
}
