import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { PropertyModel } from '@sohuu-platform/models';
import { catchError, map, take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Apollo } from 'apollo-angular';
import { handleGraphQLError } from '../../shared/util.function';
import { RouteNames, routePaths } from '../route.names';
import * as isEmpty from 'lodash/isEmpty';
import * as findLastIndex from 'lodash/findLastIndex';
import { Params, UrlSegment } from '@angular/router';
import { PropertySearchInput } from '@sohuu-platform/input-types';
import { propertyFeatures, PropertyStatus, propertyStatuses, PropertyType, propertyTypes } from '@sohuu-platform/enums';
import { Pagination, PropertySearchCriteria } from '@sohuu-platform/interfaces';
import {
    getHomePagePropertiesGql,
    getPublicPropertiesGql,
    getPublicPropertyGql,
    getRelatedPropertiesGql,
} from '@sohuu/app/core/services/gql/property.gql';

@Injectable({ providedIn: 'root' })
export class PropertyPublicService {
    constructor(private http: HttpClient, private apollo: Apollo) {}

    public getProperty(id: number, slug: string): Observable<Partial<PropertyModel>> {
        return this.apollo
            .query({
                query: getPublicPropertyGql,
                variables: {
                    findByIdAndSlugInput: {
                        id,
                        slug,
                    },
                },
                fetchPolicy: 'cache-first',
            })
            .pipe(
                take(1),
                map((result: any) => {
                    result = result.data.getPublicProperty;
                    if (result.metadata) {
                        this.parseMetadata(result.metadata);
                    }
                    return result;
                }),
                catchError((e) => throwError(handleGraphQLError(e)))
            );
    }

    public getHomePageProperties(): Observable<Partial<PropertyModel>[]> {
        return this.apollo
            .query({
                query: getHomePagePropertiesGql,
                fetchPolicy: 'cache-first',
            })
            .pipe(
                take(1),
                map((result: any) => {
                    result = result.data.getLatestProperties;
                    if (result && result.length > 0) {
                        result.forEach((property) => {
                            this.parseMetadata(property.metadata);
                        });
                    }
                    return result;
                }),
                catchError((e) => throwError(handleGraphQLError(e)))
            );
    }

    public getProperties(searchCriteria: PropertySearchInput): Observable<Pagination> {
        return this.apollo
            .query({
                query: getPublicPropertiesGql,
                variables: {
                    propertySearchInput: searchCriteria,
                },
                fetchPolicy: 'cache-first',
            })
            .pipe(
                take(1),
                map((result: any) => {
                    result = result.data.getPublicProperties.data;
                    const data = result.results;
                    if (data && data.length > 0) {
                        data.forEach((property) => {
                            this.parseMetadata(property.metadata);
                        });
                    }
                    return {
                        data,
                        total: result.total,
                    };
                }),
                catchError((e) => throwError(handleGraphQLError(e)))
            );
    }

    public getRelatedProperties(id: number, slug: string): Observable<Partial<PropertyModel>[]> {
        return this.apollo
            .query({
                query: getRelatedPropertiesGql,
                variables: {
                    publicPropertyInput: {
                        id,
                        slug,
                    },
                },
                fetchPolicy: 'cache-first',
            })
            .pipe(
                take(1),
                map((result: any) => {
                    result = result.data.getRelatedProperties;
                    if (result && result.length > 0) {
                        result.forEach((property) => {
                            this.parseMetadata(property.metadata);
                        });
                    }
                    return result;
                }),
                catchError((e) => throwError(handleGraphQLError(e)))
            );
    }

    public mapSearchCriteriaToSearchInput(searchCriteria: PropertySearchCriteria): PropertySearchInput {
        const {
            propertyType,
            propertyStatus,
            priceFrom,
            priceTo,
            bedrooms,
            bathrooms,
            areaFrom,
            areaTo,
            page,
            pageSize,
        } = searchCriteria;

        let { city, district, ward } = searchCriteria;

        if (isEmpty(city)) {
            city = null;
        }

        if (isEmpty(district)) {
            district = null;
        }

        if (isEmpty(ward)) {
            ward = null;
        }

        return {
            propertyType,
            propertyStatus,
            priceFrom: priceFrom ? parseInt(priceFrom, 10) : undefined,
            priceTo: priceTo ? parseInt(priceTo, 10) : undefined,
            city,
            district,
            ward,
            bedrooms: bedrooms ? parseInt(bedrooms, 10) : undefined,
            bathrooms: bathrooms ? parseInt(bathrooms, 10) : undefined,
            areaFrom: areaFrom ? parseInt(areaFrom, 10) : undefined,
            areaTo: areaTo ? parseInt(areaTo, 10) : undefined,
            page,
            pageSize,
        };
    }

    public mapSearchCriteriaToRoutePath(searchCriteria: PropertySearchCriteria): any {
        if (!searchCriteria) {
            return {
                routeCommands: [routePaths.properties],
            };
        }

        const {
            propertyType,
            propertyStatus,
            priceFrom,
            priceTo,
            city,
            district,
            ward,
            bedrooms,
            bathrooms,
            areaFrom,
            areaTo,
            features,
            page,
            pageSize,
        } = searchCriteria;

        let routePath;
        const routeCommands = [];
        const queryParams = {};

        switch (propertyStatus) {
            case PropertyStatus.ForRent:
                routePath = routePaths.rentProperties;
                switch (propertyType) {
                    case PropertyType.House:
                        routePath = routePaths.rentHouses;
                        break;
                    case PropertyType.Apartment:
                        routePath = routePaths.rentApartments;
                        break;
                    case PropertyType.Office:
                        routePath = routePaths.rentOffice;
                        break;
                }
                break;
            case PropertyStatus.ForSale:
                routePath = routePaths.buyProperties;
                switch (propertyType) {
                    case PropertyType.House:
                        routePath = routePaths.buyHouses;
                        break;
                    case PropertyType.Apartment:
                        routePath = routePaths.buyApartments;
                        break;
                    case PropertyType.Office:
                        routePath = routePaths.rentOffice;
                        break;
                    default:
                        break;
                }
                break;
            default:
                routePath = routePaths.buyRent;
                switch (propertyType) {
                    case PropertyType.House:
                        routePath = routePaths.buyRentHouses;
                        break;
                    case PropertyType.Apartment:
                        routePath = routePaths.buyRentApartments;
                        break;
                    case PropertyType.Office:
                        routePath = routePaths.rentOffice;
                        break;
                    default:
                        break;
                }
                break;
        }

        routeCommands.push(routePath);
        if (city) {
            routeCommands.push(city.slug);
            if (district) {
                routeCommands.push(district.slug);

                if (ward) {
                    routeCommands.push(ward.slug);
                }
            }
        }

        if (priceFrom || priceTo) {
            queryParams['gia'] = `${priceFrom || 0},${priceTo || ''}`;
        }

        if (areaFrom || areaTo) {
            queryParams['dien-tich'] = `${areaFrom || 0},${areaTo || ''}`;
        }

        if (bathrooms) {
            queryParams['phong-tam'] = bathrooms;
        }

        if (bedrooms) {
            queryParams['phong-ngu'] = bedrooms;
        }

        if (features && features.length > 0 && features.filter((p) => p.selected).length > 0) {
            queryParams['tien-ich'] = features
                .filter((item) => item.selected)
                .map((item) => item.id)
                .join(',');
        }

        queryParams['trang'] = page;
        queryParams['hien-thi'] = pageSize;

        return {
            routeCommands: routeCommands || [routePaths.properties],
            queryParams,
        };
    }

    public mapRoutePathToSearchCriteria(segments: UrlSegment[], queryParams: Params): PropertySearchCriteria {
        let propertyStatus, propertyType;

        if (segments && segments.length > 0) {
            const rawPropertyStatus = segments[0].path;

            switch (rawPropertyStatus) {
                case RouteNames.RentProperties:
                    propertyStatus = propertyStatuses[1];
                    break;
                case RouteNames.BuyProperties:
                    propertyStatus = propertyStatuses[0];
                    break;
                case RouteNames.BuyHouses:
                    propertyStatus = propertyStatuses[0];
                    propertyType = propertyTypes[0];
                    break;
                case RouteNames.BuyApartments:
                    propertyStatus = propertyStatuses[0];
                    propertyType = propertyTypes[1];
                    break;
                case RouteNames.RentHouses:
                    propertyStatus = propertyStatuses[1];
                    propertyType = propertyTypes[0];
                    break;
                case RouteNames.RentApartments:
                    propertyStatus = propertyStatuses[1];
                    propertyType = propertyTypes[1];
                    break;
                case RouteNames.BuyRentHouses:
                    propertyType = propertyTypes[0];
                    break;
                case RouteNames.BuyRentApartments:
                    propertyType = propertyTypes[1];
            }
        }

        let priceFrom, priceTo;
        let areaFrom, areaTo;
        let bedrooms, bathrooms;
        let featureIds = [];

        if (queryParams['gia']) {
            const prices = queryParams['gia'].split(',');
            if (prices.length > 0) {
                priceFrom = parseInt(prices[0], 10) || 0;
                if (prices.length > 1) {
                    priceTo = parseInt(prices[1], 10) || 0;
                }
            }
        }
        if (queryParams['dien-tich']) {
            const areas = queryParams['dien-tich'].split(',');
            if (areas.length > 0) {
                areaFrom = parseInt(areas[0], 10) || 0;
                if (areas.length > 1) {
                    areaTo = parseInt(areas[1], 10) || 0;
                }
            }
        }

        if (queryParams['phong-ngu']) {
            bedrooms = parseInt(queryParams['phong-ngu'], 10) || undefined;
        }

        if (queryParams['phong-tam']) {
            bathrooms = parseInt(queryParams['phong-tam'], 10) || undefined;
        }

        if (queryParams['tien-ich']) {
            featureIds = queryParams['tien-ich']
                .split(',')
                .filter((p) => !isNaN(parseInt(p, 10)))
                .map((p) => parseInt(p, 10));
        }

        const page = queryParams['trang'] ? parseInt(queryParams['trang'] || 0, 10) : 0;
        const pageSize = queryParams['hien-thi'] ? parseInt(queryParams['hien-thi'] || 12, 10) : 12;

        const features = propertyFeatures.map((feature) => {
            return {
                id: feature,
                selected: findLastIndex(featureIds, (id) => id === feature) >= 0,
            };
        });

        return {
            propertyStatus,
            propertyType,
            priceFrom,
            priceTo,
            areaFrom,
            areaTo,
            bedrooms,
            bathrooms,
            features,
            page,
            pageSize,
            city: null,
            district: null,
            ward: null,
        };
    }

    private parseMetadata(metadata) {
        metadata.forEach((meta) => {
            // If the parsedValue already parsed before in MemoryCache, don't do that again
            if (meta.parsedValue) {
                return;
            }
            if (meta.name === 'legal') {
                meta.parsedValue = parseInt(meta.value, 10);
                return;
            }
            meta.parsedValue = JSON.parse(meta.value);
        });
    }
}
