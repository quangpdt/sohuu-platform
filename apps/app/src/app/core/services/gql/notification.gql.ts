import gql from 'graphql-tag';

export const requestPropertyCallbackGql = gql`
    mutation requestPropertyCallback($requestCallbackInput: RequestCallbackInput!) {
        requestPropertyCallback(requestCallbackInput: $requestCallbackInput) {
            message
        }
    }
`;

export const summaryNotificationsGql = gql`
    query getSummaryNotifications {
        getSummaryNotifications {
            id
            type
            status
            parsedMessage
            user {
                id
                name
            }
        }
    }
`;

export const getNotificationsGql = gql`
    query getNotifications($getNotificationsInput: GetNotificationsInput!) {
        getNotifications(getNotificationsInput: $getNotificationsInput) {
            data
        }
    }
`;

export const markAsReadGql = gql`
    mutation markNotificationAsRead($id: ID!) {
        markNotificationAsRead(id: $id) {
            message
        }
    }
`;

export const markAllAsReadGql = gql`
    mutation markAllNotificationsAsRead {
        markAllNotificationsAsRead {
            message
        }
    }
`;

export const deleteGql = gql`
    mutation deleteNotification($id: ID!) {
        deleteNotification(id: $id) {
            message
        }
    }
`;

export const deleteAllGql = gql`
    mutation deleteAllNotifications {
        deleteAllNotifications {
            message
        }
    }
`;
