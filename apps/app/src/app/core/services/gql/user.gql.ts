import gql from 'graphql-tag';

export const updateBasicInfoGql = gql`
    mutation updateBasicInfo($updateBasicInfoInput: UpdateBasicInfoInput!) {
        updateBasicInfo(updateBasicInfoInput: $updateBasicInfoInput) {
            id
            name
            email
            isPhoneVerified
            phone
            isActivated
        }
    }
`;

export const updateUserAddressGql = gql`
    mutation updateUserAddress($updateUserAddressInput: UpdateUserAddressInput!) {
        updateUserAddress(updateUserAddressInput: $updateUserAddressInput) {
            message
        }
    }
`;
