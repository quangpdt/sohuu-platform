import gql from 'graphql-tag';

export const submitPropertyGql = gql`
    mutation submitProperty($submitPropertyInput: SubmitPropertyInput!) {
        submitProperty(submitPropertyInput: $submitPropertyInput) {
            id
        }
    }
`;

export const updatePropertyGql = gql`
    mutation updateProperty($submitPropertyInput: SubmitPropertyInput!) {
        updateProperty(submitPropertyInput: $submitPropertyInput) {
            id
        }
    }
`;

export const reSubmitPropertyGql = gql`
    mutation reSubmitProperty($id: ID!) {
        reSubmitProperty(id: $id) {
            message
        }
    }
`;

export const getCitiesGql = gql`
    query getCities {
        getCities {
            result
        }
    }
`;

export const getDistrictsGql = gql`
    query getDistricts {
        getDistricts {
            result
        }
    }
`;

export const getWardsGql = gql`
    query getWards {
        getWards {
            result
        }
    }
`;

export const getPropertyGql = gql`
    query getProperty($id: ID!) {
        getProperty(id: $id) {
            id
            title
            description
            propertyType
            location
            cityId
            districtId
            wardId
            submitStatus
            fullLocation
            lat
            lng
            statuses {
                status
                price
                negotiable
            }
            metadata {
                name
                value
            }
            files {
                id
                fileName
            }
        }
    }
`;

export const getOwnedPropertiesGql = gql`
    query getOwnedProperties {
        getOwnedProperties {
            id
            slug
            title
            submitStatus
            views
            publishedAt
            expiredAt
            files {
                fileName
            }
        }
    }
`;

export const getPublicPropertyGql = gql`
    query getPublicProperty($findByIdAndSlugInput: FindByIdAndSlugInput!) {
        getPublicProperty(findByIdAndSlugInput: $findByIdAndSlugInput) {
            id
            title
            slug
            description
            propertyType
            location
            cityId
            districtId
            wardId
            submitStatus
            fullLocation
            owner
            publishedAt
            lat
            lng
            statuses {
                status
                price
                negotiable
            }
            metadata {
                name
                value
            }
            files {
                id
                fileName
            }
        }
    }
`;

export const getPublicPropertiesGql = gql`
    query getPublicProperties($propertySearchInput: PropertySearchInput!) {
        getPublicProperties(propertySearchInput: $propertySearchInput) {
            data
        }
    }
`;

export const getHomePagePropertiesGql = gql`
    query getLatestProperties {
        getLatestProperties {
            id
            title
            slug
            description
            propertyType
            location
            cityId
            districtId
            wardId
            submitStatus
            fullLocation
            owner
            publishedAt
            statuses {
                status
                price
                negotiable
            }
            metadata {
                name
                value
            }
            files {
                id
                fileName
            }
        }
    }
`;

export const getRelatedPropertiesGql = gql`
    query getRelatedProperties($publicPropertyInput: PublicPropertyInput!) {
        getRelatedProperties(publicPropertyInput: $publicPropertyInput) {
            id
            title
            slug
            description
            propertyType
            location
            cityId
            districtId
            wardId
            submitStatus
            fullLocation
            owner
            publishedAt
            statuses {
                status
                price
                negotiable
            }
            metadata {
                name
                value
            }
            files {
                id
                fileName
            }
        }
    }
`;
