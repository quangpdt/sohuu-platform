import gql from 'graphql-tag';

export const loginGql = gql`
    mutation login($loginInput: LoginInput!) {
        login(loginInput: $loginInput) {
            expiresIn
            refreshToken
            accessToken
        }
    }
`;

export const registerGql = gql`
    mutation register($registerInput: RegisterInput!) {
        register(registerInput: $registerInput) {
            data
            message
        }
    }
`;

export const reActivateUserGql = gql`
    mutation reActivateUser($email: String!) {
        reActivateUser(email: $email) {
            data
            message
        }
    }
`;

export const activateUserGql = gql`
    mutation activateUser($activateUserInput: ActivateUserInput!) {
        activateUser(activateUserInput: $activateUserInput) {
            data
            message
        }
    }
`;

export const forgotPasswordGql = gql`
    mutation forgotPassword($forgotPasswordInput: ForgotPasswordInput!) {
        forgotPassword(forgotPasswordInput: $forgotPasswordInput) {
            data
            message
        }
    }
`;

export const resetPasswordGql = gql`
    mutation resetPassword($resetPasswordInput: ResetPasswordInput!) {
        resetPassword(resetPasswordInput: $resetPasswordInput) {
            data
            message
        }
    }
`;

export const whoAmIGql = gql`
    query whoAmI {
        whoAmI {
            id
            name
            email
            isPhoneVerified
            phone
            isActivated
            metadata {
                name
                value
            }
        }
    }
`;

export const requestPhoneVerificationGql = gql`
    mutation requestPhoneVerification($phoneVerificationInput: PhoneVerificationInput!) {
        requestPhoneVerification(phoneVerificationInput: $phoneVerificationInput) {
            data
            message
        }
    }
`;

export const validatePhoneNumberGql = gql`
    mutation validatePhoneNumber($code: String!) {
        validatePhoneNumber(code: $code) {
            data
            message
        }
    }
`;
