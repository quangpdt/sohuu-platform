import gql from 'graphql-tag';

export const getHomePageProjectsGql = gql`
    query getHomePageProjects($sortOption: Int!) {
        getHomePageProjects(sortOption: $sortOption) {
            id
            title
            slug
            location
            metadata {
                name
                value
            }
            files {
                id
                fileName
            }
        }
    }
`;

export const getPublicProjectGql = gql`
    query getPublicProject($findByIdAndSlugInput: FindByIdAndSlugInput!) {
        getPublicProject(findByIdAndSlugInput: $findByIdAndSlugInput) {
            id
            title
            slug
            description
            location
            files {
                id
                fileName
            }
        }
    }
`;
