import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UploadedFile } from '@sohuu-platform/interfaces';

@Injectable({ providedIn: 'root' })
export class UploadService {
    constructor(private httpClient: HttpClient) {}

    public uploadFiles(files: File[]) {
        const formData = new FormData();
        files.forEach((file: File) => {
            formData.append('files', file);
        });

        return this.httpClient.post<UploadedFile[]>('/api/resource/property-files', formData);
    }

    public fetchDownloadedImage(url: string): Promise<Blob> {
        return this.httpClient
            .get(url, {
                responseType: 'blob',
            })
            .toPromise();
    }
}
