import { Injectable } from '@angular/core';
import { map, take } from 'rxjs/operators';
import { Apollo } from 'apollo-angular';
import { City, District, Ward } from '@sohuu-platform/interfaces';
import { updateBasicInfoGql, updateUserAddressGql } from '@sohuu/app/core/services/gql/user.gql';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private apollo: Apollo) {}

    public updateUserBasicInfo(name: string) {
        return this.apollo
            .mutate({
                mutation: updateBasicInfoGql,
                variables: {
                    updateBasicInfoInput: {
                        name,
                    },
                },
            })
            .pipe(
                take(1),
                map((result: any) => result)
            );
    }

    public updateUserAddress(location: string, city: City, district: District, ward: Ward) {
        return this.apollo
            .mutate({
                mutation: updateUserAddressGql,
                variables: {
                    updateUserAddressInput: {
                        location,
                        city,
                        district,
                        ward,
                    },
                },
            })
            .pipe(
                take(1),
                map((result: any) => result)
            );
    }
}
