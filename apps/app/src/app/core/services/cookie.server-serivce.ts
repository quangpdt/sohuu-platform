import { Inject, Injectable, Optional } from '@angular/core';
import { REQUEST } from '@nguniversal/express-engine/tokens';
import * as isEmpty from 'lodash/isEmpty';
import * as trim from 'lodash/trim';

@Injectable()
export class CookieServerService {
    constructor(@Optional() @Inject(REQUEST) private request: any) {}

    public getAll() {
        return this.request.headers.cookie;
    }

    public get(name?: string) {
        const cookie = this.request.headers.cookie;
        if (isEmpty(cookie)) {
            return null;
        }

        const cookies = cookie.split(';').map((p) => {
            p = trim(p);
            const [key, value] = p.split('=').map((item) => item.trim());
            return {
                key,
                value,
            };
        });

        if (name) {
            return cookies[name];
        }

        return cookies;
    }
}
