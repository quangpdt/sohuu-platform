import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Observable, throwError } from 'rxjs';
import { ProjectModel } from '@sohuu-platform/models';
import { catchError, map, take } from 'rxjs/operators';
import { handleGraphQLError } from '@sohuu/app/shared/util.function';
import { getHomePageProjectsGql, getPublicProjectGql } from '@sohuu/app/core/services/gql/project.gql';
import { ProjectSortOption } from '@sohuu-platform/enums';

@Injectable({
    providedIn: 'root',
})
export class ProjectPublicService {
    constructor(private apollo: Apollo) {}

    public getPublicProject(id: number, slug: string): Observable<Partial<ProjectModel>> {
        return this.apollo
            .query({
                query: getPublicProjectGql,
                variables: {
                    findByIdAndSlugInput: {
                        id,
                        slug,
                    },
                },
                fetchPolicy: 'cache-first',
            })
            .pipe(
                take(1),
                map((result: any) => {
                    result = result.data.getPublicProject;
                    return result;
                }),
                catchError((e) => throwError(handleGraphQLError(e)))
            );
    }

    public getHomePageProjects(
        sortOption: ProjectSortOption = ProjectSortOption.RecentlyAdded
    ): Observable<Partial<ProjectModel>[]> {
        return this.apollo
            .query({
                query: getHomePageProjectsGql,
                fetchPolicy: 'cache-first',
                variables: {
                    sortOption,
                },
            })
            .pipe(
                take(1),
                map((result: any) => {
                    result = result.data.getHomePageProjects;
                    return result;
                }),
                catchError((e) => throwError(handleGraphQLError(e)))
            );
    }
}
