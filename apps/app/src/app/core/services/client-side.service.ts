import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable({ providedIn: 'root' })
export class ClientSideService {
    constructor(@Inject(PLATFORM_ID) private platformId: Object) {}

    public doOnClient(func: any): void {
        if (isPlatformBrowser(this.platformId)) {
            func();
        }
    }
}
