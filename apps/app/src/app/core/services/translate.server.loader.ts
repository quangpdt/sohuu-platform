import { TranslateLoader } from '@ngx-translate/core';
import { join } from 'path';
import { makeStateKey, StateKey, TransferState } from '@angular/platform-browser';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { take } from 'rxjs/operators';
import { readFileSync } from 'fs';

export class TranslateServerLoader implements TranslateLoader {
    constructor(
        private prefix: string = 'i18n',
        private suffix: string = '.json',
        private transferState: TransferState
    ) {}

    public getTranslation(lang: string): Observable<any> {
        const assetsFolder = join(process.cwd(), 'dist', 'apps', 'app', 'browser', this.prefix);

        const jsonData = JSON.parse(readFileSync(`${assetsFolder}/${lang}${this.suffix}`, 'utf8'));

        const key: StateKey<number> = makeStateKey<number>(`${environment.stateKey.translate}-${lang}`);
        this.transferState.set(key, jsonData);
        return of(jsonData).pipe(take(1));
    }
}
