import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';

@Injectable({ providedIn: 'root' })
export class NotificationService {
    private swal: any;

    constructor(private translateService: TranslateService) {
        this.swal = Swal.mixin({
            customClass: {
                container: 'notification-container',
            },
        });
    }

    public notifySuccess(title: string) {
        return this.swal.fire({
            position: 'top-end',
            icon: 'success',
            text: this.translateService.instant(title || 'success'),
            showConfirmButton: false,
            timer: 2000,
            timerProgressBar: true,
        });
    }

    public notifyError(message: string, autoDismiss: boolean = true) {
        return this.swal.fire({
            icon: 'error',
            position: 'center',
            text: this.translateService.instant(message || 'errorOccurred'),
            showConfirmButton: true,
            timer: autoDismiss ? 3000 : undefined,
        });
    }

    public notifyConfirmation(title: string, content: string) {
        return this.swal
            .fire({
                icon: 'info',
                title: this.translateService.instant(title),
                text: this.translateService.instant(content),
                showConfirmButton: true,
                showCancelButton: true,
                confirmButtonText: this.translateService.instant('confirm'),
                cancelButtonText: this.translateService.instant('cancel'),
                reverseButtons: true,
                customClass: {
                    container: 'confirmation-container',
                },
            })
            .then((result) => {
                return !!result.value;
            });
    }
}
