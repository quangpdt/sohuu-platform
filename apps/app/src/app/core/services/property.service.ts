import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Apollo } from 'apollo-angular';
import * as findLastIndex from 'lodash/findLastIndex';
import * as isEmpty from 'lodash/isEmpty';
import * as get from 'lodash/get';
import { PropertyModel } from '@sohuu-platform/models';
import { map, take } from 'rxjs/operators';
import { SubmitPropertyInput } from '@sohuu-platform/input-types';
import { City, District, LocationSearchResult, Ward } from '@sohuu-platform/interfaces';
import { propertyFeatures, PropertyMetadataName, PropertyStatus } from '@sohuu-platform/enums';
import {
    getCitiesGql,
    getDistrictsGql,
    getOwnedPropertiesGql,
    getPropertyGql,
    getWardsGql,
    reSubmitPropertyGql,
    submitPropertyGql,
    updatePropertyGql,
} from '@sohuu/app/core/services/gql/property.gql';

@Injectable({
    providedIn: 'root',
})
export class PropertyService {
    public static cities: City[];
    public static wards: Ward[];
    public static districts: District[];
    private vietMapsAPIKey = '200efe9747190d9480869f35074be6c3865aa8de39706c46';

    constructor(private http: HttpClient, private apollo: Apollo) {}

    public getCities(): Observable<City[]> {
        return this.apollo
            .query({
                query: getCitiesGql,
                fetchPolicy: 'cache-first',
            })
            .pipe(
                take(1),
                map((data: any) => data.data.getCities.result)
            );
    }

    public getDistricts(): Observable<District[]> {
        return this.apollo
            .query({
                query: getDistrictsGql,
                fetchPolicy: 'cache-first',
            })
            .pipe(
                take(1),
                map((data: any) => data.data.getDistricts.result)
            );
    }

    public getWards(): Observable<Ward[]> {
        return this.apollo
            .query({
                query: getWardsGql,
                fetchPolicy: 'cache-first',
            })
            .pipe(
                take(1),
                map((data: any) => data.data.getWards.result)
            );
    }

    public createProperty(data: any): Observable<any> {
        const submitPropertyInput: SubmitPropertyInput = this.mapFormToPropertyInput(data);

        return this.apollo.mutate({
            mutation: submitPropertyGql,
            variables: {
                submitPropertyInput,
            },
        });
    }

    public updateProperty(propertyId: number, data: any): Observable<any> {
        const submitPropertyInput: SubmitPropertyInput = this.mapFormToPropertyInput(data);
        submitPropertyInput.id = propertyId;

        return this.apollo.mutate({
            mutation: updatePropertyGql,
            variables: {
                submitPropertyInput,
            },
        });
    }

    public reSubmitProperty(propertyId: number): Observable<any> {
        return this.apollo
            .mutate({
                mutation: reSubmitPropertyGql,
                variables: {
                    id: propertyId,
                },
            })
            .pipe(take(1));
    }

    private mapFormToPropertyInput(formData: any): SubmitPropertyInput {
        if (formData.basic.rentPrice) {
            formData.basic.rentPrice = parseInt(formData.basic.rentPrice, 10);
        }
        if (formData.basic.salePrice) {
            formData.basic.salePrice = parseInt(formData.basic.salePrice, 10);
        }

        formData.additional.bedrooms = parseInt(formData.additional.bedrooms, 10);
        formData.additional.bathrooms = parseInt(formData.additional.bathrooms, 10);
        formData.additional.floors = parseInt(formData.additional.floors, 10);
        formData.additional.area = parseInt(formData.additional.area, 10);

        if (formData.additional.features) {
            formData.additional.features = formData.additional.features
                .filter((item) => item.selected)
                .map((item) => item.id);
        }

        if (formData.additional.additionalFeatures) {
            formData.additional.additionalFeatures = formData.additional.additionalFeatures.filter(
                (item) => !isEmpty(item.name) && !isEmpty(item.value)
            );
        }

        return formData as SubmitPropertyInput;
    }

    public getProperty(id: number): Observable<Partial<PropertyModel>> {
        return this.apollo
            .query({
                query: getPropertyGql,
                variables: {
                    id,
                },
            })
            .pipe(
                take(1),
                map((result: any) => {
                    result = result.data.getProperty;
                    if (result.metadata) {
                        result.metadata.forEach((meta) => {
                            if (meta.name === 'legal') {
                                meta.parsedValue = meta.value;
                                return;
                            }
                            meta.parsedValue = JSON.parse(meta.value);
                        });
                    }
                    return result;
                })
            );
    }

    public getOwnedProperties(): Observable<Partial<PropertyModel>[]> {
        return this.apollo
            .query({
                query: getOwnedPropertiesGql,
            })
            .pipe(
                take(1),
                map((result: any) => {
                    return result.data.getOwnedProperties;
                })
            );
    }

    public mapPropertyToForm(propertyModel: Partial<PropertyModel>): any {
        const form = {
            basic: {},
            address: {},
            additional: {},
        };

        const { title, description, location, cityId, districtId, wardId } = propertyModel;

        let salePrice, rentPrice, saleNegotiable, rentNegotiable;
        let mainDoorDirection, kitchenDirection, balconyDirection;
        let bedrooms, bathrooms, floors, area;
        let legal;
        let features, additionalFeatures;

        const propertyType = propertyModel.propertyType;
        const propertyStatus = propertyModel.statuses.map((item) => {
            const status = item.status;
            if (status === PropertyStatus.ForRent) {
                rentPrice = item.price;
                rentNegotiable = item.negotiable || false;
            } else if (status === PropertyStatus.ForSale) {
                salePrice = item.price;
                saleNegotiable = item.negotiable || false;
            }

            return status;
        });

        propertyModel.metadata.forEach((meta) => {
            const data = meta.parsedValue;
            switch (meta.name) {
                case PropertyMetadataName.Fengshui:
                    mainDoorDirection = data.mainDoorDirection;
                    balconyDirection = data.balconyDirection;
                    kitchenDirection = data.kitchenDirection;
                    break;
                case PropertyMetadataName.Structure:
                    bedrooms = data.bedrooms;
                    bathrooms = data.bathrooms;
                    floors = data.floors;
                    area = data.area;
                    break;
                case PropertyMetadataName.Features:
                    features = propertyFeatures.map((feature) => ({
                        id: feature,
                        selected: findLastIndex(data.features, (p) => p === feature) >= 0,
                    }));
                    additionalFeatures = data.additionalFeatures || [];
                    break;
                case PropertyMetadataName.Legal:
                    legal = parseInt(data, 10);
                    break;
            }
        });

        form.basic = {
            title,
            description,
            propertyType,
            propertyStatus,
            salePrice,
            rentPrice,
            saleNegotiable,
            rentNegotiable,
        };
        form.address = {
            location,
            cityId,
            districtId,
            wardId,
        };

        form.additional = {
            bedrooms,
            bathrooms,
            floors,
            area,
            legal,
            mainDoorDirection,
            kitchenDirection,
            balconyDirection,
            features,
            additionalFeatures,
        };

        return form;
    }

    public searchAddress(keyword: string, lat?: number, lng?: number): Observable<LocationSearchResult[]> {
        let isKeywordModified = false;
        let actualHouseNumber;

        if (keyword.indexOf('/') > 0) {
            isKeywordModified = true;
            // Only search for combination: first house number + street name + ...
            const parts = keyword.split('/');
            const firstHouseNumber = parts[0].trim();

            // 5 Le Duan, Buon Ma Thuot => Le Duan, Buon Ma Thuot
            const rawRestOfAddress = parts[parts.length - 1];
            const lastHouseNumber = rawRestOfAddress.split(' ').shift();
            const restOfAddress = parts.join(' ');

            keyword = `${firstHouseNumber} ${restOfAddress}`;
            parts.pop();
            actualHouseNumber = `${parts.join('/')}/${lastHouseNumber}`;
        }

        const params = {
            'api-version': '1.1',
            layers: 'address',
            apikey: this.vietMapsAPIKey,
            text: keyword,
        };

        if (lat) {
            params['lat'] = lat.toString(10);
        }

        if (lng) {
            params['lng'] = lng.toString(10);
        }

        return this.http
            .get<any>('/maps/api/search', {
                params,
            })
            .pipe(
                map((result) => result.data?.features || []),
                map((locations) => {
                    const searchResults: LocationSearchResult[] = [];
                    if (locations.length === 0) {
                        return searchResults;
                    }

                    locations.forEach((location) => {
                        const rawCoordinates = get(location, 'geometry.coordinates');
                        const rawProperties = get(location, 'properties');

                        if (!(rawCoordinates && rawProperties)) {
                            return;
                        }

                        const coordinates = {
                            lng: rawCoordinates[0],
                            lat: rawCoordinates[1],
                        };

                        const searchResult: LocationSearchResult = {
                            ward: rawProperties.locality,
                            district: rawProperties.county,
                            city: rawProperties.region,
                            street: rawProperties.street,
                            houseNumber: actualHouseNumber || rawProperties.housenumber,
                            coordinates,
                        } as LocationSearchResult;

                        const houseAddress = [searchResult.houseNumber, searchResult.street]
                            .filter((p) => !!p)
                            .join(' ');

                        searchResult.fullLocation = [
                            houseAddress,
                            searchResult.ward,
                            searchResult.district,
                            searchResult.city,
                        ]
                            .filter((p) => !!p)
                            .join(', ');
                        searchResults.push(searchResult);
                    });

                    return searchResults;
                })
            );
    }
}
