import { Injectable } from '@angular/core';
import { routePaths } from '../route.names';
import { Params } from '@angular/router';
import { InterestInput } from '@sohuu-platform/interfaces';

@Injectable({
    providedIn: 'root',
})
export class InterestCalculatorService {
    public calculateInterest(years: number, interestRate: number, amount: number) {
        const months = years * 12;
        const results = [];
        interestRate = interestRate / 100;
        const installment = amount / months;
        for (let i = 0; i < years; i++) {
            const annualResults = [];
            let annualTotal = 0;
            for (let j = 1; j <= 12; j++) {
                const interest = (interestRate / 12) * amount;
                const total = interest + installment;
                amount -= installment;
                annualTotal += total;
                const result = {
                    period: i * 12 + j,
                    interest,
                    installment,
                    total,
                    amount,
                };
                annualResults.push(result);
            }
            results.push({
                year: i,
                annualTotal,
            });
            results.push(...annualResults);
        }
        return results;
    }

    public mapInterestInputToRoutePath(input: InterestInput): any {
        if (!input) {
            return {
                routeCommands: [routePaths.interest],
            };
        }

        const { years, amount, maximumDebt, interestRate } = input;

        const queryParams = {
            nam: years,
            'so-tien-vay': amount,
            'vay-toi-da': maximumDebt,
            'lai-suat': interestRate,
            id: input.propertyId,
            'tin-dang': input.propertySlug,
        };

        return {
            routeCommands: [routePaths.interest],
            queryParams,
        };
    }

    public mapRoutePathToInterestInput(queryParams: Params): InterestInput {
        const input: InterestInput = {} as InterestInput;
        if (queryParams['nam']) {
            input.years = parseInt(queryParams['nam'], 10) || null;
        }

        if (queryParams['so-tien-vay']) {
            input.amount = parseInt(queryParams['so-tien-vay'], 10) || null;
        }

        if (queryParams['vay-toi-da']) {
            input.maximumDebt = parseFloat(queryParams['vay-toi-da']) || null;
        }

        if (queryParams['lai-suat']) {
            input.interestRate = parseFloat(queryParams['lai-suat']) || null;
        }

        if (queryParams['id'] && queryParams['tin-dang']) {
            input.propertyId = parseInt(queryParams['id'], 10) || null;
            input.propertySlug = queryParams['tin-dang'];
        }

        return input;
    }
}
