import { Inject, Injectable, Optional, PLATFORM_ID } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Apollo } from 'apollo-angular';
import { catchError, map, switchMap, take, tap } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { UserModel } from '@sohuu-platform/models';
import { environment } from '../../../environments/environment';
import { ClientSideService } from './client-side.service';
import { isPlatformServer } from '@angular/common';
import { CookieServerService } from './cookie.server-serivce';
import { handleGraphQLError } from '../../shared/util.function';
import {
    activateUserGql,
    forgotPasswordGql,
    loginGql,
    reActivateUserGql,
    registerGql,
    requestPhoneVerificationGql,
    resetPasswordGql,
    validatePhoneNumberGql,
    whoAmIGql,
} from '@sohuu/app/core/services/gql/authentication.gql';

@Injectable({
    providedIn: 'root',
})
export class AuthenticationService {
    public jwtHelperService;

    constructor(
        private http: HttpClient,
        private apollo: Apollo,
        private cookieService: CookieService,
        private localService: ClientSideService,
        @Inject(PLATFORM_ID) private platformId: Object,
        @Optional() private cookieServerService: CookieServerService
    ) {
        this.jwtHelperService = new JwtHelperService();
    }

    public isAuthenticated(): boolean {
        let token;
        if (isPlatformServer(this.platformId)) {
            token = this.cookieServerService.get('accessToken');
        } else {
            token = this.cookieService.get('accessToken');
        }
        if (!token) {
            return false;
        }

        return !this.jwtHelperService.isTokenExpired(token);
    }

    public login(email: string, password: string, token: string): Observable<any> {
        return this.apollo
            .mutate({
                mutation: loginGql,
                variables: {
                    loginInput: {
                        email,
                        password,
                        token,
                    },
                },
            })
            .pipe(
                take(1),
                switchMap((data) => this.whoAmI())
            );
    }

    public register(formValue: any) {
        return this.apollo
            .mutate({
                mutation: registerGql,
                variables: {
                    registerInput: formValue,
                },
            })
            .pipe(take(1));
    }

    public logout(): void {
        this.cookieService.delete('accessToken', '/', undefined, environment.cookieSecure, 'Strict');
        this.cookieService.delete('refreshToken', '/', undefined, environment.cookieSecure, 'Strict');
        this.localService.doOnClient(() => localStorage.removeItem('user'));
    }

    public forgotPassword(email: string): Observable<boolean> {
        return this.apollo
            .mutate({
                mutation: forgotPasswordGql,
                variables: {
                    forgotPasswordInput: {
                        email,
                    },
                },
            })
            .pipe(
                take(1),
                map((result: any) => true),
                catchError(() => of(false))
            );
    }

    public resetPassword(formValue: any): Observable<boolean> {
        return this.apollo
            .mutate({
                mutation: resetPasswordGql,
                variables: {
                    resetPasswordInput: formValue,
                },
            })
            .pipe(
                take(1),
                map((result: any) => true),
                catchError(() => of(false))
            );
    }

    public whoAmI(useCache: boolean = false): Observable<UserModel> {
        if (!this.isAuthenticated()) {
            return of(null);
        }
        return this.apollo
            .query({
                query: whoAmIGql,
                fetchPolicy: useCache ? 'cache-first' : 'network-only',
            })
            .pipe(
                take(1),
                map((result: any) => result.data.whoAmI),
                tap(() => this.apollo.getClient().cache.reset())
            );
    }

    public activateUser(email: string, token: string) {
        return this.apollo
            .mutate({
                mutation: activateUserGql,
                variables: {
                    activateUserInput: {
                        email,
                        token,
                    },
                },
            })
            .pipe(take(1));
    }

    public reActivateUser(email: string) {
        return this.apollo
            .mutate({
                mutation: reActivateUserGql,
                variables: { email },
            })
            .pipe(take(1));
    }

    public requestPhoneVerification(phoneNumber: string, recaptchaToken: string) {
        return this.apollo
            .mutate({
                mutation: requestPhoneVerificationGql,
                variables: { phoneVerificationInput: { phoneNumber, recaptchaToken } },
            })
            .pipe(
                take(1),
                catchError((error) => throwError(handleGraphQLError(error)))
            );
    }

    public validatePhoneNumber(code: string) {
        return this.apollo
            .mutate({
                mutation: validatePhoneNumberGql,
                variables: { code },
            })
            .pipe(
                take(1),
                catchError((error) => throwError(handleGraphQLError(error)))
            );
    }
}
