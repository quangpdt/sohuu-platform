import { Inject, Injectable } from '@angular/core';
/// <reference types="node" />
import { REQUEST } from '@nguniversal/express-engine/tokens';
import * as express from 'express';
import { HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class TranslateInterceptor implements HttpInterceptor {
    private readonly PORT = environment.clientPort;

    constructor(@Inject(REQUEST) private request: express.Request) {}

    public getBaseUrl(req: express.Request) {
        const { protocol, hostname } = req;
        return this.PORT ? `${protocol}://${hostname}:${this.PORT}` : `${protocol}://${hostname}`;
    }

    public intercept(request: HttpRequest<any>, next: HttpHandler) {
        if (request.url.startsWith('./assets')) {
            const baseUrl = this.getBaseUrl(this.request);
            request = request.clone({
                url: `${baseUrl}/${request.url.replace('./assets', 'assets')}`,
            });
        }
        return next.handle(request);
    }
}
