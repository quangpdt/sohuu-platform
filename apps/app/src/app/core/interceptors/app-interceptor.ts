import { Injectable } from '@angular/core';
import {
    HttpErrorResponse,
    HttpEvent,
    HttpHandler,
    HttpHeaders,
    HttpInterceptor,
    HttpRequest,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { CookieServerService } from '../services/cookie.server-serivce';

@Injectable()
export class AppInterceptor implements HttpInterceptor {
    constructor(private cookieServerService: CookieServerService) {}

    public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const cookies = this.cookieServerService.getAll();
        const headers = new HttpHeaders();
        if (cookies) {
            headers.set('cookie', cookies);
        }
        req = req.clone({
            withCredentials: true,
            headers: headers,
        });

        return next.handle(req).pipe(
            map((event: HttpEvent<any>) => event),
            catchError((error: HttpErrorResponse) => {
                const started = Date.now();
                const elapsed = Date.now() - started;
                console.log(error);
                console.log(`Request for ${req.urlWithParams} failed after ${elapsed} ms.`);
                // debugger;
                return throwError(error);
            })
        );
    }
}
