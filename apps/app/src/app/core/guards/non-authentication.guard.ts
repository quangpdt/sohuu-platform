import { Injectable } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Router } from '@angular/router';
import { routePaths } from '../route.names';

@Injectable({ providedIn: 'root' })
export class NonAuthenticationGuard {
    constructor(public authenticationService: AuthenticationService, public router: Router) {}

    public canActivate(): boolean {
        if (this.authenticationService.isAuthenticated()) {
            this.router.navigate([routePaths.home]);
            return false;
        }
        return true;
    }
}
