import { Injectable } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { routePaths } from '../route.names';

@Injectable({ providedIn: 'root' })
export class AccountVerificationGuard {
    constructor(public authenticationService: AuthenticationService, private router: Router) {}

    public canActivate(): Observable<boolean> {
        return this.authenticationService.whoAmI(true).pipe(
            map((data) => {
                const isPhoneVerified = !!data.phone && data.isPhoneVerified;
                if (isPhoneVerified) {
                    return true;
                } else {
                    this.router.navigate([routePaths.verifyAccount]);
                    return false;
                }
            }),
            catchError((err) => {
                this.router.navigate([routePaths.home]);
                return of(false);
            })
        );
    }
}
