import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class AppDataGuard {
    constructor(private translateService: TranslateService) {}

    public canActivate(): Observable<boolean> {
        this.translateService.setDefaultLang('vi');
        return this.translateService.use('vi').pipe(
            take(1),
            map(() => true),
            catchError(() => of(false))
        );
    }
}
