const fs = require('fs');
const path = require('path');
const prettier = require('prettier');

export const routes = [
    { route: '', name: 'home' },
    { route: 'dang-nhap', name: 'login' },
    { route: 'dang-ky', name: 'register' },
    { route: 'quen-mat-khau', name: 'forgotPassword' },
    { route: 'khoi-phuc-mat-khau', name: 'recoverPassword' },
    { route: 'kich-hoat-tai-khoan', name: 'activateAccount' },
    { route: 'xac-thuc-tai-khoan', name: 'verifyAccount' },
    { route: 'dang-xuat', name: 'logout' },
    { route: 've-chung-toi', name: 'aboutUs' },
    { route: 'lien-he', name: 'contact' },
    { route: 'dai-ly', name: 'agencies' },
    { route: 'so-sanh', name: 'compare' },
    { route: 'faq', name: 'fAQ' },
    { route: 'dieu-khoan-su-dung', name: 'termsConditions' },
    { route: 'dieu-khoan-bao-mat', name: 'privacyPolicies' },
    {
        route: 'tai-khoan',
        name: 'account',
        children: [
            { route: 'ho-so', name: 'profile' },
            { route: 'yeu-thich', name: 'myFavorites' },
            { route: 'tai-san', name: 'myProperties' },
            { route: 'thong-bao', name: 'myNotifications' },
        ],
    },
    {
        route: 'dang-tin',
        name: 'submitProperty',
        children: [
            {
                route: 'moi',
                name: 'newProperty',
            },
            {
                route: 'thanh-cong',
                name: 'propertySubmitted',
            },
            {
                route: 'sua',
                name: 'editProperty',
            },
        ],
    },
    {
        route: 'bat-dong-san',
        name: 'properties',
        children: [
            { route: 'thue', name: 'rentProperties' },
            { route: 'mua', name: 'buyProperties' },
            { route: 'mua-nha-dat', name: 'buyHouses' },
            { route: 'mua-can-ho', name: 'buyApartments' },
            { route: 'thue-nha-dat', name: 'rentHouses' },
            { route: 'thue-can-ho', name: 'rentApartments' },
            { route: 'thue-van-phong', name: 'rentOffice' },
            { route: 'mua-thue', name: 'buyRent' },
            { route: 'mua-thue-nha-dat', name: 'buyRentHouses' },
            { route: 'mua-thue-can-ho', name: 'buyRentApartments' },
        ],
    },
    {
        route: 'du-an',
        name: 'projects',
    },
    {
        route: 'khong-tim-thay',
        name: 'notFound',
    },
    {
        route: 'lai-suat',
        name: 'interest',
    },
];

function generateEnums(data) {
    const arr = [];
    data.forEach((route) => {
        const name = route.name.charAt(0).toUpperCase() + route.name.slice(1);
        arr.push(`${name} = '${route.route}'`);

        if (route.children) {
            arr.push(...generateEnums(route.children));
        }
    });
    return arr;
}

function generateRoute(data, parentRoute?: string) {
    const urls = [];
    data.forEach((route) => {
        const url = `${route.name}: '${parentRoute ? '/' + parentRoute : ''}/${route.route}'`;
        urls.push(url);

        if (route.children) {
            urls.push(...generateRoute(route.children, route.route));
        }
    });
    return urls;
}

const result = generateRoute(routes);
const enums = generateEnums(routes);
const enumStr = `export enum RouteNames {${enums}};`;
const routeStr = `export const routePaths={${result.toString()}};`;
const unFormattedCode = `${enumStr}\r\n${routeStr}`;
const formattedCode = prettier.format(unFormattedCode, { parser: 'typescript', tabWidth: 4 });

fs.writeFileSync(path.join(process.cwd(), 'apps/app/src/app/core', 'route.names.ts'), formattedCode, {
    encoding: 'utf8',
});
