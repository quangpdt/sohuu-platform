export enum RouteNames {
    Home = '',
    Login = 'dang-nhap',
    Register = 'dang-ky',
    ForgotPassword = 'quen-mat-khau',
    RecoverPassword = 'khoi-phuc-mat-khau',
    ActivateAccount = 'kich-hoat-tai-khoan',
    VerifyAccount = 'xac-thuc-tai-khoan',
    Logout = 'dang-xuat',
    AboutUs = 've-chung-toi',
    Contact = 'lien-he',
    Agencies = 'dai-ly',
    Compare = 'so-sanh',
    FAQ = 'faq',
    TermsConditions = 'dieu-khoan-su-dung',
    PrivacyPolicies = 'dieu-khoan-bao-mat',
    Account = 'tai-khoan',
    Profile = 'ho-so',
    MyFavorites = 'yeu-thich',
    MyProperties = 'tai-san',
    MyNotifications = 'thong-bao',
    SubmitProperty = 'dang-tin',
    NewProperty = 'moi',
    PropertySubmitted = 'thanh-cong',
    EditProperty = 'sua',
    Properties = 'bat-dong-san',
    RentProperties = 'thue',
    BuyProperties = 'mua',
    BuyHouses = 'mua-nha-dat',
    BuyApartments = 'mua-can-ho',
    RentHouses = 'thue-nha-dat',
    RentApartments = 'thue-can-ho',
    RentOffice = 'thue-van-phong',
    BuyRent = 'mua-thue',
    BuyRentHouses = 'mua-thue-nha-dat',
    BuyRentApartments = 'mua-thue-can-ho',
    Projects = 'du-an',
    NotFound = 'khong-tim-thay',
    Interest = 'lai-suat',
}
export const routePaths = {
    home: '/',
    login: '/dang-nhap',
    register: '/dang-ky',
    forgotPassword: '/quen-mat-khau',
    recoverPassword: '/khoi-phuc-mat-khau',
    activateAccount: '/kich-hoat-tai-khoan',
    verifyAccount: '/xac-thuc-tai-khoan',
    logout: '/dang-xuat',
    aboutUs: '/ve-chung-toi',
    contact: '/lien-he',
    agencies: '/dai-ly',
    compare: '/so-sanh',
    fAQ: '/faq',
    termsConditions: '/dieu-khoan-su-dung',
    privacyPolicies: '/dieu-khoan-bao-mat',
    account: '/tai-khoan',
    profile: '/tai-khoan/ho-so',
    myFavorites: '/tai-khoan/yeu-thich',
    myProperties: '/tai-khoan/tai-san',
    myNotifications: '/tai-khoan/thong-bao',
    submitProperty: '/dang-tin',
    newProperty: '/dang-tin/moi',
    propertySubmitted: '/dang-tin/thanh-cong',
    editProperty: '/dang-tin/sua',
    properties: '/bat-dong-san',
    rentProperties: '/bat-dong-san/thue',
    buyProperties: '/bat-dong-san/mua',
    buyHouses: '/bat-dong-san/mua-nha-dat',
    buyApartments: '/bat-dong-san/mua-can-ho',
    rentHouses: '/bat-dong-san/thue-nha-dat',
    rentApartments: '/bat-dong-san/thue-can-ho',
    rentOffice: '/bat-dong-san/thue-van-phong',
    buyRent: '/bat-dong-san/mua-thue',
    buyRentHouses: '/bat-dong-san/mua-thue-nha-dat',
    buyRentApartments: '/bat-dong-san/mua-thue-can-ho',
    projects: '/du-an',
    notFound: '/khong-tim-thay',
    interest: '/lai-suat',
};
