import { BrowserMock } from './app/mocks/browser.mock';

const browserMock = new BrowserMock();

global['window'] = browserMock.getWindow();
global['document'] = browserMock.getDocument();
global['navigator'] = browserMock.getNavigator();
global['Node'] = browserMock.getNode();

export { AppServerModule } from './app/app.server.module';
export { renderModule, renderModuleFactory } from '@angular/platform-server';
