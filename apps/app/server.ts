import 'zone.js/dist/zone-node';

import { ngExpressEngine } from '@nguniversal/express-engine';
import * as express from 'express';
import { join } from 'path';
import { AppServerModule } from './src/main.server';
import { APP_BASE_HREF } from '@angular/common';
import { existsSync } from 'fs';
import { createProxyMiddleware } from 'http-proxy-middleware';
import * as process from 'process';

require('dotenv').config({ path: join(process.cwd(), 'apps', 'app', '.env') });

export function app() {
    const server = express();
    const distFolder = join(process.cwd(), 'dist', 'apps', 'app', 'browser');
    const indexHtml = existsSync(join(distFolder, 'index.original.html')) ? 'index.original.html' : 'index';

    server.engine(
        'html',
        ngExpressEngine({
            bootstrap: AppServerModule,
        })
    );

    server.set('view engine', 'html');
    server.set('views', distFolder);

    if (process.env.ENVIRONMENT !== 'PRODUCTION') {
        server.use('/api', createProxyMiddleware({ target: 'http://localhost:3333' }));
        server.use('/static', createProxyMiddleware({ target: 'http://localhost:3333' }));
        server.use('/graphql', createProxyMiddleware({ target: 'http://localhost:3333' }));
        server.use('/maps', createProxyMiddleware({ target: 'https://maps.vietmap.vn' }));
    }

    server.get(
        '*.*',
        express.static(distFolder, {
            maxAge: '1y',
        })
    );

    server.get(
        /^\/(tai-khoan|dang-tin|dang-tin-thanh-cong|sua-tin|kich-hoat-tai-khoan|khoi-phuc-mat-khau|quen-mat-khau|dang-ky|dang-nhap|oauth-success)/,
        (req, res) => {
            res.sendFile(join(distFolder, 'index.html'));
        }
    );

    server.get('*', (req, res) => {
        res.render(indexHtml, { req, providers: [{ provide: APP_BASE_HREF, useValue: req.baseUrl }] });
    });

    return server;
}

function run() {
    const port = 4200;

    // Start up the Node server
    const server = app();

    server.listen(port, () => {
        console.log(`Node Express server listening on http://localhost:${port}`);
    });
}

// Webpack will replace 'require' with '__webpack_require__'
// '__non_webpack_require__' is a proxy to Node 'require'
// The below code is to ensure that the server is run only when not requiring the bundle.
declare const __non_webpack_require__: NodeRequire;
const mainModule = __non_webpack_require__.main;
const moduleFilename = (mainModule && mainModule.filename) || '';
if (moduleFilename === __filename || moduleFilename.includes('iisnode')) {
    run();
}

export * from './src/main.server';
