#!/usr/bin/env bash

git stash
git pull
npm install
rm -rf dist
unzip dist.zip
rm -rf dist.zip
pm2 restart sohuu
pm2 restart sohuu-app

# test gitlab ci cache
