export interface Pagination {
    total: number;
    data: any;
}
