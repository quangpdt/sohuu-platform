import { KeyValuePair } from './key-value-pair.interface';
import { City, District, Ward } from './administrative-division.interface';
import { PropertyStatus, PropertyType } from '@sohuu-platform/enums';

export interface PropertySearchCriteria {
    propertyType?: PropertyType;
    propertyStatus?: PropertyStatus;
    city?: City;
    district?: District;
    ward?: Ward;
    bedrooms?: string;
    bathrooms?: string;
    priceFrom?: string;
    priceTo?: string;
    areaFrom?: string;
    areaTo?: string;
    features?: KeyValuePair[];
    page: number;
    pageSize: number;
}
