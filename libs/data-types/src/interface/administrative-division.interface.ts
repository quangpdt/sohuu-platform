export interface City {
    id: number;
    code: string;
    name: string;
    slug: string;
}

export interface District {
    id: number;
    cityId: number;
    name: string;
    type: string;
    slug: string;
}

export interface Ward {
    id: number;
    districtId: number;
    name: string;
    type: string;
    slug: string;
}
