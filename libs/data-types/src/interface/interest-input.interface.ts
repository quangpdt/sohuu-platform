export interface InterestInput {
    years: number;
    amount: number;
    maximumDebt: number;
    interestRate: number;
    propertyId?: number;
    propertySlug?: string;
}
