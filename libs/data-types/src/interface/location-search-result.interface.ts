export interface LocationSearchResult {
    coordinates: Coordinates;
    fullLocation: string;
    street?: string;
    ward?: string;
    district?: string;
    city?: string;
    houseNumber?: string;
}

export interface Coordinates {
    lat: number;
    lng: number;
}
