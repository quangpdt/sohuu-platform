export interface UploadedFile {
    id?: number;
    originalName?: string;
    name: string;
    size?: number;
}
