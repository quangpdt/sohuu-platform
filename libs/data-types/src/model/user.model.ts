import { Model, RelationMappings } from 'objection';
import { BaseModel } from './base.model';
import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { PropertyModel } from './property.model';
import { UserMetadataModel } from './user-metadata.model';
import { UserNotificationModel } from './user-notification.model';
import { UserFavoriteModel } from 'libs/data-types/src/model/user-favorite.model';

@ObjectType()
export class UserModel extends BaseModel {
    public static tableName = 'users';
    static get relationMappings(): RelationMappings {
        return {
            properties: {
                relation: Model.HasManyRelation,
                modelClass: PropertyModel,
                join: {
                    from: 'users.id',
                    to: 'properties.ownerId',
                },
            },
            metadata: {
                relation: Model.HasManyRelation,
                modelClass: UserMetadataModel,
                join: {
                    from: 'users.id',
                    to: 'user_metadata.userId',
                },
            },
            notifications: {
                relation: Model.HasManyRelation,
                modelClass: UserNotificationModel,
                join: {
                    from: 'users.id',
                    to: 'user_notifications.userId',
                },
            },
            favorites: {
                relation: Model.HasManyRelation,
                modelClass: UserFavoriteModel,
                join: {
                    from: 'users.id',
                    to: 'user_favorites.userId',
                },
            },
        };
    }

    @Field(() => ID)
    public id: number;

    @Field()
    public name: string;

    @Field()
    public email: string;

    @Field({ nullable: true })
    public phone: string;

    @Field()
    public password?: string;

    @Field({ nullable: true })
    public activateToken?: string;

    @Field({ nullable: true })
    public activateExpirationTime?: Date;

    @Field({ nullable: true })
    public forgotPasswordToken?: string;

    @Field({ nullable: true })
    public forgotPasswordExpirationTime?: Date;

    @Field({ nullable: true })
    public isActivated?: boolean;

    @Field(() => Int, { nullable: true })
    public role?: number;

    @Field(() => ID, { nullable: true })
    public googleId?: string;

    @Field(() => ID, { nullable: true })
    public facebookId?: string;

    @Field({ nullable: true })
    public isPhoneVerified: boolean;

    @Field(() => [PropertyModel], { nullable: true })
    public properties?: PropertyModel[];

    @Field({ nullable: true })
    public createdAt?: Date;

    @Field({ nullable: true })
    public updatedAt?: Date;

    @Field({ nullable: true })
    public deletedAt?: Date;

    @Field({ nullable: true })
    public deleted?: boolean;

    @Field(() => [UserMetadataModel])
    public metadata: UserMetadataModel[];

    @Field(() => [UserNotificationModel])
    public notifications: UserNotificationModel[];

    public $beforeInsert() {
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    public $beforeUpdate() {
        this.updatedAt = new Date();
    }

    static get modifiers() {
        return {
            propertyOwner(builder) {
                const columns = ['id', 'name'].map((item) => `users.${item}`);
                builder.select(...columns);
            },
            propertyOwnerWithContacts(builder) {
                const columns = ['id', 'name', 'email', 'phone'].map((item) => `users.${item}`);
                builder.select(...columns);
            },
        };
    }
}
