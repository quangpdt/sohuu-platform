import { Field, ID, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';
import { Model, RelationMappings } from 'objection';
import { UserModel } from './user.model';
import { JSONObject } from '../scalars/types';

@ObjectType()
export class UserMetadataModel extends BaseModel {
    public static tableName = 'user_metadata';
    static get relationMappings(): RelationMappings {
        return {
            owner: {
                relation: Model.BelongsToOneRelation,
                modelClass: UserModel,
                join: {
                    from: 'user_metadata.userId',
                    to: 'users.id',
                },
            },
        };
    }

    @Field(() => ID, { nullable: true })
    public id: number;

    @Field(() => ID)
    public userId: number;

    @Field()
    public name: string;

    @Field()
    public value: string;

    @Field(() => JSONObject, { nullable: true })
    public parsedValue: any;

    static get modifiers() {
        return {
            all(builder) {
                const columns = ['name', 'value'].map((item) => `user_metadata.${item}`);
                builder.select(...columns);
            },
        };
    }
}
