import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class AuthenticationModel {
    @Field()
    public expiresIn: string;

    @Field()
    public accessToken: string;

    @Field()
    public refreshToken: string;
}
