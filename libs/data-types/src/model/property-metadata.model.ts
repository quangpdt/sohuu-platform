import { Field, ID, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';
import { Model, RelationMappings } from 'objection';
import { PropertyModel } from './property.model';
import { JSONObject } from '../scalars/types';

@ObjectType()
export class PropertyMetadataModel extends BaseModel {
    public static tableName = 'property_metadata';
    static get relationMappings(): RelationMappings {
        return {
            owner: {
                relation: Model.BelongsToOneRelation,
                modelClass: PropertyModel,
                join: {
                    from: 'property_metadata.propertyId',
                    to: 'properties.id',
                },
            },
        };
    }

    @Field(() => ID, { nullable: true })
    public id: number;

    @Field()
    public name: string;

    @Field()
    public value: string;

    @Field(() => JSONObject, { nullable: true })
    public parsedValue: any;

    static get modifiers() {
        return {
            all(builder) {
                const columns = ['name', 'value'].map((item) => `property_metadata.${item}`);
                builder.select(...columns);
            },
        };
    }
}
