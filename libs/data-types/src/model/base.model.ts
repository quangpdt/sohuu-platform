import { Model } from 'objection';
import * as path from 'path';
import * as process from 'process';

export class BaseModel extends Model {
    public static modelPaths = [path.join(process.cwd(), 'libs/data-types/src/model')];
}
