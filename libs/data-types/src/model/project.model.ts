import { Field, ID, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';
import { Model, RelationMappings } from 'objection';
import { PropertyModel } from './property.model';
import { FileModel } from './file.model';
import { ProjectMetadataModel } from './project-metadata.model';
import { NewsModel } from './news.model';

@ObjectType()
export class ProjectModel extends BaseModel {
    public static tableName = 'projects';

    static get relationMappings(): RelationMappings {
        return {
            properties: {
                relation: Model.HasManyRelation,
                modelClass: PropertyModel,
                join: {
                    from: 'projects.id',
                    to: 'properties.projectId',
                },
            },
            metadata: {
                relation: Model.HasManyRelation,
                modelClass: ProjectMetadataModel,
                join: {
                    from: 'projects.id',
                    to: 'project_metadata.projectId',
                },
            },
            files: {
                relation: Model.ManyToManyRelation,
                modelClass: FileModel,
                join: {
                    from: 'projects.id',
                    through: {
                        from: 'project_files.projectId',
                        to: 'project_files.fileId',
                    },
                    to: 'files.id',
                },
            },
            news: {
                relation: Model.HasManyRelation,
                modelClass: NewsModel,
                join: {
                    from: 'projects.id',
                    through: {
                        from: 'project_news.projectId',
                        to: 'project_news.newsId',
                    },
                    to: 'news.id',
                },
            },
        };
    }

    @Field(() => ID)
    public id: number;

    @Field()
    public title: string;

    @Field()
    public description: string;

    @Field({ nullable: true })
    public slug: string;

    @Field({ nullable: true })
    public coordinates?: string;

    @Field({ nullable: true })
    public location?: string;

    @Field({ nullable: true })
    public price?: string;

    @Field(() => [ProjectMetadataModel], { nullable: true })
    public metadata?: ProjectMetadataModel[];

    @Field(() => [PropertyModel], { nullable: true })
    public properties?: PropertyModel[];

    @Field(() => [FileModel], { nullable: true })
    public files?: FileModel[];

    @Field({ nullable: true })
    public createdAt?: Date;

    @Field({ nullable: true })
    public updatedAt?: Date;

    public $beforeInsert() {
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    public $beforeUpdate() {
        this.updatedAt = new Date();
    }
}
