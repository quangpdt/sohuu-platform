import { Field, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';

@ObjectType()
export class NewsCategoryModel extends BaseModel {
    @Field()
    public name: string;

    @Field()
    public description: string;

    @Field({ nullable: true })
    public slug: string;

    @Field({ nullable: true })
    public parent?: NewsCategoryModel;
}
