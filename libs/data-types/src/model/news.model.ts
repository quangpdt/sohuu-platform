import { Field, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';
import { NewsCategoryModel } from './news-category.model';
import { Model, RelationMappings } from 'objection';
import { FileModel } from './file.model';

@ObjectType()
export class NewsModel extends BaseModel {
    public static tableName = 'news';

    static get relationMappings(): RelationMappings {
        return {
            category: {
                relation: Model.BelongsToOneRelation,
                modelClass: NewsCategoryModel,
                join: {
                    from: 'news.categoryId',
                    to: 'news_categories.id',
                },
            },
            files: {
                relation: Model.HasManyRelation,
                modelClass: FileModel,
                join: {
                    from: 'news.id',
                    through: {
                        from: 'news_files.newsId',
                        to: 'news_files.fileId',
                    },
                    to: 'files.id',
                },
            },
        };
    }

    @Field()
    public title: string;

    @Field({ nullable: true })
    public slug: string;

    @Field()
    public shortDescription: string;

    @Field()
    public fullDescription: string;

    @Field({ nullable: true })
    public category?: NewsCategoryModel;

    @Field(() => [FileModel], { nullable: true })
    public files?: FileModel[];
}
