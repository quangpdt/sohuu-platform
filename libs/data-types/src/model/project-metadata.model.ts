import { Field, ID, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';
import { Model, RelationMappings } from 'objection';
import { ProjectModel } from './project.model';

@ObjectType()
export class ProjectMetadataModel extends BaseModel {
    public static tableName = 'project_metadata';
    static get relationMappings(): RelationMappings {
        return {
            project: {
                relation: Model.BelongsToOneRelation,
                modelClass: ProjectModel,
                join: {
                    from: 'project_metadata.projectId',
                    to: 'projects.id',
                },
            },
        };
    }

    @Field(() => ID)
    public id: number;

    @Field()
    public name: string;

    @Field()
    public value: string;

    static get modifiers() {
        return {
            selectNameAndValue(builder) {
                builder.select('project_metadata.name', 'project_metadata.value');
            },
        };
    }
}
