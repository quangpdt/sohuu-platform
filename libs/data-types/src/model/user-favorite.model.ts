import { Field, ID, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';

@ObjectType()
export class UserFavoriteModel extends BaseModel {
    public static tableName = 'user_favorites';

    @Field(() => ID)
    public userId: number;

    @Field(() => ID)
    public propertyId: number;

    @Field({ nullable: true })
    public createdAt?: Date;
}
