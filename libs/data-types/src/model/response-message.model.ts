import { Field, ObjectType } from '@nestjs/graphql';
import { JSON, JSONObject } from '../scalars/types';

@ObjectType()
export class ResponseMessage {
    @Field(() => JSONObject, { nullable: true })
    public data?: any;

    @Field({ nullable: true })
    public message?: string;

    @Field(() => JSON, { nullable: true })
    public result?: any;
}
