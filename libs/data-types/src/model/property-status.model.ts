import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';
import { Model, RelationMappings } from 'objection';
import { PropertyModel } from '@sohuu-platform/models';
import { IsOptional, ValidateNested } from 'class-validator';

@ObjectType()
export class PropertyStatusModel extends BaseModel {
    public static tableName = 'property_statuses';

    static get relationMappings(): RelationMappings {
        return {
            property: {
                relation: Model.BelongsToOneRelation,
                modelClass: PropertyModel,
                join: {
                    from: 'property_statuses.property_id',
                    to: 'properties.id',
                },
            },
        };
    }

    @Field(() => ID)
    public id: number;

    @Field(() => ID)
    public propertyId: number;

    @Field(() => Int)
    public status: number;

    @Field()
    public price: number;

    @Field()
    public negotiable: boolean;

    @IsOptional()
    @ValidateNested()
    public property?: PropertyModel;

    static get modifiers() {
        return {
            all(builder) {
                const columns = ['status', 'price', 'negotiable'].map((item) => `property_statuses.${item}`);
                builder.select(...columns);
            },
            status(builder, status) {
                const columns = ['status', 'price', 'negotiable'].map((item) => `property_statuses.${item}`);
                builder.where('property_statuses.status', status);
            },
        };
    }
}
