import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';

@ObjectType()
export class FileModel extends BaseModel {
    public static tableName = 'files';

    @Field(() => ID, { nullable: true })
    public id: number;

    @Field()
    public fileName: string;

    @Field(() => Int, { nullable: true })
    public fileSize?: number;

    @Field(() => Int, { nullable: true })
    public fileType?: number;
}
