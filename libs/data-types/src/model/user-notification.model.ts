import { BaseModel } from './base.model';
import { Field, ID, ObjectType } from '@nestjs/graphql';
import { Model, RelationMappings } from 'objection';
import { UserModel } from './user.model';
import { JSONObject } from '../scalars/types';

@ObjectType()
export class UserNotificationModel extends BaseModel {
    public static tableName = 'user_notifications';

    static get relationMappings(): RelationMappings {
        return {
            requestedBy: {
                relation: Model.HasOneRelation,
                modelClass: UserModel,
                join: {
                    from: 'user_notifications.requestedByUserId',
                    to: 'users.id',
                },
            },
            user: {
                relation: Model.HasOneRelation,
                modelClass: UserModel,
                join: {
                    from: 'user_notifications.userId',
                    to: 'users.id',
                },
            },
        };
    }

    @Field(() => ID)
    public id: number;

    @Field(() => ID, { nullable: true })
    public requestedByUserId?: number;

    @Field(() => ID)
    public userId: number;

    @Field()
    public status: number;

    @Field()
    public type: number;

    @Field()
    public message: string;

    @Field({ nullable: true })
    public createdAt?: Date;

    @Field(() => UserModel, { nullable: true })
    public requestedBy: any;

    @Field(() => UserModel)
    public user: any;

    @Field(() => JSONObject, { nullable: true })
    public parsedMessage?: any;

    public $beforeInsert() {
        this.createdAt = new Date();
    }
}
