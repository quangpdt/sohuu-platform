import { Field, ID, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';

@ObjectType()
export class UserPhoneSessionModel extends BaseModel {
    public static tableName = 'user_phone_session';

    @Field(() => ID)
    public userId: number;

    @Field()
    public phoneSession: string;

    @Field()
    public phone: string;
}
