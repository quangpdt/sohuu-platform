import { Field, ID, Int, ObjectType } from '@nestjs/graphql';
import { BaseModel } from './base.model';
import { Model, RelationMappings } from 'objection';
import { UserModel } from './user.model';
import { PropertyMetadataModel } from './property-metadata.model';
import { FileModel } from './file.model';
import { ProjectModel } from './project.model';
import { PropertyStatusModel } from './property-status.model';
import { JSONObject } from '../scalars/types';
import { SubmitStatus } from '@sohuu-platform/enums';

@ObjectType()
export class PropertyModel extends BaseModel {
    public static tableName = 'properties';

    static get relationMappings(): RelationMappings {
        return {
            owner: {
                relation: Model.BelongsToOneRelation,
                modelClass: UserModel,
                join: {
                    from: 'properties.ownerId',
                    to: 'users.id',
                },
            },
            statuses: {
                relation: Model.HasManyRelation,
                modelClass: PropertyStatusModel,
                join: {
                    from: 'properties.id',
                    to: 'property_statuses.propertyId',
                },
            },
            project: {
                relation: Model.BelongsToOneRelation,
                modelClass: ProjectModel,
                join: {
                    from: 'properties.projectId',
                    to: 'projects.id',
                },
            },
            metadata: {
                relation: Model.HasManyRelation,
                modelClass: PropertyMetadataModel,
                join: {
                    from: 'properties.id',
                    to: 'property_metadata.propertyId',
                },
            },
            files: {
                relation: Model.ManyToManyRelation,
                modelClass: FileModel,
                join: {
                    from: 'properties.id',
                    through: {
                        from: 'property_files.propertyId',
                        to: 'property_files.fileId',
                    },
                    to: 'files.id',
                },
            },
        };
    }

    @Field(() => ID)
    public id: number;

    @Field()
    public title: string;

    @Field({ nullable: true })
    public slug?: string;

    @Field()
    public description: string;

    @Field(() => ID, { nullable: true })
    public ownerId?: number;

    @Field(() => ID, { nullable: true })
    public projectId?: number;

    @Field(() => Int)
    public propertyType: number;

    @Field(() => Int)
    public cityId: number;

    @Field(() => Int)
    public districtId: number;

    @Field(() => Int)
    public wardId: number;

    @Field()
    public location: string;

    @Field(() => Int)
    public submitStatus: SubmitStatus;

    @Field(() => [PropertyStatusModel])
    public statuses: PropertyStatusModel[];

    @Field(() => [PropertyMetadataModel])
    public metadata: PropertyMetadataModel[];

    // Cannot set object type to UserModel because of circular dependency check
    @Field(() => JSONObject, { nullable: true })
    public owner?: any;

    @Field(() => [FileModel])
    public files?: FileModel[];

    @Field({ nullable: true })
    public createdAt?: Date;

    @Field({ nullable: true })
    public updatedAt?: Date;

    @Field({ nullable: true })
    public deletedAt?: Date;

    @Field({ nullable: true })
    public deleted?: boolean;

    @Field({ nullable: true })
    public views?: boolean;

    @Field({ nullable: true })
    public publishedAt?: Date;

    @Field({ nullable: true })
    public expiredAt?: Date;

    @Field({ nullable: true })
    public fullLocation?: string;

    @Field({ nullable: true })
    public lat: number;

    @Field({ nullable: true })
    public lng: number;

    public $beforeInsert() {
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    public $beforeUpdate() {
        this.updatedAt = new Date();
    }
}
