export enum PropertyLegal {
    Unknown = 1,
    HouseOwnershipCertificate = 2,
    Contract = 3,
    Deposit = 4,
}

export const propertyLegals: PropertyLegal[] = [
    PropertyLegal.Unknown,
    PropertyLegal.HouseOwnershipCertificate,
    PropertyLegal.Contract,
    PropertyLegal.Deposit,
];
