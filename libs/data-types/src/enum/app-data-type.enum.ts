export enum AppDataType {
    DebtPeriod = 'debtPeriod',
    EmailType = 'emailType',
    OauthProvider = 'oauthProvider',
    PropertyDirection = 'propertyDirection',
    PropertyFeature = 'propertyFeature',
    PropertyReportType = 'propertyReportType',
    PropertyLegal = 'propertyLegal',
    PropertyMetadata = 'propertyMetadata',
    PropertyStatus = 'propertyStatus',
    PropertyType = 'propertyType',
    SortOption = 'sortOption',
    ProjectSortOption = 'projectSortOption',
    SubmitStatus = 'submitStatus',
    NotificationType = 'notificationType',
    NotificationTypeTitle = 'notificationTypeTitle',
    NotificationTypeInDetail = 'notificationTypeInDetail',
}
