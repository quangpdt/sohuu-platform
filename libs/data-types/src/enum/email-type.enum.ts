export enum EmailType {
    UserActivation = 'user-activation',
    UserForgotPassword = 'user-forgot-password',
}
