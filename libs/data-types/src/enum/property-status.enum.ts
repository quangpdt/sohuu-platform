export enum PropertyStatus {
    ForSale = 1,
    ForRent = 2,
    Sold = 3,
}

export const propertyStatuses: PropertyStatus[] = [PropertyStatus.ForSale, PropertyStatus.ForRent];
