export enum PropertyFeature {
    AirConditioning = 1,
    Microwave = 2,
    Tv = 3,
    Refrigerator = 4,
    Wifi = 5,
    Pool = 6,
    Gym = 7,
}

export const propertyFeatures: PropertyFeature[] = [
    PropertyFeature.AirConditioning,
    PropertyFeature.Microwave,
    PropertyFeature.Tv,
    PropertyFeature.Refrigerator,
    PropertyFeature.Wifi,
    PropertyFeature.Pool,
    PropertyFeature.Gym,
];
