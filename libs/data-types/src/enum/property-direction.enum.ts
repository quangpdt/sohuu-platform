export enum PropertyDirection {
    Unknown = 1,
    East = 2,
    West = 3,
    South = 4,
    North = 5,
    EastNorth = 6,
    WestNorth = 7,
    EastSouth = 8,
    WestSouth = 9,
}

export const propertyDirections: PropertyDirection[] = [
    PropertyDirection.Unknown,
    PropertyDirection.East,
    PropertyDirection.West,
    PropertyDirection.South,
    PropertyDirection.North,
    PropertyDirection.EastNorth,
    PropertyDirection.WestNorth,
    PropertyDirection.EastSouth,
    PropertyDirection.WestSouth,
];
