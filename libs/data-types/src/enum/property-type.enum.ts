export enum PropertyType {
    House = 1,
    Apartment = 2,
    Office = 3,
}

export const propertyTypes: PropertyType[] = [PropertyType.House, PropertyType.Apartment, PropertyType.Office];
