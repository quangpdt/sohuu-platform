export enum SortOption {
    Default = 'sortByDefault',
    Newest = 'sortByNewest',
    Oldest = 'sortByOldest',
    Popular = 'sortByViews',
    PriceDescending = 'sortByPriceDescending',
    PriceAscending = 'sortByPriceAscending',
}
