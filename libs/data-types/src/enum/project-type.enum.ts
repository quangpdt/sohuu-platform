export enum ProjectType {
    Apartment = 1,
    House,
    ShopHouse,
    FoundationSoil,
    Villa,
    Unknown,
}
