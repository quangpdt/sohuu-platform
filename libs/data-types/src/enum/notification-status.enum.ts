export enum NotificationStatus {
    Unread = 1,
    Read = 2,
}
