export enum UserRole {
    SuperAdmin = 1,
    Admin,
    CompanyOwner,
    BranchOwner,
    Employee,
    Manager,
    Member,
}
