export enum ProjectSaleStatus {
    OpeningSoon = 1,
    Selling,
    HandedOver,
    Unknown,
}
