export enum ProjectAttributeType {
    SaleStatus = 1,
    PropertyType,
    NumberOfBlocks,
    NumberOfFloors,
    NumberOfApartments,
    NumberOfShopHouses,
    BuildingDensity,
    AverageSquare,
    DesignedBy,
    ManagedBy,
    ConstructionTime,
}
