export enum ProjectSortOption {
    RecentlyAdded = 1,
    HandOverTime,
    ConstructionTime,
    PriceDescending,
    PriceAscending,
}
