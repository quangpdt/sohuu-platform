export enum DebtPeriod {
    Year10 = 10,
    Year15 = 15,
    Year20 = 20,
    Year25 = 25,
    Year30 = 30,
}

export const debtPeriods: DebtPeriod[] = [
    DebtPeriod.Year10,
    DebtPeriod.Year15,
    DebtPeriod.Year20,
    DebtPeriod.Year25,
    DebtPeriod.Year30,
];
