export enum PropertyReportType {
    FalseInformation = 1,
    FalseContactInformation = 2,
    SoldProperty = 3,
    WrongAddress = 4,
    DuplicatedProperty = 5,
}

export const propertyReportTypes: PropertyReportType[] = [
    PropertyReportType.FalseInformation,
    PropertyReportType.FalseContactInformation,
    PropertyReportType.SoldProperty,
    PropertyReportType.WrongAddress,
    PropertyReportType.DuplicatedProperty,
];
