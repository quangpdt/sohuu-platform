export enum UserListKind {
    BranchUsers = 'branchUsers',
    CompanyUsers = 'companyUsers',
}
