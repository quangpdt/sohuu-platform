export enum ProjectFeature {
    ParkingLot = 1,
    Elevator,
    Pool,
    CommonRoom,
    Gym,
    Security,
    ChildrenSpace,
    BBQ,
    Mall,
    Park,
}

export const projectFeatures = [
    ProjectFeature.ParkingLot,
    ProjectFeature.Elevator,
    ProjectFeature.Pool,
    ProjectFeature.CommonRoom,
    ProjectFeature.Gym,
    ProjectFeature.Security,
    ProjectFeature.ChildrenSpace,
    ProjectFeature.BBQ,
    ProjectFeature.Mall,
    ProjectFeature.Park,
];
