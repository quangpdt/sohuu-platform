import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class UpdateBasicInfoInput {
    @Field()
    public name: string;
}
