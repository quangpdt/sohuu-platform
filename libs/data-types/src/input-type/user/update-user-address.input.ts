import { Field, InputType } from '@nestjs/graphql';
import { JSONObject } from '../../scalars/types';

@InputType()
export class UpdateUserAddressInput {
    @Field()
    public location: string;

    @Field(() => JSONObject)
    public city: any;

    @Field(() => JSONObject)
    public district: any;

    @Field(() => JSONObject)
    public ward: any;
}
