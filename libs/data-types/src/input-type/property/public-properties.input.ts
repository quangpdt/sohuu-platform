import { Field, InputType, Int } from '@nestjs/graphql';
import { IsInt, Min } from 'class-validator';
import { PropertySearchInput } from './property-search.input';

@InputType()
export class PublicPropertiesInput {
    @Field(() => Int)
    @IsInt()
    @Min(0)
    public page: number;

    @Field(() => Int)
    @IsInt()
    @Min(1)
    public pageSize: number;

    @Field(() => PropertySearchInput, { nullable: true })
    public searchCriteria?: PropertySearchInput;
}
