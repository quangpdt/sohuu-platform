import { Field, ID, InputType } from '@nestjs/graphql';
import { BasicPropertyInput } from './basic-property.input';
import { AddressPropertyInput } from './address-property.input';
import { AdditionalPropertyInput } from './additional-property.input';

@InputType()
export class SubmitPropertyInput {
    @Field(() => ID, { nullable: true })
    public id?: number;

    @Field(() => BasicPropertyInput)
    public basic: BasicPropertyInput;

    @Field(() => AddressPropertyInput)
    public address: AddressPropertyInput;

    @Field(() => AdditionalPropertyInput)
    public additional: AdditionalPropertyInput;
}
