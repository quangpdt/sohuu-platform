import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class PublicPropertyInput {
    @Field(() => ID)
    public id: number;

    @Field()
    public slug: string;
}
