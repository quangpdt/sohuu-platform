import { Field, InputType, Int } from '@nestjs/graphql';
import { JSONObject } from '../../scalars/types';

@InputType()
export class PropertySearchInput {
    @Field(() => Int, { nullable: true })
    public propertyType?: number;

    @Field(() => Int, { nullable: true })
    public propertyStatus?: number;

    @Field(() => Int, { nullable: true })
    public priceFrom?: number;

    @Field(() => Int, { nullable: true })
    public priceTo?: number;

    @Field(() => JSONObject, { nullable: true })
    public city?: any;

    @Field(() => JSONObject, { nullable: true })
    public district?: any;

    @Field(() => JSONObject, { nullable: true })
    public ward?: any;

    @Field(() => Int, { nullable: true })
    public bedrooms?: number;

    @Field(() => Int, { nullable: true })
    public bathrooms?: number;

    @Field(() => Int, { nullable: true })
    public areaFrom?: number;

    @Field(() => Int, { nullable: true })
    public areaTo?: number;

    @Field(() => [Int], { nullable: true })
    public features?: number[];

    @Field(() => Int)
    public page: number;

    @Field(() => Int)
    public pageSize: number;
}
