import { Field, InputType, Int } from '@nestjs/graphql';
import { UploadedFileInput } from '@sohuu-platform/input-types';

@InputType()
export class BasicPropertyInput {
    @Field()
    public title: string;

    @Field()
    public description: string;

    @Field(() => Int, { nullable: true })
    public rentPrice: number;

    @Field(() => Int, { nullable: true })
    public salePrice: number;

    @Field({ nullable: true })
    public saleNegotiable: boolean;

    @Field({ nullable: true })
    public rentNegotiable: boolean;

    @Field(() => Int)
    public propertyType: number;

    @Field(() => [Int])
    public propertyStatus: number[];

    @Field(() => [UploadedFileInput])
    public files: UploadedFileInput[];
}
