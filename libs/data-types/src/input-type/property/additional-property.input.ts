import { Field, InputType, Int } from '@nestjs/graphql';
import { KeyValuePairInput } from '../key-value-pair.input';

@InputType()
export class AdditionalPropertyInput {
    @Field(() => Int, { nullable: true })
    public mainDoorDirection?: number;

    @Field(() => Int, { nullable: true })
    public balconyDirection?: number;

    @Field(() => Int, { nullable: true })
    public kitchenDirection?: number;

    @Field(() => Int, { nullable: true })
    public legal?: number;

    @Field(() => Int, { nullable: true })
    public bedrooms?: number;

    @Field(() => Int, { nullable: true })
    public bathrooms?: number;

    @Field(() => Int, { nullable: true })
    public floors?: number;

    @Field(() => Int, { nullable: true })
    public area?: number;

    @Field(() => [Int], { nullable: true })
    public features?: number[];

    @Field(() => [KeyValuePairInput], { nullable: true })
    public additionalFeatures?: KeyValuePairInput[];
}
