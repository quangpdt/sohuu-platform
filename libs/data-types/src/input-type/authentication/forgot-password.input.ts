import { Field, InputType } from '@nestjs/graphql';
import { Email } from '../../scalars/types';

@InputType()
export class ForgotPasswordInput {
    @Field(() => Email)
    public email: string;
}
