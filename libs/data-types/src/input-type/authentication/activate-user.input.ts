import { Field, InputType } from '@nestjs/graphql';
import { Email } from '../../scalars/types';

@InputType()
export class ActivateUserInput {
    @Field(() => Email)
    public email: string;

    @Field()
    public token: string;
}
