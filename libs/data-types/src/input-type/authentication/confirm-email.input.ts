import { Field, InputType } from '@nestjs/graphql';
import { Email } from '../../scalars/types';

@InputType()
export class ConfirmEmailInput {
    @Field(() => Email)
    public email: string;

    @Field()
    public activateToken: string;
}
