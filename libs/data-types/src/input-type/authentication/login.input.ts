import { Field, InputType } from '@nestjs/graphql';
import { UserModel } from '../..';
import { Email } from '../../scalars/types';

@InputType()
export class LoginInput {
    @Field()
    public token: string;

    @Field(() => Email)
    public email: string;

    @Field()
    public password: string;

    public user?: Partial<UserModel>;
}
