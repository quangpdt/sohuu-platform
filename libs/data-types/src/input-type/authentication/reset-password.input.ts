import { Field, InputType } from '@nestjs/graphql';
import { Email } from '../../scalars/types';

@InputType()
export class ResetPasswordInput {
    @Field()
    public token: string;

    @Field(() => Email)
    public email: string;

    @Field()
    public password: string;

    @Field()
    public confirmPassword: string;
}
