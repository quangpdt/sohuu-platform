import { Field, InputType } from '@nestjs/graphql';
import { Email } from '../../scalars/types';

@InputType()
export class RegisterInput {
    @Field()
    public name: string;

    @Field(() => Email)
    public email: string;

    @Field()
    public password: string;

    @Field()
    public confirmPassword: string;
}
