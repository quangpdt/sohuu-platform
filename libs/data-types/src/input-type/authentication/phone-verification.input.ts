import { Field, InputType } from '@nestjs/graphql';
import { Email } from '../../scalars/types';

@InputType()
export class PhoneVerificationInput {
    @Field()
    public phoneNumber: string;

    @Field()
    public recaptchaToken: string;
}
