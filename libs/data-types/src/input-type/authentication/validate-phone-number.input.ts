import { Field, InputType } from '@nestjs/graphql';
import { Email } from '../../scalars/types';

@InputType()
export class ValidatePhoneNumberInput {
    @Field()
    public phoneNumber: string;

    @Field()
    public code: string;
}
