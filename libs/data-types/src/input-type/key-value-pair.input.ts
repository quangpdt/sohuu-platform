import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class KeyValuePairInput {
    @Field()
    public name: string;

    @Field()
    public value: string;
}
