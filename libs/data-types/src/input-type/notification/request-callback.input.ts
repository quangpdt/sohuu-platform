import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class RequestCallbackInput {
    @Field(() => ID)
    public propertyId: number;

    @Field()
    public name: string;

    @Field({ nullable: true })
    public email?: string;

    @Field()
    public phone: string;

    @Field({ nullable: true })
    public message?: string;
}
