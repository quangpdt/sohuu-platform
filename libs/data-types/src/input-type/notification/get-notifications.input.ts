import { Field, InputType, Int } from '@nestjs/graphql';

@InputType()
export class GetNotificationsInput {
    @Field(() => Int)
    public page: number;

    @Field(() => Int)
    public pageSize: number;
}
