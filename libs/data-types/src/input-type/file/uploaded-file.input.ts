import { Field, ID, InputType } from '@nestjs/graphql';

@InputType()
export class UploadedFileInput {
    @Field(() => ID, { nullable: true })
    public id?: number;

    @Field()
    public fileName: string;
}
