import {
    GraphQLDateTime,
    GraphQLEmail,
    GraphQLLimitedString,
    GraphQLPassword,
    GraphQLURL,
    GraphQLUUID,
} from './scalars';

import { GraphQLCustomScalarType } from './types';

import { Factory } from './factory';

export default {
    Factory,
    GraphQLCustomScalarType,
    GraphQLEmail,
    GraphQLURL,
    GraphQLLimitedString,
    GraphQLPassword,
    GraphQLDateTime,
    GraphQLUUID,
};
