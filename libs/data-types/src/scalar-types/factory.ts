import { GraphQLError } from 'graphql/error';
import { Kind } from 'graphql/language';
import { GraphQLCustomScalarType } from './types';

export class Factory {
    public getRegexScalar({ description, error, name, regex: re }) {
        error = error || 'Query error: ' + name;

        const parser = function (ast) {
            if (ast.kind !== Kind.STRING) {
                throw new GraphQLError('Query error: Can only parse strings got a: ' + ast.kind, [ast]);
            }

            if (!re.test(ast.value)) {
                throw new GraphQLError(error, [ast]);
            }

            return ast.value;
        };

        return this.getCustomScalar(name, description, parser);
    }

    public getCustomScalar(name, description, parser) {
        return new GraphQLCustomScalarType(name, description, parser);
    }
}
