import { CustomScalar, Scalar } from '@nestjs/graphql';
import { ValueNode } from 'graphql';
import { Kind, print } from 'graphql/language';
import { JSONObject } from './types';

@Scalar('JSONObject', (type) => JSONObject)
export class JSONObjectScalar implements CustomScalar<any, JSONObject> {
    public name = 'JSONObject';
    public description =
        'The `JSONObject` scalar type represents JSON objects as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf)';

    public parseValue(value: any): any {
        return this.ensureObject(value);
    }

    public serialize(value: any): any {
        return this.ensureObject(value);
    }

    // @ts-ignore
    public parseLiteral(ast: ValueNode, variables): any {
        if (ast.kind !== Kind.OBJECT) {
            throw new TypeError(`JSONObject cannot represent non-object value: ${print(ast)}`);
        }

        return this.parseObject('JSONObject', ast, variables);
    }

    public parseObject(typeName, ast, variables) {
        const value = Object.create(null);
        ast.fields.forEach((field) => {
            // eslint-disable-next-line no-use-before-define
            value[field.name.value] = this.parseLiteralItem(typeName, field.value, variables);
        });

        return value;
    }

    public ensureObject(value) {
        if (typeof value !== 'object' || value === null || Array.isArray(value)) {
            throw new TypeError(`JSONObject cannot represent non-object value: ${value}`);
        }

        return value;
    }

    public parseLiteralItem(typeName, ast, variables) {
        switch (ast.kind) {
            case Kind.STRING:
            case Kind.BOOLEAN:
                return ast.value;
            case Kind.INT:
            case Kind.FLOAT:
                return parseFloat(ast.value);
            case Kind.OBJECT:
                return this.parseObject(typeName, ast, variables);
            case Kind.LIST:
                return ast.values.map((n) => this.parseLiteralItem(typeName, n, variables));
            case Kind.NULL:
                return null;
            case Kind.VARIABLE:
                return variables ? variables[ast.name.value] : undefined;
            default:
                throw new TypeError(`${typeName} cannot represent value: ${print(ast)}`);
        }
    }
}
