import { CustomScalar, Scalar } from '@nestjs/graphql';
import { ValueNode } from 'graphql';
import { Kind, print } from 'graphql/language';
import { JSON } from './types';

@Scalar('JSON', (type) => JSON)
export class JSONScalar implements CustomScalar<any, JSON> {
    public name = 'JSON';
    public description =
        'The `JSON` scalar type represents JSON objects as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf)';

    public parseValue(value: any): any {
        return value;
    }

    public serialize(value: any): any {
        return value;
    }

    // @ts-ignore
    public parseLiteral(ast: ValueNode, variables): any {
        return this.parseLiteralItem('JSON', ast, variables);
    }

    public parseObject(typeName, ast, variables) {
        const value = Object.create(null);
        ast.fields.forEach((field) => {
            // eslint-disable-next-line no-use-before-define
            value[field.name.value] = this.parseLiteralItem(typeName, field.value, variables);
        });

        return value;
    }

    public parseLiteralItem(typeName, ast, variables) {
        switch (ast.kind) {
            case Kind.STRING:
            case Kind.BOOLEAN:
                return ast.value;
            case Kind.INT:
            case Kind.FLOAT:
                return parseFloat(ast.value);
            case Kind.OBJECT:
                return this.parseObject(typeName, ast, variables);
            case Kind.LIST:
                return ast.values.map((n) => this.parseLiteralItem(typeName, n, variables));
            case Kind.NULL:
                return null;
            case Kind.VARIABLE:
                return variables ? variables[ast.name.value] : undefined;
            default:
                throw new TypeError(`${typeName} cannot represent value: ${print(ast)}`);
        }
    }
}
