import { Scalar, CustomScalar } from '@nestjs/graphql';
import { Kind, ValueNode } from 'graphql';
import { GraphQLError } from 'graphql/error';
import { Email } from './types';

@Scalar('Email', (type) => Email)
export class EmailScalar implements CustomScalar<string, Email> {
    public name = 'Email';
    public regex = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    public description = 'The Email scalar type represents E-Mail addresses compliant to RFC 822.';
    public error = 'Query error: Not a valid Email address';

    public parseValue(value: string): string {
        return value;
    }

    public serialize(value: string): string {
        const ast = {
            kind: Kind.STRING,
            value: value,
        };
        return this.parseLiteral(ast);
    }

    public parseLiteral(ast: ValueNode): string {
        if (ast.kind !== Kind.STRING) {
            throw new GraphQLError('Query error: Can only parse strings got a: ' + ast.kind, [ast]);
        }

        if (!this.regex.test(ast.value)) {
            throw new GraphQLError(this.error, [ast]);
        }

        return ast.value;
    }
}
