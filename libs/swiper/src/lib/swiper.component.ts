import {
    AfterViewInit,
    ChangeDetectorRef,
    Component,
    ElementRef,
    EventEmitter,
    Inject,
    Input,
    NgZone,
    OnDestroy,
    Optional,
    Output,
    PLATFORM_ID,
    ViewChild,
    ViewEncapsulation,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { SwiperDirective } from './swiper.directive';

import { SWIPER_CONFIG, SwiperConfig, SwiperConfigInterface, SwiperEvent, SwiperEvents } from './swiper.interfaces';

@Component({
    selector: 'swiper',
    exportAs: 'ngxSwiper',
    templateUrl: './swiper.component.html',
    styleUrls: ['./swiper.component.css', '../../../../node_modules/swiper/css/swiper.min.css'],
    encapsulation: ViewEncapsulation.None,
})
export class SwiperComponent implements AfterViewInit, OnDestroy {
    private mo: MutationObserver | null = null;

    public swiperConfig: any = null;
    public paginationBackup: any = null;
    public paginationConfig: any = null;

    @Input() public index: number | null = null;

    @Input() public disabled = false;

    @Input() public performance = false;

    @Input() public config?: SwiperConfigInterface;

    @Input() public useSwiperClass = true;

    @Output() public indexChange = new EventEmitter<number>();

    @ViewChild('swiperSlides', { static: false }) public swiperSlides?: ElementRef;

    @ViewChild(SwiperDirective, { static: false }) public directiveRef?: SwiperDirective;

    get isAtLast(): boolean {
        return !this.directiveRef || !this.directiveRef.swiper() ? false : this.directiveRef.swiper()['isEnd'];
    }

    get isAtFirst(): boolean {
        return !this.directiveRef || !this.directiveRef.swiper() ? false : this.directiveRef.swiper()['isBeginning'];
    }

    @Output('init') public S_INIT = new EventEmitter<any>();
    @Output('beforeDestroy') public S_BEFOREDESTROY = new EventEmitter<any>();

    @Output('scroll') public S_SCROLL = new EventEmitter<any>();
    @Output('progress') public S_PROGRESS = new EventEmitter<any>();
    @Output('keyPress') public S_KEYPRESS = new EventEmitter<any>();

    @Output('resize') public S_RESIZE = new EventEmitter<any>();
    @Output('breakpoint') public S_BREAKPOINT = new EventEmitter<any>();
    @Output('zoomChange') public S_ZOOMCHANGE = new EventEmitter<any>();
    @Output('afterResize') public S_AFTERRESIZE = new EventEmitter<any>();
    @Output('beforeResize') public S_BEFORERESIZE = new EventEmitter<any>();

    @Output('beforeLoopFix') public S_BEFORELOOPFIX = new EventEmitter<any>();
    @Output('loopFix') public S_LOOPFIX = new EventEmitter<any>();

    @Output('sliderMove') public S_SLIDERMOVE = new EventEmitter<any>();
    @Output('slideChange') public S_SLIDECHANGE = new EventEmitter<any>();

    @Output('setTranslate') public S_SETTRANSLATE = new EventEmitter<any>();
    @Output('setTransition') public S_SETTRANSITION = new EventEmitter<any>();

    @Output('fromEdge') public S_FROMEDGE = new EventEmitter<any>();
    @Output('reachEnd') public S_REACHEND = new EventEmitter<any>();
    @Output('reachBeginning') public S_REACHBEGINNING = new EventEmitter<any>();

    @Output('autoplay') public S_AUTOPLAY = new EventEmitter<any>();
    @Output('autoplayStart') public S_AUTOPLAYSTART = new EventEmitter<any>();
    @Output('autoplayStop') public S_AUTOPLAYSTOP = new EventEmitter<any>();

    @Output('imagesReady') public S_IMAGESREADY = new EventEmitter<any>();
    @Output('lazyImageLoad') public S_LAZYIMAGELOAD = new EventEmitter<any>();
    @Output('lazyImageReady') public S_LAZYIMAGEREADY = new EventEmitter<any>();

    @Output('scrollDragEnd') public S_SCROLLDRAGEND = new EventEmitter<any>();
    @Output('scrollDragMove') public S_SCROLLDRAGMOVE = new EventEmitter<any>();
    @Output('scrollDragStart') public S_SCROLLDRAGSTART = new EventEmitter<any>();

    @Output('navigationHide') public S_NAVIGATIONHIDE = new EventEmitter<any>();
    @Output('navigationShow') public S_NAVIGATIONSHOW = new EventEmitter<any>();

    @Output('paginationRender') public S_PAGINATIONRENDER = new EventEmitter<any>();
    @Output('paginationUpdate') public S_PAGINATIONUPDATE = new EventEmitter<any>();
    @Output('paginationHide') public S_PAGINATIONHIDE = new EventEmitter<any>();
    @Output('paginationShow') public S_PAGINATIONSHOW = new EventEmitter<any>();

    @Output('swiperTap') public S_TAP = new EventEmitter<any>();
    @Output('swiperClick') public S_CLICK = new EventEmitter<any>();
    @Output('swiperDoubleTap') public S_DOUBLETAP = new EventEmitter<any>();
    @Output('swiperTouchEnd') public S_TOUCHEND = new EventEmitter<any>();
    @Output('swiperTouchMove') public S_TOUCHMOVE = new EventEmitter<any>();
    @Output('swiperTouchStart') public S_TOUCHSTART = new EventEmitter<any>();
    @Output('swiperTouchMoveOpposite') public S_TOUCHMOVEOPPOSITE = new EventEmitter<any>();
    @Output('swiperTransitionEnd') public S_TRANSITIONEND = new EventEmitter<any>();
    @Output('swiperTransitionStart') public S_TRANSITIONSTART = new EventEmitter<any>();

    @Output('slidePrevTransitionEnd') public S_SLIDEPREVTRANSITIONEND = new EventEmitter<any>();
    @Output('slidePrevTransitionStart') public S_SLIDEPREVTRANSITIONSTART = new EventEmitter<any>();
    @Output('slideNextTransitionEnd') public S_SLIDENEXTTRANSITIONEND = new EventEmitter<any>();
    @Output('slideNextTransitionStart') public S_SLIDENEXTTRANSITIONSTART = new EventEmitter<any>();
    @Output('slideChangeTransitionEnd') public S_SLIDECHANGETRANSITIONEND = new EventEmitter<any>();
    @Output('slideChangeTransitionStart') public S_SLIDECHANGETRANSITIONSTART = new EventEmitter<any>();

    constructor(
        private zone: NgZone,
        private cdRef: ChangeDetectorRef,
        @Inject(PLATFORM_ID) private platformId: Object,
        @Optional() @Inject(SWIPER_CONFIG) private defaults: SwiperConfigInterface
    ) {}

    public ngAfterViewInit(): void {
        if (!isPlatformBrowser(this.platformId)) {
            return;
        }

        this.zone.runOutsideAngular(() => {
            this.updateClasses();

            if (this.swiperSlides && typeof MutationObserver !== 'undefined') {
                this.mo = new MutationObserver(() => {
                    this.updateClasses();
                });

                this.mo.observe(this.swiperSlides.nativeElement, { childList: true });
            }
        });

        window.setTimeout(() => {
            if (this.directiveRef) {
                this.S_INIT.emit();

                this.directiveRef.indexChange = this.indexChange;

                SwiperEvents.forEach((eventName: SwiperEvent) => {
                    if (this.directiveRef) {
                        const output = `S_${eventName.replace('swiper', '').toUpperCase()}`;

                        const directiveOutput = output as keyof SwiperDirective;
                        const componentOutput = output as keyof SwiperComponent;

                        this.directiveRef[directiveOutput] = this[componentOutput] as never;
                    }
                });
            }
        }, 0);
    }

    public ngOnDestroy(): void {
        if (this.mo) {
            this.mo.disconnect();
        }

        if (this.config && this.paginationBackup) {
            this.config.pagination = this.paginationBackup;
        }
    }

    public getConfig(): SwiperConfigInterface {
        this.swiperConfig = new SwiperConfig(this.defaults);

        this.swiperConfig.assign(this.config); // Custom configuration

        if (
            this.swiperSlides &&
            (this.swiperConfig.pagination === true ||
                (this.swiperConfig.pagination &&
                    typeof this.swiperConfig.pagination === 'object' &&
                    (!this.swiperConfig.pagination.type || this.swiperConfig.pagination.type === 'bullets') &&
                    !this.swiperConfig.pagination.renderBullet &&
                    this.swiperConfig.pagination.el === '.swiper-pagination'))
        ) {
            this.config = this.config || {};

            if (!this.paginationConfig) {
                this.paginationBackup = this.config.pagination;

                this.paginationConfig = {
                    el: '.swiper-pagination',

                    renderBullet: (index: number, className: string) => {
                        let children = this.swiperSlides ? Array.from(this.swiperSlides.nativeElement.children) : [];

                        children = children.filter((child: any) => child.classList.contains('swiper-slide'));

                        let bullet = `<span class="${className} ${className}-middle" index="${index}"></span>`;

                        if (index === 0) {
                            bullet = `<span class="${className} ${className}-first" index="${index}"></span>`;
                        } else if (index === children.length - 1) {
                            bullet = `<span class="${className} ${className}-last" index="${index}"></span>`;
                        }

                        return `<span class="swiper-pagination-handle" index="${index}">${bullet}</span>`;
                    },
                };
            }

            if (this.swiperConfig.pagination === true) {
                this.config.pagination = this.paginationConfig;
            } else {
                this.config.pagination = Object.assign({}, this.config.pagination, this.paginationConfig);
            }
        }

        return this.config as SwiperConfigInterface;
    }

    private updateClasses(): void {
        if (this.swiperSlides) {
            let updateNeeded = false;

            const children = this.swiperSlides.nativeElement.children;

            for (let i = 0; i < children.length; i++) {
                if (/swiper-.*/.test(children[i].className) === false) {
                    updateNeeded = true;

                    children[i].classList.add('swiper-slide');
                }
            }

            if (updateNeeded && this.directiveRef) {
                this.directiveRef.update();
            }
        }

        this.cdRef.detectChanges();
    }

    public onPaginationClick(index: number): void {
        if (
            this.config &&
            this.directiveRef &&
            (this.config.pagination === true ||
                (this.config.pagination &&
                    typeof this.config.pagination === 'object' &&
                    (!this.config.pagination.type || this.config.pagination.type === 'bullets') &&
                    this.config.pagination.clickable &&
                    this.config.pagination.el === '.swiper-pagination'))
        ) {
            this.directiveRef.setIndex(index);
        }
    }
}
