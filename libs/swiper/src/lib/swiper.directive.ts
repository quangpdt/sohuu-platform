import { Lazy, Navigation, Pagination, Swiper } from 'swiper/js/swiper.esm.js';
import {
    AfterViewInit,
    Directive,
    DoCheck,
    ElementRef,
    EventEmitter,
    Inject,
    Input,
    KeyValueDiffer,
    KeyValueDiffers,
    NgZone,
    OnChanges,
    OnDestroy,
    Optional,
    Output,
    PLATFORM_ID,
    SimpleChanges,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

import { SWIPER_CONFIG, SwiperConfig, SwiperConfigInterface, SwiperEvent, SwiperEvents } from './swiper.interfaces';

Swiper.use([Navigation, Lazy, Pagination]);

@Directive({
    selector: '[swiper]',
    exportAs: 'ngxSwiper',
})
export class SwiperDirective implements AfterViewInit, OnDestroy, DoCheck, OnChanges {
    private instance: any;

    private initialIndex: number | null = null;

    private configDiff: KeyValueDiffer<string, any> | null = null;

    @Input()
    set index(index: number) {
        if (index != null) {
            this.setIndex(index);
        }
    }

    @Input() public disabled = false;

    @Input() public performance = false;

    @Input('swiper') public config?: SwiperConfigInterface;

    @Output() public indexChange = new EventEmitter<number>();

    @Output('init') public S_INIT = new EventEmitter<any>();
    @Output('beforeDestroy') public S_BEFOREDESTROY = new EventEmitter<any>();

    @Output('scroll') public S_SCROLL = new EventEmitter<any>();
    @Output('progress') public S_PROGRESS = new EventEmitter<any>();
    @Output('keyPress') public S_KEYPRESS = new EventEmitter<any>();

    @Output('resize') public S_RESIZE = new EventEmitter<any>();
    @Output('breakpoint') public S_BREAKPOINT = new EventEmitter<any>();
    @Output('zoomChange') public S_ZOOMCHANGE = new EventEmitter<any>();
    @Output('afterResize') public S_AFTERRESIZE = new EventEmitter<any>();
    @Output('beforeResize') public S_BEFORERESIZE = new EventEmitter<any>();

    @Output('loopFix') public S_LOOPFIX = new EventEmitter<any>();
    @Output('beforeLoopFix') public S_BEFORELOOPFIX = new EventEmitter<any>();

    @Output('sliderMove') public S_SLIDERMOVE = new EventEmitter<any>();
    @Output('slideChange') public S_SLIDECHANGE = new EventEmitter<any>();

    @Output('setTranslate') public S_SETTRANSLATE = new EventEmitter<any>();
    @Output('setTransition') public S_SETTRANSITION = new EventEmitter<any>();

    @Output('fromEdge') public S_FROMEDGE = new EventEmitter<any>();
    @Output('reachEnd') public S_REACHEND = new EventEmitter<any>();
    @Output('reachBeginning') public S_REACHBEGINNING = new EventEmitter<any>();

    @Output('autoplay') public S_AUTOPLAY = new EventEmitter<any>();
    @Output('autoplayStart') public S_AUTOPLAYSTART = new EventEmitter<any>();
    @Output('autoplayStop') public S_AUTOPLAYSTOP = new EventEmitter<any>();

    @Output('imagesReady') public S_IMAGESREADY = new EventEmitter<any>();
    @Output('lazyImageLoad') public S_LAZYIMAGELOAD = new EventEmitter<any>();
    @Output('lazyImageReady') public S_LAZYIMAGEREADY = new EventEmitter<any>();

    @Output('scrollDragEnd') public S_SCROLLDRAGEND = new EventEmitter<any>();
    @Output('scrollDragMove') public S_SCROLLDRAGMOVE = new EventEmitter<any>();
    @Output('scrollDragStart') public S_SCROLLDRAGSTART = new EventEmitter<any>();

    @Output('navigationHide') public S_NAVIGATIONHIDE = new EventEmitter<any>();
    @Output('navigationShow') public S_NAVIGATIONSHOW = new EventEmitter<any>();

    @Output('paginationRender') public S_PAGINATIONRENDER = new EventEmitter<any>();
    @Output('paginationUpdate') public S_PAGINATIONUPDATE = new EventEmitter<any>();
    @Output('paginationHide') public S_PAGINATIONHIDE = new EventEmitter<any>();
    @Output('paginationShow') public S_PAGINATIONSHOW = new EventEmitter<any>();

    @Output('swiperTap') public S_TAP = new EventEmitter<any>();
    @Output('swiperClick') public S_CLICK = new EventEmitter<any>();
    @Output('swiperDoubleTap') public S_DOUBLETAP = new EventEmitter<any>();
    @Output('swiperTouchEnd') public S_TOUCHEND = new EventEmitter<any>();
    @Output('swiperTouchMove') public S_TOUCHMOVE = new EventEmitter<any>();
    @Output('swiperTouchStart') public S_TOUCHSTART = new EventEmitter<any>();
    @Output('swiperTouchMoveOpposite') public S_TOUCHMOVEOPPOSITE = new EventEmitter<any>();
    @Output('swiperTransitionEnd') public S_TRANSITIONEND = new EventEmitter<any>();
    @Output('swiperTransitionStart') public S_TRANSITIONSTART = new EventEmitter<any>();

    @Output('slidePrevTransitionEnd') public S_SLIDEPREVTRANSITIONEND = new EventEmitter<any>();
    @Output('slidePrevTransitionStart') public S_SLIDEPREVTRANSITIONSTART = new EventEmitter<any>();
    @Output('slideNextTransitionEnd') public S_SLIDENEXTTRANSITIONEND = new EventEmitter<any>();
    @Output('slideNextTransitionStart') public S_SLIDENEXTTRANSITIONSTART = new EventEmitter<any>();
    @Output('slideChangeTransitionEnd') public S_SLIDECHANGETRANSITIONEND = new EventEmitter<any>();
    @Output('slideChangeTransitionStart') public S_SLIDECHANGETRANSITIONSTART = new EventEmitter<any>();

    constructor(
        @Inject(PLATFORM_ID) private platformId: Object,
        private zone: NgZone,
        private elementRef: ElementRef,
        private differs: KeyValueDiffers,
        @Optional() @Inject(SWIPER_CONFIG) private defaults: SwiperConfigInterface
    ) {}

    public ngAfterViewInit(): void {
        if (!isPlatformBrowser(this.platformId)) {
            return;
        }

        const params = new SwiperConfig(this.defaults);

        params.assign(this.config); // Custom configuration

        if (params.scrollbar === true) {
            params.scrollbar = {
                el: '.swiper-scrollbar',
            };
        }

        if (params.pagination === true) {
            params.pagination = {
                el: '.swiper-pagination',
            };
        }

        if (params.navigation === true) {
            params.navigation = {
                prevEl: '.swiper-button-prev',
                nextEl: '.swiper-button-next',
            };
        }

        if (this.disabled) {
            params.allowSlidePrev = false;
            params.allowSlideNext = false;
        }

        if (this.initialIndex != null) {
            params.initialSlide = this.initialIndex;

            this.initialIndex = null;
        }

        params.on = {
            slideChange: () => {
                if (this.instance && this.indexChange.observers.length) {
                    this.emit(this.indexChange, this.instance.realIndex);
                }
            },
        };

        this.zone.runOutsideAngular(() => {
            this.instance = new Swiper(this.elementRef.nativeElement, params);
        });

        if (params.init !== false && this.S_INIT.observers.length) {
            this.emit(this.S_INIT, this.instance);
        }

        // Add native Swiper event handling
        SwiperEvents.forEach((eventName: SwiperEvent) => {
            let swiperEvent = eventName.replace('swiper', '');

            swiperEvent = swiperEvent.charAt(0).toLowerCase() + swiperEvent.slice(1);

            this.instance.on(swiperEvent, (...args: any[]) => {
                if (args.length === 1) {
                    args = args[0];
                }

                const output = `S_${swiperEvent.toUpperCase()}`;

                const emitter = this[output as keyof SwiperDirective] as EventEmitter<any>;

                if (emitter.observers.length) {
                    this.emit(emitter, args);
                }
            });
        });

        if (!this.configDiff) {
            this.configDiff = this.differs.find(this.config || {}).create();

            this.configDiff.diff(this.config || {});
        }
    }

    public ngOnDestroy(): void {
        if (this.instance) {
            this.zone.runOutsideAngular(() => {
                this.instance.destroy(true, this.instance.initialized || false);
            });

            this.instance = null;
        }
    }

    public ngDoCheck(): void {
        if (this.configDiff) {
            const changes = this.configDiff.diff(this.config || {});

            if (changes) {
                this.initialIndex = this.getIndex(true);

                this.ngOnDestroy();

                this.ngAfterViewInit();

                this.update();
            }
        }
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (this.instance && changes['disabled']) {
            if (changes['disabled'].currentValue !== changes['disabled'].previousValue) {
                if (changes['disabled'].currentValue === true) {
                    this.zone.runOutsideAngular(() => {
                        this.ngOnDestroy();

                        this.ngAfterViewInit();
                    });
                } else if (changes['disabled'].currentValue === false) {
                    this.zone.runOutsideAngular(() => {
                        this.ngOnDestroy();

                        this.ngAfterViewInit();
                    });
                }
            }
        }
    }

    private emit(emitter: EventEmitter<any>, value: any): void {
        if (this.performance) {
            emitter.emit(value);
        } else {
            this.zone.run(() => emitter.emit(value));
        }
    }

    public swiper(): any {
        return this.instance;
    }

    public init(): void {
        if (this.instance) {
            this.zone.runOutsideAngular(() => {
                this.instance.init();
            });
        }
    }

    public update(): void {
        setTimeout(() => {
            if (this.instance) {
                this.zone.runOutsideAngular(() => {
                    this.instance.update();
                });
            }
        }, 0);
    }

    public getIndex(real?: boolean): number {
        if (!this.instance) {
            return this.initialIndex || 0;
        } else {
            return real ? this.instance.realIndex : this.instance.activeIndex;
        }
    }

    public setIndex(index: number, speed?: number, silent?: boolean): void {
        if (!this.instance) {
            this.initialIndex = index;
        } else {
            let realIndex = index * this.instance.params.slidesPerGroup;

            if (this.instance.params.loop) {
                realIndex += this.instance.loopedSlides;
            }

            this.zone.runOutsideAngular(() => {
                this.instance.slideTo(realIndex, speed, !silent);
            });
        }
    }

    public prevSlide(speed?: number, silent?: boolean): void {
        if (this.instance) {
            this.zone.runOutsideAngular(() => {
                this.instance.slidePrev(speed, !silent);
            });
        }
    }

    public nextSlide(speed?: number, silent?: boolean): void {
        if (this.instance) {
            this.zone.runOutsideAngular(() => {
                this.instance.slideNext(speed, !silent);
            });
        }
    }

    public stopAutoplay(reset?: boolean): void {
        if (reset) {
            this.setIndex(0);
        }

        if (this.instance && this.instance.autoplay) {
            this.zone.runOutsideAngular(() => {
                this.instance.autoplay.stop();
            });
        }
    }

    public startAutoplay(reset?: boolean): void {
        if (reset) {
            this.setIndex(0);
        }

        if (this.instance && this.instance.autoplay) {
            this.zone.runOutsideAngular(() => {
                this.instance.autoplay.start();
            });
        }
    }
}
