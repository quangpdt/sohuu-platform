import { v4 as uuid } from 'uuid';

const uuidV4Pattern = /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i;

export async function asyncForEach(array: any[], callback: any) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

export async function asyncDelay(ms: number) {
    return new Promise((resolve) => setTimeout(resolve, ms));
}

export function generateUUID(): string {
    return uuid();
}

export function isUUIDValid(value: string) {
    return uuidV4Pattern.test(value);
}
